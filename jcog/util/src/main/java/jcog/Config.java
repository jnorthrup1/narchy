package jcog;

import com.google.common.io.Resources;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.io.IOException;

//import kotlin.jvm.JvmStatic;
//import kotlin.jvm.internal.Intrinsics;


public enum Config {
	;
	private static final Logger logger = Log.log(Config.class);



	static {
		//HACK

		String defaultsFile = "defaults.ini";
		try {
			System.getProperties().load(Resources.getResource(defaultsFile).openStream());
		} catch (IllegalArgumentException | IOException e) {
			//logger.warn("{} missing", defaultsFile);
			//e.printStackTrace();
		}
	}


	public static String get(String key, @Nullable Object def) {
		return get(key, def, false);
	}


	/**
	 * @param key
	 * @param def   default value
	 * @param quiet log or not
	 */
	public static String get(String key, @Nullable Object def, boolean quiet) {

		String defString = def != null ? def.toString() : null;
		//Intrinsics.checkParameterIsNotNull(configKey, "configKey");

		//Intrinsics.checkExpressionValueIsNotNull(var10000, "(this as java.lang.String).toLowerCase()");

		String javapropname = key.toLowerCase().replace('_', '.');//, false, 4, (Object)null);

		String y = System.getenv(key); //HACK

		if (y == null) {
			y = System.getProperty(javapropname);
			if (y == null) {
				y = System.getenv(javapropname);
				if (y == null) {
					y = System.getProperty(key);
				}
			}
		}

		if (y != null) {

			y = Str.unquote(y);

			System.setProperty(javapropname, y);
			if (!quiet) {
				reportConfig(javapropname, y);
			}
			return y;

		} else {
			if (defString == null)
				throw new RuntimeException("configuration unknown: " + key);

			return defString;
		}
	}

//	// $FF: synthetic method
//
//	public static @Nullable String get2$default(String var0, String var1, boolean var2, int var3, Object var4) {
//		if ((var3 & 4) != 0) {
//			var2 = false;
//		}
//
//		return get(var0, var1, var2);
//	}


	private static String reportConfig(String javapropname, String val) {
		//Intrinsics.checkParameterIsNotNull(javapropname, "javapropname");
		//Intrinsics.checkParameterIsNotNull(val, "val");
//      boolean var3 = false;
//      boolean var4 = false;
//      int var6 = false;
		//System.err.println("-D" + javapropname + "=\"" + val + '"');
		logger.info("-D{}={}", javapropname, val);
		return val;
	}



	public static int INT(String key, int def) {
		return Integer.parseInt(get(key, def));
	}

	public static float FLOAT(String key, float def) {
		return Float.parseFloat(get(key, def));
	}

	public static double DOUBLE(String key, double def) {
		return Double.parseDouble(get(key, def));
	}

//   
//   public static boolean notConfig(String key) {
//      //Intrinsics.checkParameterIsNotNull(key, "key");
//      return configIs(key, "false");
//   }

//   
//   @NotNull
//   public static String getK(String key, String var1) {
//      //Intrinsics.checkParameterIsNotNull(key, "key");
//      //Intrinsics.checkParameterIsNotNull(var1, "default");
//      String var10000 = get2(key, var1, false);
//      if (var10000 == null) {
//         //Intrinsics.throwNpe();
//      }
//
//      return var10000;
//   }

	public static boolean IS(String key, boolean def) {
		return IS(key, def ? +1 : 0);
	}

	public static boolean IS(String key) {
		return IS(key, -1);
	}

	public static boolean IS(String key, int def) {
		return switch(get(key, "")) {
			case "true", "t", "yes", "y", "1" -> true;
			case "false", "f", "no", "n", "0" -> false;
			default -> {
				if (def < 0)
					throw new UnsupportedOperationException(key + ": expected boolean value");
				yield def==1;
			}
		};
	}


//	public static @Nullable String get(String s) {
//		return get(s, null);
//	}

}