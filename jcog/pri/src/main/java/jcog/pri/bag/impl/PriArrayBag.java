package jcog.pri.bag.impl;

import jcog.pri.Prioritizable;
import jcog.pri.op.PriMerge;
import org.jetbrains.annotations.Nullable;

public class PriArrayBag<X extends Prioritizable> extends ArrayBag<X,X> {

    public PriArrayBag(PriMerge merge, int capacity) {
        this(merge);
        setCapacity(capacity);
    }

    public PriArrayBag(PriMerge merge) {
        super(merge, new ListArrayBagModel<>());
    }

//    @Override
//    protected void removed(X x) {
//        //dont affect the result
//    }

    @Override
    public @Nullable X key(X k) {
        return k;
    }


}
