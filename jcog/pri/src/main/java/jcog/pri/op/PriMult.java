package jcog.pri.op;

import jcog.pri.Prioritizable;

import java.util.function.Consumer;

public final class PriMult<P extends Prioritizable> implements Consumer<P> {

    public float mult;

    public PriMult(float factor) {
//        //TEMPORARY
//        if (factor<=0)
//            Util.nop();

        this.mult = factor;
    }

    @Override
    public void accept(P x) {
        x.priMul(mult);
    }
}