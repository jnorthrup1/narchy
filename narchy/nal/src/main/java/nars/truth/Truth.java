/*
 * TruthValue.java
 *
 * Copyright (C) 2008  Pei Wang
 *
 * This file is part of Open-NARS.
 *
 * Open-NARS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Open-NARS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-NARS.  If not, see <http:
 */
package nars.truth;

import com.google.common.io.ByteArrayDataOutput;
import jcog.Is;
import jcog.Str;
import jcog.Util;
import nars.NAL;
import nars.Op;
import nars.truth.func.TruthFunctions;
import org.jetbrains.annotations.Nullable;

import java.io.DataInput;
import java.io.IOException;

import static jcog.WTF.WTF;
import static nars.truth.func.TruthFunctions.c2e;


/**
 * scalar (1D) truth value "frequency", stored as a floating point value
 *
 * floating-point precision testing tool:
 *      http://herbie.uwplse.org/demo/
 */
@Is({"Four-valued_logic", "Bennett%27s_laws"}) public abstract class Truth implements Truthed {


    public static void assertDithered(@Nullable Truth t, NAL n) {
        if (t != null) {
            Truth d = t.dither(n);
            if (!t.equals(d))
                throw WTF("not dithered");
        }
    }

    /**
     * A simplified String representation of a TruthValue, where each factor is
     * accurate to 1%
     */
    public static StringBuilder appendString(StringBuilder sb, int decimals, float freq, float conf) {

        sb.ensureCapacity(3 + 2 * (2 + decimals));

        return sb
                .append(Op.TRUTH_VALUE_MARK)
                .append(Str.n(freq, decimals))
                .append(Op.VALUE_SEPARATOR)
                .append(Str.n(conf, decimals))
                .append(Op.TRUTH_VALUE_MARK);
    }


    public static float freq(float f, float epsilon) {
        return Util.unitizeSafe(Util.round(f, epsilon));
    }

    public static double conf(double c, double epsilon) {
		return Util.clampSafe(
        Math.max(NAL.truth.CONF_DITHER_ROUND_UP ? epsilon : 0,
            Util.round(c, epsilon)
        ), 0, 1.0 - epsilon);
    }

    public static void write(Truth t, ByteArrayDataOutput out)  {
        out.writeFloat(t.freq());
		out.writeFloat((float) t.conf());
    }

    public static Truth read(DataInput in) throws IOException {
        float f = in.readFloat();
        float c = in.readFloat();
        return PreciseTruth.byConf(f, c);
    }

    private static PreciseTruth byEvi(float f, double e, float freqRes, double confRes) {
        return PreciseTruth.byConfEvi(
            freq(f, freqRes),
            conf(TruthFunctions.e2c(e), confRes),
            e);
    }

    public static PreciseTruth byEvi(float f, double e, NAL nar) {
        return byEvi(f, e, nar.freqResolution.floatValue(), nar.confResolution.doubleValue());
    }

    public static PreciseTruth byConf(float f, double c, NAL nar) {
        return byEvi(f, c2e(c), nar);
    }

    /** even if the truths are .equals equivalent, but have different .evi() */
    public static Truth stronger(Truth x, Truth y) {
        return x==y || x.evi() >= y.evi() ? x : y;
    }

    /** returns the un-negated, positive only form */
    public Truth pos() {
        return NEGATIVE() ? neg() : this;
    }

    public String detailString() {
        return "%" + Str.n(freq(), 8) + ";" + Str.n(conf(), 8) + '%';
    }

//    public final Truth confMult(double factor, double eviMin /* NOT confMin */) {
//        return factor == 1 ? this : eviClone(c2e(conf() * factor), eviMin);
//    }

    @Nullable public final Truth eviMult(double factor, double eviMin) {
        return factor == 1 ? this : eviClone(evi() * factor, eviMin);
    }

    @Nullable public final Truth eviClone(double eNext, double eviMin) {
        return eNext < eviMin ? null : PreciseTruth.byEvi(freq(), eNext);
    }

    public StringBuilder appendString(StringBuilder sb) {
		return appendString(sb, 2, freq(), (float) conf());
    }

    String _toString() {
        return appendString(new StringBuilder(7)).toString();
    }

    /**
     * the negated (1 - freq) of this truth value
     */
    public Truth neg() {
        return PreciseTruth.byEvi(freqNeg(), evi());
    }

    public boolean equals(@Nullable Truth x, float freqRes, float confRes) {
		return x == this || (x != null
                && equalConf(x, confRes)
                && equalFreq(x, freqRes)
        );
    }
    public boolean equals(float freq, float conf, float freqRes, float confRes) {
        return equalConf(freq, confRes)
                && equalFreq(conf, freqRes);
    }

    public boolean equalConf(@Nullable Truth x, float confRes) {
        return equalConf(x.conf(), confRes);
    }

    public boolean equalConf(double xc, float confRes) {
        return Util.equals(conf(), xc, confRes);
    }

    public boolean equalFreq(@Nullable Truth x, float freqRes) {
        return equalFreq(x, false, freqRes);
    }

    public boolean equalFreq(@Nullable Truth x, boolean neg, float freqRes) {
        return equalFreq(x.freqNegIf(neg), freqRes);
    }

    public boolean equalFreq(float xf, float freqRes) {
        return Util.equals(freq(), xf, freqRes);
    }

    public boolean equals(@Nullable Truth x, float res) {
        return equals(x, res, res);
    }

    public boolean equals(@Nullable Truth x, NAL nar) {
        return equals(x, nar.freqResolution.floatValue(), nar.confResolution.floatValue());
    }

    public boolean equalsStronger(@Nullable Truth x, boolean neg, NAL nar) {
        return (!neg && this == x) ||
               (equalFreq(x, neg, nar) && gteConf(x, nar));
    }

    public boolean equalFreq(@Nullable Truth x, boolean neg, NAL nar) {
        return (!neg && this == x) ||
               equalFreq(x, neg, nar.freqResolution.floatValue());
    }

    public boolean gteConf(@Nullable Truth x, NAL nar) {
        return this==x || gteConf(x, nar.confResolution.floatValue());
    }

    /** conf >= x.conf, within tolerance */
    public boolean gteConf(@Nullable Truth x, float confRes) {
        return conf() + confRes > x.conf()/2;
    }

    public Truth negIf(boolean negate) {
        return negate ? neg() : this;
    }

    public final Truth dither(NAL n) {
        return dither(n.freqResolution.floatValue(), n.confResolution.doubleValue());
    }

    public final Truth dither(float freqRes, double confRes) {
        return byEvi(freq(), evi(), freqRes, confRes);
    }


    static class TruthException extends RuntimeException {
        TruthException(String reason, double value) {
            super(reason + ": " + value);
        }
    }

}