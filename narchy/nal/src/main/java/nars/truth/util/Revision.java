package nars.truth.util;

import jcog.math.LongInterval;
import nars.NAL;
import nars.Term;
import nars.task.NALTask;
import nars.truth.PreciseTruth;
import nars.truth.Truth;
import nars.truth.Truthed;
import nars.truth.proj.IntegralTruthProjection;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Truth/Task Revision & Projection (Revection)
 */
public enum Revision {
    ;


    public static Truth revise(@Nullable Truth a, @Nullable Truth b) {
        if (a == null) return b;
        else if (b == null) return a;
        else return revise(a, b, 0);
    }

    /**
     * classic eternal revision
     */
    private static @Nullable Truth revise(Truthed a, Truthed b, float minEvi) {

        double ae = a.evi(), be = b.evi();
        double e = ae + be;

        return e <= minEvi ?
                null :
                PreciseTruth.byEvi(
                        (ae * a.freq() + be * b.freq()) / e,
                        e
                );
    }


    public static NALTask mergeIntersect(NALTask existing, NALTask incoming, Term exT, float freqRes, float confRes) {
        //final long s = Math.min(is, es), e = Math.max(ie, ee);
        var c = IntegralTruthProjection.revise(LongInterval.TIMELESS, LongInterval.TIMELESS, List.of(existing, incoming));
//        c.eviMerge = MutableTruthProjection.EviMerge.
//            Mean;
//            //Min;
//            //Max;
        //c.overlapIgnore = true;
        Truth t = c.truth();
        if (t == null)
            return null; //shouldnt happen

        if (NAL.truth.DITHER_TEMPORAL_MERGE)
            t = t.dither(freqRes, confRes);

        NALTask y = NALTask.clone(existing, exT, t, existing.punc(), c.start(), c.end(), existing.stamp(), false);

//        if (existing.equals(y))
//            return existing;
//        if (incoming.equals(y))
//            return incoming;

        y.copyMeta(incoming);
        c.fund(y, c.eviPrioritizer());

        return y;
    }
}