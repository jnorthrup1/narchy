package nars.truth.util;

import jcog.Util;
import nars.task.util.TaskList;
import org.eclipse.collections.api.block.function.primitive.IntToFloatFunction;
import org.jetbrains.annotations.Nullable;

public abstract class TaskEviList extends TaskList {
	/**
	 * active evidence cache
	 */
	@Nullable protected double[] evi;

	public TaskEviList(int initialCap) {
		super(initialCap);
	}


//	public final double eviInteg(int i) {
//		return evi[i];
//	}

	@Deprecated public final boolean valid(int i) {
		return items[i]!=null && eviValid(evi[i]);
	}

	protected final boolean nonNull(int i) {
		return items[i] != null;
	}

//	@Nullable public final NALTask getValidOrNull(int i) {
//		return valid(i) ? items[i] : null;
//	}

	public final IntToFloatFunction eviPrioritizer() {
		return new EviPrioritizer() {
			@Override public double evi(int i) {
				return evi[i];
			}
		};
	}

	public double eviMax() {
		return Util.max(evi); //TODO max(evi, 0, size)
	}
	public double eviSum() {
		return Util.sum(evi); //TODO max(evi, 0, size)
	}

}