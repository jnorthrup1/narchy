package nars.truth;

import jcog.Util;
import nars.NAL;
import nars.truth.func.TruthFunctions;

/**
 * extends DiscreteTruth's raw hash representation with
 * a freq and evidence float pairs.
 *
 * this allows it to store, internally, more precision than the
 * discrete representation, yet is comparable with DiscreteTruth
 * (according to system TRUTH_EPSILON tolerance).
 *
 * the additional precision is useful for intermediate calculations
 * where premature rounding could snowball into significant error.
 *
 */
public final class PreciseTruth extends DiscreteTruth {

    private final float f;

    private /*volatile?*/ double e;

    public PreciseTruth(Truth truth) {
        this(truth.freq(), (float)truth.conf(), truth.evi());
    }
    private PreciseTruth(float freq, float conf, double evi) {
        super(freq, conf);

        if (freq!=freq)
            throw new TruthException("NaN freq", freq);

        if (NAL.DEBUG && evi < NAL.truth.EVI_MIN_safe)
            throw new TruthException("evidence underflow", evi);

        //this.e = Util.clamp(evi, NAL.truth.EVI_MIN, NAL.truth.EVI_MAX);
        this.e = Util.min(evi, NAL.truth.EVI_MAX);
        this.f = freq;
    }

    public static PreciseTruth byConf(double freq, double conf) {
        return byConfEvi((float)freq, conf, TruthFunctions.c2e(conf));
    }

    public static PreciseTruth byEvi(double freq, double evi) {
        return byConfEvi((float)freq, TruthFunctions.e2c(evi), evi);
    }

    static PreciseTruth byConfEvi(float freq, double conf, double evi) {
        return new PreciseTruth(freq, (float)conf, evi);
    }

    @Override
    public float freq() {
        return f;
    }

    @Override
    public double conf() {
        return TruthFunctions.e2c(e);
    }

    @Override
    public double evi() { return e; }

    /** assumes equality has already been tested as true.  sets the evidence to the maximum */
    public void absorb(Truth o) {
        this.e = Math.max(e, o.evi());
    }
}