package nars.truth;

import org.jetbrains.annotations.Nullable;

/**
 * strategy for mapping unit scalar values to truth values
 */
public abstract class Truther {

    @Nullable public final Truth truth(float value) { return truth(value, 0); }

    @Nullable
    public abstract Truth truth(float value, float valueRes);

    /**
     * returns a value in 0..1 of how different the truths are
     */
    public abstract float dist(Truth x, Truth y);

    public static class FreqTruther extends Truther {

        float freqRes;
        double conf;
        static final boolean precise = true;

        public FreqTruther() {
            this(0.5);
        }

        public FreqTruther(double conf) {
            conf(conf);
        }

        public FreqTruther freqRes(float f) {
            this.freqRes = f;
            return this;
        }

        public FreqTruther conf(double c) {
            this.conf = c;
            return this;
        }

        public FreqTruther conf(float c, float confRes) {
            return conf(Truth.conf(c, confRes));
        }

        @Override
        public Truth truth(float value, float valueRes) {
            if (value!=value) return null;
            float f = Truth.freq(value, Math.max(this.freqRes, valueRes));
            return precise ? PreciseTruth.byConf(f, conf) : DiscreteTruth.the(f, conf);
        }

        @Override
        public float dist(Truth x, Truth y) {
            return Math.abs(x.freq() - y.freq());
        }
    }

    public static class ConfTruther extends Truther {
        float confRes;
        float freq;
        boolean precise;

        public ConfTruther(float freq) {
            freq(freq);
        }

        public ConfTruther confRes(float c) {
            this.confRes = c;
            return this;
        }

        public ConfTruther freq(float f) {
            this.freq = f;
            return this;
        }

        public ConfTruther freq(float f, float freqRes) {
            return freq(Truth.freq(f, freqRes));
        }

        @Override
        public Truth truth(float value, float valueRes) {
            if (value!=value) return null;
            double c = Truth.conf(value, Math.max(valueRes, confRes));
            return precise ? PreciseTruth.byConf(freq, c) : DiscreteTruth.the(freq, c);
        }

        /** Expectation-based.  TODO test */
        @Override public float dist(Truth x, Truth y) {
            double xc = x.conf(), yc = y.conf();
            double cRange = Math.abs(xc - yc), cMin = Math.min(xc, yc);
            double xcn = (xc - cMin) / cRange, ycn = (yc - cMin) / cRange;
            return (float) Math.abs(x.freq()*xcn - y.freq()*ycn);
        }

    }
}