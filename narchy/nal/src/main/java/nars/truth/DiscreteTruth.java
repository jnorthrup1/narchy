package nars.truth;

import jcog.Util;
import nars.$;
import nars.NAL;
import nars.truth.func.TruthFunctions;


/**
 * truth rounded to a fixed size precision
 * to support hashing and equality testing
 * internally stores representation as one 32-bit integer
 */
public class DiscreteTruth extends Truth {

	/**
	 * truth component resolution corresponding to Param.TRUTH_EPSILON
	 */
	private final int hash;

	DiscreteTruth(float f, double c) {
		this(hash(f, c));
	}

	DiscreteTruth(int hash) {
		this.hash = hash;
	}

	/**
	 * attempts to get a shared instance
	 */
	public static DiscreteTruth the(float f, double c) {
		if (c < NAL.truth.DISCRETE_TRUTH_CONF_MIN)
			return $.t(f,c); //precise
		else
			return the(hash(f, c));
	}

	public static DiscreteTruth the(int i) {
		int index = i % shared.length;
		DiscreteTruth t = shared[index];
		return t == null || t.hash != i ?
		 	shared[index] = new DiscreteTruth(i)
			:
			t;
	}

	/**
	 * The hash code of a TruthValue, perfectly condensed,
	 * into the two 16-bit words of a 32-bit integer.
	 * <p>
	 * Since the same epsilon used in other truth
	 * resolution here (Truth components do not necessarily utilize the full
	 * resolution of a floating point value, and also do not necessarily
	 * need the full resolution of a 16bit number when discretized)
	 * the hash value can be used for equality comparisons
	 * as well as non-naturally ordered / non-lexicographic
	 * but deterministic compareTo() ordering.
	 * correct behavior of this requires epsilon
	 * large enough such that: 0 <= h < 2^15:
	 */
	private static int truthToInt(float freq, short freqDisc, double conf, short confDisc) {

		if (!Float.isFinite(freq) || freq < 0 || freq > 1)
			throw new TruthException("invalid freq", freq);

		if (!Double.isFinite(conf) || conf < 0)
			throw new TruthException("invalid conf", conf);
		if (conf > NAL.truth.CONF_MAX) {
			if (NAL.DEBUG)
				throw new TruthException("over-confident", conf);
			conf = NAL.truth.CONF_MAX; //max-out
		}

		int freqHash = Util.toInt(freq, freqDisc);
		int confHash = Util.toInt(conf /*Math.min(NAL.truth.CONF_MAX, conf)*/, confDisc);
		return freqHash << 16 | confHash;
	}


	static {
		//noinspection ConstantConditions
		if (NAL.truth.TRUTH_EPSILON < 1/256.0)
			throw new UnsupportedOperationException("precision underflow " + DiscreteTruth.class);
	}

	private static int hash(float freq, double conf) {
		int confHash = Util.toInt(hashConf(freq, conf), hashDiscretenessCoarse);
		int freqHash = Util.toInt(freq, hashDiscretenessCoarse);
		return freqHash << 16 | confHash;
	}

	private static double hashConf(float freq, double conf) {
		if (!Float.isFinite(freq) || freq < 0 || freq > 1)
			throw new TruthException("invalid freq", freq);

		if (!Double.isFinite(conf) || conf < 0)
			throw new TruthException("invalid conf", conf);

		if (conf > NAL.truth.CONF_MAX) {
			if (NAL.DEBUG)
				throw new TruthException("over-confident", conf);
			else
				return NAL.truth.CONF_MAX; //max-out
		}
		return conf;
	}

	private static int freqI(int h) {
		return h >> 16;
	}

	private static int confI(int h) {
		return h & 0xffff;
	}

	public static int hash(Truth truth) {
		return (truth instanceof DiscreteTruth ?
				(DiscreteTruth) truth :
				the(truth.freq(), truth.conf()))
			.hash;
	}

	@Override
	public float freq() {
		return Util.toFloat(freqI(hash) /* & 0xffff*/, hashDiscretenessCoarse);
	}

	@Override
	public double conf() {
		return Util.toDouble(confI(hash), hashDiscretenessCoarse);
	}

	@Override
	public double evi() {
		return TruthFunctions.c2e(conf());
	}

	@Override
	public String toString() {
		return _toString();
	}

	@Override
	public final boolean equals(Object that) {
		return
			this == that
				||
			(that instanceof DiscreteTruth ?
					hash == ((DiscreteTruth) that).hash :
				equals((Truth) that, NAL.truth.TRUTH_EPSILON));
	}

	@Override
	public final int hashCode() {
		return hash;
	}

	public static final short hashDiscretenessCoarse = (short) Math.round(1.0 / NAL.truth.TRUTH_EPSILON);
	static { assert(hashDiscretenessCoarse < Short.MAX_VALUE); }
	//public static final int hashDiscretenessFine = (int) Math.round(1.0 / NAL.truth.TASK_REGION_CONF_EPSILON);
	private static final DiscreteTruth[] shared = new DiscreteTruth[(int) Util.sqr(Math.ceil(1f / NAL.truth.TRUTH_EPSILON))];

}