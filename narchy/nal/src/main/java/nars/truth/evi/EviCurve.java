package nars.truth.evi;

import jcog.Research;
import jcog.TODO;
import jcog.Util;

import static java.lang.Math.*;

/**
 * absolute temporal projection functions
 * <p>
 * computes the projected evidence at a specific distance (dt) from a perceptual moment evidence
 * with a perceptual duration used as a time constant
 * dt >= 0
 *
 * @param dt  > 0
 * @param dur > 0
 *            <p>
 *            evi(baseEvidence, dt, dur)
 *            many functions will work here, provided:
 *            <p>
 *            evidential limit
 *            integral(evi(x, 0, d), evi(x, infinity, d)) is FINITE (convergent integral for t>=0)
 *            <p>
 *            temporal identity; no temporal difference,
 *            evi(x, 0, d) = 1
 *            <p>
 *            no duration, point-like
 *            evi(x, v, 0) = 0
 *            <p>
 *            monotonically decreasing
 *            for A >= B: evi(x, A, d) >= evi(x, B, d)
 *            since dt>=0, dur
 *            <p>
 *            see:
 *            https://en.wikipedia.org/wiki/List_of_definite_integrals
 *            https://en.wikipedia.org/wiki/Template:Series_(mathematics)
 *
 *            http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLygxK3gqeCkiLCJjb2xvciI6IiM1NzRBMTYifSx7InR5cGUiOjAsImVxIjoiMS8oMSt4KSIsImNvbG9yIjoiIzJGNzUxNyJ9LHsidHlwZSI6MCwiZXEiOiIxLygxK3NxcnQoeCkpIiwiY29sb3IiOiIjMThBRDgwIn0seyJ0eXBlIjowLCJlcSI6IjEvKDErbG9nKDEreCkpIiwiY29sb3IiOiIjMzkyRTlDIn0seyJ0eXBlIjoxMDAwLCJ3aW5kb3ciOlsiMCIsIjEyIiwiMCIsIjEiXX1d
 */
@Research public interface EviCurve {

    double integDiff(long t, float dur);

    /**
     * project a point (1 time unit)
     */
    double project(long dt, float dur);

    /**
     * integrate range (>1 time unit)
     */
    default double integ(long qs, long qe, long ts, long te, float dur) {
        //assert(qs!=ETERNAL && ts!=ETERNAL && qs!=TIMELESS && ts != TIMELESS);

        long intersectLen = min(qe, te) - max(qs, ts);
        double i = intersectLen >= 0 ? intersectLen + 1 : 0;

        if (dur <= 0)
            return i;

        long beforeStart = ts - min(qe, ts), beforeEnd = ts - min(qs, ts);
        double j = beforeStart < beforeEnd ?
                integDiff(beforeEnd, dur) - integDiff(beforeStart, dur)
                : 0;

        long afterStart = max(qs, te) - te, afterEnd = max(qe, te) - te;
        if (afterStart < afterEnd)
            j += integDiff(afterEnd, dur) - integDiff(afterStart, dur);

        return Util.fma(dur, j, i);
    }

    /**
     * Math.exp(-(dt/dur)) warning: not finite definitely integrable
     */
    EviCurve Exponential = new EviCurve() {
        @Override
        public double project(long dt, float dur) {
        	return exp(-dt / dur);
        }

        /**
         * https://www.symbolab.com/solver/indefinite-integral-calculator/%5Cint_%7Ba%7D%5E%7Bb%7D%20e%5E%7B-%5Cleft(%5Cfrac%7Bx%7D%7Bd%7D%5Cright)%5E%7B1%7D%7Ddx
         */
        @Override
        public double integDiff(long t, float dur) {
            double nDUR = -dur;
            return nDUR * Util.exp(t / nDUR);
        }
    };
    /**
     * 1/(1+(x/dur)^0.5)
     */
    EviCurve InverseSqrt = new EviCurve() {
        @Override
        public double project(long dt, float dur) {
            return 1 / (1 + sqrt(((double) dt) / dur));
        }


        /**   2(sqrt(d)−ln(sqrt(d)+1)) from https://www.integral-calculator.com/ */
        @Override
        public double integDiff(long t, float dur) {
            double d = t/dur;
            final double sqrtD = sqrt(d);
            return 2*(sqrtD - log(sqrtD + 1));
        }
    };


    /** 1/(1+log(1+x))
     *  http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLygxK2xvZygxK3gpKSIsImNvbG9yIjoiI0Q0MDAwMCJ9LHsidHlwZSI6MCwiZXEiOiIxLygxK3gpIiwiY29sb3IiOiIjMDAwMDAwIn0seyJ0eXBlIjowLCJlcSI6IjEvKDEreCp4KSIsImNvbG9yIjoiIzAwRkYzQyJ9LHsidHlwZSI6MTAwMCwid2luZG93IjpbIjAiLCIxMiIsIjAiLCIxIl19XQ--
     * */
    class InverseLog implements EviCurve {

        static final float scale = 1;

        @Override
        public double integDiff(long t, float dur) {
            //e−1(E1(−ln(a+1)−1)−E1(−ln(b+1)−1))
            throw new TODO();
        }

        @Override
        public double project(long dt, float dur) {
            double d = ((double) dt) / (scale * dur);
            return 1 / (1 + log(1 + d));
        }

    }

    /**
     * 1/(1+(x/dur)^2) inverse quadratic decay: integral finite from to infinity, see: https://en.wikipedia.org/wiki/List_of_definite_integrals
     *
     * https://www.symbolab.com/solver/indefinite-integral-calculator/%5Cint_%7Ba%7D%5E%7Bb%7D%20%5Cfrac%7B1%7D%7B%5Cleft(1%2B%5Cleft(%5Cfrac%7Bx%7D%7Bd%5Ccdot%20s%7D%5Cright)%5E%7B2%7D%5Cright)%7Ddx
     * https://www.symbolab.com/solver/indefinite-integral-calculator/%5Cint_%7Ba%7D%5E%7Bb%7D%20%5Cfrac%7B1%7D%7B%5Cleft(1%2B%5Cleft(%5Cfrac%7Bx%7D%7Bd%7D%5Cright)%5E%7B2%7D%5Cright)%7Ddx
     * https://www.wolframalpha.com/input/?i=integral+1%2F%281+%2B+%28t%2Fd%29%5E2%29+from+a+to+b
     *
     * TODO try 1/(1+(x^2)/dur)
     */
    record InverseSquare(float scale) implements EviCurve {

        @Override
        public double project(long dt, float dur) {
            //return 1 / (1 + sqr(((double) dt) / (scale * dur)));
            double d = ((double) dt) / (scale * dur);
            return 1 / Util.fma(d, d, 1);
        }


        /**
         * integral 1/(1 + (x/d)^2) from a to b
         * d * s * (atan(d/a) - atan(d/b)), 0 < Re[a] < b && a == Re[a]
         * <p>
         * https://www.wolframalpha.com/input/?i=atan2%28x%2C0%29  simplification for dur=0
         */
        @Override
        public double integDiff(long t, float dur) {
            return scale * atan2(t, scale * dur);
            //return Math.atan(t/((double)dur));
        }
    }

    /**
     * 1/(1+x/dur)) inverse linear decay
     * https://www.symbolab.com/solver/indefinite-integral-calculator/%5Cint_%7Ba%7D%5E%7Bb%7D%20%5Cfrac%7B1%7D%7B%5Cleft(1%2B%5Cfrac%7Bx%7D%7Bd%5Ccdot%20s%7D%5Cright)%7Ddx
     * https://www.symbolab.com/solver/indefinite-integral-calculator/%5Cint_%7Ba%7D%5E%7Bb%7D%20%5Cfrac%7B1%7D%7B%5Cleft(1%2B%5Cfrac%7Bx%7D%7Bd%7D%5Cright)%7Ddx
     */
    record InverseLinear(double scale) implements EviCurve {

        @Override
        public double project(long dt, float dur) {
            return 1 / (1 + dt / (dur * scale));
        }

        /**
         * integral 1/(1 + (x/d)) from a to b
         *
         *
         * TODO check this for scale!=1
         */
        @Override
        public double integDiff(long t, float dur) {
            double d = dur * scale;
            return scale * Util.log((d + t) / d);
        }
    }

//            //cubic decay:
//            //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLTEvKDErZV4oLXgpKSIsImNvbG9yIjoiIzAwNzdGRiJ9LHsidHlwZSI6MCwiZXEiOiIxLygxK3gqeCkiLCJjb2xvciI6IiNENDFBMUEifSx7InR5cGUiOjAsImVxIjoiMS8oMSt4KngqeCkiLCJjb2xvciI6IiM4OUFEMDkifSx7InR5cGUiOjEwMDAsIndpbmRvdyI6WyIwIiwiMTgiLCIwIiwiMSJdfV0-
//            //double e = (evi / (1.0 + Util.cube(((double)dt) / dur)));
//
//            //constant duration linear decay ("trapezoidal")
//            //e = (float) (evi * Math.max(0, (1.0 - dt / dur)));
//
//            //max(constant duration, cubic floor)
//            //e = (float) (evi * Math.max((1.0 - dt / dur), 1.0/(1.0 + Util.cube(((double)dt) / dur))));
//
//            //exponential decay: see https://en.wikipedia.org/wiki/Exponential_integral
//            //TODO
//
//            //constant duration quadratic decay (sharp falloff)
//            //e = evi * Math.max(0, (float) (1.0 - Math.sqrt(dt / decayTime)));
//
//            //constant duration quadratic discharge (slow falloff)
//            //e = evi * Math.max(0, 1.0 - Util.sqr(dt / decayTime));
//
//            //---------
//
//            //eternal noise floor (post-filter)
//            //e = ee + ((e - ee) / (1.0 + (((float)dt) / (falloffDurs * dur))));
//
//            return e;
//
//            //return evi / (1.0f +    Util.sqr(((float)dt) / (falloffDurs * dur)));
//            //return evi / (1.0f +    Util.sqr(((float)dt) / dur)/falloffDurs);
//
//
//            //return evi / (1.0f + ( Math.max(0,(dt-dur/2f)) / (dur)));
//
//            //return evi / (1.0f + ( Math.max(0f,(dt-dur)) / (dur)));
//
//
//            //return evi * 1/sqrt(Math.log(1+(Math.pow((dt/dur),3)*2))*(dt/dur)+1); //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLyhsb2coMSsoeCp4KngqMikpKih4KSsxKV4wLjUiLCJjb2xvciI6IiMyMTE1QUIifSx7InR5cGUiOjAsImVxIjoiMS8oMSt4KSIsImNvbG9yIjoiIzAwMDAwMCJ9LHsidHlwZSI6MTAwMCwid2luZG93IjpbIi0xLjg4NDM2OTA0NzQ3Njc5OTgiLCI4LjUxNTYzMDk1MjUyMzE2OCIsIi0yLjMxMTMwMDA4MTI0NjM4MTgiLCI0LjA4ODY5OTkxODc1MzU5OCJdLCJzaXplIjpbNjQ2LDM5Nl19XQ--
//            //return (float) (evi / (1.0 + Util.sqr(((double)dt) / dur)));
//            //return evi * 1/(Math.log(1+((dt/dur)*0.5))*(dt/dur)+1); //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLyhsb2coMSsoeCowLjUpKSooeCkrMSleMC41IiwiY29sb3IiOiIjMjExNUFCIn0seyJ0eXBlIjowLCJlcSI6IjEvKDEreCkiLCJjb2xvciI6IiMwMDAwMDAifSx7InR5cGUiOjEwMDAsIndpbmRvdyI6WyIyLjYzMDEyOTMyODgxMzU2ODUiLCIxOC44ODAxMjkzMjg4MTM1MzUiLCItMy45NTk4NDE5MDg3NzE5MTgiLCI2LjA0MDE1ODA5MTIyODA1NyJdLCJzaXplIjpbNjQ4LDM5OF19XQ--
//            //return evi * (Util.tanhFast((-(((float)dt)/dur)+2))+1)/2; //http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIodGFuaCgteCsyKSsxKS8yIiwiY29sb3IiOiIjMjExNUFCIn0seyJ0eXBlIjowLCJlcSI6IjEvKDEreCkiLCJjb2xvciI6IiMwMDAwMDAifSx7InR5cGUiOjEwMDAsInNpemUiOls2NDgsMzk4XX1d
//
//            //return (float) (evi / (1.0 + Math.log(1 + ((double)dt) / dur)));

}