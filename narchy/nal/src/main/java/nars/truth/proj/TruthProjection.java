package nars.truth.proj;

import nars.task.NALTask;
import nars.time.Moment;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

public interface TruthProjection extends Iterable<NALTask> {

    @Nullable NALTask task();

    /**
     * @param s if ETERNAL or TIMELESS, auto-focus  */
    @Deprecated @Nullable default Truth truth(long s, long e) {
        time(s, e);
        return truth();
    }


    default /* final */ boolean time(long s, long e) {
        return time(s, e, 0);
    }

    /**
     * true if the start or end has changed
     */
    default /* final */ boolean time(long s, long e, int ditherDT) {
        return when().whenChanged(s, e, ditherDT);
    }

    Moment when();

    @Nullable Truth truth();

    /** query start */
    long start();
    /** query end */
    long end();


    void delete();

    /** weighted by evidence */
    default double priWeighted() {
        double pSum = 0;
        double pEvi = 0;
        for (NALTask x : this) {
            if (x!=null) {
                double e = x.evi();
                pEvi += e;
                pSum += x.priElseZero() * e;
            }
        }
        return pEvi < Float.MIN_NORMAL ? 0 : pSum / pEvi;
    }

    default double minComponentEvi() {
        double min = Double.POSITIVE_INFINITY;
        for (NALTask i : this) {
            double ie = i.evi();
            if (ie < min) min = ie;
        }
        return min;
    }

    void clear();
}