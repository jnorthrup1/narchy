package nars.truth.proj;

import jcog.Util;
import nars.task.NALTask;
import nars.truth.Truth;
import nars.truth.evi.EviInterval;

import java.util.Collection;

/**
 * result truth:
 * frequency = linear combination of frequency values weighted by evidence;
 * evidence = evidence sum
 * <p>
 * this implememnt aggregates combined evidence via linear inteprolation
 */
public class IntegralTruthProjection extends LinearTruthProjection {

    public IntegralTruthProjection(int capacity) {
        super(capacity);
    }

    public IntegralTruthProjection(long start, long end) {
        super(start, end);
    }

    public IntegralTruthProjection(long start, long end, int capacity) {
        super(start, end, capacity);
    }

    /** convenience method for revising a set of tasks */
    public static IntegralTruthProjection revise(long s, long e, Collection/*TODO Set*/<NALTask> x) {
        var c = new IntegralTruthProjection(s, e, 2);
        c.add(x);
        return c;
    }

    public static double reviseConf(Collection<NALTask> x, long start, long end) {
        IntegralTruthProjection p = revise(start, end, x);
        Truth t = p.truth();
        return t == null ? 0 : t.conf();
    }

    @Override protected boolean computeComponents(NALTask[] tasks, int from, int to, EviInterval at, double[] evi) {
        boolean mean = at.s == ETERNAL; //mean mode
        boolean changed = false;
        for (int i = from; i < to; i++) {
            NALTask t = tasks[i];
            double e0 = evi[i];
            double e = mean ? t.evi() : at.eviInteg(t);
            evi[i] = e;
            changed = changed || !Util.equals(e, e0, Double.MIN_NORMAL);
        }
        return changed;
    }

}