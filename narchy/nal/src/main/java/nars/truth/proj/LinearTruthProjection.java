package nars.truth.proj;

import jcog.Util;
import nars.NAL;
import nars.truth.PreciseTruth;
import org.jetbrains.annotations.Nullable;

import static jcog.Util.fma;

public abstract class LinearTruthProjection extends MutableTruthProjection {

    protected LinearTruthProjection(long start, long end) {
        super(start, end);
    }

    protected LinearTruthProjection(long start, long end, int capacity) {
        super(start, end, capacity);
    }

    protected LinearTruthProjection(int capacity) {
        super(capacity);
    }

    @Override
    @Nullable
    public final PreciseTruth computeTruth() {

        //if (NAL.DEBUG) assertNotNull(); ////removeNulls();

        int n = this.size();

        long start = this.start(), end = this.end();

        double[] evi = this.evi;
        double eSum = Util.sum(evi, 0, n);
        long range = 1 + (end - start);
        double E = switch (eviMerge) {
            case Plus -> eSum;
            case Mean -> eSum / n;
            case Max  -> Util.max(evi, 0, n);
            case Min  -> Util.min(evi, 0, n);
        } / range;

        if (E < eviMin) {
            if (NAL.truth.EVI_STRICT)
                return null;
            else {
                /* round up: */
                E = eviMin;
            }
        }

        //TODO abstract the different impls

        double F = n > 1 ?
            fN(start, end, eSum) :
            freq(0, start, end); //f1

        return PreciseTruth.byEvi(F, E);
    }

    private double fN(long start, long end, double eSum) {
        int n = size;
        double[] evi = this.evi;
        double freqWeightedSum = 0;//, eMin = Double.POSITIVE_INFINITY;
        for (int i = 0; i < n; i++) {
            double fi = freq(i, start, end);
            double ei = evi[i]; //if (!eviValid(e)) continue;

            //e*e; //quadratic
            //Math.exp(e/32)-1; //exponential, like a normalized softmax WARNING: can overflow
            //w2c(e) //conf
            //e * Math.abs(f-0.5f)*2f; /* polarity weighting */
            //e * (0.5f + Math.abs(f-0.5f)); /* polarity partial weighting */
            //e * (1 + (2*Math.abs(f-0.5f))); /* 2:1 compression polarity partial weighting */

            freqWeightedSum = fma(fi, ei, freqWeightedSum); //freqWeightedSum += e * f;
        }

        return freqWeightedSum / eSum;
    }

    private float freq(int task, long start, long end) {
        return this.items[task].freq(start, end);
    }


}