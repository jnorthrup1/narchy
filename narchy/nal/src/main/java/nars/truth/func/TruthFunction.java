package nars.truth.func;

import nars.NAL;
import nars.Term;
import nars.term.atom.Atomic;
import nars.truth.MutableTruth;
import nars.truth.PreciseTruth;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;


public interface TruthFunction {

    static @Nullable Truth identity(Truth t, float minConf) {
        return t.conf() < minConf ? null : t;
    }

    default boolean truth(MutableTruth y, @Nullable Truth task, @Nullable Truth belief, double eviMin) {
        Truth t = truth(task, belief, eviMin);
        if (t==null) return false;
        //return y.set(t).is();
        y.set(t);
        return true;
    }

    default @Nullable Truth truth(@Nullable Truth task, @Nullable Truth belief, double eviMin) {
        if (NAL.truth.EVI_STRICT) {
            return apply(task, belief, (float) TruthFunctions.e2c(eviMin));
        } else {
            return truthLenient(task, belief, eviMin);
        }
    }

    @Nullable
    private Truth truthLenient(@Nullable Truth task, @Nullable Truth belief, double eviMin) {
        //raise to min conf
        Truth x = apply(task, belief, NAL.truth.CONF_MINf);
        return x != null && x.evi() < eviMin ? PreciseTruth.byEvi(x.freq(), eviMin) : null;
    }

    /**
     * @param confMin if confidence is less than minConf, it can return null without creating the Truth instance;
     *                if confidence is equal to or greater, then it is valid
     *                very important for minConf >= Float.MIN_NORMAL and not zero.
     */
    @Nullable Truth apply(@Nullable Truth task, @Nullable Truth belief, float confMin);

    boolean allowOverlap();

    boolean single();

    default boolean taskTruthSignificant() {
        return true;
    }

    /**
     * only tested/applicable if !single()
     */
    default boolean beliefTruthSignificant() {
        return true;
    }

    default @Nullable Truth preTask(@Nullable Truth t) {
        return t;
    }

    default @Nullable Truth preBelief(@Nullable Truth t) {
        return t;
    }

    @Nullable
    default /* final */ Truth truth(@Nullable Truth task, @Nullable Truth belief) {
        return truth(task, belief, NAL.truth.EVI_MIN);
    }

    default Term term() {
        return Atomic.the(toString());
    }

    abstract class ProxyTruthFunction implements TruthFunction {
        final TruthFunction o;
        private final boolean allowOverlap, single;

        ProxyTruthFunction(TruthFunction o) {
            this.o = o;
            this.allowOverlap = o.allowOverlap();
            this.single = o.single();
        }

        public abstract String toString();


        @Override
        public final boolean taskTruthSignificant() {
            return o.taskTruthSignificant();
        }

        @Override
        public final boolean beliefTruthSignificant() {
            return o.beliefTruthSignificant();
        }

        @Override
        public final @Nullable Truth apply(@Nullable Truth task, @Nullable Truth belief, float confMin) {
            return o.apply(task, belief, confMin);
        }

        @Override
        public boolean truth(MutableTruth y, @Nullable Truth task, @Nullable Truth belief, double eviMin) {
            return o.truth(y, task, belief, eviMin);
        }

        @Override
        public final boolean allowOverlap() {
            return allowOverlap;
        }

        @Override
        public final boolean single() {
            return single;
        }

    }

    /**
     * swaps the task truth and belief truth
     */
    final class SwappedTruth extends ProxyTruthFunction {

        SwappedTruth(TruthFunction o) {
            super(o);
        }

        @Override
        public boolean truth(MutableTruth y, @Nullable Truth task, @Nullable Truth belief, double eviMin) {
            return super.truth(y, belief, task, eviMin);
        }

        @Override
        public String toString() {
            return o.toString() + 'X';
        }

    }

    /**
     * polarity specified for each component:
     * -1 = negated, 0 = depolarized, +1 = positive
     */
    final class RepolarizedTruth extends ProxyTruthFunction {

        final int taskPolarity;
        final int beliefPolarity;
        @Deprecated
        final boolean swap; //send thru another proxy
        private final String suffix;

        RepolarizedTruth(TruthFunction o, int taskPolarity, int beliefPolarty, String suffix) {
            this(o, taskPolarity, beliefPolarty, false, suffix);
        }

        RepolarizedTruth(TruthFunction o, int taskPolarity, int beliefPolarty, boolean swap, String suffix) {
            super(o);
            this.taskPolarity = taskPolarity;
            this.beliefPolarity = beliefPolarty;
            this.suffix = suffix;
            this.swap = swap;
        }

        private static @Nullable Truth repolarize(Truth t, int polarity) {
            return polarity == -1 || (polarity == 0 && t.NEGATIVE()) ? t.neg() : t;
        }

        /**
         * special handling for applying the polarization to the original inputs before swapping
         */
        RepolarizedTruth swapped() {
            assert (!swap);
            return new RepolarizedTruth(o, taskPolarity, beliefPolarity, true, suffix);
        }

        @Override
        public boolean truth(MutableTruth y, @Nullable Truth task, @Nullable Truth belief, double eviMin) {
            return swap ?
                    super.truth(y, belief, task, eviMin)
                    :
                    super.truth(y, task, belief, eviMin);
        }

        @Override
        public Truth preTask(Truth t) {
            return repolarize(t, taskPolarity);
        }

        @Override
        public Truth preBelief(Truth t) {
            return repolarize(t, beliefPolarity);
        }

        @Override
        public final String toString() {
            return o + suffix + (swap ? "X" : "");
        }
    }
}