package nars.control;

import jcog.util.ArrayUtil;
import nars.$;
import nars.NAL;
import nars.Term;
import nars.subterm.ShortSubterms;
import nars.subterm.Subterms;
import nars.term.LazyTerm;
import nars.term.Termed;
import nars.term.atom.Int;
import org.eclipse.collections.api.block.procedure.primitive.ShortFloatProcedure;
import org.eclipse.collections.api.block.procedure.primitive.ShortProcedure;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.tuple.primitive.ShortFloatPair;
import org.eclipse.collections.impl.map.mutable.primitive.ShortFloatHashMap;
import org.eclipse.collections.impl.set.mutable.primitive.ShortHashSet;
import org.jetbrains.annotations.Nullable;
import org.roaringbitmap.RoaringBitmap;
import org.roaringbitmap.RoaringBitmapWriter;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static nars.Op.SETe;

public enum Why {
    ;

    public static Term why(short why) {
        return Int.the(why);
    }

    static Term why(short[] why, int capacity) {
        if (capacity == 0 || why.length == 0)
            return null; //TODO prevent from having to reach here
        if (why.length == 1)
            return why(why[0]);
        if (capacity < /*2?*/3)
            return null; //cant do any better as a compound

        int excess = why.length - (capacity - 1);
        if (excess > 0) {
            //TODO if (excess == 1) simple case

            //why = sample(capacity-1, true, why);
            why = why.clone();
            ArrayUtil.shuffle(why,
                ThreadLocalRandom.current()
                //new XoRoShiRo128PlusRandom()
            );
            why = Arrays.copyOf(why, capacity - 1);
        }
        return $.seteShort(why);
    }

    @Nullable private static Term why(RoaringBitmap why, int capacity) {
        int ss = why.getCardinality();
        if (ss == 0)
            return null;
        else if (ss == 1) {
            return Int.the(why.first());
        } else {
            return ss < capacity ?
                    $.seteShort(why)
                    :
                    why($.shortArray(why), capacity); //over-capacity; convert to array for sub-sampling
        }
    }

//    public static @Nullable Caused whyLazy(@Nullable Caused... c) {
//////        for (Caused x : c) {
//////            if (x.getClass().getSimpleName().contains("Deriver"))
//////                throw new WTF("circular");
//////        }
////
//        return whyLazy(NAL.causeCapacity.intValue(), c);
////
////        //return why(c);
////
//    }

    @Nullable
    public static Caused whyLazy(int capacity, @Nullable Caused... c) {
        if (capacity == 0) return null;
        switch (c.length) {
            case 0:
                return null;
            case 1:
                return c[0];
            case 2: {
                if (c[0] == c[1] || c[1] == null) return c[0];
                if (c[0] == null) return c[1];
                //??? why doesnt this work (infinte loop)
//				Term a = c[0].why(), b = c[1].why();
//				if (a == null) return b;
//				if (b == null) return a;
//				if (a.equals(b))
//					return a;
                break;
            }
        }
        return new LazyCaused(c, capacity);
    }


    @Nullable public static Termed whyMerge(@Nullable Termed x, @Nullable Termed y) {
        int c = NAL.causeCapacity.intValue(); if (c <= 0) return null;
        if (y == null || x == y) return x;
        if (x == null) return y;
        if (x.equals(y)) return x;
        return why2(c, x.term(), y.term());
    }

    @Deprecated
    public static Term why(Caused... x) {
        int c = NAL.causeCapacity.intValue();
        return c > 0 ? why(c, x) : null;
    }
//    public @Nullable static Term why2(Term a, Term b) {
//        return why2(NAL.causeCapacity.intValue(), a, b);
//    }

    @Nullable
    public static Term why(int capacity, Caused... c) {
        if (capacity == 0) return null;
        return switch (c.length) {
            case 0 -> null;
            case 1 -> c[0].why(); //TODO check capacity
            case 2 -> why2(capacity, c);
            default -> whyN(capacity, c);
        };
    }

    @Nullable
    private static Term whyN(int capacity, Caused[] c) {
        Term[] ww = null;
        int vt = 0;
        boolean nulls = false;
        int j = 0;
        for (int i = 0, cLength = c.length; i < cLength; i++) {
            Caused ci = c[i];
            if (ci != null) {
                Term wi = ci.why();
                if (wi != null) {
                    if (ww == null) ww = new Term[c.length - i];
                    ww[j++] = wi;
                    vt += wi.volume();
                } else
                    nulls = true;
            } else
                nulls = true;
        }
        if (vt == 0)
            return null; //assert(j==0);
        if (j == 1)
            return ww[0];

        return _why(capacity, vt, nulls ? ArrayUtil.removeNulls(ww) : ww);
    }

    @Nullable
    private static Term why2(int capacity, Caused[] c) {
        if (capacity == 0) return null;
        Caused A = c[0], B = c[1];
        Term a = A.why();
        return A == B ? a : why2(capacity, a, B.why());
    }

    public static @Nullable Term why2(int capacity, Term a, Term b) {
        if (capacity == 0) return null;
        if (b == null || a == b) return a;
        if (a == null) return b;
        if (a.equals(b))
            return a;
        else
            return _why(capacity, a.volume() + b.volume(), a, b);
    }

    @Nullable
    private static Term _why(int capacity, int volumeSum, Term... ww) {
        if (capacity == 0)
            return null;
//        capacity--; //margin
        if (volumeSum < capacity) {
            return bundle(ww);
        } else {
            if (volumeSum < capacity * 1.5f)
                return flattenAndSample(capacity, ww, volumeSum);
            else
                return flattenAndSortByOccurrences(capacity, ww, volumeSum);
        }
    }

    /**
     * naive flatten and shuffle sample; does not consider the importance of duplicate entries
     */
    @Deprecated
    @Nullable
    private static Term flattenAndSample(int outerCapacity, Term[] ww, int innerCapacity) {
        RoaringBitmapWriter<RoaringBitmap> s = newRoaringBitmap(innerCapacity);
        ShortProcedure sAdd = s::add;
        for (Term cc : ww) {
            if (cc != null) each(cc, sAdd);
        }
        RoaringBitmap flattenedAll = s.get();
        return why(flattenedAll, outerCapacity);
    }

    @Deprecated
    @Nullable
    private static Term flattenAndSortByOccurrences(int outerCapacity, Term[] ww, int innerCapacity) {
        ShortFloatHashMap m = new ShortFloatHashMap(innerCapacity);
        ShortFloatProcedure addToM = m::addToValue;
        for (Term cc : ww) {
            if (cc != null)
                eval(cc, 1f, addToM);
        }

        int uniques = m.size();
        int excess = uniques - outerCapacity;
        short[] k;
        //TODO excess=1 special case
        if (excess > 0) {
            //TODO if little difference between max and min, just sample random

            //remove weakest
            MutableList<ShortFloatPair> mm = m.keyValuesView().toSortedList((a, b) -> Float.compare(b.getTwo(), a.getTwo()));
            k = new short[outerCapacity];
            for (int i = 0; i < outerCapacity; i++)
                k[i] = mm.get(i).getOne();
        } else
            k = m.keysView().toArray();
        return why(k, outerCapacity);
    }

    private static Term bundle(Term[] ct) {
        if (ct.length == 1) return ct[0];
        Term x = SETe.the(ct);
        return x.subs() == 1 ? x.sub(0) : x; //HACK
    }

//	@Deprecated public static Term why(@Nullable Term whyA, @Nullable Term whyB) {
//		return why(whyA, whyB, NAL.causeCapacity.intValue());
//	}

//	public static Term why(@Nullable Term whyA, @Nullable Term whyB, int capacity) {
//		if (capacity <= 0)
//			return null;
//
//		if (whyA == null)
//			return whyB; //TODO check cap
//		if (whyB == null)
//			return whyA; //TODO check cap
//
//		int wa = whyA.volume();
//		if (whyA.equals(whyB) && wa <= capacity)
//			return whyA; //same
//
//		int wb = whyB.volume();
//		if (wa + wb + 1 <= capacity) {
//			//return $.sFast(true, new Term[] { whyA, whyB });
//			return SETe.the(whyA, whyB);
//		} else {
//			//must reduce or sample
//			RoaringBitmapWriter<RoaringBitmap> s = newRoaringBitmap(wa+wb);
//			toSet(whyA, s);
//			toSet(whyB, s);
//			return why(
//				//s
//				s.get()
//				, capacity);
//		}
//	}

    private static RoaringBitmapWriter<RoaringBitmap> newRoaringBitmap(int cap) {
        return RoaringBitmapWriter.writer()
                .initialCapacity(cap)
                //.constantMemory()
                //.doPartialRadixSort()
                .get();
    }

    @FunctionalInterface
    public interface CauseVisitor<X> {
        void value(short cause, float pri, X[] x);
    }

    public static <X> void eval(@Nullable Term why, float pri, X[] causes, CauseVisitor<X> each) {
        if (why == null)
            return;
        eval(why, pri, (short w, float much) -> each.value(w, much, causes));
    }

    public static <X> void eval(@Nullable Term why, float pri, ShortFloatProcedure each) {
        if (why == null)
            return;

        if (why instanceof Int) {
            each.value(s(why), pri);
        } else {
            //split
            Subterms s = why.subterms();
            int n = s.subs();
            if (n == 0) return;

            assert (why.opID() == SETe.id && n > 1);

            float priEach = pri / n;
            if (s instanceof ShortSubterms ss) {
                for (short x : ss.subs)
                    each.value(x, priEach);
            } else {
                for (int i = 0; i < n; i++)
                    eval(s.sub(i), priEach, each);
            }
        }
    }


    public static void forEachUnique(Term why, ShortProcedure s) {
        if (why instanceof Int) {
            //optimized case
            s.value(s(why));
        } else {
            //TODO optimized case of simple set with no recursive inner-sets
            toSet(why).forEach(s); //TODO RoaringBitmap
        }
    }

    @Deprecated
    public static ShortHashSet toSet(Term why) {
        ShortHashSet s = new ShortHashSet(why.volume() /* estimate */);
        toSet(why, s);
        return s;
    }

    @Deprecated
    private static void toSet(Term whyA, ShortHashSet s) {
        if (whyA == null)
            return;
        whyA.recurseTermsOrdered(x -> true, (e) -> {
            if (e instanceof Int)
                s.add(s(e));
            return true;
        }, null);
    }

    public static void each(Term w, ShortProcedure each) {
        if (w instanceof Int) {
            each.value(s(w));
        } else {
            Subterms ww = w.subterms();
            if (ww instanceof ShortSubterms) {
                ((ShortSubterms) ww).addAllTo(each);
            } else {
                int wn = ww.subs();
                if (wn > 0) {
                    assert (w.opID() == SETe.id && wn > 1);
                    for (int i = 0; i < wn; i++)
                        each(ww.sub(i), each);
                }
            }
        }
    }

    private static short s(Term why) {
        return (short) Int.the(why);
    }

    private static final class LazyCaused extends LazyTerm implements Caused {
        private Caused[] c;
        private final int capacity;

        LazyCaused(Caused[] c, int capacity) {
            this.c = c;
            this.capacity = capacity;
        }

        @Override
        protected @Nullable Term build() {

            @Nullable Term y = Why.why(capacity, c);
            c = null; //release for GC
            return y;
        }

        @Override
        public @Nullable Term why() {
            return term();
        }
    }

//	private static final class LazyWhy extends LazyTerm {
//		private final Term[] c;
//		private final int capacity;
//
//		public LazyWhy(Term[] c, int capacity) {
//			this.c = c;
//			this.capacity = capacity;
//		}
//
//		@Override
//		protected @Nullable Term build() {
//			return Why.why(capacity, c);
//		}
//	}

//	static short[] sample(int maxLen, boolean deduplicate, short[]... s) {
//		int ss = s.length;
//		int totalItems = 0;
//		short[] lastNonEmpty = null;
//		int nonEmpties = 0;
//		for (short[] t : s) {
//			int tl = t.length;
//			totalItems += tl;
//			if (tl > 0) {
//				lastNonEmpty = t;
//				nonEmpties++;
//			}
//		}
//		if (nonEmpties == 1)
//			return lastNonEmpty;
//		if (totalItems == 0)
//			return ArrayUtil.EMPTY_SHORT_ARRAY;
//
//
//		ShortBuffer ll = new ShortBuffer(Math.min(maxLen, totalItems));
//		RoaringBitmap r = deduplicate ? new RoaringBitmap() : null;
//		int ls = 0;
//		int n = 0;
//		int done;
//		main:
//		do {
//			done = 0;
//			for (short[] c : s) {
//				int cl = c.length;
//				if (n < cl) {
//					short next = c[cl - 1 - n];
//					if (deduplicate)
//						if (!r.checkedAdd(next))
//							continue;
//
//					ll.add/*adder.accept*/(next);
//					if (++ls >= maxLen)
//						break main;
//
//				} else {
//					done++;
//				}
//			}
//			n++;
//		} while (done < ss);
//
//		//assert (ls > 0);
//		short[] lll = ll.toArray();
//		//assert (lll.length == ls);
//		return lll;
//	}
}