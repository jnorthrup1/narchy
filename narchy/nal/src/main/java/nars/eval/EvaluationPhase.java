package nars.eval;

import jcog.Is;
import jcog.data.list.Lst;
import nars.Term;
import nars.func.Equal;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.atom.Bool;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Function;

import static nars.term.atom.Bool.*;

/** implements an individual evaluation pass */
@Is("Director_string") abstract class EvaluationPhase implements Function<Evaluation,Term> {


    private static Term call(Evaluation e, Compound a) {
        Term x = a.sub(0), y = a.sub(1);
		return a.EQ() ?
			Equal.compute(e, x, y) :
			((Functor) y).apply(e, x.subtermsDirect());
    }

    /** the starting term */
	public abstract Term start();

	@Deprecated public static final EvaluationPhase NULL = new EvaluationPhase() {
		@Override
		public Term start() {
			return null;
		}

		@Override public Term apply(Evaluation e) {
			return Null;
		}
	};


	/** default interpreter impl */
	static final class Recipe extends EvaluationPhase {

		private final Term x;
		private final Consumer<Evaluation>[] steps;

		/** pre-process: prioritize constant evaluables */
        Recipe(Term x, Lst<Compound> yy) {
			this.x = x;
			int len = yy.size();
			this.steps = new Consumer[len] /* if steps can add > 1 each, this is an estimate */;
			for (int i = 0; i < len; i++)
				steps[i] = new EvaluationPhase.ReEvaluate(yy.get(i), false, false);
		}

		@Override
		public Term start() {
			return x;
		}

		@Override public @Nullable Term apply(Evaluation e) {

			Term pre = e.z, next;
			for (Consumer<Evaluation> s : steps) {
				s.accept(e);
				next = e.z;
				if (!pre.equals(next)) {
					//terminate or force recompute
					//TODO determine if subsequent steps are actually affected, and fully or partially elide recompute
					return next;
				}
			}

			return pre;
		}
	}

	static class ReEvaluate implements Consumer<Evaluation> {

		final Compound a;
		final boolean retransform, singular;

		/** singular if its the only or final step */
		ReEvaluate(Compound clause, boolean retransform, boolean singular) {
			this.a = clause;
			this.retransform = retransform;
			this.singular = singular;
		}

		@Override
		public void accept(Evaluation e) {
			Compound a = this.a;

//			int before = retransform ? e.now() : -1;

			Term b = call(e, a/*, e.subs*/);
			if (b!=null && b != a) {
				if (singular || b == Null) {
					e.z = b;
//					return;
				} else {
					e.z = e.z.replace(a, b, Evaluator.B);
				}
			}

//			if (retransform && e.now() > before)
//				e.z = e.transform(e.z); //apply new assignments
		}
	}

	static class Constant extends EvaluationPhase {
		final Term x;

		private static final Constant
			TRUE = new Constant(True),
			FALSE = new Constant(False),
			NULL = new Constant(Null);

		private Constant(Term x) {
			this.x = x;
		}

		@Override
		public Term start() {
			return x;
		}

		static Constant the(Term a) {
			if (a instanceof Bool) {
				     if (a == Null)  return NULL;
				else if (a == True)  return TRUE;
				else if (a == False) return FALSE;
			}
			return new Constant(a);
		}

		@Override
		public Term apply(Evaluation e) {
			return x;
		}
	}

	static class Itself extends Constant {

		Itself(Compound a) {
			super(a);
		}

		@Override
		public Term apply(Evaluation e) {
			return call(e, (Compound)x);
		}
	}

	static class PartOfItself extends Constant {
		private final Compound a;

		PartOfItself(Term x, Compound a) {
			super(x);
			this.a = a;
		}

		@Override
		public Term apply(Evaluation e) {
			Term b = call(e, a);
			if (b == null) return null;
			else if (b == Null) return Null;
			else return e.z = e.z.replace(a, b, Evaluator.B);
		}
	}
}