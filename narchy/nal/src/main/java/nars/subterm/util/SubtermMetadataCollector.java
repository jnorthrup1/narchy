package nars.subterm.util;

import jcog.Util;
import nars.Term;
import nars.term.Variable;
import nars.term.atom.Atomic;

import static nars.Op.*;

public final class SubtermMetadataCollector {
    public int structure;
    public int hash = 1;
    public int vol = 1;
    public int varPattern;
    public int varQuery;
    public int varDep;
    public int varIndep;

    public SubtermMetadataCollector() {

    }

    public SubtermMetadataCollector(Term[] terms) {
        for (Term x : terms)  add(x);
    }

    private void collectNonVar(int op, int hash) {
        this.vol++;
        this.structure |= (1<<op);
        this.hash = Util.hashCombine(this.hash, hash);
    }

    static {
        assert(VAR_PATTERN.id == 9  && VAR_QUERY.id == 10 && VAR_INDEP.id == 11 && VAR_DEP.id == 12);
    }
    private void collectVar(int type) {
		switch (type) {
			case 9 -> varPattern++;
			case 10 -> varQuery++;
			case 11 -> varIndep++;
			case 12 -> varDep++;
			default -> throw new UnsupportedOperationException();
		}
    }

    public void add(Term x) {
        int hash = x.hashCode();
        if (x instanceof Atomic) {

            int xo = x.opID();

            collectNonVar(xo, hash);

            if (x instanceof Variable)
                collectVar(xo);

        } else {
            this.hash = Util.hashCombine(this.hash, hash);

            int xstructure = x.structure();
            if ((xstructure & VAR_PATTERN.bit) != 0)
                this.varPattern += x.varPattern();
            if ((xstructure & VAR_DEP.bit) != 0)
                this.varDep += x.varDep();
            if ((xstructure & VAR_INDEP.bit) != 0)
                this.varIndep += x.varIndep();
            if ((xstructure & VAR_QUERY.bit) != 0)
                this.varQuery += x.varQuery();

            this.structure |= xstructure;
            this.vol += x.volume();

        }

    }

}