package nars.subterm;

import jcog.Util;
import jcog.util.SingletonIterator;
import nars.Op;
import nars.Term;
import nars.term.Compound;

import java.util.Iterator;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

abstract class AbstractUnitSubterm implements Subterms {

    protected abstract Term sub();

    @Override
    public final String toString() {
        return Subterms.toString(sub());
    }

    @Override
    public final Term[] arrayClone() {
        return new Term[]{sub()};
    }

    @Override
    public final int volume() {
        return 1 + sub().volume();
    }

    @Override
    public final int complexity() {
        return 1 + sub().complexity();
    }

    @Override
    public final float voluplexity() {
        return 1 + sub().voluplexity();
    }

    @Override
    public boolean BOOL(Predicate<? super Term> t, boolean andOrOr) {
        return t.test(sub());
    }

    @Override
    public final <X> boolean ORwith(BiPredicate<Term, X> p, X param) {
        return ANDwith(p, param);
    }

    @Override
    public final <X> boolean ANDwith(BiPredicate<Term, X> p, X param) {
        return p.test(sub(), param);
    }

    @Override
    public final <X> boolean ANDwithOrdered(BiPredicate<Term, X> p, X param) {
        return ANDwith(p, param);
    }

//    @Override
//    public @Nullable Term subSub(int start, int end, byte[] path) {
//        byte a = path[start];
//        if (a != 0)
//            return null;
//        else {
//            Term s = sub();
//            return end - start == 1 ?
//                    s :
//                    s.sub(start + 1, end);
//        }
//    }

//    @Override
//    public final void forEachI(ObjectIntProcedure<Term> t) {
//        t.value(sub(), 0);
//    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Subterms s)) return false;
        return s.subs() == 1 && (sub().equals(s.sub(0)));
    }


    @Override
    public final int hashCode() {
        return Util.hashCombine1(sub());
    }

    @Override
    public final int hashCodeSubterms() {
        return Subterms.hash(sub());
    }

    @Override
    public final Term sub(int i) {
        if (i != 0)
            throw new ArrayIndexOutOfBoundsException();
        return sub();
    }

    public final Term[] removing(int index) {
        assert (index == 0);
        return Op.EmptyTermArray;
    }

    @Override
    public final int subs() {
        return 1;
    }

    @Override
    public void setNormalized() {
        Term t = sub();
        if (t instanceof Compound)
            ((Compound)t).setNormalized();
    }

    @Override
    public final void forEach(Consumer<? super Term> c) {
        c.accept(sub());
    }

    @Override
    public final Stream<Term> subStream() {
        return Stream.of(sub());
    }

    @Override
    public final Iterator<Term> iterator() {
        return new SingletonIterator<>(sub());
    }

    @Override
    public final int sum(ToIntFunction<Term> value) {
        return value.applyAsInt(sub());
    }

//    @Override
//    public @Nullable Term subSub(int start, int end, byte[] path) {
//        byte a = path[start];
//        if (a != 0)
//            return null;
//        else {
//            Term s = sub();
//            return end - start == 1 ?
//                    s :
//                    s.subPath(start + 1, end);
//        }
//    }


}