package nars.subterm;

import jcog.The;
import nars.Term;

/** minimal light-weight wrapper of a single target as a Subterms impl */
public final class UnitSubterm extends AbstractUnitSubterm implements The {

    private final Term the;

    public UnitSubterm(Term the) {
        this.the = the;
        //testIfInitiallyNormalized();
    }


    @Override public Term sub() {
        return the;
    }


}