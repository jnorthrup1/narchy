package nars.subterm;

import com.google.common.base.Joiner;
import com.google.common.collect.Streams;
import com.google.common.io.ByteArrayDataOutput;
import jcog.Hashed;
import jcog.TODO;
import jcog.Util;
import jcog.data.bit.MetalBitSet;
import jcog.decide.Roulette;
import nars.Op;
import nars.Term;
import nars.subterm.util.TermMetadata;
import nars.term.*;
import nars.term.atom.Bool;
import nars.term.buffer.TermBuffer;
import nars.term.util.Terms;
import nars.term.var.NormalizedVariable;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;
import org.eclipse.collections.api.block.predicate.primitive.IntObjectPredicate;
import org.eclipse.collections.api.block.predicate.primitive.LongObjectPredicate;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;

import static nars.Op.*;


/**
 * Methods common to both Term and Subterms
 * T = subterm type
 */
public interface Subterms extends Termlike, Iterable<Term> {

//    IntObjectToIntFunction<Term> HASH_COMBINER = Util::hashCombine;

    static int hash(Term onlySub) {
        return Util.hashCombine1(onlySub);
    }


    static int hash(Term[] term, int n) {
        int h = 1;
        for (int i = 0; i < n; i++)
            h = Util.hashCombine(h, term[i]);
        return h;
    }

    default int hashExhaustive() {
        //return s.intifyShallow(1, HASH_COMBINER);
        int n = subs(), v = 1;
        for (int i = 0; i < n; i++)
            v = Util.hashCombine(v, sub(i).hashCode());
        return v;
    }

//    static boolean commonSubtermsRecursive(Term a, Term b, boolean excludeVariables) {
//
//        Subterms aa = a.subterms(), bb = b.subterms();
//
//        int commonStructure = aa.structure() & bb.structure();
//        if (excludeVariables)
//            commonStructure &= ~(Op.Variables) & AtomicConstant;
//
//        if (commonStructure == 0)
//            return false;
//
//        Collection<Term> scratch = new UnifiedSet<>(0);
//        aa.recurseSubtermsToSet(commonStructure, scratch, true);
//        return bb.recurseSubtermsToSet(commonStructure, scratch, false);
//    }

    static String toString(Iterable<? extends Term> subterms) {
        return '(' + Joiner.on(',').join(subterms) + ')';
    }

    static String toString(Term... subterms) {
        return '(' + Joiner.on(',').join(subterms) + ')';
    }

    static int compare(Subterms a, Subterms b) {
        if (a == b)
            return 0;

        int s;
        int diff;
        if ((diff = Integer.compare(s = a.subs(), b.subs())) != 0)
            return diff;

        for (int i = 0; i < s; i++) {
            //for (int i = s-1; i >= 0; i--) {
            int d = a.sub(i).compareTo(b.sub(i));
            if (d != 0) return d;
        }

        return 0;
    }

    private static boolean onlyNormalizedVariable(Variable z) {
        return z instanceof NormalizedVariable && ((NormalizedVariable) z).id() == 1;
    }

    //    default boolean equalTermsIdentical(Subterms x) {
//        if (this == x) return true;
//        int n = subs();
//        if (x.subs()!=n) return false;
//        return IntStream.range(0, n).noneMatch(i -> sub(i) != x.sub(i));
//    }

    private static boolean hashedSubterms(Subterms x) {
        return !(x instanceof Compound) && x instanceof Hashed;
    }

    default Subterms transformSubs(UnaryOperator<Term> f) {

        int s = subs();
        TmpTermList out = new TmpTermList(s);

        boolean changed = false;
		for (int j = 0; j < s; j++) {

            Term x = sub(j);
            Term y = f.apply(x); //assert(k!=null);
			if (y == Bool.Null)
				return null;
			if (x != y) changed = true;

            out.addFast(y);
		}

		return changed ? out : this;
    }


//    /**
//     * returns sorted ready for commutive; null if nothing in common
//     */
//    static @Nullable MutableSet<Term> intersect(Subterms a, Subterms b) {
//        if ((a.structure() & b.structure()) != 0) {
//
//            Set<Term> as = a.toSet();
//            MutableSet<Term> ab = b.toSet(as::contains);
//            if (ab != null)
//                return ab;
//        }
//        return null;
//    }

    @Override
    default boolean hasVars() {
        return hasAny(Variables);
    }

    @Override
    default int vars() {
        return sum(Termlike::vars);
    }


//    /*@NotNull*/
//    static boolean commonSubterms(Compound a, Compound b, boolean excludeVariables) {
//
//        int commonStructure = a.structure() & b.structure();
//        if (excludeVariables)
//            commonStructure = commonStructure & ~(Op.Variable);
//
//        if (commonStructure == 0)
//            return false;
//
//        Set<Term> scratch = new UnifiedSet(a.subs());
//        Subterms.subtermsToSet(a, commonStructure, scratch, true);
//        return Subterms.subtermsToSet(b, commonStructure, scratch, false);
//
//    }


//    default @Nullable Term subSub(byte[] path) {
//        return subSub(0, path.length, path);
//    }


//    default boolean containsAll(Subterms ofThese) {
//        return ofThese.AND(contains(false, false));
//    }

    //    /**
//     * a and b must be instances of input, and output must be of size input.length-2
//     */
//    /*@NotNull*/
//    static Term[] except(Subterms input, Term a, Term b, Term[] output) {
//
//
//        int j = 0;
//        int l = input.subs();
//        for (int i = 0; i < l; i++) {
//            Term x = input.sub(i);
//            if ((x != a) && (x != b))
//                output[j++] = x;
//        }
//
//        if (j != output.length)
//            throw new RuntimeException("permute underflow");
//
//        return output;
//    }

    @Override
    default boolean TEMPORAL_VAR() {
        return !(this instanceof CondAtomic) && (hasAny(Temporals) && ORunneg(Termlike::TEMPORAL_VAR));
    }

    default boolean containsAny(Subterms ofThese) {
        return ofThese.OR(contains(false, false));
    }

    default <X> X[] array(Function<Term, X> map, IntFunction<X[]> arrayizer) {
        int s = subs();
        X[] xx = arrayizer.apply(s);
        for (int i = 0; i < s; i++)
            xx[i] = map.apply(sub(i));
        return xx;
    }

    default int seqDurSub(int i, boolean xternalSensitive) {
        return this instanceof CondAtomic ? 0 : subUnneg(i).seqDur(xternalSensitive);
    }

    default @Nullable Term subRoulette(FloatFunction<Term> subValue, Random rng) {
        int s = subs();
        return switch (s) {
            case 0 -> null;
            case 1 -> sub(0);
            default -> sub(Roulette.selectRoulette(s, i -> subValue.floatValueOf(sub(i)), rng));
        };
    }

    @Override
    default int structure() {
        return structureSubs();
    }

    @Override
    default int structureSurface() {
        //return intifyShallow(0, (s, x) -> s | x.opBit());
        int n = subs(), s = 0;
        for (int i = 0; i < n; i++)
            s |= sub(i).opBit();
        return s;
    }

//	default void forEachI(ObjectIntProcedure<Term> t) {
//		int s = subs();
//		for (int i = 0; i < s; i++)
//			t.value(sub(i), i);
//	}
//
//	default <X> void forEachWith(BiConsumer<Term, X> t, X argConst) {
//		int s = subs();
//		for (int i = 0; i < s; i++)
//			t.accept(sub(i), argConst);
//	}

    default int structureSurfaceUnneg() {
        int n = subs(), s = 0;
        for (int i = 0; i < n; i++)
            s |= subUnneg(i).opBit();
        return s;
    }

    /**
     * overridden only in Compounds
     */
    @Override
    default int structureSubs() {
        //return intifyShallow(0, (s, x) -> s | x.structure());
        int n = subs();
        int s = 0;
        for (int i = 0; i < n; i++)
            s |= sub(i).structure();
        return s;
    }

    /**
     * sorted and deduplicated
     */
    default Subterms commuted() {
        if (subs() <= 1) return this;
        Term[] x = arrayShared();
        Term[] y = Terms.commute(x);
        return x == y ? this : terms.subterms(y);
    }

    default Subterms sorted() {
        if (subs() <= 1) return this;
        Term[] x = arrayShared();
        Term[] y = Terms.sort(x);
        return x == y ? this : terms.subterms(y);
    }

    default boolean isSorted() {
        int s = subs();
        if (s < 2) return true;
        Term p = sub(0);
        for (int i = 1; i < s; i++) {
            Term n = sub(i);
            if (p != n && p.compareTo(n) > 0)
                return false;
            p = n;
        }
        return true;
    }

    default boolean isCommuted() {
        int s = subs();
        if (s < 2) return true;
        Term p = sub(0);
        for (int i = 1; i < s; i++) {
            Term n = sub(i);
            if (p.compareTo(n) >= 0)
                return false;
            p = n;
        }
        return true;
    }

    /**
     * TODO constructing a Stream like this just for the Iterator is not very efficient
     */
    @Override
    default Iterator<Term> iterator() {
        return new SubtermsIterator(this);
        //return IntStream.range(0, subs()).mapToObj(this::sub).iterator();
    }

    default boolean conjDecompose(LongObjectPredicate<Term> each, long when, int dt, boolean dE, boolean dX) {
        if ((dE && !dX && dt==DTERNAL) || (dX && !dE && dt==XTERNAL)) {
            return conjDecomposePar(each, when);
        } else {
            return conjDecomposeSeq(each, when, dt, dE, dX);
        }
    }

    private boolean conjDecomposePar(LongObjectPredicate<Term> each, long t) {
        return AND(x -> each.accept(t, x));
    }

    private boolean conjDecomposeSeq(LongObjectPredicate<Term> each, long t, int dt, boolean dE, boolean dX) {
        boolean parallel = (dt == DTERNAL) || (dt == XTERNAL);
        boolean changeDT = !parallel;// && t != ETERNAL && t != TIMELESS /* motionless in time */;

        if (t == ETERNAL || t == TIMELESS)
            throw new UnsupportedOperationException();

        boolean fwd;
        if (changeDT) {
            if (!(fwd = (dt >= 0)))
                dt = -dt;
        } else
            fwd = true;

        int s = subs() - 1;
        for (int i = 0; i <= s; i++) {
            Term ii = sub(fwd ? i : s - i);

            boolean ic = ii.CONJ();
            if (ic ? !((Compound) ii).eventsAND(each, t, dE, dX) : !each.accept(t, ii))
                return false;

            if (changeDT && i < s)
                t += dt + ii.seqDur();
        }

        return true;
    }

    /**
     * HACK probably best not to call directly
     * TODO move to MegaHeapTermBuilder
     */
    default Subterms negated() {
        if (0 == (structureSurface() & NEG.bit))
            return new NegatedSubterms(this);
        else
            throw new TODO();
    }
//	/** the surface structure common to each subterm */
//	default int structureSurfaceIntersection() {
//		return switch (subs()) {
//			case 0 -> 0;
//			case 1 -> sub(0).opBit();
//			default -> intifyShallow(~(0), (s, y) -> s != 0 ? (y.opBit() & s) : 0);
//		};
//	}

    default void appendTo(TermBuffer b) {
        //forEach(b::append);
        int s = subs();
        for (int i = 0; i < s; i++)
            b.append(sub(i));
    }

    /**
     * the structure common to each subterm
     */
    default int structureIntersection() {
        return switch (subs()) {
            case 0 -> 0;
            case 1 -> sub(0).structure();
            default -> intifyShallow(~(0), (s, y) -> s != 0 ? (y.structure() & s) : 0);
        };
    }

    /**
     * the minimum volume of any subterm
     */
    default int volMin() {
        return switch (subs()) {
            case 0 -> 0;
            case 1 -> sub(0).volume();
            default -> intifyShallow(Integer.MAX_VALUE, (v, y) -> Math.min(v, y.volume()));
        };
    }

    default int seqDur(boolean xternalSensitive) {
        if (this instanceof CondAtomic)
            return 0;
        int r = 0;
        int s = subs();
        for (int i = 0; i < s; i++) {
            int di = seqDurSub(i, xternalSensitive);
            if (xternalSensitive && di == XTERNAL) return XTERNAL;
            r = Math.max(r, di);
        }
        return r;
    }

    default boolean subEquals(int i, Term x) {
        return sub(i).equals(x);
    }

//	@SuppressWarnings("LambdaUnfriendlyMethodOverload")
//	default SortedSet<Term> toSetSorted(UnaryOperator<Term> map) {
//		SortedSet<nars.term.Term> u = new TreeSet(); //new MetalTreeSet();
//		addAllTo(u);
//		return u;
//	}


//    /**
//     * returns whether the set operation caused a change or not
//     */
//    /*@NotNull*/
//    private static boolean subtermsToSet(Subterms ss, int inStructure, Collection<Term> t, boolean addOrRemoved) {
//        boolean r = false;
//
//        int l = ss.subs();
//        for (int i = 0; i < l; i++) {
//            /*@NotNull*/
//            Term s = ss.sub(i);
//            if (inStructure == -1 || ((s.structure() & inStructure) > 0)) {
//                r |= (addOrRemoved) ? t.addAt(s) : t.remove(s);
//                if (!addOrRemoved && r)
//                    return true;
//            }
//        }
//        return r;
//    }

//	default SortedSet<Term> toSetSorted(Predicate<Term> t) {
//		int s = subs();
//		switch (s) {
//			case 1: {
//				Term the = sub(0);
//				return t.test(the) ? ArrayUnenforcedSortedSet.the(the) : ArrayUnenforcedSortedSet.empty;
//			}
//			case 2: {
//				Term a = sub(0), b = sub(1);
//				boolean aok = t.test(a), bok = t.test(b);
//				if (aok && bok)
//					return ArrayUnenforcedSortedSet.the(a, b);
//				else if (!aok && !bok)
//					return ArrayUnenforcedSortedSet.empty;
//				else if (!bok)
//					return ArrayUnenforcedSortedSet.the(a);
//				else
//					return ArrayUnenforcedSortedSet.the(b);
//			} default: {
//				List<Term> u = new FasterList<>(s);
//				for (Term x : this) {
//					if (t.test(x)) u.add(x);
//				}
//				int us = u.size();
//				if (us == s) {
//					if (this instanceof TermVector)
//						return ArrayUnenforcedSortedSet.the(this.arrayShared());
//				}
//				switch (us) {
//					case 0:
//						return ArrayUnenforcedSortedSet.empty;
//					case 1:
//						return ArrayUnenforcedSortedSet.the(u.get(0));
//					case 2:
//						return ArrayUnenforcedSortedSet.the(u.get(0), u.get(1));
//					default:
//						return ArrayUnenforcedSortedSet.the(u.toArray(Op.EmptyTermArray));
//				}
//			}
//		}
//
//	}

    default SortedSet<Term> toSetSorted() {
        SortedSet<Term> u = new TreeSet<>(); //new MetalTreeSet();
        addAllTo(u);
        return u;
    }

    /**
     * an array of the subterms, which an implementation may allow
     * direct access to its internal array which if modified will
     * lead to disaster. by default, it will call 'toArray' which
     * guarantees a clone. override with caution
     */
    default Term[] arrayShared() {
        return arrayClone();
    }

    /**
     * an array of the subterms
     * this is meant to be a clone always
     */
    default Term[] arrayClone() {
        int s = subs();
        return s == 0 ? EmptyTermArray : arrayClone(new Term[s], 0, s);
    }

    default Term[] arrayClone(Term[] target, int from, int to) {
        for (int i = from, j = 0; i < to; i++, j++)
            target[j] = sub(i);

        return target;
    }

//	default TermList toListIfNotList() {
//		return toList();
////		return this instanceof TermList ?
////				(TermList)this :
////				toList();
//	}

    default TermList toList() {
        return new TermList(this);
    }

    default TmpTermList toTmpList() {
        return new TmpTermList(this);
    }

    /**
     * @return a Mutable Set, unless empty
     */
    default MutableSet<Term> toSet() {
        int s = subs();
        UnifiedSet<Term> u = new UnifiedSet<>(s);
        //if (s > 0)
        addAllTo(u);
        return u;
    }

    default @Nullable <C extends Collection<Term>> C collect(Predicate<Term> ifTrue, C c) {
        int s = subs();
        //UnifiedSet<Term> u = null;
        for (int i = 0; i < s; i++) {
            /*@NotNull*/
            Term x = sub(i);
            if (ifTrue.test(x)) {
//                    if (c == null)
//                        c = new UnifiedSet<>((s - i) * 2);
                c.add(x);
            }
        }

        return c;
    }

    /**
     * by default this does not need to do anything
     * but implementations can cache the normalization
     * in a boolean because it only needs done once.
     */
    default void setNormalized() {
        //throw new UnsupportedOperationException();
    }

    /**
     * assume its normalized if no variables are present
     */
    default boolean NORMALIZED() {
		/*
		 TODO
			int normalized(Term x, int offset)
				--continues by keeping or increasing the variable offset, or returns -1 on first inconsistency encountered.

		 */

        int v;
        return switch (v = this.vars()) {
            case 0 -> true;

            //optimized 1-ary case
            case 1 -> this.ANDrecurse(Subterms::hasVars, z ->
                            !(z instanceof Variable) || onlyNormalizedVariable((Variable) z),
                    null);

            //general n-nary case
            default -> {
                if (this.subs() == 1) {
                    Term x0 = this.sub(0);
                    if (x0 instanceof Compound)
                        yield ((Compound) x0).NORMALIZED();
                    else if (x0 instanceof Variable)
                        yield onlyNormalizedVariable((Variable) x0);
                    else
                        yield true;
                } else
                    yield this.recurseTermsOrdered(TermMetadata.VarPreNormalization::descend, new TermMetadata.VarPreNormalization(v), null);
            }
        };
    }

    /**
     * gets the set of unique recursively contained terms of a specific type
     * TODO generalize to a provided lambda predicate selector
     */
    /*@NotNull*/
    default MutableSet<Term> recurseSubtermsToSet(Op _onlyType) {
        return recurseSubtermsToSet(_onlyType.bit);
    }

    default MutableSet<Term> recurseSubtermsToSet(int structIs) {

        if (!hasAny(structIs))
            return Sets.mutable.empty();

        MutableSet<Term> t = new UnifiedSet<>(volume());
        ANDrecurse(
                tt -> tt.hasAny(structIs),
                tt -> {
                    if (tt.isAny(structIs))
                        t.add(tt);
                    return true;
                }, null);
        return t;
    }

    /*@NotNull*/
    default boolean recurseSubtermsToSet(int inStructure, Collection<Term> t, boolean untilAddedORwhileNotRemoved) {
        boolean[] r = {false};


        ANDrecurse(
                inStructure == -1 ? any -> true : p -> p.hasAny(inStructure),

                s -> {

                    if (!untilAddedORwhileNotRemoved && r[0])
                        return false;

                    if (s.hasAny(inStructure)) {
                        r[0] |= (untilAddedORwhileNotRemoved) ? t.add(s) : t.remove(s);
                    }

                    return true;
                }, null);


        return r[0];
    }


    default /* final */boolean containsRecursively(Term t) {
        return containsRecursively(t, null);
    }

    default boolean containsRecursively(Term x, @Nullable Predicate<Compound> subTermOf) {

        int xv = x.volume();
        if (!impossibleSubVolume(xv)) {
            int xs = x.structure();
            if (!impossibleSubStructure(xs)) {
                int s = subs();
                Term prev = null;
                for (int i = s - 1; i >= 0; i--) {
                    Term y = sub(i);
                    if (prev != y) {
                        int yv = y.volume();
                        if (yv == xv) {
                            if (x.equals(y))
                                return true;
                        }
                        if (y instanceof Compound && yv > xv) {
                            if (((Compound) y).containsRecursively(x, subTermOf))
                                return true;
                        }
                        prev = y;
                    }
                }
            }
        }
        return false;
    }

    default boolean equalTerms(Term[] x) {
        if (this instanceof TermList && ((TermList)this).array() == x) return true;

        int n = x.length;
        if (subs()!= n) return false;
        for (int i = 0; i < n; i++) {
            if (!subEquals(i, x[i])) return false;
        }
        return true;
    }

    default boolean equalTerms(Subterms b) {
        if (this == b) return true;

//		if (this instanceof SeparateSubtermsCompound && b instanceof SeparateSubtermsCompound)
//			return ((SeparateSubtermsCompound) this).subterms().equals(((SeparateSubtermsCompound)b).subterms());

        if (hashedSubterms(this) && hashedSubterms(b) && hashCodeSubterms() != b.hashCodeSubterms())
            return false;

        int s = this.subs();
        if (s != b.subs())
            return false;
        for (--s; s >= 0; s--) {
            if (!subEquals(s, b.sub(s)))
                return false;
        }
        return true;
    }

    default void addAllTo(Collection<Term> target) {
        forEach(target::add);
    }

    default Term[] terms(IntObjectPredicate<Term> filter) {
        TmpTermList l = null;
        int s = subs();
        for (int i = 0; i < s; i++) {
            Term t = sub(i);
            if (filter.accept(i, t)) {
                if (l == null)
                    l = new TmpTermList(s - i);
                l.addFast(t);
            }
        }
        return l == null ? EmptyTermArray : l.arrayTake();
    }

    default void forEach(Consumer<? super Term> action, int start, int stop) {
        if (start < 0 || stop > subs())
            throw new ArrayIndexOutOfBoundsException();

        for (int i = start; i < stop; i++)
            action.accept(sub(i));
    }

    @Override
    default void forEach(Consumer<? super Term> action) {
        forEach(action, 0, subs());
    }

    /**
     * return whether a subterm op at an index is an operator.
     */
    default boolean subIs(int i, Op o) {
        return subOpID(i) == o.id;
    }

    default int subOpID(int i) {
        return sub(i).opID();
    }

    /**
     * counts subterms matching the predicate
     */
    default int count(Predicate<? super Term> match) {
        return intifyShallow(0, (c, sub) -> match.test(sub) ? c + 1 : c);
    }

    default boolean countEquals(Predicate<Term> match, int n) {
        int s = subs();
        if (n > s) return false; //impossible
        int c = 0;
        for (int i = 0; i < s; i++) {
            if (match.test(sub(i))) {
                if (++c > n)
                    return false;
            } else {
                //TODO
//				if (c + (s-1-i) < n)
//					return false; //too few remain
            }
        }
        return c == n;
    }

    /**
     * counts subterms matching the supplied op
     */
    default int count(Op matchingOp) {
        int matchingOpID = matchingOp.id;
        return count(x -> x.opID() == matchingOpID);
    }

    /**
     * first index of; follows normal indexOf() semantics; -1 if not found
     */
    default /* final */ int indexOf(Term x) {
        return indexOf(x, -1);
    }

    default /* final */ int indexOfInstance(Term x) {
        int n = subs();
        for (int i = 0; i < n; i++) {
            if (sub(i)==x)
                return i;
        }
        return -1;
    }

    default int indexOf(Term t, int after) {
        return indexOf(t.equals(), after);
    }

//    static boolean unifyLinearN_TwoPhase0(Subterms x, Subterms y, int n, Unify u) {
//        Term[] p = null;
//        int dynPairs = 0;
//        for (int i = 0; i < n; i++) {
//            Term xi = x.sub(i);
//            Term yi = y.sub(i);
//
//            if (xi == yi)
//                continue;
//
//            boolean now = (i == n - 1) || ((u.var(xi) && u.var(yi)));
//
//            if (now) {
//
//                if (!xi.unify(yi, u))
//                    return false;
//            } else {
//                if (p == null)
//                    p = new Term[(n - i - 1) * 2];
//
//                //backwards order
//                p[dynPairs++] = yi;
//                p[dynPairs++] = xi;
//            }
//        }
//
//
//        if (p != null) {
//            int pairs = dynPairs/2;
//            if (pairs == 1) {
//                return p[1].unify(p[0], u);
//            } else {
//
//                //TODO sort deferredPairs so that smaller non-commutive subterms are tried first
//                if (pairs ==2 ) {
//                    boolean forward = choose(1f/(p[0].voluplexity() + p[1].voluplexity()), 1f/(p[2].voluplexity() + p[3].voluplexity()), u
//                    );
//
//                    if (forward) {
//                        return p[1].unify(p[0], u) && p[3].unify(p[2], u);
//                    } else {
//                        return p[3].unify(p[2], u) && p[1].unify(p[0], u);
//                    }
//                } else {
//
//                    do {
//                        if (!p[--dynPairs].unify(p[--dynPairs], u))
//                            return false;
//                    } while (dynPairs > 0);
//                }
//            }
//        }
//
//        return true;
//    }

//    static boolean choose(float forwardWeight, float reverseWeight, Unify u) {
//        return u.random.nextFloat() < (forwardWeight /(forwardWeight + reverseWeight));
//
////                    if (v01 < v23) {
////                        //try 01 first
////                        forward = true;
////                    } else if (v01 > v23) {
////                        forward = false;
////                    } else {
////                        forward = u.random.nextBoolean();
////                    }
//    }

    /**
     * stream of each subterm
     */
    default Stream<Term> subStream() {
        return Streams.stream(this);
    }

//	/**
//	 * return the first subterm matching the predicate, or null if none match
//	 */
//	default @Nullable Term subFirst(Predicate<Term> match) {
//		int i = indexOf(match);
//		return i != -1 ? sub(i) : null;
//	}


//    default Term[] termsExcept(RoaringBitmap toRemove) {
//        int numRemoved = toRemove.getCardinality();
//        int size = subs();
//        int newSize = size - numRemoved;
//        Term[] t = new Term[newSize];
//        int j = 0;
//        for (int i = 0; i < size; i++) {
//            if (!toRemove.contains(i))
//                t[j++] = sub(i);
//        }
//        return (t.length == 0) ? Op.EmptyTermArray : t;
//    }

    /**
     * allows the subterms to hold a different hashcode than hashCode when comparing subterms
     */
    default int hashCodeSubterms() {
        return hashExhaustive();
    }

    /**
     * TODO write negating version of this that negates only up to subs() bits
     */
    default MetalBitSet indicesOfBits(Predicate<Term> match) {
        int n = subs();
        MetalBitSet m = MetalBitSet.bits(n);
        for (int i = 0; i < n; i++) {
            if (match.test(sub(i)))
                m.set(i);
        }
        return m;
    }

//	default @Nullable MetalBitSet indicesOfBitsOrNull(Predicate<Term> match) {
//		MetalBitSet m = indicesOfBits(match);
//		return m.cardinality() == 0 ? null : m;
//	}

    @Nullable default Term[] subs(Predicate<Term> toKeep) {
        return subsIncExc(indicesOfBits(toKeep), true);
    }

//	default Term[] subsIncluding(MetalBitSet toKeep) {
//		return subsIncExc(toKeep, true);
//	}

    default Term[] removing(MetalBitSet toRemove) {
        return subsIncExc(toRemove, false);
    }

    default @Nullable Term[] subsIncExc(MetalBitSet s, boolean includeOrExclude) {

        int c = s.cardinality();

        if (c == 0) {
//            if (!includeOrExclude)
//                throw new UnsupportedOperationException("should not reach here");
            return includeOrExclude ? EmptyTermArray : arrayShared();
        }

        int size = subs();
        assert (c <= size) : "bitset has extra bits setAt beyond the range of subterms";

        if (includeOrExclude) {
            if (c == size) return arrayShared();
            if (c == 1) return new Term[]{sub(s.first(true))};
        } else {
            if (c == size) return EmptyTermArray;
            if (c == 1) return removing(s.first(true));
        }


        int newSize = includeOrExclude ? c : size - c;
        Term[] t = new Term[newSize];
        int j = 0;
        for (int i = 0; i < size; i++) {
            if (s.test(i) == includeOrExclude)
                t[j++] = sub(i);
        }
        return t;
    }

    default /* final */ Term[] subRangeReplace(int from, @Nullable Term fromInst, @Nullable Term toInst) {
        return subRangeReplace(from, subs(), fromInst, toInst);
    }

    /**
     * match a range of subterms of Y.
     * if replacing, fromInst matches by instance,
     * or NEGATED instance.  negations preserved in replacement
     *
     * WARNING: provides a shared (non-cloned) copy if the entire range is selected
     *
     * @param fromInst if a subterm is instance identical to fromInst, inline replaces with toInst
     */
    @Deprecated default Term[] subRangeReplace(int from, int to, @Nullable Term fromInst, @Nullable Term toInst) {
        int n = subs();
        if (from < 0) from = 0;
        if (to > n) to = n;

        if (from == 0 && to == n && fromInst == null) {
            return arrayShared();
        } else {

            int s = to - from;
            if (s == 0)
                return EmptyTermArray;
            else {
                Term[] y = new Term[s];
                int j = from;
                for (int i = 0; i < s; i++) {
                    Term x = sub(j++);
                    if (x == fromInst) x = toInst;
                    else if (x instanceof Neg && x.unneg() == fromInst) x = toInst.neg();
                    y[i] = x;
                }
                return y;
            }
        }
    }

    default int indexOf(Predicate<Term> p) {
        return indexOf(p, -1);
    }

    default int indexOf(Predicate<Term> p, int after) {
        int s = subs();
        Term prev = null;
        for (int i = after + 1; i < s; i++) {
            Term next = sub(i);
            if (prev != next) {
                if (p.test(next))
                    return i;
                prev = next;
            }
        }
        return -1;
    }

    default boolean BOOL(Predicate<? super Term> t, boolean andOrOr) {
        int s = subs();
        Term prev = null;
        for (int i = 0; i < s; i++) {
            Term next = sub(i);
            if (prev != next) {
                if (t.test(next) != andOrOr)
                    return !andOrOr;
                prev = next;
            }
        }
        return andOrOr;
    }

    default /* final */ boolean AND(Predicate<? super Term> t) {
        return BOOL(t, true);
    }

    default /* final */ boolean OR(Predicate<? super Term> t) {
        return BOOL(t, false);
    }
    default /* final */ boolean ORunneg(Predicate<? super Term> t) {
        return OR(x -> t.test(x.unneg()));
    }
    default /* final */ boolean ANDunneg(Predicate<? super Term> t) {
        return AND(x -> t.test(x.unneg()));
    }

    /**
     * warning: elides test for repeated subterm
     */
    default <X> boolean ORwith(BiPredicate<Term, X> p, X param) {
        int s = subs();
        Term prev = null;
        for (int i = 0; i < s; i++) {
            Term next = sub(i);
            if (prev != next) {
                if (p.test(next, param))
                    return true;
                prev = next;
            }
        }
        return false;
    }

//	default boolean OR(Predicate<Term> p) {
//		int s = subs();
//		Term prev = null;
//		for (int i = 0; i < s; i++) {
//			Term next = sub(i);
//			if (different(prev, next)) {
//				if (p.test(next))
//					return true;
//				prev = next;
//			}
//		}
//		return false;
//	}

    /**
     * warning: elides test for repeated subterm
     */
    default <X> boolean ANDwith(BiPredicate<Term, X> p, X param) {
        int s = subs();
        Term prev = null;
        for (int i = 0; i < s; i++) {
            Term next = sub(i);
            if (prev != next) {
                if (!p.test(next, param))
                    return false;
                prev = next;
            }
        }
        return true;
    }

    /**
     * visits each, incl repeats
     */
    default <X> boolean ANDwithOrdered(BiPredicate<Term, X> p, X param) {
        int s = subs();
        for (int i = 0; i < s; i++) {
            if (!p.test(sub(i), param))
                return false;
        }
        return true;
    }

    /**
     * must be overriden by any Compound subclasses
     */
    @Override
    default boolean boolRecurse(Predicate<Compound> inSuperCompound, BiPredicate<Term, Compound> whileTrue, Compound parent, boolean andOrOr) {
        if (this instanceof Term) {
            if (!inSuperCompound.test((Compound) this)) //skip
                return andOrOr;
            else if (whileTrue.test((Term) this, parent) != andOrOr) //short-circuit condition
                return !andOrOr;
        }

        return boolRecurseSubterms(inSuperCompound, whileTrue, compoundParent(parent), andOrOr);
    }

    default boolean boolRecurseSubterms(Predicate<Compound> inSuperCompound, BiPredicate<Term, Compound> whileTrue, Compound parent, boolean andOrOr) {
        //return AND(s -> s.ANDrecurse(aSuperCompoundMust, whileTrue, parent));
        int s = subs();
//        Term prev = null;
        for (int i = 0; i < s; i++) {
            Term next = sub(i);
//            if (prev != next) continue;
            if (next.boolRecurse(inSuperCompound, whileTrue, parent, andOrOr) != andOrOr)
                return !andOrOr;
//            prev = next;
        }
        return andOrOr;
    }

    private Compound compoundParent(Compound parent) {
        return this instanceof Compound ? ((Compound) this) : parent;
    }

    default /* final */ boolean ANDrecurse(Predicate<Compound> inSuperCompound, Predicate<Term> whileTrue, Compound parent) {
        return boolRecurse(inSuperCompound, (x, p) -> whileTrue.test(x), compoundParent(parent), true);
    }

    /**
     * incl repeats
     */
    default boolean recurseTermsOrdered(Predicate<Term> inSuperCompound, Predicate<Term> whileTrue, Compound parent) {
        if (this instanceof Compound c) {
            if (!inSuperCompound.test(c))
                return true;
            if (!whileTrue.test(c))
                return false;
        }

        parent = compoundParent(parent);

        int s = subs();
        for (int i = 0; i < s; i++)
            if (!sub(i).recurseTermsOrdered(inSuperCompound, whileTrue, parent))
                return false;
        return true;
    }

    default Subterms reversed() {
        return ReversedSubterms.reverse(this);
    }

    /**
     * removes first occurrence only
     */
    default Term[] removing(int index) {
        int s = subs();
        Term[] x = new Term[s - 1];
        int k = 0;
        for (int j = 0; j < s; j++) {
            if (j != index)
                x[k++] = sub(j);
        }
        return x;

        //return ArrayUtils.remove(arrayShared(), Term[]::new, i);
    }

//	default @Nullable Term[] removing(Term x) {
//		if (impossibleSubTerm(x))
//			return null;
//		MetalBitSet toRemove = indicesOfBits(x::equals);
//		return toRemove.cardinality() == 0 ? null : removing(toRemove);
//	}

    default void copyTo(Term[] y, int start, int end) {
        for (int i = start; i < end; i++)
            y[i] = sub(i);
    }


//    /**
//     * dont override
//     */
//    default Subterms replaceSub(Term from, Term to) {
//        return !from.equals(to) && !impossibleSubTerm(from) ? transformSubs(MapSubst.replace(from, to), ATOM) : this;
//    }

//	default Subterms transformSub(int which, UnaryOperator<Term> f) {
//		Term x = sub(which);
//		Term y = f.apply(x);
//		if (!differentlyTransformed(x, y))
//			return this;
//
//		Term[] yy = arrayClone();
//		yy[which] = y;
//		return Op.terms.subterms(yy);
//	}

    /**
     * whether all subterms are internable
     */
    default boolean internables() {
        //return AND(Term::internable);
        // iterate in reverse so it can potentially fail faster since smaller terms are more likely to be at the end, at least when commutive
        int n = subs();
        for (int i = n - 1; i >= 0; i--)
            if (!sub(i).internable()) return false;
        return true;
    }

    default Predicate<Term> contains(boolean pn, boolean preFilter) {
        Predicate<Term> equality;
        switch (subs()) {
            case 0 -> equality = x -> false;
            case 1 -> {
                Term s0 = sub(0);
                equality = pn ? s0::equalsPN : s0.equals();
            }
            default -> equality = pn ? this::containsPN : this::contains;
        }
        if (preFilter) {
            int is = structureSubs();
            Predicate<Term> r = ((Predicate<Term>) x -> Op.hasAll(is, x.structure())).and(equality);
            return pn ? r.and(x -> r.test(x.unneg())) : r;
        } else
            return equality;
    }




    /**
     * (first-level only, non-recursive)
     * if contained within; doesnt match this target (if it's a target);
     * false if target is atomic since it can contain nothing
     */
    default boolean contains(Term x) {
        return indexOf(x) != -1;
    }

    default boolean containsInstance(Term t) {
        return ORwith((i, T) -> i == T, t);
    }

    default boolean containsInstancePN(Term t) {
        return ORwith((i, T) -> i.unneg() == T, t.unneg());
    }

    default boolean containsNeg(Term x) {
        return x instanceof Neg ?
            contains(x.unneg())
            :
            _containsNeg(x);
    }

    private boolean _containsNeg(Term x) {
        return (!(this instanceof TermMetadata) || hasAny(NEG)) //!impossibleSubStructure(x.structure() | NEG.bit) && !impossibleSubVolume(x.volume() + 1)
            &&
            ORwith((z, xu) -> z instanceof Neg && xu.equals(z.unneg()), x);
    }

    /**
     * tries both polarities
     */
    default boolean containsPN(Term x) {
        //TODO optimize

        //simple impl:
        //return contains(x) || containsNeg(x);

        Term xu = x.unneg();
        return !impossibleSubTerm(xu) && ORwith((y, XU) -> XU.equals(y.unneg()), xu);
    }



    default int volume() {
        //return 1 + sum(Termlike::volume);
        int s = subs(), v = 1;
        for (int i = 0; i < s; i++)
            v += sub(i).volume();
        return v;
    }

    default int height() {
        return subs() == 0 ? 1 : 1 + max(Termlike::height);
    }

//	default int vars(int varBits) {
//		int t = 0;
//		if ((varBits & VAR_PATTERN.bit)!=0) t += varPattern();
//		if ((varBits & VAR_QUERY.bit)!=0) t += varQuery();
//		if ((varBits & VAR_INDEP.bit)!=0) t += varIndep();
//		if ((varBits & VAR_DEP.bit)!=0) t += varDep();
//		return t;
//	}

    default int varDep() {
        return sum(Term::varDep);
    }

    default int varIndep() {
        return sum(Term::varIndep);
    }

    default int varQuery() {
        return sum(Term::varQuery);
    }

    default int varPattern() {
        return sum(Term::varPattern);
    }

    default void write(ByteArrayDataOutput out) {
        int n = subs(); //assert(n < Byte.MAX_VALUE);
        out.writeByte(n);
        for (int i = 0; i < n; i++)
            sub(i).write(out);
    }

//    /** for use as CONJ subterms */
//    default int eventCount() {
//        if (this instanceof EventAtomic)
//            return 0;
//        return Op.hasAny(structureSubs(), CONJ) ? new EventCounter(this).c : subs();
//    }

    /** TODO protected */
    default boolean condOfInh(Subterms xu, int polarity) {
        if (this instanceof CondAtomic)
            return false;
        Term c0 = sub(0), c1 = sub(1);
        boolean c0c = c0.CONJ(), c1c = c1.CONJ();
        return (c0c || c1c) && condOfInh(xu, polarity, c0, c1, c0c, c1c);
    }

    private static boolean condOfInh(Subterms xu, int polarity, Term c0, Term c1, boolean c0c, boolean c1c) {

        Term e0 = xu.sub(0), e1 = xu.sub(1);
        if (c0c && c1c) {
            return switch(polarity) {
                case +1 -> condOfInhN(c0, c1, e0, e1);
                case -1 -> condOfInhNNeg(c0, c1, e0, e1);
                case  0 -> condOfInhN(c0, c1, e0, e1) || condOfInhN(c0, c1, e0.neg(), e1.neg()) || condOfInhNNeg(c0, c1, e0, e1);
                default -> throw new UnsupportedOperationException();
            };
        } else if (c1c)
            return c0.equals(e0) && ((Compound) c1).condOfConj(e1, polarity);
        else if (c0c)
            return c1.equals(e1) && ((Compound) c0).condOfConj(e0, polarity);
        else
            return false;
    }

    private static boolean condOfInhNNeg(Term c0, Term c1, Term e0, Term e1) {
        return condOfInhN(c0, c1, e0.neg(), e1) || condOfInhN(c0, c1, e0, e1.neg());
    }

    private static boolean condOfInhN(Term c0, Term c1, Term e0, Term e1) {
        return (c0.equals(e0) || ((Compound) c0).condOfConj(e0, +1))
               &&
               (c1.equals(e1) || ((Compound) c1).condOfConj(e1, +1));
    }

    default boolean hasSeq() {
        return !(this instanceof CondAtomic) && ORunneg(Term::SEQ);
    }

//    final class EventCounter implements Consumer<Term> {
//        private int c;
//
//        EventCounter(Subterms x) {
//            if (x instanceof Compound)
//                accept((Compound)x);
//            else
//                x.forEach(this);
//        }
//
//
//        @Override
//        public void accept(Term x) {
//            if (x instanceof Neg)
//                x = x.unneg();
//
//            if (x.CONJ()) {
//                ((Compound)x).events(this, x.dt()==DTERNAL, x.dt()==XTERNAL);
//            } else
//                c++;
//        }
//    }


//    default boolean identical(Subterms x) {
//		if (this == x) return true;
//		int s = subs(); if (x.subs()!=s) return false;
//		for (int i = 0; i < s; i++) {
//			if (sub(i)!=x.sub(i)) return false;
//		}
//		return true;
//	}


    final class SubtermsIterator implements Iterator<Term> {
        private final int s;
        private final Subterms terms;
        int i;

        private SubtermsIterator(Subterms terms) {
            this.terms = terms;
            this.s = terms.subs();
        }

        @Override
        public boolean hasNext() {
            return i < s;
        }

        @Override
        public Term next() {
            return terms.sub(i++);
        }
    }

}