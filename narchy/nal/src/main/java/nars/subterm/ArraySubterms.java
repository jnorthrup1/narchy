package nars.subterm;

import jcog.data.iterator.ArrayIterator;
import nars.Term;

import java.util.Iterator;

/**
 * Holds a vector or tuple of terms.
 * Useful for storing a fixed number of subterms
 */
public class ArraySubterms extends CachedSubterms {

    /*@NotNull*/
    /*@Stable*/
    private final Term[] terms;

    public ArraySubterms(Term... terms) {
        super(terms);
        this.terms = terms;
    }

    @Override
    public final String toString() {
        return Subterms.toString(terms);
    }

    @Override
    public final Term sub(int i) {
        return terms[i];
    }

    @Override
    public final Term[] arrayClone() {
        return terms.clone();
    }

    @Override
    public final Term[] arrayShared() {
        return terms;
    }

    @Override
    public final int subs() {
        return terms.length;
    }

    @Override
    public final Iterator<Term> iterator() {
        return ArrayIterator.iterate(terms);
    }

}