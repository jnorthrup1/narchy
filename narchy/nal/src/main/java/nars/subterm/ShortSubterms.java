package nars.subterm;

import jcog.math.NumberException;
import nars.Op;
import nars.Term;
import nars.subterm.util.SubtermMetadataCollector;
import nars.term.CondAtomic;
import nars.term.atom.Int;
import org.eclipse.collections.api.block.procedure.primitive.ShortProcedure;
import org.roaringbitmap.PeekableIntIterator;
import org.roaringbitmap.RoaringBitmap;

import java.util.Arrays;

/** Subterms implementation that holds Int's , specified by short[] */
public class ShortSubterms extends CachedSubterms implements CondAtomic {

	public final short[] subs;

	private ShortSubterms(short[] subs, SubtermMetadataCollector meta) {
		super(meta);
		this.subs = subs;
		internableKnown = internables = true;
		normalizedKnown = normalized = true;
	}



	public static Subterms the(short[] x) {
		if (x.length == 0) return Op.EmptySubterms;
		SubtermMetadataCollector c = new SubtermMetadataCollector();
		for (short i : x) c.add(Int.the(i));
		return new ShortSubterms(x, c);
	}

	/** warning: short range not checked */
	public static Subterms the(int[] x) {
		int s = x.length;
		if (s == 0) return Op.EmptySubterms;
		SubtermMetadataCollector c = new SubtermMetadataCollector();
		short[] y = new short[s];
		for (int j = 0, xLength = x.length; j < xLength; j++) c.add(Int.the(y[j] = (short) x[j]));
		return new ShortSubterms(y, c);
	}
	public static Subterms the(byte[] x) {
		int s = x.length;
		if (s == 0) return Op.EmptySubterms;
		SubtermMetadataCollector c = new SubtermMetadataCollector();
		short[] y = new short[s];
		for (int j = 0; j < s; j++) c.add(Int.the(y[j] = x[j]));
		return new ShortSubterms(y, c);
	}

	/** warning: doesnt guarantee value is within Short's capacity */
	public static Subterms the(RoaringBitmap x) {
		int s = x.getCardinality();
		if (s == 0) return Op.EmptySubterms;
		SubtermMetadataCollector c = new SubtermMetadataCollector();
		short[] y = new short[s];
		PeekableIntIterator ii = x.getIntIterator();
		for (int j = 0; j < s; j++) c.add(Int.the(y[j] = validShort(ii.next())));
		return new ShortSubterms(y, c);
	}

	private static short validShort(int i) {
		if (i > Short.MAX_VALUE || i < Short.MIN_VALUE) throw new NumberException("exceeds Short range", i);
		return (short)i;
	}

	@Override
	public final Term sub(int i) {
		return Int.the(subs[i]);
	}

	@Override
	public final int subs() {
		return subs.length;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof ShortSubterms oo) {
			return hash == oo.hash && Arrays.equals(subs, oo.subs);
		} else
			return obj instanceof Subterms && equalTerms((Subterms)obj);
	}

	public void addAllTo(ShortProcedure each) {
		for (short x : subs) each.value(x);
	}
}