package nars.task.proxy;

import nars.Term;
import nars.task.NALTask;
import nars.task.ProxyTask;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

/** term and truth value are inverted, so the semantics remain the same */
public final class SpecialNegTask extends ProxyTask {

    public SpecialNegTask(NALTask x) {
        super(x);
        assert(!(x instanceof SpecialNegTask) && x.BELIEF_OR_GOAL());
        copyMeta(x);
    }

    @Override
    public Term term() {
        return super.term().neg();
    }

    @Override
    public @Nullable Truth truth() {
        return super.truth().neg();
    }

}