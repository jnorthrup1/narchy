package nars.task.proxy;

import nars.NAL;
import nars.task.NALTask;
import nars.task.ProxyTask;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class SpecialTruthTask extends ProxyTask {

	private final Truth truth;

	SpecialTruthTask(NALTask task, Truth truth) {
		super(task instanceof SpecialTruthTask ? ((SpecialTruthTask)task).task : task);
		this.truth = truth;
	}

	public static NALTask proxy(NALTask t, @Nullable Truth tr, NAL nal) {
		if (tr.equals(t.truth(), nal))
			return t;
		if (t instanceof SpecialTruthTask) {
			t = ((SpecialTruthTask)t).task;
			if (Objects.equals(t.truth(), tr)) return t; //check again
		}
		return new SpecialTruthTask(t, tr);
	}

	@Override
	public @Nullable Truth truth() {
		return truth;
	}

}