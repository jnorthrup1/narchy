package nars.task.util;

import jcog.signal.meter.SafeAutoCloseable;
import nars.task.NALTask;
import nars.truth.Stamp;

import java.util.function.Predicate;

import static jcog.math.LongInterval.ETERNAL;
import static jcog.math.LongInterval.intersectsRaw;

public final class StampOverlapping implements Predicate<NALTask>, SafeAutoCloseable {

    private long[] xStamp;
    private long xs, xe;

    public StampOverlapping() {

    }

    public StampOverlapping set(NALTask n) {
        long xs = n.start();
        return set(n.stamp(), xs, xs == ETERNAL ? ETERNAL : n.end());
    }

    private StampOverlapping set(long[] xStamp, long xs, long xe) {
        this.xs = xs;
        this.xe = xe;
        this.xStamp = xStamp;
        return this;
    }

    @Override
    public boolean test(NALTask y) {
        if (Stamp.overlapsAny(xStamp, y.stamp())) {
            if (xs == ETERNAL) return true;
            long ys = y.start();
            if (ys == ETERNAL) return true;
            long ye = y.end();
            return intersectsRaw(xs, xe, ys, ye);
        }
        return false;

    }

    @Override public void close() {
        xStamp = null;
    }
}