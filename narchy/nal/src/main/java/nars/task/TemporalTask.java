package nars.task;

import nars.Term;
import nars.task.util.TaskException;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;


/** generic NAL Task with stored start,end time */
public class TemporalTask extends AbstractNALTask {

	private final long start, end;

    protected TemporalTask(Term term, byte punc, @Nullable Truth truth, long start, long end, long[] stamp) throws TaskException {
		super(term, punc, _truth(truth), start, end, stamp);

//        if (punc==BELIEF && term.IMPL() && term.dt()==DTERNAL) {
//            Util.nop();
//        }

//		if (start!=ETERNAL && start!=TIMELESS && start%10!=0)
//		    throw new WTF(); //TEMPORARY

//        if (end-start > 40_000)
//            Util.nop(); //TEMPORARY

		this.start = start;
        this.end = end;
    }

    @Override
    public final long start() {
        return start;
    }

    @Override
    public final long end() {
        return end;
    }

}