package nars.task;

import nars.Task;
import nars.Term;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

/**
 * base class for concrete immutable on-heap NAL Task implementation,
 * with mutable cause[] and initially empty meta table
 */
public abstract class AbstractNALTask extends NALTask {

    /*@Stable*/  /*final*/ long[] stamp;

    private final Term term;
    private final Truth truth;
    private final byte punc;
    private final int hash;

    protected AbstractNALTask(Term term, byte punc, @Nullable Truth truth, long start, long end, long[] stamp) {
        super();

        this.stamp = stamp;
        this.term = term;
        this.truth = truth;
        this.punc = punc;
        this.hash = hashCalculate(start, end, stamp); //must be last
    }

    @Nullable
    static Truth _truth(@Nullable Truth t) {
        return t instanceof MutableTruth ? ((MutableTruth) t).immutableUnsafe() : t;
    }


    protected int hashCalculate(long start, long end, long[] stamp) {
        return Task.hash(
                term,
                truth,
                punc,
                start, end, stamp);
    }

    @Override
    public final int hashCode() {
        return hash;
    }

    @Override
    public final long[] stamp() {
        return stamp;
    }


    @Override
    public final @Nullable Truth truth() {
        return truth;
    }

    @Override
    public byte punc() {
        return punc;
    }

    @Override
    public Term term() {
        return term;
    }

    @Override
    public String toString() {
        return appendTo(null).toString();
    }

}