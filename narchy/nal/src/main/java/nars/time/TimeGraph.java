package nars.time;

import jcog.TODO;
import jcog.Util;
import jcog.WTF;
import jcog.data.graph.MapNodeGraph;
import jcog.data.graph.MutableNode;
import jcog.data.graph.Node;
import jcog.data.graph.edge.LazyMutableDirectedEdge;
import jcog.data.graph.path.FromTo;
import jcog.data.graph.search.Search;
import jcog.data.iterator.CartesianIterator;
import jcog.data.iterator.Concaterator;
import jcog.data.list.Lst;
import jcog.data.map.UnifriedMap;
import jcog.data.set.ArrayHashSet;
import jcog.data.set.ArrayUnenforcedSet;
import jcog.random.RandomBits;
import jcog.util.ArrayUtil;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.util.builder.TermBuilder;
import nars.term.util.conj.*;
import nars.term.var.CommonVariable;
import org.eclipse.collections.api.block.function.primitive.IntFunction;
import org.eclipse.collections.api.block.procedure.primitive.LongObjectProcedure;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.api.tuple.primitive.BooleanObjectPair;
import org.eclipse.collections.api.tuple.primitive.LongObjectPair;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.lang.Math.min;
import static java.util.Collections.EMPTY_MAP;
import static java.util.Collections.EMPTY_SET;
import static jcog.Util.emptyIterable;
import static jcog.Util.hashCombine;
import static nars.Op.*;
import static nars.time.TimeSpan.TS_ZERO;
import static org.eclipse.collections.impl.tuple.Tuples.pair;


/**
 * represents a multigraph of events and their relationships
 * calculates unknown times by choosing from the possible
 * pathfinding results.
 * <p>
 * it can be used in various contexts:
 * a) the tasks involved in a derivation
 * b) as a general purpose temporal index, ie. as a meta-layer
 * attached to one or more concept belief tables
 * <p>
 * DTERNAL relationships can be maintained separate
 * from +0.
 * <p>
 * TODO
 * subclass of MapNodeGraph which allows tagging edges by an ID.  then index
 * the edges by these keys so they can be efficiently iterated and removed by tag.
 * <p>
 * then use it to store the edges in categories:
 * task time edges
 * belief time edges
 * conclusion time edges
 * autonegations
 * hypothesized (anything created during search)
 * etc.
 * <p>
 * then the premise can clear the conclusion edges while retaining task, belief edges throughout
 * each separate derivation of a premise.
 * also the tags can be used to heuristically bias the search via associated weight.  so autonegation
 * can be weighted less allowing preferential traversal of concrete edges in oscillating pos/neg event pathways.
 */
public abstract class TimeGraph extends MapNodeGraph<TimeGraph.Event, TimeSpan> {

    private static final int ABSOLUTE_PAIR_SOURCE_TRY = 2;
    private static final int SubXternal_SUBSOLUTIONS_TRY = 1;
    private static final int RECURSION_MAX = 5;

    /**
     * scan non-temporal events recursively for contained temporal terms
     */
    private static final boolean decomposeDeep = true;

    private static final long SAFETY_PAD = 32 * 1024;
    private static final IntFunction<Event> AbsolutesFirst = x -> x instanceof Absolute ? -1 : +1;
    //new HashMap<>(0);
    private static final IntFunction<FromTo<Node<Event, TimeSpan>, TimeSpan>> AbsoluteLinksFirst =
            x -> -(
                    (x.from().id() instanceof Absolute ? +1 : 0)
                            +
                            (x.to().id() instanceof Absolute ? +1 : 0));
    public final ArrayHashSet<Event> solutions = new ArrayHashSet<>(0);
    @Deprecated static public final transient boolean decomposeEvents = true;
    protected final RandomBits rng;
    private final Map<Term, Set<Event>> events = new UnifriedMap<>(0);
    @Deprecated
    public transient boolean autoNeg = true;
    /**
     * current solution term template
     */
    protected transient Term target;
    /**
     * whether to solve occ, or settle for non-xternal only
     */
    private transient boolean solveOcc = true;
    /**
     * current recursion level
     */
    private transient int depth;
//    private transient Map<Term, BiConsumer<Term, TimeGraph>> lazy;


    protected TimeGraph(Random rng) {
        this(new RandomBits(rng));
    }

    protected TimeGraph(RandomBits rng) {
        super();
        this.rng = rng;
    }

    private static boolean termsEvent(Term e) {
        return e != null && e.CONDABLE();
    }

//    private static Compound _inh(Term subj, Term pred) {
//        return CachedCompound.the(INH, new BiSubterm(subj, pred));
//    }

    private static Term dtConj(Compound x, boolean dir, int dt) {
        return dtConj(x, dir, dt, terms);
    }

    private static Term dtConj(Compound x, boolean dir, int dt, TermBuilder B) {
        Subterms xx = x.subterms();
        Term xEarly, xLate;
        if (x.dt() == XTERNAL) {


            //use the provided 'path' and 'dir'ection, if non-null, to correctly order the sequence, which may be length>2 subterms
            Term x0 = xx.sub(0), x1 = xx.sub(1);

            if (dir) {
                xEarly = x0;
                xLate = x1;
            } else {
                xEarly = x1;
                xLate = x0;
                dt = -dt;
            }


        } else {

            int early = Conj.conjEarlyLate(x, true);
            if (early == 1)
                dt = -dt;

            xEarly = xx.sub(early);
            xLate = xx.sub(1 - early);
        }

        return ConjSeq.conjAppend(xEarly, dt, xLate, false, B);
    }

    private static long shift(long w, int dur, boolean fwd) {
        return w != ETERNAL && dur != XTERNAL ? w + (fwd ? dur : -dur) : w;
    }

    @Override
    public void clear() {
        super.clear();
        solutions.clear();
        events.clear();
//        if (lazy != null) {
//            lazy.clear();
//            lazy = null;
//        }
        target = null;
    }

    public final Relative know(Term v) {
        return (Relative) know(v, false);
    }

    public final Event know(Event e) {
        return e instanceof Absolute ? know(e.id, e.start(), e.end()) : know(e.id);
    }

    @Nullable
    public final Event know(Term v, boolean weak) {
        return know(v, weak, false);
    }

    private void known(Term v, boolean weak) {
        if (v.dt() == XTERNAL) {
            //dont add, but decompose in case components have specific timing
            decompose(new Relative(v));
        } else {
            know(v, weak, true);
        }
    }

    @Nullable
    private Event know(Term v, boolean weak, boolean async) {
        Collection<Event> existing = events(v);
        if (existing != null) {
            int s = existing.size();
            if (s > 0) {
                int numAbs = Util.count(x -> x instanceof Absolute, existing);
                if (weak && numAbs == 1) {
                    if (async)
                        return null;
                    @Nullable Event onlyAbs = Util.first(x -> x instanceof Absolute, existing);
                    if (onlyAbs != null)
                        return onlyAbs;
                }
                if (numAbs < s) {
                    Event onlyRel = Util.first(x -> x instanceof Relative, existing);
                    if (onlyRel != null)
                        return onlyRel; //fast lookup, dont need to re-recurse

                    //all absolute, so unless async, add a relative to tie them together
                    if (async)
                        return null; //dont need a result
                }
            }
        }
        return know(v, TIMELESS);
    }
//    public final Event knowShallow(Term t) {
//        return event(t, TIMELESS, TIMELESS, true, false);
//    }

    public final Event know(Term t, long start) {
        return know(t, start, start);
    }

    public final Event know(Term t, long start, long end) {
        return know(t, start, end, true);
    }

    public final Event know(Term t, long start, long end, boolean decompose) {
        return event(t, start, end, true, decompose);
    }

    private Event event(Term t, long start, long end, boolean add, boolean decompose) {

        Event event;
        if (start == TIMELESS) {
            event = new Relative(t);
        } else {
            //optional dithering
            event = absolute(t, start, end);
        }

        if (add) {
            Event added = addNode(event).id;
            if (added == event) {
                if (events.computeIfAbsent(event.id, e ->
                        //new UnifiedSet()
                        new ArrayUnenforcedSet<>(Event.EmptyEventArray)).add(event)) {

                    if (decompose)
                        decompose(event);
                }
            }
            return added;
        }
        return event;
    }

    protected Event absolute(Term t, long when) {
        return new Absolute(t, eventOcc(when));
    }

    protected Event absolute(Term t, long start, long end) {
        start = eventOcc(start);
        end = eventOcc(end);
        return (end == start) ? new Absolute(t, start) : new AbsoluteRange(t, start, end);
    }

    private @Nullable Collection<Event> events(Term t) {

        return _events(t);
    }

    public Set<Event> _events(Term t) {
        return events.get(t);
    }

    private boolean link(Event before, TimeSpan e, Event after) {

        MutableNode<Event, TimeSpan> x = addNode(before);
        MutableNode<Event, TimeSpan> y = before.equals(after) ? x : addNode(after);

        return addEdgeByNode(x, e, y);
    }

    public boolean link(Event x, long dt, Event y) {

        boolean parallel = dt == ETERNAL || dt == TIMELESS || dt == 0;
        int vc = x.compareTo(y);
        if (vc == 0) { //equal?
            if (parallel)
                return false;
            y = x; //use same instance, they could differ
            if (dt < 0)
                dt = -dt; //use only positive dt values for self loops
        } else {
            if (vc > 0) {
                if (!parallel)
                    dt = -dt;

                Event z = x;
                x = y;
                y = z;
            }
        }

        return link(x, TimeSpan.the(dt), y);
    }

    @Deprecated
    private void decompose(Event X) {

        if (!decomposeAddedEvent(X))
            return;

        Term x = X.id;

        if (x instanceof CommonVariable c) {

            //TODO dynamic
            for (Term v : c.common())
                simultaneous(X, know(v)); //equivalence

        } else {


            switch (x.op()) {
                case NEG -> {
                    if (autoNeg) {
                        simultaneous(
                                X instanceof Relative ? X : know(x), know(x.unneg())
                        );
                    } else {
                        know(x.unneg());
                    }
                }
                default -> {
                    if (decomposeDeep) {
                        x.ANDrecurse(
                                t -> !t.INH() && !t.SIM() && t.TEMPORALABLE(), s -> {
                                    if (s.TEMPORAL())
                                        known(s, false); /* absolute context for temporal subterm of non-temporal */
                                });
                    }
                }
                case INH -> {
                    if (ConjBundle.bundled(x)) {
                        ConjBundle.events(x, s -> simultaneous(X, know(s)));
                    }
                }
                case IMPL -> {
                    int edt = x.dt();
                    Term subj = x.sub(0), pred = x.sub(1);
                    Event se = know(subj), pe = know(pred);
                    if (edt == XTERNAL) {
                        /* without any known relative timing */
                    } else {
                        int dt = (edt == DTERNAL ? 0 : edt) + subj.seqDur();
                        link(se, dt, pe);

//                        //cross-linkages
//                        if (subj.unneg().CONJ() && subj.dt()==DTERNAL) {
//                            for (Term sx : subj.unneg().subtermsDirect())
//                                link(know(sx), dt, pe);
//                        }
//                        if (pred.CONJ() && pred.dt()==DTERNAL) {
//                            for (Term px : pred.subtermsDirect())
//                                link(se, dt, know(px));
//                        }
                    }
                }
                case CONJ -> {

                    int edt = x.dt();
                    if (edt == XTERNAL) {

                        ((Subterms) x).forEach(this::know); //floating, with some potential temporal information

                    } else {
                        Compound c = (Compound) x;

                        LongObjectProcedure<Term> ee;
                        long cStart;
                        long eventStart = X.start();
                        if (eventStart == TIMELESS) {
                            //chain the events to the parent event
                            //ee = (w, y) -> link(event, w, know(y));

                            ee = new SequenceChainer(X);
                            cStart = 0; //N/A
                        } else if (eventStart == ETERNAL) {
                            //in eternity
                            ee = (w, y) -> simultaneous(X, know(y));
                            cStart = 0; //N/A
                        } else {
                            //SEQUENCE

                            //chain individual events absolutely:
                            int dur = X.dur();
                            cStart = eventStart;
                            ee = (w, y) -> know(y, w, w + dur);

                            //chain events in sequence: (NOT WORKING)
                            //cStart = 0;
                            //ee = new SequenceChainer(X);
                        }

                        c.events(ee/*(w, cc) -> {
                            if (cc.EVENTABLE()) ee.value(w, cc); //HACK
                        }*/, cStart, edt == DTERNAL, false);
                    }
                }
            }
        }
    }

    @Nullable
    public final Event knowWeak(Term x) {
        return know(x, true);
    }

    protected final boolean simultaneous(Event a, Event b) {
        return link(a, 0, b);
    }

    private static boolean decomposeAddedEvent(Event event) {
        return decomposeEvents && event.id instanceof Compound;
    }

    private boolean solveXternal(Compound x, Predicate<Event> each) {

        Subterms xx = x.subterms();
        int s = xx.subs();

        if (x.CONJ()) {
            List<Absolute>[] subEvents = new Lst[s];
            int eventsAbsolute = solveAbsolutes(xx, subEvents);

            if (eventsAbsolute >= 1 && s >= 2) {
                if (!solveConjPermuteAbsolutes(xx, subEvents, each))
                    return false;
            }
        }


        if (s == 2)
            return solveDT_2(x, each);

        return true;

//        if (!termsEvent(x))
//            return true; //??
//
//        assert (x.dt() == XTERNAL);
//
//        Subterms xx = x.subterms();
//        int s = xx.subs();
//
//        if (x.CONJ()) {
//            List<Absolute>[] subEvents = new Lst[s];
//            int eventsAbsolute = solveAbsolutes(xx, subEvents);
//
//            if (eventsAbsolute >= 1 && s >= 2) {
//                if (!solveConjPermuteAbsolutes(xx, subEvents, each))
//                    return false;
//            }
//        }
//

    }

//    /**
//     * first result that matches the filter
//     */
//    private Term solveTerm(Compound x, Predicate<Compound> filter) {
//        if (x instanceof Neg) {
//            Term xu = x.unneg();
//            if (!(xu instanceof Compound)) return x; //no change
//            return solveTerm((Compound) xu, filter).neg();
//        }
//
//
//        Term[] result = {Bool.Null};
//        solveXternal(x, (Y -> {
//            Term y = Y.id;
//            if (y instanceof Compound && !y.equals(x) && filter.test((Compound) y)) {
//                result[0] = y;
//                return false;
//            }
//            return true;
//        }));
//        return result[0];
//    }

    private boolean solveDT_2(Compound x, Predicate<Event> each) {
        Subterms xx = x.subterms();
        Term a = xx.sub(0), b = xx.sub(1);
        return solveDTAbsolutePair(x, a, b, each) && solveDTpair(x, a, b, each);
    }

    private int solveAbsolutes(Subterms xx, List<Absolute>[] subEvents) {
        int abs = 0;
        int s = subEvents.length;
        Lst<Absolute> f = new Lst<>(s);
        Predicate<Absolute> adder = f::add;

        for (int i = 0; i < s; i++) {
            solveExact(xx.sub(i), adder);

            int fs = f.size();
            Lst<Absolute> ff;
            if (fs > 0) {
                ff = f.clone(); //TODO keep Absolute[], not List<Absolute>
                f.clear();
                if (fs > 1)
                    ff.shuffleThis(rng);
                abs++;
            } else
                ff = null;

            subEvents[i] = ff;
        }
        return abs;
    }

    private boolean solveConjPermuteAbsolutes(Subterms x, List<Absolute>[] subEvents, Predicate<Event> each) {

        int n = x.subs();

        ConjBuilder cc = null;
        //new ConjTree();

        TermList unknowns = null;

        CartesianIterator<Event> ci = new CartesianIterator<>(Event[]::new, subEvents) {
            @Override
            protected Iterable<? extends Event> iterableNull(int i) {
                return new Lst<>(new Event[]{new Relative(x.sub(i))});
            }
        };

        nextPermute:
        while (ci.hasNext()) {

            Event[] ss = ci.next();

            if (cc != null) cc.clear();

            if (unknowns != null) unknowns.clear();

            long dur = Long.MAX_VALUE, start = Long.MAX_VALUE;
            for (int i = 0; i < n; i++) {
                Event e = ss[i];

                if (!(e instanceof Absolute)) {
                    if (n == 2)
                        return true; //>=1 unknown, <=1 known; nothing to be gained from continuing

                    if (unknowns == null) unknowns = new TermList(1);
                    unknowns.addIfNotPresent(e.id);
                    continue;
                }

                dur = min(e.dur(), dur);

                long es = e.start();
                if (start == TIMELESS || start == ETERNAL)
                    start = es;
                else if (es != ETERNAL) {
                    //override with specific temporal
                    start = min(start, es); //start == ETERNAL ? es : Math.min(es, start);
                }

                if (cc == null) cc = new ConjList(n);
                if (!cc.add(es, e.id))
                    continue nextPermute;
            }

            if (cc == null)
                continue;

            Term y = cc.term();

            if (!termsEvent(y))
                continue;

            if (unknowns != null) {
                y = CONJ.the(XTERNAL, y, unknowns.size() > 1 ? CONJ.the(XTERNAL, (Subterms) unknowns) : unknowns.sub(0));
                if (!termsEvent(y))
                    continue;
            }

            if (!each.test(shadow(y, start, start != TIMELESS && start != ETERNAL && dur != XTERNAL ? start + dur : start)))
                return false;
        }

        return true;
    }

    private boolean solveDTpair(Compound x, Term a, Term b, Predicate<Event> each) {


        boolean aEqB = a.equals(b);
        Collection<Event> aa = events(a);

        Collection<Event> ab;
        if (aEqB) {
            ab = aa;
        } else {
            Collection<Event> bb = events(b);
            if (bb != null) {
                //the smaller set, TODO if tie: simpler term
                ab = (aa == null) ? bb : ((aa.size() <= bb.size()) ? aa : bb);
            } else {
                ab = aa;
            }
        }

        if (ab == null) return true; //nothing

        return bfs(shuffle(ab, true), new DTPairSolver(a, b, ab == aa, x, each));
    }

    private boolean solveDTAbsolutePair(Compound x, Term a, Term b, Predicate<Event> each) {
        UnifiedSet<Event> ae = occurrences(a, ABSOLUTE_PAIR_SOURCE_TRY);
        if (ae == null)
            return true;

        Event[] aa = eventArray(ae);
        int aes = aa.length;

        boolean aEqB = a.equals(b);
        if (aEqB) {


            if (aes > 1) {
                //IMPL must be tried both directions since it isnt commutive
                boolean bidi = switch (x.op()) {
                    case IMPL -> true;
                    case CONJ -> false;
                    default -> throw new TODO(); //??
                };

                int n = aa.length;
                for (int i = 0; i < n; i++) {
                    Event ii = aa[i];
                    for (int j = bidi ? 0 : i + 1; j < n; j++) {
                        if (i == j) continue;
                        if (!solveDTAbsolutePair(x, ii, aa[j], each))
                            return false;
                    }
                }
            }

            return true;

        } else {
            ae.clear();
            return solveOcc(b, bx -> {
                if ((bx instanceof Absolute) && ae.add(bx)) {
                    for (Event ax : aa) {
                        if (ax != bx && !solveDTAbsolutePair(x, ax, bx, each))
                            return false;
                    }
                }
                return true;
            });

        }
    }

    /**
     * samples occurrences
     */
    private UnifiedSet<Event> occurrences(Term a, int limit) {

        UnifiedSet<Event> ae = new UnifiedSet<>(0);

        solveOcc(a, ax ->
                !(ax instanceof Absolute) || !ae.add(ax) || ae.size() < limit);

        return ae.isEmpty() ? null : ae;
    }

    private Event[] eventArray(UnifiedSet<Event> ae) {
        Event[] aa = ae.toArray(Event.EmptyEventArray);
        if (aa.length > 1) ArrayUtil.shuffle(aa, rng);
        return aa;
    }

    private boolean solveDTAbsolutePair(Compound x, Event a, Event b, Predicate<Event> each) {

        boolean conj = x.CONJ();
        if (conj) {
            //swap to correct sequence order
            if (a.start() > b.start()) {
                Event z = a;
                a = b;
                b = z;
            }
        }
//        assert (!a.equals(b));

        long aWhen = a.start(), bWhen = b.start(), when = TIMELESS;
        int dt;
        if (aWhen == ETERNAL || bWhen == ETERNAL) {
            dt = 0;
            when = aWhen == ETERNAL ? bWhen : aWhen;
        } else {
            if (conj) {
                assert (aWhen != TIMELESS && bWhen != TIMELESS);
                dt = occToDT(bWhen - aWhen);
                when = aWhen;
            } else {
                dt = occToDT(bWhen - aWhen - a.id.seqDur());
            }
        }


        int dur = min(a.dur(), b.dur());

        //for impl and other types cant assume occurrence corresponds with subject
        if (conj)
            return solveOcc(
                    ConjSeq.conjAppend(a.id, dt, b.id, false, terms)
                    , when, dur, each);
        else {
            return solveDT(x, TIMELESS, dt, dur, true, each);
        }
    }

    /**
     * TODO make this for impl only because the ordering of terms is known implicitly from 'x' unlike CONJ
     */
    private boolean solveDT(Compound x, long start, int dt, int dur, boolean dir, Predicate<Event> each) {

        assert (dt != XTERNAL && dt != DTERNAL);

        Term y = switch (x.op()) {
            default -> throw new UnsupportedOperationException();
            case IMPL -> x.dt(dt);
            case CONJ -> dtConj(x, dir, dt);
        };

        if (!termsEvent(y)) return true;

        return solveOcc ?
                solveOcc(y, start, dur, each) :
                each.test(new Relative(y));
    }

    private boolean solveOcc(@Nullable Term y, long start, int dur, Predicate<Event> each) {
        if (!termsEvent(y))
            return true; //keep trying

        return start == TIMELESS ? solveOcc(y, each) :
                each.test(shadow(y, start, (start != ETERNAL && dur != XTERNAL) ? start + dur : start));
    }

    /**
     * dt computation for term construction
     */
    protected int occToDT(long x) {
        //if (x == TIMELESS) x = XTERNAL;
        assert (x != TIMELESS);
        return x == ETERNAL ? DTERNAL : Tense.occToDT(x);
    }

    /**
     * internal time representation, override to filter occ (ex: dither)
     */
    public long eventOcc(long when) {
        return when;
    }

    /**
     * returns false to terminate solution process.  true doesnt necessarily mean it was accepted, and vice-versa
     */
    private boolean trySolution(Event y) {
        if (validSolution(y) && solutions.add(y))
            return solution(y);
        else
            return true; //try again
    }

    /**
     * @param s will be a new unique solution to the search
     *          return false to stop search; true to continue
     */
    protected abstract boolean solution(Event s);

    private boolean solveExact(Term x, Predicate<? super Absolute> each) {
        Collection<Event> ee = events(x);
        if (ee == null) return true;

        ee = shuffle(ee, true);

        for (Event e : ee) {
            if (e instanceof Absolute) {
                if (!each.test((Absolute) e)) return false;
            } else
                break; //since sorted
        }

        return true;
    }

    /**
     * main entry point to the solver
     *
     * @see callee may need to clear the provided seen if it is being re-used
     */
    public final void solve(Term x, boolean occ) {
        if (!x.CONDABLE())
            throw new UnsupportedOperationException();

        this.solveOcc = occ;
        this.depth = 0;
        this.solutions.clear();
        this.target = x;

        //absorb assumed patterns contained in the target
        know(x, true, true);


        solveAll(x, this::trySolution);
    }

    private boolean hasTerm(Term x) {
        return _events(x) != null;
    }

    protected boolean validSolution(Event s) {
        return termsEvent(s.id) && (s instanceof Absolute || !target.equals(s.id));
    }

    private boolean solveAll(Term x, Predicate<Event> each) {
        if (++depth > RECURSION_MAX)
            return true;

        try {
            if (x instanceof Compound) {
                if (x.subtermsDirect().TEMPORAL_VAR())
                    return solveRecursive(x, each); //inner XTERNAL

                if (x.dt() == XTERNAL)
                    return solveXternalTop((Compound) x, each);
            }

            return solveDirect(x, each);
        } finally {
            depth--;
        }
    }

    /**
     * top-level XTERNAL
     */
    private boolean solveXternalTop(Compound x, Predicate<Event> each) {
        return solveXternal(x, y ->
                y instanceof Absolute || (depth >= RECURSION_MAX) ? each.test(y) : solveDirect(y.id, each));
    }

    private boolean solveDirect(Term x, Predicate<Event> each) {
        return solveOcc ? solveOcc(x, each) : each.test(new Relative(x));
    }

//    /**
//     * collapse any relative nodes to unique absolute nodes,
//     * possibly also map less-specific xternal-containing terms to more-specific unique variations
//     */
//    public TimeGraph compact() {
//        for (Collection<Event> e : byTerm.values()) {
//            if (e.size() == 2) {
//                Iterator<Event> ab = e.iterator();
//                Event a = ab.next(), b = ab.next();
//                if (a instanceof Absolute ^ b instanceof Absolute) {
//                    //System.out.println("BEFORE: "); print();
//
//
//                    if (b instanceof Absolute) {
//                        //swap
//                        Event x = a;
//                        a = b;
//                        b = x;
//                    }
//
//                    mergeNodes(b, a);
//
//                    //System.out.println("AFTER: "); print(); System.out.println();
//                }
//            }
//        }
//        return this;
//    }

    private boolean solveRecursive(Term x, Predicate<Event> each) {

//        if (x instanceof Compound && x.TEMPORAL_VAR()) {
//            Subterms xx = x.subterms();
//            TmpTermList y = xx.toTmpList();
//            int xo = x.opID();
//            if (y.replaceAllUnlessNull(z ->
//                    z instanceof Compound && z.TEMPORAL_VAR() ?
//                            solveTerm((Compound) z, w -> w.opID == xo) : z)) {
//                Term z = x.op().the(x.dt(), (Subterms) y);
//                if (termsEvent(z) && !z.equals(x)) {
//                    if (!z.TEMPORAL_VAR())
//                        return each.test(shadow(z));
//                    else
//                        x = z;
//                }
//            }
//        }

        Map<Compound, Set<Term>> s = new SubXternalSolver(x).solutions();
        return switch (s.size()) {
            case 0 -> true;
            case 1 -> solveRecursive1(x, each, s);
            default -> solveRecursiveN(x, each, s);
        };
    }

    /**
     * TODO use CartesianIterator instead of this random sampling
     */
    private boolean solveRecursiveN(Term _x, Predicate<Event> each, Map<Compound, java.util.Set<Term>> subSolved) {
        int ns = subSolved.size();
        Pair<Compound, Term[]>[] substs = new Pair[ns];
        Term x = _x;
        int permutations = 1;
        int j = 0;
        for (Map.Entry<Compound, java.util.Set<Term>> entry : subSolved.entrySet()) {
            Term[] ww = ArrayUnenforcedSet.toArrayShared(entry.getValue());
            assert (ww.length > 0);
            Compound k = entry.getKey();
            if (ww.length == 1) {
                substs[j] = null;
                x = x.replace(k, ww[0]);
                if (!termsEvent(x))
                    return true;
                //save structure of only result
                //knowWeak(ww[0]);
            } else {
                permutations *= ww.length;
                substs[j] = pair(k, ww);
            }
            j++;
        }

        if (permutations == 1) {
            return solveAllIfDifferent(_x, x, each);
        } else {


            Map<Term, Term> m = new UnifriedMap<>(ns);
            while (permutations-- > 0) {
                for (Pair<Compound, Term[]> si : substs) {
                    if (si == null)
                        continue;
                    Term[] ssi = si.getTwo();
                    m.put(si.getOne(), ssi[ssi.length > 1 ? rng.nextInt(ssi.length) : 0]);
                }
                if (!solveAllIfDifferent(x, x.replace(m), each))
                    return false;
                m.clear();
            }
            return true;
        }
    }

    private boolean solveRecursive1(Term x, Predicate<Event> each, Map<Compound, Set<Term>> subSolved) {
        Map.Entry<Compound, Set<Term>> xy = subSolved.entrySet().iterator().next();

        Set<Term> sy = xy.getValue();
        if (!sy.isEmpty()) {
            Term[] TOs = ArrayUnenforcedSet.toArrayShared(sy);
            if (TOs.length > 1) ArrayUtil.shuffle(TOs, rng);


            Term from = xy.getKey();
            for (Term to : TOs) {
                Term y = x.replace(from, to);
                if (termsEvent(y)) {
                    //if (TOs.length == 1) { knowWeak(TOs[0]);  } //only result: save its structure

                    if (!solveAllIfDifferent(x, y, each))
                        return false;
                }
            }
        }
        return true;
    }

    private boolean solveAllIfDifferent(Term x, Term y, Predicate<Event> each) {
        return !termsEvent(y) || x.equals(y) || solveAll(y, each);
    }

    private boolean solveOcc(Term x, Predicate<Event> each) {
        return solveOcc(x, true, each);
    }

    private boolean solveOcc(Term x, boolean negTry, Predicate<Event> each) {

        Relative X = null;
        Collection<Event> ee = events(x);
        if (ee != null) {
            ee = shuffle(ee, true /*false*/);
            for (Event eee : ee) {
                if (eee instanceof Absolute) {
                    if (!each.test(eee)) return false;
                } else
                    X = (Relative) eee;
            }
        }

        if (X == null) X = know(x);

        return bfs(X, new OccSolver(each)) && solveSelfLoop(X, each) &&
                (!negTry || solveNeg(X, each)) &&
                each.test(X);
    }

    private boolean solveNeg(Relative x, Predicate<Event> each) {
        if (autoNeg) {
            Term xn = x.id.neg();
            if (hasTerm(xn))
                return solveOcc(xn, false, e -> !(e instanceof Absolute) || each.test(e.negate()));
        }

        return true;
    }

    /**
     * check for any self-loops and propagate forward and/or reverse
     * HACK i forget how this works and what it does
     */
    @Deprecated
    private boolean solveSelfLoop(Event x, Predicate<Event> each) {

        Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> eii = node(x).edges(false, true,
                e -> e.id().dt() != 0 && e.loop());
        if (eii == emptyIterable)
            return true;
        Lst<FromTo<Node<Event, TimeSpan>, TimeSpan>> eei = new Lst<>(eii);

        Term t = x.id;
        return eei.isEmpty() || solveExact(t, s -> {
            if (s.start() == ETERNAL) return true; //skip

//                    //TODO shuffle found self-loops, there could be sevreal

            for (FromTo<Node<Event, TimeSpan>, TimeSpan> e : eei) {
                long dt = e.id().dt();
                /*if (dt != 0 && e.loop()) {*/

                if (rng.nextBoolean())
                    dt = -dt; //vary order

                if (!each.test(s.shift(+dt, this)))
                    return false;
                if (!each.test(s.shift(-dt, this)))
                    return false;

            }

            return true;
        });

    }

    private Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> shuffle(Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> e) {
        if (e == emptyIterable) return e;

        if (e instanceof Collection) {
            int s = ((Collection) e).size();
            switch (s) {
                case 0:
                    return emptyIterable;
                case 1:
                    return e;
            }
        }

        Lst<FromTo<Node<Event, TimeSpan>, TimeSpan>> ee = new Lst<>(e);
        switch (ee.size()) {
            case 0:
                return emptyIterable;
            case 1:
                return ee;
            default:
                ee.shuffleThis(rng);
                ee.sortThisByInt(AbsoluteLinksFirst);
                return ee;
        }
    }

    private Collection<Event> shuffle(Collection<Event> e, boolean sortAbsolutesFirst) {

        int n = e.size();
        if (n > 1) {
            Lst<Event> ee = new Lst<>(e);
            ee.shuffleThis(rng);
            if (sortAbsolutesFirst)
                ee.sortThisByInt(AbsolutesFirst);
            return ee;
        } else
            return e;
    }

    private Event shadow(Term x, long s, long e) {
        return event(x, s, e, false, false);
    }

    public Stream<? extends Event> events() {
        return events.values().stream().flatMap(Collection::stream);
    }

    /**
     * absolutely specified event
     */

    public static class Absolute extends Event {
        private final long start;

        private Absolute(Term t, long start, long end) {
            super(t, start, end);

            this.start = start;
        }

        Absolute(Term t, long start) {
            this(t, start, start);
        }

        @Override
        public final long start() {
            return start;
        }

        public long end() {
            return start;
        }

        Event shift(long dt, TimeGraph g) {
            assert (dt != 0 && dt != ETERNAL && dt != TIMELESS);
            long s = start + dt;
            return this instanceof AbsoluteRange ?
                    g.absolute(id, s, end() + dt) :
                    g.absolute(id, s);
        }


    }

    public static final class AbsoluteRange extends Absolute {
        private final long end;

        AbsoluteRange(Term t, long start, long end) {
            super(t, start, end);
            if (end <= start || start == ETERNAL || end == TIMELESS)
                throw new RuntimeException("invalid AbsoluteRange start/end times: " + start + ".." + end);
            this.end = end;
        }

        public long end() {
            return end;
        }

    }

    /**
     * TODO RelativeRange?
     */
    public static final class Relative extends Event {

        Relative(Term id) {
            super(id, id.hashCode() /*hashCombine(id.hashCode(), TIMELESS)*/);
        }

        @Override
        public final long start() {
            return TIMELESS;
        }

        @Override
        public final long end() {
            return TIMELESS;
        }

        @Override
        public long range() {
            throw new UnsupportedOperationException();
        }

    }

    public abstract static class Event implements LongObjectPair<Term> {

        static final Event[] EmptyEventArray = new Event[0];

        private static final Comparator<Event> cmp = Comparator
                .comparingLong(Event::start)
                .thenComparingLong(Event::end)
                .thenComparing(e -> e.id);

        public final Term id;
        private final int hash;

        Event(Term id, int hash) {
            this.id = id;
            this.hash = hash;
        }

        protected Event(Term t, long start, long end) {
            this(t, hashCombine(t.hashCode(), start, end));

            if (/*start == TIMELESS || */(start != ETERNAL && (!(start > ETERNAL + SAFETY_PAD) || start >= TIMELESS - SAFETY_PAD)))
                throw new ArithmeticException();
        }

        public abstract long start();

        public abstract long end();

        @Override
        public final int hashCode() {
            return hash;
        }

        @Override
        public final boolean equals(Object obj) {
            if (this == obj) return true;
            Event e = (Event) obj;
            return (hash == e.hash) && (start() == e.start()) && (end() == e.end()) && id.equals(e.id);
        }

        @Override
        public final String toString() {
            long s = start();

            if (s == TIMELESS) {
                return id.toString();
            } else if (s == ETERNAL) {
                return id + "@ETE";
            } else {
                long e = end();
                return e == s ? id + "@" + s : id + "@" + s + ".." + e;
            }
        }

        @Override
        public long getOne() {
            return start();
        }

        @Override
        public final Term getTwo() {
            return id;
        }

        @Override
        public int compareTo(LongObjectPair<Term> e) {
            return this == e ? 0 : cmp.compare(this, (Event) e);
        }

        public long range() {
            //return end() - start();

            long s = start();
            if (s == ETERNAL || s == TIMELESS)
                throw new WTF(); //TEMPORARY
            return end() - s;
        }

        final int dur() {
            long s = start();
            return s == ETERNAL || s == TIMELESS ? XTERNAL : Tense.occToDT(end() - s);
        }

        final Event negate() {
            Term y = id.neg();
            if (this instanceof Relative)
                return new Relative(y);

            long s = start();
            return this instanceof AbsoluteRange ?
                    new AbsoluteRange(y, s, end()) :
                    new Absolute(y, s)
                    ;
        }
    }

    private static class DTPathVisitor implements Consumer<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> {
        long dt;
        int dur = XTERNAL;
        boolean fwd;
        FromTo<Node<Event, TimeSpan>, TimeSpan> e;

        DTPathVisitor(List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path) {
            path.forEach(this);
        }

        private static int shrinkDur(FromTo<Node<Event, TimeSpan>, TimeSpan> e, boolean fwd, int dur) {
            return dur <= 0 ? dur :
                    min(dur, (fwd ? e.from() : e.to()).id().dur());
        }

        @Override
        public void accept(BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>> span) {
            fwd = span.getOne();
            e = span.getTwo();

            dt += (fwd ? +1 : -1) * e.id().dt();
            dur = shrinkDur(e, fwd, dur);
        }

        long[] get() {
            dur = shrinkDur(e, !fwd, dur); //terminal event
            return new long[]{dt, dur};
        }
    }

    private abstract class CrossTimeSolver extends Search<Event, TimeSpan> {

        /**
         * enabled layers
         */
        final boolean tangent;


//		final boolean decompose = true;

        CrossTimeSolver(boolean tangent) {
            this.tangent = tangent;
        }

        /**
         * computes dt, the length of time spanned from start to the end of the given path [0],
         * and the range [1]
         */
        static long[] pathDT(List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path) {
            return new DTPathVisitor(path).get();
        }

        protected abstract boolean go(List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path, Node<Event, TimeSpan> next);

        @Override
        protected Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> search(Node<Event, TimeSpan> x, List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path) {

            Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> y = shuffle(x.edges(true, true, filter(x), null));

            if (this.tangent) {
                Event N = x.id();
                if (N instanceof Relative)
                    return () -> Concaterator.concat(y, (Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>>) () -> shuffle(edges(x, N.id)).iterator());
            }

            return y;
        }

        private Predicate<FromTo<Node<Event, TimeSpan>, TimeSpan>> filter(Node<Event, TimeSpan> x) {
            return e -> {
                Node<Event, TimeSpan> from = e.from();
                Node<Event, TimeSpan> to = e.to();
                return (to == x && !visited(from))
                        ||
                        (from == x && !visited(to));
            };
        }

        final Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> edges(Node<Event, TimeSpan> from, Term to) {
            return edges(from, events(to));
        }

        Iterable<FromTo<Node<Event, TimeSpan>, TimeSpan>> edges(Node<Event, TimeSpan> from, Collection<Event> to) {

            Lst<FromTo<Node<Event, TimeSpan>, TimeSpan>> l = null;

            if (to != null) {
                for (Event t : to) {
                    Node<Event, TimeSpan> T = node(t);
                    if (T != null && T != from) {
                        if (!visited(T)) {
                            if (l == null) l = new Lst<>(1);
                            l.add(new LazyMutableDirectedEdge<>(from, TS_ZERO, T));
                        }
                    }
                }
            }

//            final Term fn = from.id().id.neg();
//            if (terms.containsKey(fn)) {
//                Node<Event, TimeSpan> Fneg = node(shadow(fn));
//                if (Fneg != null) {
//                    l.add(new LazyMutableDirectedEdge<>(from, TS_ZERO, Fneg));
//                }
//            }


            return l == null ? emptyIterable : l;
        }

    }

    private class DTPairSolver extends CrossTimeSolver {

        /**
         * true: paths start from a, false: paths start from b
         */
        final boolean dir;
        private final Term a;
        private final Term b;
        private final Compound x;
        private final Predicate<Event> each;

        private Predicate<Term> ae, be;

        DTPairSolver(Term a, Term b, boolean dir, Compound x, Predicate<Event> each) {
            super(false);
            this.dir = dir;
            this.a = a;
            this.b = b;
            this.x = x;
            this.each = each;
        }

        protected boolean at(Term x, boolean dir) {
            if (equals(dir).test(x))
                return true;

            Term y = dir ? b : a;
            //HACK check if the events intersect, then they can be considered at the same time
//            if (x.CONDS() && y.CONDS() && !x.SEQ() && !y.SEQ()) {
//                if (Conj.commonSubCond(x, y, false, 1))
//                    return true;
//            }

            //...
//            if (!(y instanceof Compound)) return false;
//
//            if (y.CONJ()) {
//                //CONJ parallel
//                final Compound Y = (Compound) y;
//
//                final int dt = y.dt();
//                final boolean xConjPar = x.CONJ() && x.dt() == DTERNAL;
//                if (dt == DTERNAL) {
//                    if (xConjPar)
//                        return Y.containsAny(x.subterms());
//                    else
//                        return Y.contains(x);
//                } else if (dt != XTERNAL) {
//                    final Term yf = Conj.firstEvent(y);
//                    if (xConjPar)
//                        return x.subterms().contains(yf);
//                    else
//                        return x.equals(yf); //TODO cache first event
//                }
//            } else if (y instanceof Neg && y.unneg().CONJ()) {
//                //DISJ parallel
//                Compound yu = (Compound) y.unneg();
//                final int dt = yu.dt();
//                final boolean xConjPar = x.CONJ() && x.dt() == DTERNAL;
//                if (dt == DTERNAL) {
//                    if (xConjPar)
//                        return ((Compound)x).OR(yu::containsNeg);
//                    else
//                        return yu.containsNeg(x);
//                } else if (dt != XTERNAL) {
//                    final Term yf = Conj.firstEvent(yu);
//                    if (xConjPar)
//                        return ((Compound)x).OR(yf::equalsNeg);
//                    else
//                        return x.equalsNeg(yf); //TODO cache first event
//                }
//            }
            return false;
        }

        private Predicate<Term> equals(boolean dir) {
            Predicate<Term> Y = dir ? be : ae;
            if (Y == null) Y = dir ? (be = b.equals()) : (ae = a.equals());
            return Y;
        }

        @Override
        protected boolean go(List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path, Node<Event, TimeSpan> next) {

            if (!at(pathEnd(path).id().id, dir))
                return true; //proceed further

//            if (NAL.DEBUG) {
//                if (!pathStart(path).id().id.equals(dir ? a : b))
//                    throw new WTF();
//            }

            long[] pt = pathDT(path);

            long dt = pt[0];

            if (dt == ETERNAL)
                dt = 0; //HACK

            if (!dir)
                dt = -dt;

            if (x.IMPL())
                dt -= a.seqDur();

            int dur = (int) pt[1];

            return solveDT(x, TIMELESS, occToDT(dt), dur, dir, each);
        }
    }

    private class OccSolver extends CrossTimeSolver {

        private final Predicate<Event> each;
        private transient Event start;
        private transient long pathStartTime;

        OccSolver(Predicate<Event> each) {
            super(true);
            this.each = each;
        }

        @Override
        public void clear() {
            super.clear();
            start = null; //invalidate
        }

        @Override
        protected boolean go(List<BooleanObjectPair<FromTo<Node<Event, TimeSpan>, TimeSpan>>> path, Node<Event, TimeSpan> n) {

            Event end = n.id();
            if (!(end instanceof Absolute))
                return true;

            if (start == null) {
                start = pathStart(path).id();
                pathStartTime = start.start();
            }

            long pathEndTime = end.start();
            if (pathStartTime == TIMELESS && pathEndTime == TIMELESS)
                return true; //no absolute occurrence can be determined from this

            long startTime, endTime;

            long[] pt = pathDT(path);

            long dt = pt[0];
            int dur = (int) pt[1];

            if (pathStartTime == ETERNAL) {
                startTime = end.start();
                endTime = shift(shift(startTime, dur, true), Tense.occToDT(dt), true);
            } else if (pathEndTime == ETERNAL) {
                if (start instanceof Absolute) {
                    startTime = pathStartTime;
                    endTime = shift(shift(startTime, dur, true), Tense.occToDT(dt), true);
                } else {
                    startTime = endTime = ETERNAL;
                }
            } else {
                if (pathStartTime != TIMELESS) {
                    startTime = pathStartTime;
                    endTime = shift(shift(startTime, Tense.occToDT(dt), true), dur, true);
                } else {
                    startTime = shift(pathEndTime, Tense.occToDT(dt), false);
                    endTime = shift(startTime, dur, true);
                }
            }

            return each.test(shadow(start.id, startTime, endTime));
        }

    }

    @Deprecated
    private final class SubXternalSolver implements Predicate<Term> {

        final Map<Compound, Set<Term>> solutions = new UnifriedMap<>(0);

        Set<Term> s;
        int subSolutionsMaxTries = -1;

        SubXternalSolver(Term x) {
            x.subterms().ANDrecurse(Term::TEMPORAL_VAR, this, null);
        }

        @Override
        public boolean test(Term y) {
            if (y instanceof Compound && y.dt() == XTERNAL/* && !y.subterms().hasXternal()*/) {
                solutions.computeIfAbsent((Compound) y, yy -> {
                    if (s == null) s = new UnifiedSet<>(0);
                    else s.clear();

                    subSolutionsMaxTries = SubXternal_SUBSOLUTIONS_TRY;

                    solveXternal(yy, z -> {
                        Term zz = z.id;
                        if (termsEvent(zz) && !yy.equals(zz)) {
                            if (s.add(zz))
                                return --subSolutionsMaxTries > 0;
                        }
                        return true;
                    });

                    return s.isEmpty() ? EMPTY_SET : new ArrayUnenforcedSet<>(s.toArray(EmptyTermArray));
                });
            }

            return true;
        }

        Map<Compound, Set<Term>> solutions() {
            solutions.values().removeIf(z -> z == EMPTY_SET);
            return solutions.isEmpty() ? EMPTY_MAP : solutions;
        }
    }

    private final class SequenceChainer implements LongObjectProcedure<Term> {

        Event prevE;
        long prevW;

        private SequenceChainer(Event start) {
            this.prevE = start;
        }

        @Override
        public void value(long w, Term t) {
            Event next = know(t);
            link(prevE, w - prevW, next);
            prevE = next;
            prevW = w;
        }
    }

}