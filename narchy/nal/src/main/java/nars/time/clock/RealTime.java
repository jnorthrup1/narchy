package nars.time.clock;

import nars.time.Time;
import tec.uom.se.quantity.time.TimeQuantities;

import javax.measure.Quantity;
import java.util.UUID;

/**
 * Created by me on 7/2/15.
 */
public abstract class RealTime extends Time {


    private final int unitsPerSecond;
    private final boolean immediate;

    long startNS;
    public final boolean relativeToStart;
    long start;

    private float dur = 1;
    private float nextDur = 1;

    RealTime(int unitsPerSecond, boolean immediate, boolean relativeToStart) {
        super(Math.abs(UUID.randomUUID().getLeastSignificantBits() ) & 0xffff0000);

        this.immediate = immediate;
        this.relativeToStart = relativeToStart;
        this.unitsPerSecond = unitsPerSecond;

        reset();
    }


    public final double secondsToUnits(double s) {
        return s / unitsToSeconds(1);
    }

    @Override
    public long now() {
        if (immediate)
            _next();
        return super.now();
    }

    @Override
    public void reset() {
        this.startNS = System.nanoTime();
        this.start = relativeToStart ?
            Math.round((System.currentTimeMillis() / 1000.0) * unitsPerSecond)
            : 0L;
        _next();
    }

    @Override
    public final void next() {
        _next();
    }

    private void _next() {
        this.dur = nextDur;
        _set(realtime());
    }

    protected abstract long realtime();

//    double secondsSinceStart() {
//        return unitsToSeconds(now() - start);
//    }

    private double unitsToSeconds(double l) {
        return l / unitsPerSecond;
    }


    @Override
    public Time dur(float d) {
        assert(d > 0);
        this.nextDur = d;
        return this;
    }

    private Time durSeconds(double seconds) {
        return dur(Math.max(1, (int) Math.round(secondsToUnits(seconds))));
    }

    @Override
    public float dur() {
        return dur;
    }

    public Time durFPS(double fps) {
        durSeconds(1.0/fps);
        return this;
    }

//    @Override
//    public String timeString(long time) {
//        return Texts.timeStr(unitsToSeconds(time) * 1.0E9);
//    }

    /** ratio of duration to fps */
    public float durSeconds() {
        return (float) unitsToSeconds(dur);
    }
    public long durNS() {
        return Math.round(1.0E9 * durSeconds());
    }

    public double secondsPerUnit() {
        return (float) unitsToSeconds(1);
    }

//    /** get real-time frames per duration */
//    public float durRatio(Loop l) {
//        float fps = l.getFPS();
//        if (fps > Float.MIN_NORMAL)
//            return durSeconds() * fps;
//        else
//            return 1;
//    }
//    /** set real-time frames per duration */
//    public void durRatio(Loop l, float ratio) {
//        durSeconds(ratio / l.getFPS());
//    }

    @Override
    public long toCycles(Quantity q) {
        double s = TimeQuantities.toTimeUnitSeconds(q).doubleValue(null);
        return Math.round(s * unitsPerSecond);
    }



    /** decisecond (0.1) accuracy */
    protected static class DS extends RealTime {


        DS(boolean relativeToStart, boolean imm) {
            super(10, imm, relativeToStart);
        }

        @Override
        protected long realtime() {
            return start + (System.nanoTime() - startNS) / (100 * 1_000_000);
        }

    }


    /** centisecond (0.01) accuracy */
    public static class CS extends RealTime {


        public CS() {
            this(false, true);
        }

        CS(boolean relativeToStart, boolean imm) {
            super(100, imm, relativeToStart);
        }

        @Override
        protected long realtime() {
            return start + (System.nanoTime() - startNS) / (10 * 1_000_000);
        }

    }

    /** millisecond accuracy */
    public static class MS extends RealTime {


        public MS() {
            this(false, true);
        }


        public MS(boolean relativeToStart, boolean immediate) {
            super(1000, immediate, relativeToStart);
        }

        @Override
        protected long realtime() {
            return start + (System.nanoTime() - startNS) / (1_000_000);
        }

    }

    /** nanosecond accuracy */
    protected static class NS extends RealTime {


        protected NS(boolean relativeToStart, boolean imm) {
            super(1000*1000*1000, imm, relativeToStart);
        }

        @Override
        protected long realtime() {
            return start + (System.nanoTime() - startNS);
        }

    }
}