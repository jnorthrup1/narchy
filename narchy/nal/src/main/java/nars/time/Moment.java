package nars.time;

import jcog.math.LongInterval;
import nars.NAL;

/** describes a perceptual moment in time
 *  TODO extends MutableLongInterval superclass
 * */
public class Moment implements LongInterval {

    /**
     *
     *      * this represents the 'match duration',
     *      * which isnt necessarily the same as the
     *      * 'truth projection duration' which is
     *      * applied to the match to
     *      * determine truth or task results.
     *
     * duration >= 0 */
    public float dur;

    /** start */
    public long s = TIMELESS;
    /** end */
    public long e = TIMELESS;


    /** dur >= 0 */
    public Moment dur(float dur) {
        if (dur < 0 || !Float.isFinite(dur))
            throw new UnsupportedOperationException();
        this.dur = dur;
        return this;
    }


    /** TODO rename 'time()...' */
    public final Moment when(long w) {
        return when(w,w);
    }

    /** TODO rename 'time()...' */
    public final Moment when(long s, long e) {
        if (e < s) {
            /* TODO this is likely caused by a Task being stretched in another thread */
            if (NAL.DEBUG)
                throw new UnsupportedOperationException();
            else
                e = s;
        }

        this.s = s; this.e = e;
        return this;
    }

    public final Moment when(long s, long e, NAL n) {
        return when(s, e, n.dtDither());
    }

    public final Moment when(long s, long e, int ditherDT) {
        if (ditherDT > 1) {
            s = Tense.dither(s, ditherDT);
            if (s == ETERNAL)
                return eternal();

            e = Tense.dither(e, ditherDT);
        }
        return when(s, e);
    }

    public final LongInterval dither(int ditherDT) {
        return ditherDT > 1 ? when(s, e, ditherDT) : this;
    }

    public final Moment setCenter(long mid, int dur) {
        return (mid == ETERNAL ?
            eternal() :
            when(mid - dur / 2, mid + dur / 2)).dur(dur);
    }

//    public final Moment setCenter(long mid, int dur, int ditherDT) {
//        return mid == ETERNAL ?
//                eternal() :
//                when(mid - dur / 2, mid + dur / 2, ditherDT);
//    }

    public final Moment eternal() {
        return when(ETERNAL,ETERNAL);
    }

    @Override
    public long start() {
        return s;
    }

    @Override
    public long end() {
        return e;
    }


//    public final Moment setCenterDur(long when, float range) {
//        return setCenter(when, Math.round(range)).dur(range);
//    }
//    public final Moment setCenterDur(long when, float range, int ditherDT) {
//        return setCenter(when, Math.round(range), ditherDT).dur(range);
//    }

    @Deprecated public long[] toStartEndArray() {
        return new long[] {s, e};
    }

    public boolean whenChanged(long s, long e, int ditherDT) {
        if (ditherDT > 1 && s != TIMELESS && s != ETERNAL) {
            s = Tense.dither(s, ditherDT/*, -1*/);
            e = Tense.dither(e, ditherDT/*, +1*/);
        }
        return whenChanged(s, e);
    }



    public final boolean durIfChanged(float dur) {
        if (this.dur!=dur) {
            this.dur(dur);
            return true;
        }
        return false;
    }

    public final boolean whenChanged(long start, long end) {
        if (this.s != start || this.e != end) {
            when(start,end);
            return true;
        } else
            return false;
    }

}