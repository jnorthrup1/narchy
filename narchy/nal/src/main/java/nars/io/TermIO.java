package nars.io;

import com.google.common.io.ByteArrayDataOutput;
import jcog.data.IntCoding;
import nars.*;
import nars.term.Compound;
import nars.term.anon.Anom;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.atom.BytesAtom;
import nars.term.atom.Int;
import nars.term.util.TermException;
import nars.term.util.builder.TermBuilder;

import java.io.DataInput;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static nars.Op.*;
import static nars.io.IO.SPECIAL_BYTE;
import static nars.io.IO.encoding;
import static nars.term.atom.Bool.Null;

/**
 * term i/o codec
 */
public interface TermIO {

    static void writeCompoundPrefix(byte opByte, int dt, ByteArrayDataOutput out) {

        //byte opByte = o.id;
        boolean dtSpecial = false;
        if (dt != DTERNAL/* && o.temporal*/) {
            switch (dt) {
                case XTERNAL -> opByte |= DefaultTermIO.TEMPORAL_BIT_0;
                case 0 -> opByte |= DefaultTermIO.TEMPORAL_BIT_0 | DefaultTermIO.TEMPORAL_BIT_1;
                default -> {
                    opByte |= DefaultTermIO.TEMPORAL_BIT_0 | DefaultTermIO.TEMPORAL_BIT_1;
                    dtSpecial = true;
                }
            }
        }
        out.writeByte(opByte);
        if (dtSpecial)
            DefaultTermIO.writeDT(dt, out);
    }

    Term read(TermBuilder B, DataInput in) throws IOException;


    default void writeSubterms(ByteArrayDataOutput out, Term... subs) {
        int n = subs.length;
        //assert(n < Byte.MAX_VALUE);
        out.writeByte(n);
        for (Term s : subs)
            s.write(out);
    }

 
    /**
     * default term codec
     */
    DefaultTermIO the = new DefaultTermIO();

    class DefaultTermIO implements TermIO {

        /** lower 5 bits (bits 0..4) = base op */
        static final byte OP_MASK = (0b00011111);
        /** upper control flags for the op byte */
        static final byte TEMPORAL_BIT_0 = 1 << 5;
        static final byte TEMPORAL_BIT_1 = 1 << 6;

        static {
            Op[] v = values();
            assert(v.length < OP_MASK);
            for (Op o : v) assert !o.temporal || (o.id != OP_MASK); /* sanity test to avoid temporal Op id appearing as SPECIAL_BYTE if the higher bits were all 1's */
        }

        @Override
        public Term read(TermBuilder B, DataInput in) throws IOException {
            byte opByte = in.readByte();
            if (opByte == SPECIAL_BYTE) {
                try {
                    return Narsese.term(in.readUTF(), false);
                } catch (Narsese.NarseseException e) {
                    throw new IOException(e);
                }
            }
            Op o = the(opByte & OP_MASK);
            switch (o) {
                case VAR_DEP:
                case VAR_INDEP:
                case VAR_PATTERN:
                case VAR_QUERY:
                    return $.v(o, in.readByte());
                case IMG:
                    return in.readByte() == ((byte) '/') ? ImgExt : ImgInt;
                case BOOL:
                    byte code = in.readByte();
					return switch (code) {
						case -1 -> Null;
						case  0 -> Bool.False;
						case +1 -> Bool.True;
						default -> throw new UnsupportedEncodingException();
					};
                case ATOM:
					return switch (encoding(opByte)) {
						case 0 -> Atomic.the(in.readUTF());
						case 1 -> Anom.the(in.readByte());
						case 2 -> BytesAtom.atomBytes(in);
						default -> throw new IOException("unknown ATOM encoding: " + encoding(opByte));
					};
                case INT:
                    return Int.the(IntCoding.readZigZagInt(in));

                case NEG:
                    return read(B, in).neg();
                default: {

                    int temporalFlags = (opByte & (TEMPORAL_BIT_0|TEMPORAL_BIT_1)) >> 5;
                    int dt = switch (temporalFlags) {
						case 0 -> DTERNAL;
						case 1 -> XTERNAL;
						case 2 -> 0;
						/*case 3:*/
						default -> IntCoding.readZigZagInt(in);
					};

					int siz = in.readByte();

                    assert (siz < NAL.term.SUBTERMS_MAX);

                    Term[] s = new Term[siz];
                    for (int i = 0; i < siz; i++) {
                        Term read = (s[i] = read(B, in));
                        if (read == null)
                            throw new TermException("read invalid", PROD /* consider the termvector as a product */, s);
                    }

                    Term y = o.build(B, dt, s);
                    if (!(y instanceof Compound))
                        throw new TermException("read invalid compound", o, dt, s);

                    return y;
                }

            }
        }


        static void writeDT(int dt, ByteArrayDataOutput out) {
            IntCoding.writeZigZagInt(dt, out);
        }


    }

    static void outNegByte(ByteArrayDataOutput out) {
        out.writeByte(NEG.id);
    }

}