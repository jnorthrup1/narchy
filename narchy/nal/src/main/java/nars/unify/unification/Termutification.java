package nars.unify.unification;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.data.map.UnifriedMap;
import jcog.data.set.ArrayHashSet;
import jcog.random.RandomBits;
import nars.Term;
import nars.term.Variable;
import nars.term.atom.Bool;
import nars.unify.Unification;
import nars.unify.Unify;
import nars.unify.mutate.Termutator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Termutator-permuted Unification
 *      not thread-safe
 */
public class Termutification extends ArrayHashSet<DeterministicUnification> implements Unification {

    private final Unify base;
    private final Termutator[] termutes;

    public Termutification(Unify u, DeterministicUnification pre, Termutator[] termutes) {
        super(0);
        this.termutes = termutes;

        UnifriedMap<Term, Term> baseMap = new UnifriedMap<>(4, 1f);
        Unify.ContinueUnify base = new Unify.ContinueUnify(u, baseMap);
        boolean applied = pre.apply(base);
        assert(applied);
        baseMap.trimToSize();
        this.base = base;


//        restart = discovery.size();
    }


    /**
     * returns how many TTL used
     */
    public void discover(Unify ctx, int discoveriesMax, int ttl) {


        Discovery u = new Discovery(this.base, discoveriesMax);
        u.setTTL(ttl);

        u.match(termutes, -1);

        int spent = Util.clamp(ttl - u.ttl, 0, ttl);

        ctx.use(spent);
    }

    @Override
    public final int forkKnown() {
        return size();
    }

    @Override
    public Iterable<Term> apply(Term x) {
        switch (size()) {
            case 0:
                return Util.emptyIterable;
            case 1:
                return first().apply(x);
            default:
                //HACK could be better
                /* equals between Term and Unification:
                Reports calls to .equals() where the target and argument are of incompatible types. While such a call might theoretically be useful, most likely it represents a bug. */
                List<Term> result;
                List<DeterministicUnification> s = shuffle(this, base.random);
                List<Term> l = new Lst<>(s.size());
                for (DeterministicUnification a : s) {
                    Term b = a.transform(x);
                    if (b!= Bool.Null)
                        l.add(b);
                }

                result = l.stream().filter(z -> z != null
                        &&
                        z != Unification.Null).collect(Collectors.toList());
                return result;
        }
    }

    private static List<DeterministicUnification> shuffle(ArrayHashSet<DeterministicUnification> fork, RandomBits rng) {
        return fork.list.clone().shuffleThis(rng);
    }

//    public List<DeterministicUnification> listClone() {
//        Lst<DeterministicUnification> l = list;
//        return switch (l.size()) {
//            case 0 -> EMPTY_LIST;
//            case 1 -> List.of(l.getOnly());
//            default -> list.clone();
//        };
//    }


    private class Discovery extends Unify.ContinueUnify {

        private final Unify parent;
        private int discoveriesRemain;

        /**
         * if xy is null then inherits the Map<Term,Term> from u
         * otherwise, no mutable state is shared between parent and child
         *
         * @param parent
         * @param xy
         */
        public Discovery(Unify parent, int discoveriesRemain) {
            super(parent, new UnifriedMap<>(0));
            this.parent = parent;
            this.discoveriesRemain = discoveriesRemain;
        }

        @Override
        @Deprecated public Term resolveVar(Variable x) {
            Term y = parent.resolveVar(x);
            if (y != null && y != x) {
                if (size==0 || !var(y))
                    return y; //constant

                x = (Variable) y;   //recurse thru this resolver
            }

            return size > 0 ? super.resolveVar(x) : x;
        }

//        @Override
//        public boolean live() {
//            return super.live() && discoveriesRemain > 0;
//        }

        @Override
        protected boolean match() {

            Unification z = unification(false);
            if (z != Unification.Null) {
                if (z!=Self && z instanceof DeterministicUnification) {
                    if (Termutification.this.add((DeterministicUnification) z)) {
                        //TODO calculate max possible permutations from Termutes, and set done
                    }
                } else {
                    /*else {
                        return Iterators.getNext(a.apply(x).iterator(), null); //HACK
                    }*/
                }
            }

            return --discoveriesRemain > 0;
        }
    }
}