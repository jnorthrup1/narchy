package nars.unify.constraint;

import nars.$;
import nars.Term;
import nars.subterm.util.SubtermCondition;
import nars.term.Compound;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.atom.Int;
import nars.unify.Unify;

public class SubConstraint<U extends Unify> extends RelationConstraint<U> {
    private final boolean forward;

    private final SubtermCondition contains;


    /**
     * containment of the target positively (normal), negatively (negated), or either (must test both)
     */
    private final int polarityCompare;

    public SubConstraint(Variable x, Variable y, SubtermCondition contains) {
        this(x, y, contains, +1);
    }

//    public SubConstraint(Variable x, Term yPN, SubtermCondition contains) {
//        this(x, (Variable) yPN.unneg(), contains, yPN instanceof Neg? -1 : +1);
//    }

    public SubConstraint(Variable x, Variable y, SubtermCondition contains, int polarityCompare) {
        this(x, y, true, contains, polarityCompare);
    }


    private SubConstraint(Variable x, Variable y, /* HACK change to forward semantics */ boolean forward, SubtermCondition contains, int polarityCompare) {
        super(SubConstraint.class, x, y,
            $.p(
                Atomic.atom(contains.name()),
                (forward ? Int.the(+1) : Int.the(-1)),
                Int.the(polarityCompare)
            )
        );


        this.forward = forward;
        this.contains = contains;
        this.polarityCompare = polarityCompare;
    }

    @Override
    protected RelationConstraint newMirror(Variable newX, Variable newY) {
        return new SubConstraint<>(newX, newY, !forward, contains, polarityCompare);
    }

    @Override
    public float cost() {
        float baseCost = contains.cost();
		return switch (polarityCompare) {
			case 1 -> baseCost;
			case 0 -> 1.9f * baseCost;
			case -1 -> 1.1f * baseCost;
			default -> throw new UnsupportedOperationException();
		};
    }

    public final boolean invalid(Term x, Term y, Unify context) {
        Term c = forward ? x : y;
        return !(
                c instanceof Compound &&
                contains.test((Compound) c, forward ? y : x, polarityCompare)
        );
    }


}