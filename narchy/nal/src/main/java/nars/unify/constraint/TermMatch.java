package nars.unify.constraint;

import jcog.Util;
import jcog.WTF;
import nars.$;
import nars.Op;
import nars.Term;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termed;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.unify.UnifyConstraint;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.function.Predicate;

import static nars.Op.SETe;
import static nars.Op.XTERNAL;

/** Is term a... */
public abstract class TermMatch implements Predicate<Term> {


    //TODO others

    public static final Comparator<? super TermMatch> paramComparator = Comparator.comparing(TermMatch::param);
    private static final TermMatch[] EmptyArray = new TermMatch[0];


    private static Term resolve(Function<?, Term> resolve) {
        return resolve instanceof Termed ? ((Termed) resolve).term() : $.quote(resolve.toString());
    }

    public static TermMatch AND(SortedSet<TermMatch> matchers) {
        return switch (matchers.size()) {
            case 0 -> throw new UnsupportedOperationException();
            case 1 -> matchers.first();
            default -> new TermMatcherAND(matchers);
        };
    }

    public Term name(@Nullable Function<?, Term> r) {
        Class<? extends TermMatch> c = this.getClass();
        String cc = c.isAnonymousClass() ? this.toString() : c.getSimpleName();
        if (cc.isEmpty())
            throw new WTF();
        return name(r, cc, this.param());
    }

    private static Term name(@Nullable Function<?, Term> r, String cc, Term p) {
        Atomic a = Atomic.atom(cc);
        if (r != null) {
            Term s = resolve(r);
            return p != null ? $.func(a, s, p) : $.func(a, s);
        } else {
            return p != null ? $.func(a, p) : a;
        }
    }

    public final Term name() {
        return name(null);
    }

    /**
     * target representing any unique parameters beyond the the class name which is automatically incorporated into the predicate it forms
     */
    @Nullable
    public Term param() {
        return null;
    }

//    public static class Eventable extends TermMatch {
//
//
//        public static final Eventable the = new Eventable();
//
//        private Eventable() {
//            super();
//        }
//
//        @Override
//        public float cost() {
//            return 0.035f;
//        }
//
//        @Override
//        public boolean test(Term x) {
//            return x.unneg().EVENTABLE();
//        }
//
//        @Override
//        public boolean testSuper(Term sx) {
//            return true; ///TODO
//        }
//
//        @Nullable @Override public Term param() {
//            return null;
//        }
//    }

    public UnifyConstraint constraint(Variable x, boolean trueOrFalse) {
        return new UnaryConstraint<>(this, x, trueOrFalse);
    }

    public abstract float cost();

    /**
     * override to impl reductions
     */
    public boolean remainAmong(UnaryConstraint c) {
        return true;
    }

    /**
     * is the op one of the true bits of the provide vector ("is any")
     */
    public static class Is extends TermMatch {

        public final int struct;


        protected Is(int inStruct) {
            this.struct = inStruct;
            //this.cost = structureCost(0.01f, struct);
        }

        public static TermMatch is(Op op) {
            return is(op.bit);
        }

        public static TermMatch is(int struct) {
            return struct==Op.Variables ? TermMatch.VARIABLE : new Is(struct);
        }

        @Override
        public Term param() {
            return Op.strucTerm(struct);
        }

        @Override
        public float cost() {
            return 0.02f;
        }

        @Override
        public boolean test(Term x) {
            return x.isAny(struct);
        }

//        @Override
//        public boolean testSuper(Term sx) {
//            //return sx.subterms().hasAny(struct);
//            return true;
//        }

        @Override
        public boolean remainAmong(UnaryConstraint c) {

            TermMatch y = c.matcher;
            if (this != y && y instanceof Is) {
                //eliminate this if it is a subset of Y's structure
                return !Op.hasAll(((Is) y).struct, this.struct);
                //return !Op.hasAll(yStruct, xStruct) || Op.hasAll(xStruct, yStruct);
            }

            return true;
        }
    }

    public static class HasAllStruct extends TermMatch {

        public final int struct;
        //private final float cost;

        public HasAllStruct(int allStruct) {
            assert (allStruct != 0);
            this.struct = allStruct;
            //this.cost = structureCost(0.01f, struct);
        }

        @Override
        public Term param() {
            return Op.strucTerm(struct);
        }

        @Override
        public float cost() {
            return 0.03f;
        }

        @Override
        public boolean test(Term x) {
            return x.hasAll(struct);
        }

        @Override
        public boolean remainAmong(UnaryConstraint c) {

            TermMatch y = c.matcher;
            if (this != y && y instanceof HasAllStruct) {
                //eliminate this if it is a subset of Y's structure
                return !Op.hasAll(((HasAllStruct) y).struct, this.struct);
                //return !Op.hasAll(yStruct, xStruct) || Op.hasAll(xStruct, yStruct);
            }

            return true;
        }
    }

//    public static class HasSubtructure extends Has {
//
//		public HasSubtructure(int allStruct) {
//			super(allStruct);
//		}
//		@Override
//		public boolean test(Term x) {
//			return x instanceof Compound && Op.hasAll(x.structureSubs(), struct);
//		}
//	}

    public static class IsUnneg extends Is {

        private final boolean requireNegation;

        public IsUnneg(int any) {
            this(any, false);
        }

        IsUnneg(int any, boolean requireNegation) {
            super(any);
            this.requireNegation = requireNegation;
        }

        @Override
        public float cost() {
            return 0.03f + super.cost();
        }

        @Override
        public boolean test(Term x) {
            return (!requireNegation || x instanceof Neg) && super.test(x.unneg());
        }

//        @Override
//        public boolean testSuper(Term sx) {
//            return sx.hasAny(struct | (requireNegation ? NEG.bit : 0));
//        }
    }

    public static final class VolMin extends TermMatch {
        private final int min;
        private final Atomic param;

        public VolMin(int min) {
            if (min < 2)
                throw new UnsupportedOperationException();
            this.param = $.the(this.min = min);
        }

        @Override
        public Term param() {
            return param;
        }

        @Override
        public float cost() {
            return 0.025f;
        }

        @Override
        public boolean test(Term x) {
            //noinspection RedundantCast
            return x instanceof Compound && ((Compound)x).volume() >= min;
        }
    }

//    public static final class ComplexityMin extends TermMatcher {
//        private final int min;
//        private final Atomic param;
//
//        public ComplexityMin(int min) {
//            this.param = $.the(this.min = min);
//        }
//
//        @Override
//        public Term param() {
//            return param;
//        }
//
//        @Override
//        public float cost() {
//            return 0.02f;
//        }
//
//        @Override
//        public boolean test(Term term) {
//            return term instanceof Compound && term.complexity() >= min;
//        }
//    }


    public static final class VolMax extends TermMatch {
        private final int volMax;
        private final Atomic param;

        public VolMax(int volMax) {
            assert (volMax >= 1);
            this.param = $.the(this.volMax = volMax);
        }

        @Override
        public Term param() {
            return param;
        }

        @Override
        public float cost() {
            return 0.1f;
        }

        @Override
        public boolean test(Term x) {
            return x.volume() <= volMax;
        }

    }

//    /**
//     * compound is of a specific type, and has the target in its structure
//     * TODO combine Is + Has in postprocessing step
//     */
//    public static final class IsHas extends TermMatcher {
//
//        final int struct;
//        final int structSubs;
//        private final byte is;
//
//        private final Term param;
//        //private final float cost;
//
//        public IsHas(Op is, int structSubs) {
//            //assert (!is.atomic && depth > 0 || is != NEG) : "taskTerm or beliefTerm will never be --";
//
//            this.is = is.id;
//            this.struct = structSubs | is.bit;
//            this.structSubs = structSubs;
//            assert (Integer.bitCount(struct) >= Integer.bitCount(structSubs));
//
//            //this.cost = structureCost(0.02f, struct);
//
//            Atom isParam = Op.the(this.is).atom;
//            this.param = structSubs != 0 ? $.p(isParam, Op.strucTerm(structSubs)) : isParam;
//        }
//
//
//        @Override
//        public boolean test(Term term) {
//            return term instanceof Compound &&
//                    term.opID() == is &&
//                    (structSubs == 0 || Op.hasAll(term.structureSubs(), structSubs));
//        }
////
////        @Override
////        public boolean testSuper(Term term) {
//////            return term instanceof Compound &&
//////                    testVol(term) && term.subterms().hasAll(struct);
////            return true;
////        }
//
//        @Override
//        public Term param() {
//            return param;
//        }
//
//        @Override
//        public float cost() {
//            return 0.025f;
//        }
//    }

    /**
     * non-recursive containment
     */
    public static final class Contains extends TermMatch {

        public final Term x;
//        private final int xStruct;

        public Contains(Term x) {
            assert (!x.VAR_PATTERN()); //HACK
            this.x = x;
//            this.xStruct = x.structure();
        }

        @Override
        public Term param() {
            return x;
        }

        @Override
        public float cost() {
            return 0.15f;
        }

        @Override
        public boolean test(Term term) {
            return term instanceof Compound && ((Compound)term).contains(x);
        }

//        @Override
//        public boolean testSuper(Term term) {
//            //return term.hasAll(xStruct);
//                    //containsRecursively(this.x);
//            return true;
//        }
    }

//    /**
//     * non-recursive containment
//     */
//    public final static class EqualsRoot extends TermMatcher {
//
//        public final Term x;
//
//        public EqualsRoot(Term x) {
//            this.x = x;
//            assert(!(x instanceof VarPattern)); //HACK
//        }
//
//        @Override
//        public Term param() {
//            return x;
//        }
//
//        @Override
//        public float cost() {
//            return 0.15f;
//        }
//
//        @Override
//        public boolean test(Term term) {
//            return term.equalsRoot(x);
//        }
//
//        @Override
//        public boolean testSuper(Term x) {
//            return !x.impossibleSubTerm(this.x);
//        }
//    }

    /**
     * non-recursive containment
     */
    public static final class Equals extends TermMatch {

        public final Term x;
        final Predicate<Term> xEq;

        public Equals(Term x) {
            this.x = x;
            this.xEq = x.equals();
            assert (!x.VAR_PATTERN()); //HACK
        }

        @Override
        public Term param() {
            return x;
        }

        @Override
        public float cost() {
            return 0.03f;
        }

        @Override
        public boolean test(Term term) {
            return xEq.test(term);
        }

    }

    public static final class SubsMin extends TermMatch {

        final short subsMin;

        public SubsMin(short subsMin) {
            this.subsMin = subsMin;
            assert (subsMin > 0);
        }

        @Override
        public Term param() {
            return $.the(subsMin);
        }

        @Override
        public boolean test(Term term) {
            return term instanceof Compound && (subsMin == 1 || term.subs() >= subsMin);
        }

        @Override
        public float cost() {
            return 0.02f;
        }

    }





    private static final class TermMatcherAND extends TermMatch {

        final TermMatch[] m;
        final Term id;
        final float cost;

        TermMatcherAND(SortedSet<TermMatch> matchers) {
            assert(matchers.size()>1);
            m = matchers.toArray(EmptyArray);
            id = SETe.map(TermMatch::name, m);
            cost = (float)Util.sum(TermMatch::cost, m);
        }

        @Override
        public Term name(@Nullable Function<?, Term> r) {
            return r != null ? $.p(id, resolve(r)) : id;
        }

        //TODO Arrays.sort(m, PREDICATE.CostIncreasing);

        @Override
        public boolean test(Term term) {
            for (TermMatch x : m)
                if (!x.test(term)) return false;
            return true;
        }

        @Nullable
        @Override
        public Term param() {
            return id;
        }

        @Override
        public float cost() {
            return cost;
        }
    }


    /**
     * applies to all subterms
     */
    public static final class TermMatcherSubterms extends TermMatch {

        private static final Atomic _I = Atomic.the(TermMatcherSubterms.class.getSimpleName());

        private final TermMatch m;
        private final Term param;

        public TermMatcherSubterms(TermMatch m) {
            super();
            this.m = m;
            this.param = $.func(_I, m.param());
        }

        @Override
        public @Nullable Term param() {
            return param;
        }

        @Override
        public float cost() {
            return m.cost() * 2 /* TODO ? */;
        }

        @Override
        public boolean test(Term term) {
            return ((Compound) term).AND(m);
        }
    }


    public static final TermMatch SEQUENCE = new TermMatch() {

        @Override
        public boolean test(Term t) {
            return t.SEQ()
                    //|| (t.CONJ() && t.dt()==XTERNAL)
                    ;
        }

        @Override
        public float cost() {
            return 0.06f;
        }
    };

    public static final TermMatch EVENT_CONTAINER = new TermMatch() {

        @Override
        public boolean test(Term t) {
            return t.CONDS();
        }


        @Override
        public float cost() {
            return 0.04f;
        }
    };

    public static final TermMatch VARIABLE = new Is(Op.Variables);

    public static final TermMatch Timeless = new TermMatch() {

        @Override public boolean test(Term x) {
            return switch (x.op()) {
                case CONJ -> x.dt()!=XTERNAL && x.seqDur() == 0;
                case IMPL -> x.dt()!=XTERNAL && !Op.dtSpecial(x.dt()) && testSubterms((Compound)x );
                default ->
                    !(x instanceof Compound) ||
                    !x.hasAny(Op.Temporals) ||
                    testSubterms((Compound) x);
            };
        }

        private boolean testSubterms(Compound x) {
            return x.AND(this);
        }

        @Override
        public float cost() {
            return 0.25f;
        }
    };
}