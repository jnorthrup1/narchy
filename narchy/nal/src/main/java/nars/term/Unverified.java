package nars.term;

import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.util.builder.TermBuilder;
import org.jetbrains.annotations.Nullable;

import static nars.term.atom.Bool.Null;

public interface Unverified extends Subterms {
    Op op();
    int dt();

    @Nullable static Term verify(Term x, TermBuilder B) {
        if (x instanceof Neg) {
            Term xu = x.unneg(); Term yu = verify(xu, B);
            if (yu == null || yu == Null)
                return null;
            return (xu == yu) ? x : yu.neg();
        }

        return x instanceof Unverified ? ((Unverified) x).build(B) : x;

    }

    @Nullable default Term build(TermBuilder B) {
        Term[] u = arrayShared();
        Term[] u0 = u;

        for (int i = 0; i < u.length; i++) {
            Term x = u0[i];
            Term y = verify(x, B);
            if (x!=y) {
                if (y == null || y == Null)
                    return null;  //short-circuit
                if (u0==u) u = u0.clone();
                u[i] = y;
            }
        }

        Term z = op().build(B, dt(), u);
        if (z == Null) return null; //HACK
        return z;
    }

}