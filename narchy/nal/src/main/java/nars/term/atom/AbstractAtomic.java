package nars.term.atom;

import jcog.Hashed;
import jcog.Util;
import nars.Op;
import nars.term.anon.IntrinAtomic;

import java.util.Arrays;

import static java.lang.System.arraycopy;

/**
 * an Atomic impl which relies on the value provided by toString()
 */
public abstract class AbstractAtomic extends Atomic implements Hashed {


    /*@Stable*/
    /** TODO make private again */
    public final transient byte[] bytes;

    protected final transient int hash;

    protected AbstractAtomic(Op o, byte num) {
        this(IntrinAtomic.termToId(o, num), o.id, num);
        assert num > 0;
    }

    protected AbstractAtomic(int hash, byte... raw) {
        this.bytes = raw;
        this.hash = hash;
    }

    protected AbstractAtomic(byte[] raw) {
        this(Util.hash(raw), raw);
    }


    @Override
    public int opID() {
        return bytes[0];
    }

    protected AbstractAtomic(byte opID, byte[] s) {
        this(bytes(opID, s));
    }

    public static byte[] bytes(byte opID, String str) {
        return bytes(opID, str.getBytes());
    }

    private static byte[] bytes(byte opID, byte... stringbytes) {
        int slen = stringbytes.length;

        byte[] sbytes = new byte[slen + 3];
        sbytes[0] = opID;
        sbytes[1] = (byte) (slen >> 8 & 0xff);
        sbytes[2] = (byte) (slen & 0xff);
        arraycopy(stringbytes, 0, sbytes, 3, slen);
        return sbytes;
    }


    @Override
    public final byte[] bytes() {
        return bytes;
    }

    @Override
    public boolean equals(Object u) {
        return  (this == u)
                    ||
                ((u instanceof Atomic) &&
                 (!(u instanceof Hashed) || hash == u.hashCode()) &&
                 Arrays.equals(bytes, ((Atomic) u).bytes()));
    }

    @Override public String toString() {
        return new String(bytes, 3, bytes.length-3);
    }


    @Override
    public final int hashCode() {
        return hash;
    }


}