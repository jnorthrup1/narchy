package nars.term.control;

import jcog.Util;
import jcog.WTF;
import jcog.data.list.Lst;
import nars.term.Compound;
import nars.term.util.builder.SimpleHeapTermBuilder;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;

import java.lang.invoke.MethodHandle;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.lang.invoke.MethodHandles.guardWithTest;
import static jcog.data.iterator.ArrayIterator.stream;
import static nars.Op.PROD;

/**
 * AND/while predicate chain
 */
public abstract class AND<X> extends PREDICATE<X> {

    private final float cost;


    private AND(Compound id, float cost) {
        super(id);
        this.cost = cost;
    }

    @SafeVarargs
    private AND(PREDICATE<X>... cond) {
        this((Compound)
                    SimpleHeapTermBuilder.the.compound(PROD, cond)
                    //PROD.the(cond)

            ,(float)Util.sum((FloatFunction<PREDICATE<X>>) (z -> {
                float c = z.cost();
                return Float.isFinite(c) ? c : 0;
            }), cond));

        validate(cond);
    }

    private static void validate(PREDICATE[] cond) {
        for (PREDICATE x : cond)
            if (x instanceof AND)
                throw new UnsupportedOperationException("should have been flattened");
        assert (cond.length >= 2) : "unnecessary use of AndCondition";
    }

    public static <X> PREDICATE<X> the(List<PREDICATE<X>> cond) {
        int s = cond.size();
        return switch (s) {
            case 0 -> null;
            case 1 -> cond.get(0);
            default -> the(cond.toArray(PREDICATE.EmptyPredicateArray));
        };
    }

    private static <X> PREDICATE<X> the(PREDICATE<X>[] cond) {
        int s = cond.length;
        switch (s) {
            case 0:
                return null;
            case 1:
                return cond[0];
            default:
                boolean needsFlat = Arrays.stream(cond).anyMatch(c -> c instanceof AND);
                if (needsFlat) {
                    cond = stream(cond).flatMap(
                            x -> x instanceof AND ?
                                    ((AND<X>) x).conditions().stream() :
                                    Stream.of(x)
                    ).toArray(PREDICATE[]::new);
                }

                return switch (cond.length) {
                    case 0, 1 -> throw new WTF();
                    case 2 -> new AND2<>(cond[0], cond[1]);
                    case 3 -> new AND3<>(cond[0], cond[1], cond[2]);
                    default -> new ANDn<>(cond);
                };

        }
    }

//    static final Memoize<AND,AND> intern = new HijackMemoize<>(z -> z, 2048, 3);
//    public static <X> AND<X> intern(AND<X> y) {
//        return intern.apply(y);
//    }

    public synchronized ANDPredicate<X> compileLambda() {
        return _compileLambda();
//        if (lambda==null)
//            this.lambda = _compileLambda();
//        return this.lambda;
    }
//    private transient ANDPredicate<X> lambda = null;

    private ANDPredicate<X> _compileLambda() {
        return new ANDPredicate<>(ref, cost, asPredicate());
    }

    protected abstract Predicate<X> asPredicate();

    public abstract List<PREDICATE<X>> conditions();

    @Override
    public final float cost() {
        return cost;
    }

    @Override
    public PREDICATE<X> transformPredicate(Function<PREDICATE<X>, PREDICATE<X>> f) {
        PREDICATE[] xx = conditions().toArray(PREDICATE.EmptyPredicateArray);
        return f.apply(PREDICATE.transformPred(f, xx) ? the(xx) : this);
    }

    public static final class ANDPredicate<X> extends AND<X> {

        private final Predicate<X> start;

        private ANDPredicate(Compound term, float cost, Predicate<X> start) {
            super(term, cost);
            this.start = start;
        }

        @Override
        public boolean test(X x) {
            return start.test(x);
        }

        @Override
        public ANDPredicate<X> compileLambda() {
            return this;
        }

        @Override
        protected Predicate<X> asPredicate() {
            return start;
        }

        @Override
        public List<PREDICATE<X>> conditions() {
            throw new UnsupportedOperationException();
        }


    }

    public static final class ANDn<X> extends AND<X> {
        /*@Stable*/
        private final PREDICATE<X>[] cond;

        private ANDn(PREDICATE<X>[] cond) {
            super(cond);
            this.cond = cond;
        }

        @Override
        public MethodHandle method(X x) {
            MethodHandle y = null;
            List<PREDICATE<X>> conditions = conditions();
            for (int i = conditions.size() - 1; i >= 0; i--) {
                MethodHandle c = conditions.get(i).method(x);
                y = y == null ? c : guardWithTest(c, y, CONSTANT_FALSE);
            }
            return y;
        }

        @Override
        protected Predicate<X> asPredicate() {
            Predicate<X> y = cond[cond.length - 1];
            for (int i = cond.length - 2; i >= 0; i--)
                y = cond[i].and(y);
            return y;
        }

        @Override
        public final boolean test(X x) {
            for (PREDICATE<X> c : cond) {
                if (!c.test(x))
                    return false;
            }
            return true;
        }

        @Override
        public List<PREDICATE<X>> conditions() {
            return new Lst<>(cond.clone());
        }

        public PREDICATE<X> first() {
            return Util.first(cond);
        }

        public PREDICATE<X> last() {
            return Util.last(cond);
        }

    }

    public static final class AND2<X> extends AND<X> {
        /*@Stable*/
        private final PREDICATE<X> a, b;

        private AND2(PREDICATE<X> a, PREDICATE<X> b) {
            super(a, b);
            assert (!(a instanceof AND) && !(b instanceof AND));
            this.a = a;
            this.b = b;
        }

        @Override
        public MethodHandle method(X x) {
            return guardWithTest(a.method(x), b.method(x), CONSTANT_FALSE);
        }

//TODO
//  see: https://github.com/headius/jruby-handle-compiler/blob/master/src/main/java/com/headius/jruby/HandleCompiler.java
//        @Override public MethodHandle method() {
//            return MethodHandles.permuteArguments(guardWithTest(
//                    a.method().asCollector(Object[].class, 1),
//                    b.method().asCollector(Object[].class, 1), CONSTANT_FALSE)
//                ;
//        }

        @Override
        public final boolean test(X x) {
            return a.test(x) && b.test(x);
        }

        @Override
        protected Predicate<X> asPredicate() {
            return a.and(b);
        }

        @Override
        public List<PREDICATE<X>> conditions() {
            return new Lst<>(new PREDICATE[]{a, b});
        }

        public PREDICATE<X> first() {
            return a;
        }

        public PREDICATE<X> last() {
            return b;
        }

    }

    public static final class AND3<X> extends AND<X> {
        /*@Stable*/
        private final PREDICATE<X> a, b, c;

        private AND3(PREDICATE<X> a, PREDICATE<X> b, PREDICATE<X> c) {
            super(a, b, c);
            this.a = a;
            this.b = b;
            this.c = c;
        }

        @Override
        public MethodHandle method(X x) {
            return guardWithTest(
                    a.method(x),
                    guardWithTest(b.method(x), c.method(x), CONSTANT_FALSE),
                    CONSTANT_FALSE);
        }

        @Override
        public List<PREDICATE<X>> conditions() {
            return new Lst<>(new PREDICATE[]{a, b, c});
        }

        @Override
        protected Predicate<X> asPredicate() {
            return (a.and(b)).and(c);
        }

        @Override
        public final boolean test(X x) {
            return a.test(x) && b.test(x) && c.test(x);
        }

        public PREDICATE<X> first() {
            return a;
        }

        public PREDICATE<X> last() {
            return c;
        }

    }

//    public PREDICATE<D> transform(Function<AND<D>, PREDICATE<D>> outer, @Nullable Function<PREDICATE<D>, PREDICATE<D>> f) {
//        PREDICATE[] yy = transformedConditions(this.f);
//        PREDICATE<D> z = yy != cond ? AND.the(yy) : this;
//        if (z instanceof AND)
//            return outer.apply((AND)z);
//        else
//            return z;
//    }
//
//    public static <D> PREDICATE[] transformedConditions(PREDICATE[] cond, @Nullable Function<PREDICATE<D>, PREDICATE<D>> f) {
//        if (f == null)
//            return cond;
//
//        final boolean[] changed = {false};
//        PREDICATE[] yy = Util.map(x -> {
//            PREDICATE<D> y = x.transform(f);
//            if (y != x)
//                changed[0] = true;
//            return y;
//        }, new PREDICATE[cond.length], cond);
//        if (!changed[0])
//            return cond;
//        else
//            return yy;
//    }


//    public @Nullable PREDICATE<D> without(PREDICATE<D> condition) {
//        PREDICATE[] x = ArrayUtils.remove(cond, PREDICATE[]::new, ArrayUtils.indexOf(cond, condition));
//        assert(x.length != cond.length);
//        return AND.the(x);
//    }


}