package nars.term.control;

import jcog.Util;
import jcog.data.list.Lst;
import nars.$;
import nars.Term;
import nars.term.Compound;
import nars.term.ProxyCompound;
import nars.term.atom.Atomic;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.lang.invoke.MethodHandles.constant;
import static nars.$.$$$;

public abstract class PREDICATE<X> extends ProxyCompound implements Predicate<X> {

    public static final Comparator<PREDICATE> CostIncreasing = (a, b) -> {
        if (a.equals(b)) return 0;
        float ac = a.cost(), bc = b.cost();
        if (ac > bc) return +1;
        else if (ac < bc) return -1;
        else return a.compareTo(b);
    };

    static final PREDICATE[] EmptyPredicateArray = new PREDICATE[0];
    static final MethodHandle CONSTANT_TRUE = constant(boolean.class, true);
    static final MethodHandle CONSTANT_FALSE = constant(boolean.class, false);
    static final MethodType VOID = MethodType.methodType(void.class);

    @Deprecated protected PREDICATE(Term term) {
        this((Compound)term);
    }

    @Deprecated protected PREDICATE(Atomic term) {
        this((Compound)$.p(term));
    }

    public PREDICATE(Compound term) {
        super(term);
    }

    PREDICATE(String term) {
        this((Compound)$$$(term));
    }

    public static <X> PREDICATE<X>[] transform(Function<PREDICATE<X>, PREDICATE<X>> f, PREDICATE<X>[] cache) {
        return Util.mapIfChanged(x -> x.transformPredicate(f), cache);
    }

    public static <X> List<PREDICATE<X>> andCompile(List<PREDICATE<X>> x) {
        if (x.size() < 2)
            return x;

        if (!(x instanceof Lst))
            x = new Lst<>(x); //hack ensure mutable, not a sublist etc

        int ppp;
//        boolean mod = false;
        restart: while ((ppp = x.size())>1)  {
            for (int i = 0; i < ppp; i++) {
                if (x.get(i).reduceIn(x)) {
//                    mod = true;
                    continue restart; //modified
                }
            }
            break;
        }
//        if (mod) {
            //re-sort because cost sort order may have changed
//            x.sort(CostIncreasing);
//        }
        return x;
    }

    public static <X> boolean transformPred(Function<PREDICATE<X>, PREDICATE<X>> f, PREDICATE<X>[] xx) {
        boolean change = false;
        for (int i = 0, xxLength = xx.length; i < xxLength; i++) {
            PREDICATE<X> x = xx[i];
            PREDICATE<X> y = x.transformPredicate(f);
            if (x != y) change = true;
            xx[i] = y;
        }
        return change;
    }

    @Override public PREDICATE<X> neg() {
        return new NegPredicate<>(this);
    }

//    static final MethodType mt =
////            //MethodType.methodType(boolean.class, MethodType.genericMethodType(1) );
//            MethodType.methodType(boolean.class, Object.class );

    private transient MethodHandle _method;
    @Deprecated
    public MethodHandle method(X x) {
        MethodHandle m = method();
        return x != null ? MethodHandles.insertArguments(m, 0, x) : m;
    }

    public MethodHandle method() {
        return (_method == null) ? _method = methodDefault() : _method;
    }

    private MethodHandle methodDefault() {
        try {
            Class<? extends PREDICATE> klass = getClass();


//            final Class<?> cc = getClass().getSuperclass();
//            if (!(cc.getGenericInterfaces()[0] instanceof ParameterizedType))
//                throw new WTF();
//
//            TypeVariable argClass = (TypeVariable) ((ParameterizedType) (cc.getGenericInterfaces()[0])).getActualTypeArguments()[0];

            MethodType mt = MethodType.methodType(boolean.class, Object.class);

//            if (x !=null) {
//                Object[] args = new Object[] {x};
//                return MethodHandles.insertArguments(m, 0, args);
//            } else
            return MethodHandles.privateLookupIn(klass, MethodHandles.lookup())
                    //.findVirtual(klass, "test", mt)
                    .findSpecial(klass, "test", mt, klass)
                    .bindTo(this);

        } catch (IllegalAccessException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public Consumer<X> run() {
        return new MHRunner<>(method());
    }

    public PREDICATE<X> transformPredicate(Function<PREDICATE<X>, PREDICATE<X>> f) {
        return f != null ? f.apply(this) : this;
    }

    /**
     * a relative global estimate (against its possible sibling PrediTerm's)
     * of the average computational cost of running the test method
     * warning: these need to return constant values for sort consistency
     */
    public float cost() {
        return Float.POSITIVE_INFINITY;
    }

    /** optimization reducer: pp is the list of predicates in which this appears.
     *  return true if the list has been modified (ex: remove this, replace another etc)
     */
    public boolean reduceIn(List<PREDICATE<X>> p) {
        return false;
    }

    @Deprecated
    public static final class MHRunner<X2> implements Consumer<X2> {

        private final MethodHandle m;

        MHRunner(MethodHandle m) {
            this.m = m.asType(MethodType.methodType(void.class, Object.class));
        }

        @Override
        public void accept(X2 x) {
            try {
                m.invokeExact(x);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
}