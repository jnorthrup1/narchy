package nars.term.control;

import jcog.Util;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;

import static java.lang.invoke.MethodHandles.constant;
import static nars.Op.SETe;


/**
 * parallel branching
 * <p>
 * TODO generify beyond only Derivation
 */
public class FORK<X> extends PREDICATE<X> {

    /*@Stable*/
    public final PREDICATE<X>[] branch;

    private FORK(SortedSet<? extends PREDICATE<X>> actions) {
        this(actions.toArray(PREDICATE.EmptyPredicateArray));
    }

    private FORK(PREDICATE<X>[] branches) {
        super(SETe.the(branches));
        this.branch = branches;
        assert (branches.length > 1);
    }

    @Override
    public MethodHandle method(X x) {
        MethodHandle s = Util.sequence(Util.arrayOf(i ->
                branch[i].method(x).asType(VOID),
                new MethodHandle[branch.length]), (Object[]) null);

        return MethodHandles.filterReturnValue(s, constant(boolean.class, true));
    }

//TODO
//    @Override
//    public MethodHandle method() {
//        MethodHandle s = Util.sequence(Util.arrayOf(i ->
//                        branch[i].method().asType(methodType(void.class, Object.class)),
//                new MethodHandle[branch.length]), null);
//
//        return MethodHandles.filterReturnValue(s, constant(boolean.class, true));
//    }

    /**
     * simple exhaustive impl
     */
    @Override
    public final boolean test(X x) {

        for (PREDICATE<X> b : branch)
            b.test(x);

        return true;
    }


    @Override
    public PREDICATE<X> transformPredicate(Function<PREDICATE<X>, PREDICATE<X>> f) {

        TreeSet<PREDICATE<X>> yy = null;

        for (int i = 0, branchLength = branch.length; i < branchLength; i++) {
            PREDICATE<X> x = branch[i];
            PREDICATE<X> y = x.transformPredicate(f);
            if (x!=y) {
                if (yy == null) {
                    yy = new TreeSet<>();
                    //noinspection ManualArrayToCollectionCopy
                    for (int k = 0; k < i; k++)
                        yy.add(branch[k]);
                }
            }
            if (yy!=null) yy.add(y);
        }

        return f.apply(yy!=null ? fork(yy) : this);
    }


    public static <X> PREDICATE<X> fork(Collection<? extends PREDICATE<X>> c) {
        return fork(c instanceof SortedSet ? (SortedSet) c : new TreeSet<>(c));

    }
    public static <X> PREDICATE<X> fork(SortedSet<? extends PREDICATE<X>> c) {
        return fork(c, FORK::new);
    }

    private static <X> PREDICATE<X> fork(SortedSet<? extends PREDICATE<X>> c, Function<SortedSet<? extends PREDICATE<X>>, ? extends PREDICATE<X>> builder) {
        return switch (c.size()) {
            case 0 -> null;
            case 1 -> c.first();
            default -> builder.apply(c);
        };
    }


}