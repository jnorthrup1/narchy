//package nars.term.control;
//
//import nars.$;
//import nars.term.Term;
//import nars.unify.Unify;
//import nars.unify.constraint.TermMatcher;
//
//import java.util.SortedSet;
//import java.util.function.Function;
//import java.util.function.Predicate;
//
///** tests a property of all subterms of a compound at a given path (AND) */
//public final class TermMatchAllSubterms<U extends Unify> extends AbstractTermMatch<U> {
//
//	public final Predicate<Term> match;
//	private final float cost;
//
//	public TermMatchAllSubterms(SortedSet<TermMatcher> matchers, boolean trueOrFalse, Function<U, Term> resolve, float resolveCost) {
//		this(TermMatcher.AND(matchers), trueOrFalse, resolve, resolveCost);
//	}
//
//	public TermMatchAllSubterms(TermMatcher match, boolean trueOrFalse, Function<U, Term> resolve, float resolveCost) {
//		super($.func("subtermsAND", match.name(resolve).negIf(!trueOrFalse)), resolve);
//		this.match = trueOrFalse ? match : match.negate();
//		this.cost = resolveCost + match.cost() * 2 /* # of subterms, estimate */;
//	}
//
//	@Override
//	public float cost() {
//		return cost;
//	}
//
//	@Override
//	protected boolean match(Term y) {
//		return y.subterms().AND(match);
//	}
//}
