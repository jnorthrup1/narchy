package nars.term.control;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import java.security.Permissions;
import java.security.ProtectionDomain;
import java.security.SecureClassLoader;
import java.util.function.Function;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ASM9;

/** TODO */
public class PredicateCompiler extends ClassVisitor {


    public PredicateCompiler(ClassWriter cw) {
        super(ASM9, cw);
        MethodVisitor v = cw.visitMethod(ACC_PUBLIC, "test", "(Deriver)" + "b", null, null);
        v.visitCode();
        //getKey.visitVarInsn(ALOAD, 0);
        //getKey.visitFieldInsn(GETFIELD, className, "key", keyClassType);
        //getKey.visitInsn(Type.getType(classPair.keyClass).getOpcode(IRETURN));
        //getKey.visitMaxs(1, 1);
        v.visitEnd();
    }



    public static Class<?> transform(String classname, Function<ClassWriter,ClassVisitor> v, JaninoRestrictedClassLoader cl) {
        return new JaninoRestrictedClassLoader().defineClass(classname, transform(classname, v));
    }

    private static final class JaninoRestrictedClassLoader extends SecureClassLoader {

        Class<?> defineClass(String name, byte[] b) {
            return defineClass(name, b, 0, b.length, new ProtectionDomain(null,
                    new Permissions(), this, null));
        }
    }
    public static byte[] transform(String classname, Function<ClassWriter,ClassVisitor> v) {
        try {
            ClassReader classReader = new ClassReader(classname);
            ClassWriter classWriter = new ClassWriter(classReader, ClassWriter.COMPUTE_FRAMES);
            ClassVisitor classVisitor = v.apply(classWriter);
            classReader.accept(classVisitor, ClassReader.SKIP_FRAMES);
            return classWriter.toByteArray();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


}