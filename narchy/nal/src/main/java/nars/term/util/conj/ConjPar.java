package nars.term.util.conj;

import jcog.TODO;
import jcog.Util;
import jcog.data.bit.MetalBitSet;
import nars.NAL;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TmpTermList;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.util.builder.TermBuilder;
import nars.time.Tense;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectByteHashMap;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

import static jcog.data.bit.MetalBitSet.bits;
import static nars.Op.*;
import static nars.term.atom.Bool.*;

/**
 * utilities for working with commutive conjunctions (DTERNAL, parallel, and XTERNAL)
 */
public enum ConjPar {;

    public static Term parallel(int dt, TmpTermList t, boolean sort, boolean inhBundle, TermBuilder B) {
        //assert(dt!=XTERNAL);

        int n = t.size();
        switch (n) {
            case 0: return True;
            case 1: return t.getFirst();
        }

        if (t.containsInstance(Null)) return Null;
        if (t.containsInstance(False)) return False;

        if (t.countGreaterThan(1, tt -> tt.unneg().SEQ()))
            return Null; //no multiple sequences in parallel

        if (inhBundle) {
            int e = t._inhExplode(true, B);
            if (e == -1)
                return False;
            else if (e == 1)
                n = t.size();
        }

        if (n == 2 && dt == DTERNAL) {
            Term p2 = parallel2Simple(t, sort, B); //fast 2-ary tests
            if (p2 != null) return p2;
        }

        Term d = disjunctive(t, dt, B);
        if (d != null) {
            t.clear();
            t.addFast(d); //HACK
        }

        Term xt = xternalDistribute(dt, t, B);
        if (xt != null) return xt;

        return parallel(t, dt, inhBundle, B);
    }

    @Nullable private static Term parallel2Simple(TmpTermList t, boolean sort, TermBuilder B) {

        Term a = t.get(0), b = t.get(1);
        if (a == True) return b;
        if (b == True) return a;

        if (a instanceof Neg != b instanceof Neg) {
            if (a.equalsNeg(b)) return False;
        } else {
            if (a.equals(b)) return a;
        }

        return simple(a) && simple(b) ?
            B.compound(CONJ, DTERNAL, sort, t.arrayTake())
            : null;
    }

//    private static void xternalEliminate(TmpTermList t) {
//
//        int n = t.size();
//        if (n < 2) return;
//
//        @Deprecated MetalBitSet rx = null;
//        for (int i = 0; i < n; i++) {
//            Term x = t.get(i);
//            if (x.CONJ() && x.dt() == XTERNAL) { //TODO bitset
//                ((rx == null) ? (rx = bits(n)) : rx).set(i);
//            }
//        }
//
//        if (rx != null && rx.cardinality() < n) {
//            MetalBitSet elim = null;
//            nextX:
//            for (int x = 0; x < n; x++) {
//                if (rx.test(x)) {
//                    Compound X = (Compound) t.get(x);
//                    Term x0 = X.sub(0);
//                    boolean repeat = X.subs() == 2 && x0.equals(X.sub(1));
//
//                    for (int y = 0; y < n; y++) {
//                        if (x == y || rx.test(y)) continue;
//                        Term Y = t.get(y);
//                        if (repeat) {
//                            if (Y.equals(x0)) {
//                                ((elim == null) ? elim = bits(n) : elim).set(x);
//                                continue nextX;
//                            } else if (Y.equalsNeg(x0)) {
//                                //contradiction
//                                t.clear();
//                                t.addFast(False);
//                                return;
//                            }
//                        }
//                        if (X.containsRecursively(Y)) {
//                            //test for complete absorption
//                            Predicate<Term> eqY = Y.equals();
//                            if (X.eventsAND((w, z) -> eqY.test(z) || (z.CONJ() && ((Compound) z).eventOf(Y)), 0, false, true)) {
//                                ((elim == null) ? elim = bits(n) : elim).set(y);
//                                break;
//                            }
//                        }
//                        //TODO partial/complete contradiction?
//                    }
//                }
//            }
//
//            if (elim != null)
//                t.removeAll(elim);
//        }
//    }

    private static boolean simple(Term a) {
        return a instanceof Atomic || !a.hasAny(Temporals | INH.bit);
    }

    private static @Nullable Term xternalDistribute(int dt, TmpTermList xx, TermBuilder builder) {

        //HACK complex XTERNAL disj pre-filter
        //TODO multiarity comparison
        int n = xx.size();

        for (int a = 0; a < n; a++) {
            Term A = xx.get(a);
            if (A instanceof Neg) {
                Term ux = A.unneg();
                if (ux.CONJ() && ux.dt() == XTERNAL) { //TODO bitset
                    for (int b = a + 1; b < n; b++) {
                        Term B = xx.get(b);
                        Subterms uux = ux.subterms();
                        if (uux.hasAll(B.unneg().structure())) {
                            Term xn = null;
                            for (Term v : uux) {
                                if (v instanceof Neg) {
                                    Term vu = v.unneg();
                                    if (vu.CONJ()) {
                                        if (xn == null) xn = B.neg();
                                        if (((Compound) vu).containsRecursively(xn)) {
                                            //TODO test the xternal's components as if they werent xternal to see if becomes False, if so then False
                                            Term r = builder.conj(/*dt,?*/ v, B);
                                            if (r.equals(B)) {
                                                return False; //because it will be negated and --X && X == False
                                            }/* else if (r.equalsNeg(xx.get(1))) {
                                        return True;
                                    }*/
                                        }
                                    }
                                }

                                if (v.equals(B)) {
                                    //eliminate
                                    Term aa = A
                                        .replace(v.neg(), False)
                                        .replace(v, True);
                                    if (aa == False || aa == Null)
                                        return aa;
                                    xx.setFast(a, aa);
                                    return builder.conj(dt, xx);
                                }

                            }
                        }
                    }
                }
            }
        }

        int xternalCount = 0;
        int lastXternal = -1;
        for (int i = 0; i < n; i++) {
            Term t = xx.get(i);
            if (t.CONJ() && t.dt() == XTERNAL) {
                lastXternal = i;
                xternalCount++;
            }
        }

        //distribute to XTERNAL

        if (xternalCount == 1) {
            //distribute to xternal components
            Term x = xx.remove(lastXternal);
            if (xx.isEmpty()) return x; //only element

            // y = ArrayUtil.remove(xx, lastXternal);
            Term Y = parallel(DTERNAL, xx, true, false, builder);
            if (Y == Null) return Null;
            if (Y == False) return False;
            if (Y == True) return x;
            Subterms xs = x.subterms();
            Term[] ys = Util.map(xxx -> builder.conj(dt, xxx, Y),
                    new Term[xs.subs()],
                    xs.arrayShared());

            //TODO factor XTERNAL like DTERNAL
            if (xx.subs()>0 && ys.length==2) {
                Term ys0 = ys[0], ys1 = ys[1];
                if (ys0.CONDS() && ys1.CONDS()) {
                    if ((((Compound) ys0).condOf(xs.sub(0)) && ((Compound) ys0).condOf(Y))) {
                        if ((((Compound) ys1).condOf(xs.sub(1)) && ((Compound) ys1).condOf(Y))) {
                            //leave distributed, as nothing was reduced
                            xx.add(x); //HACK restore
                            return null;
                        }
                    }
                }
            }

            return builder.conj(XTERNAL, ys);
        }

        return null;
    }

    @Deprecated public static @Nullable Term disjunctive(TmpTermList X, int dt, TermBuilder B) {
        int xxn = X.size();
        if (xxn < 2) return null;

        @Deprecated MetalBitSet disjPar = null, disjSeq = null;
        for (int i = 0; i < xxn; i++) {
            Term x = X.get(i);
            if (x instanceof Neg) {
                Term xu = x.unneg();
                if (xu.CONDS()) {
                    int xdt = xu.dt();
                    if (xdt == DTERNAL || xdt == 0)
                        ((disjPar == null) ? (disjPar = bits(xxn)) : disjPar).set(i);
                    else if (xdt != XTERNAL)
                        ((disjSeq == null) ? (disjSeq = bits(xxn)) : disjSeq).set(i);
                }
            }
        }

        if (disjPar == null && disjSeq == null) return null;


//        int xSeqDur = NAL.term.SEQ_CAREFUL ? X.max((ToIntFunction<Term>)Term::seqDur) : 0;

        Term y = _disjunctive(X, dt, disjPar, disjSeq, B);

//        if (seqChanged(y, xSeqDur))
//            return Null;

        return y;
    }

    /**
     * BAD
     * TODO simplify
     */
    @Nullable private static Term _disjunctive(TmpTermList X, int dt, MetalBitSet disjPar, MetalBitSet disjSeq, TermBuilder B) {
        Term[] x = X.array();
        int xxn = X.size();

        boolean changedOuter = false;
        //foreach disjunction:
        outer: for (int j = xxn - 1; j >= 0; j--) {
            if ((disjSeq == null || !disjSeq.test(j)) && (disjPar == null || !disjPar.test(j)))
                continue;

            Term ND = x[j]; assert(ND instanceof Neg);
            Compound D = (Compound) ND.unneg(); assert(D.CONDS());

            //compare sub-events with sibling events
            ConjList dd = null;
            boolean changedInner = false;
            for (int i = xxn - 1; i >= 0; i--) {
                if (i != j) {
                    Term xi = x[i];
                    if (D.hasAll(xi.unneg().structure() /*& ~CONJ.bit*/)) { //HACK bundled events will fail impossibleSubterm
                    //if (!D.impossibleSubTerm(xi)) {
                        if (dd == null) {
                            dd = ConjList.events(D, true, false);
                            dd.inhExploded(B);
                        }

                        if (dd.containsNeg(xi)) {
                            x[j] = True; //eliminated
                            changedOuter = true;
                            continue outer;
                        }

                        changedInner |= dd.removeAll(xi);
                    }
                }
            }
            if (changedInner) {

                Term E = (D.dt() == XTERNAL ? B.conj(XTERNAL, dd.toArrayRecycled(Term[]::new)) : dd.term(B)).neg();
                if (E == Null) return Null;
                if (E == False) return False;

                changedOuter = true;
                x[j] = E;
            }
        }

        if (changedOuter) {
            X.removeInstances(True);
            int xn = X.subs();
            if (xn == 0) return True;
            if (xn == 1) return X.sub(0);
            return parallel(dt, new TmpTermList(X) /*X*/, true, true, B); //changed, restart
        }

        if (disjPar == null) {
            int seqs;
            if (disjSeq != null) {
                seqs = disjSeq.cardinality();

                if (seqs > 1) {
                    //TODO if seqs < n and seqs > 1
                    //TODO n>2
                    //see if common prefix or suffix exists
                    if (xxn == 2 && seqs == xxn) {
                        ConjList[] ss = new ConjList[seqs];
                        int sj = 0;

                        //TODO optimization: if all have 2 events, they need to have equal eventRange otherwise it wont work

                        int minEvents = Integer.MAX_VALUE;
                        for (int i = 0; i < xxn; i++) {
                            if (disjSeq.test(i)) {
                                ConjList sx = ss[sj++] = ConjList.events(x[i].unneg(),  false, false);
                                minEvents = Math.min(sx.size(), minEvents);
                            }
                        }
                        boolean fwd = true; /*fwd only*/
                        //for (boolean fwd : new boolean[]{true, false}) {
                        int shared = 0;
                        matching:
                        while (shared < minEvents) {
                            long w = TIMELESS;
                            Predicate<Term> z = null;
                            for (int i = 0; i < sj; i++) {
                                ConjList ssi = ss[i];
                                int index = fwd ? shared : (ssi.size() - 1) - shared;
                                if (i == 0) {
                                    w = ssi.when(index);
                                    z = ssi.get(index).equals();
                                } else {
                                    if (ssi.when(index) != w) {
                                        shared--;
                                        break matching; //interval has now changed
                                    }
                                    if (!z.test(ssi.get(index)))
                                        break matching;
                                }
                            }
                            shared++;
                        }
                        if (shared > 0) {
                            int s0s = ss[0].size();
                            if (shared >= s0s) {
                                if (NAL.DEBUG)
                                    throw new TODO(); //?? what does it mean
                                return Null;
                            }
                            Term common = (fwd ?
                                    ss[0].subEvents(0, shared) :
                                    ss[0].subEvents(s0s - shared, s0s)).term(B);
                            Term[] st = new Term[sj];
                            int offset = Tense.occToDT(ss[0].when(shared));
                            for (int i = 0; i < sj; i++) {
                                ConjList ssi = ss[i];
                                if (fwd) ssi.removeBelow(shared);
                                else ssi.removeAbove(shared);
                                st[i] = ssi.term(B);
                            }
                            Term orred = DISJ(st);
//                                if (!fwd && orred.unneg().seqDur() != 0)
//                                    Util.nop(); //TEMPORARY
                            return (fwd ?
                                    ConjSeq.conjAppend(common, offset - common.seqDur(), orred, B)
                                    :
                                    ConjSeq.conjAppend(orred, offset - orred.seqDur(), common, B)
                            ).neg();
                            //TODO combine with other non-seq terms
                        }
                    }
                }
            }
            return null;
        }

        int d = disjPar.cardinality();
        if (d <= 1)
            return null;

        Term[] dxx = new Term[d];
        int od = 0;
        TmpTermList cxx = null;//d != n ? new Term[n - d + 1 /* for extra slot */] : null;  int oc = 0; //conjunctive component
        ObjectByteHashMap<Term> i = null;
        boolean anyFull = false;
        for (int j = 0; j < xxn; j++) {
            Term J = x[j];
            if (disjPar.test(j)) {
                Term dc = dxx[od++] = J;
                for (Term ct : dc.unneg().subterms()) {
                    Term ctn;
                    if (i != null && i.containsKey(ctn = ct.neg())) {
                        //disqualify both permanently since factoring them would cancel each other out
                        i.put(ct, Byte.MIN_VALUE);
                        i.put(ctn, Byte.MIN_VALUE);
                    } else {
                        if (i == null) i = new ObjectByteHashMap<>(d);
                        anyFull |= (d == i.updateValue(ct, (byte) 0, v -> (v >= 0) ? (byte) (v + 1) : v));
                    }
                }
            } else {
                if (cxx == null) cxx = new TmpTermList(xxn - j);
                cxx.addFast(J);
            }
        }
        if (!anyFull)
            return null;

        i.values().removeIf(b -> b < d);
        if (!i.isEmpty()) {
            Set<Term> common = i.keySet();
            int cs = common.size();
            Term factor = cs > 1 ? B.conj(common.toArray(EmptyTermArray)) : common.iterator().next();

            if (cxx != null) {
                if (cxx.contains(factor))
                    factor = True;
                else if (cxx.containsNeg(factor)) {
                    //factor is equivalent to top-level term; eliminate
                    return _conj(cxx, dt, B);
                }
            }

            if (factor instanceof Bool) {
                if (factor == Null) return Null;
                if (factor == False) return False;
                //TODO if True
//                        return factor;
            }

            Predicate<Term> commonMissing = ((Predicate<Term>) common::contains).negate();

            int mm = dxx.length;
            for (int m = 0; m < mm; m++) {

                Term[] xxj = dxx[m].unneg().subterms().subs(commonMissing);

                if (xxj.length != 0) {
                    if ((dxx[m] = (xxj.length == 1 ? xxj[0] : B.conj(xxj)).neg()) == False)
                        break; //eliminated
                } else {
                    dxx[m] = False; //eliminated
                    break;
                }
            }
            Term disj = B.conj(dxx).neg();
            if (disj == Null)
                return Null;

            Term yd = B.conj(dt, factor, disj).neg();
            if (yd != Null && cxx != null) {
                cxx.add(yd);
                return _conj(cxx, dt, B);
            }
            return yd;
        }
        return null;
    }

    private static Term _conj(TmpTermList c, int dt, TermBuilder B) {
        return c.subs() == 1 ? c.getFirst() : B.conj(dt, c);
    }


    public static Term parallel(TmpTermList pn, int dt, boolean inhBundle, TermBuilder B) {
        int s = pn.size();
        if (s == 0) return True;
        if (s == 1) return pn.getFirst();

        if (ConjTree.complexPar(pn)) {
            try (ConjTree c = new ConjTree()) {
                return c._parallel(pn, dt, inhBundle, B);
            }
        } else {
            return _parallel(pn, inhBundle, B);
        }
    }

    public static Term _parallel(TmpTermList pn, boolean inhBundle, TermBuilder B) {
        Term err = null;
        int n = pn.subs();

        int xSeqDur = n > 0 && NAL.term.SEQ_CAREFUL ?
            pn.max((ToIntFunction<Term>) Term::seqDur) : 0;

        if (n > 1) {
            pn.sortThis();

            if (inhBundle) {
                boolean bundled;
                if (bundled = ConjBundle.inhBundle(pn.array(), null, n, B)) {
                    if (pn.containsInstance(Null)) { err = Null;
                    } else if (pn.containsInstance(False)) {
                        err = False;
                    } else {
                        pn.removeInstances(True);
                    }
                    n = pn.subs();
                }
                if (err == null) {
                    if (n > 1 && ConjTree.complexPar(pn)) {
                        _flatten(pn);
                        err = parallel(DTERNAL, pn,
                                bundled /* re-sort if bundled changed anything */,
                                false /*prevents cycle*/, B);
                        n = pn.subs();
                    }
                }
            }
        }
        if (err == null) {
            err = switch (n) {
                case 0 -> True;
                case 1 -> pn.getFirst();
                default ->
                    pn.containsInstance(Null) ? Null //HACK what causes this Null? detect earlier?
                    :
                    B.compoundNew(CONJ, DTERNAL, pn.arrayTake());
            };
        }

        return seqChanged(err, xSeqDur) ? Null : err;
    }

    /** HACK flatten any inner parallels */
    @Deprecated private static void _flatten(TmpTermList pn) {
        TmpTermList pnExtra = new TmpTermList();
        pn.removeIf(z -> {
            if (z.CONJ() && z.dt()==DTERNAL && !z.SEQ()) {
                pnExtra.addAll(z.subtermsDirect());
                return true;
            }
            return false;
        });
        if (!pnExtra.isEmpty()) {
            pn.addAll((Subterms)pnExtra);
            pnExtra.delete();
        }
    }

    private static boolean seqChanged(@Nullable Term x, int xSeqDur) {
        return  x!=null
                && NAL.term.SEQ_CAREFUL
                && !(x instanceof Bool)
                && xSeqDur > 0
                && x.seqDur() != xSeqDur;
    }
}