package nars.term.util;

import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.Img;
import nars.term.Neg;
import nars.term.atom.Bool;
import nars.term.util.builder.TermBuilder;
import nars.term.util.conj.ConjList;
import nars.term.util.conj.ConjSeq;
import nars.time.Tense;

import static nars.Op.*;
import static nars.term.atom.Bool.*;

//import nars.term.var.ellipsis.Ellipsis;

/**
 * STATEMENTS
 * inheritance -->
 * similarity  <->
 * implication ==>
 */
public enum Statement {
    ;

    public static Term statement(TermBuilder B, Op op, int dt, Term subj, Term pred) {
        if (subj == Null || pred == Null) return Null;

        if (subj instanceof Img || pred instanceof Img)
            throw new TermException("statement can not have image subterm", new TermList(subj, pred));

        if (op == IMPL) {
            if (subj == True) return pred;
            if (subj == False) return True; //according to tautological impl truth table
                //return Null; //return predicate.neg();

            if (pred == True) return True;
            if (pred == False) return False; //subject.neg();

            if (!NAL.IMPLICATION_SUBJECT_CAN_CONTAIN_IMPLICATION && subj.hasAny(IMPL))
                return Null; //throw new TODO();

            if (dt == DTERNAL) dt = 0; //HACK temporarily use dt=0
        }

        if (op == INH || op == SIM) {
//            if (NAL.DEBUG) {
//                if (subj.TEMPORAL_VAR() || pred.TEMPORAL_VAR())
//                    throw new WTF(); //TEMPORARY
//            }

//            if (subject.hasAny(IMPL) || predicate.hasAny(IMPL))
//                return Null;
        }

        boolean parallel = dt == DTERNAL || dt == 0;
        if (parallel) {
            if (op!=IMPL) {
                if (subj.equals(pred))
                    return True;
            } else {
                //TODO would need to test if subj is a sequence, which is done towards the end for IMPL
            }
            if (subj instanceof Bool || pred instanceof Bool) {
                if (subj == True  && pred == False) return False;
                if (subj == False && pred == True)  return False;
            }
        }

        if (subj instanceof Bool || pred instanceof Bool)
            return Null;

        boolean negate = false;

        if (op == IMPL) {
            if (pred instanceof Neg) {
                pred = pred.unneg();
                negate = true;
            }

            if (pred.IMPL()) {

                //not when only inner xternal, since that transformation destroys temporal information

                Term inner = pred.sub(0);
                Term newPred = pred.sub(1);

                Term newSubj = ConjSeq.conjAppend(subj, dt, inner, B);
                if (newSubj == Null) return Null;

                int newDT = pred.dt();

                if (newPred instanceof Neg) {
                    newPred = newPred.unneg();
                    negate = !negate;
                }
                if (dt != newDT || !newSubj.equals(subj) || !newPred.equals(pred))
                    return statement(B, IMPL, newDT, newSubj, newPred).negIf(negate); //recurse
            }

            if (!pred.CONDABLE())
                return Null;
        } else if (op == INH || op == SIM) {
            if (subj instanceof Neg) {
                negate = true;
                subj = subj.unneg();
            }
            if (pred instanceof Neg) {
                negate = !negate;
                pred = pred.unneg();
            }
        }

        return statementMiddle(B, op, dt, subj, pred, negate);
    }

    private static Term statementMiddle(TermBuilder B, Op op, int dt, Term subj, Term pred, boolean negate) {
        //TODO simple case when no CONJ or IMPL are present

        if (op == IMPL && dt != XTERNAL) {
            if (reducibleImpl(subj, pred)) {

                //subtract common subject components from predicate

                int subjRange = subj.seqDur();

                ConjList predEvents = ConjList.events(pred, subjRange + dt, true, false).inhExploded(B);

                int removed = predEvents.removeAll(subj.unneg(), 0, !(subj instanceof Neg));
                if (removed == -1)
                    return False.negIf(negate);

                Term predNew = null;
                long shift = TIMELESS;

                if (removed == +1) {
                    predNew = predEvents.term(B);
//                    if (pred.equals(predNew)) //no change
//                        predNew = null;
//                    else
                    shift = predEvents.shift();
                }

                predEvents.delete(); //free the ConjBuilder

                if (predNew != null && !pred.equals(predNew)) {

                    if (predNew instanceof Bool)
                        return predNew.negIf(negate); //collapse

                    if (shift == ETERNAL) {
                        //??
                        dt = 0;
                    } else {
                        dt = Tense.occToDT(shift - subjRange);

//                        if (predNew.dt() == 0 && pred.dt() == DTERNAL && ((Compound) pred).equalTerms((Compound)predNew))
//                            predNew = ((Compound) predNew).dt(DTERNAL, B); //HACK return to dternal
                    }

                    if (predNew instanceof Neg) { //attempt to exit infinite loop of negations
                        predNew = predNew.unneg();
                        negate = !negate;
                    }

//                    if (!predNew.equals(pred)) //HACK check again
                    return statement(B, IMPL, dt, subj, predNew).negIf(negate); //recurse

//                    pred = newPred; //FAST but unsafe
                }
            }
        }

        return statementNew(op, dt, subj, pred, B).negIf(negate);
    }

    private static boolean reducibleImpl(Term subj, Term pred) {
        assert (!(pred instanceof Neg));
        Term su = subj.unneg(), pu = pred.unneg();
        return (reducibleEventContainer(su) || reducibleEventContainer(pu)) &&
                Term.commonEventStructure(su, pu);
    }

    private static boolean reducibleEventContainer(Term su) {
        return su.dt() != XTERNAL && su.CONDS();
    }

    /** final step */
    private static Term statementNew(Op op, int dt, Term subject, Term predicate, TermBuilder B) {

        if (op == IMPL) {
            if (dt == 0) dt = DTERNAL; //HACK generalize to DTERNAL ==>
//            if (!subject.unneg().EVENTABLE())
//                return Null;
//            if (!predicate/*.unneg()*/.EVENTABLE())
//                return Null;
        } else {
            if (!inhSimComponentValid(subject) || !inhSimComponentValid(predicate))
                return Null;
        }

        if (dt == DTERNAL) {
            Term su = subject.unneg();
            if (op!=IMPL || !su.CONJ() || ((su.dt()!=XTERNAL && !su.SEQ()))) {
                Term pu = predicate.unneg();
                if (su.equals/*Root*/(pu))
                    return True.negIf(subject instanceof Neg != predicate instanceof Neg);
            }

            if (op != IMPL) {
                if (subject.CONJ() && subject.volume() > predicate.volume()) {
                    Subterms z = subject.subterms();
                    if (z.contains(predicate)) return True;
                    if (z.containsNeg(predicate)) return False;
                }
                if (predicate.CONJ() && predicate.volume() > subject.volume()) {
                    Subterms z = predicate.subterms();
                    if (z.contains(subject)) return True;
                    if (z.containsNeg(subject)) return False;
                }

                if (Terms.rCom(subject, predicate))
                    return Null; //HACK recursive

            }
        }

        if (op == SIM) {
            if (subject.compareTo(predicate) > 0) {
                //swap to natural order
                Term x = predicate;
                predicate = subject;
                subject = x;
            }
        }
        if (op == INH && Image.isRecursive(subject, predicate))
            return Null;

        return B.compoundNew(op, dt, subject, predicate);
    }

    private static boolean inhSimComponentValid(Term x) {
        if (x instanceof Neg) x = x.unneg();

        if (x.hasAny(IMPL))
            return false; //HACK

        if (!x.TEMPORALABLE()) return true;

        boolean b = x.dt() == DTERNAL && ((Compound) x).AND(Statement::inhSimComponentValid);
        return b;
        //return x.seqDur(true)==0;
        //return !x.CONJ() || (x.dt() == DTERNAL && ((Compound)x).AND(Statement::inhSimComponentValid));
    }

//    private static Term eliminateSectCommon(TermBuilder B, Op op, int dt, boolean negate, Subterms ssub, Subterms psub, Term[] common) {
//        int cn = common.length;
//        if (cn == ssub.subs() || cn == psub.subs())
//            return Null; //contained entirely by the other //True; //TODO negate
//        Predicate<Term> notCommon = common.length > 1 ?
//            z -> ArrayUtil.indexOf(common, z) == -1 :
//            z -> !common[0].equals(z);
//        return statement(B, op, dt,
//            B.conj(ssub.subsIncluding(notCommon)),
//            B.conj(psub.subsIncluding(notCommon))).negIf(negate);
//    }

//    private static boolean recursive(Op op, Term subject, Term predicate) {
//
//        return ;
//
//        //|| (dt == 0) /* allow parallel IMPL unless there is a sequence that could separate the events from overlap */
//        //|| (dt == 0 && !Conj.isSeq(subject) && !Conj.isSeq(predicate)
//        //more fine grained inh/sim recursion test
////            final int InhOrSim = INH.bit | SIM.bit;
////            if (op.isAny(InhOrSim)) {
////                if (subject instanceof Compound && subject.isAny(InhOrSim)) {
////                    if (predicate instanceof Compound && predicate.op()==CONJ) {
////                        if (((Compound) predicate).containsPosOrNeg(subject.sub(0)) || ((Compound) predicate).containsPosOrNeg(subject.sub(1)))
////                            return true;
////                    }
////                    if (predicate.equals(subject.sub(0)) || predicate.equals(subject.sub(1)))
////                        return true;
////                }
////                if (predicate instanceof Compound && predicate.isAny(InhOrSim)) {
////                    if (subject instanceof Compound && subject.op()==CONJ) {
////                        if (((Compound) subject).containsPosOrNeg(predicate.sub(0)) || ((Compound) subject).containsPosOrNeg(predicate.sub(1)))
////                            return true;
////                    }
////                    if (subject.equals(predicate.sub(0)) || subject.equals(predicate.sub(1)))
////                        return true;
////
////                }
////            }
//
//
//    }

}