package nars.term.util.transform;

import nars.Op;
import nars.Term;
import nars.task.NALTask;
import nars.task.proxy.SpecialTermTask;
import nars.term.Compound;
import nars.term.Termed;
import nars.term.Variable;
import nars.term.var.NormalizedVariable;

import java.util.function.Predicate;

/** shifts var indices of a compound
 * TODO store ranges for individual variable types to avoid unnecessary shifting when disjoint or partially disjoint
 * */
public class VariableShift extends VariableNormalization implements Predicate<Compound> {

	private final int varBits;

	public VariableShift() {
		this(Op.Variables);
	}

	public VariableShift(int varBits) {
		super();
		this.varBits = varBits;
	}

	/** post processes the beliefterm
	 * @param x the term being shifted
	 * @param y the base or root term that x will be modified against
	 * */
	public static <T extends Termed> T varShift(T x, Term y, boolean shiftEquals, boolean shiftContains) {

		//TODO probabailty of structural link not requiring shift

		Term xTerm = x.term();

		if (!shiftEquals && y.equals(xTerm))
			return x;

		int xv = (Op.Variables & xTerm.structure());

		int vCommon = xv & y.structure();
		if (vCommon != 0) {

			if (!shiftContains && y instanceof Compound && ((Compound)y).containsRecursively(xTerm))
				return x;

			Term yTerm = new VariableShift(vCommon).shift(y).apply(xTerm);
			if (!yTerm.equals(xTerm)) {
				Termed z = x instanceof Term ?
					yTerm :
					SpecialTermTask.proxyUnsafe((NALTask) x, yTerm).copyMeta((NALTask) x);

				if (z != null)
					return (T) z;
			}
		}
		return x;
	}

	@Override
	public boolean test(Compound superterm) {
		return superterm.hasAny(varBits);
	}

	public final VariableShift shift(Term y) {
		if (y instanceof Compound) return shift((Compound)y);
		else if (y instanceof NormalizedVariable) VariableShift.this.shift((NormalizedVariable) y);

		return this;
	}

	private void shift(NormalizedVariable y) {
		map.shift(y, varBits);
	}

	private VariableShift shift(Compound y) {
//		int[] minmax = { Integer.MAX_VALUE, Integer.MIN_VALUE };
		y.subtermsDirect().ANDrecurse(this, z -> {
			if (z instanceof NormalizedVariable)
				shift((NormalizedVariable) z);

//			if (z instanceof Variable && z.isAny(varBits)) {
//
//				//TODO else: CommonVariable - anything necessary here?
//			}
			return true;
		}, null);
//		offset = Math.max(offset, minmax[1]);
		return this;
	}

	@Override
	public Term applyVariable(Variable x) {
		return x.isAny(varBits) ? super.applyVariable(x) : x;
	}

}