package nars.term.util;

import jcog.Util;
import nars.Term;
import nars.term.Compound;
import nars.util.SoftException;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static nars.Op.INH;
import static nars.Op.SIM;

/** utilities for extracting the DT shape of a term's DT components */
public final class DTVector implements Predicate<Term> {

    private transient int[] _y;
    transient int yi = -1;

    public static int count(Term t) {
        if (!temporalable.test(t)) return 0;

        DTCounter c = new DTCounter();
        t.recurseTermsOrdered(temporalable, c, null);
        return c.count;
    }

    public void close() {
        _y = null;
    }

    @Override
    public boolean test(Term X) {
        if (X.TEMPORAL()) {
            _y[yi++] = X.DT();
            return yi != _y.length; //done?
        }
        return true;
    }

    private final static Predicate<Term> temporalable =
            x -> !x.isAny(INH.bit | SIM.bit) && x.TEMPORALABLE();

    @Deprecated public static final class ConceptShapeException extends SoftException {
    }


    public boolean set(Compound x, int[] y) throws ConceptShapeException {
        return set(x, y, 0);
    }

    public boolean set(Compound x, double[] y, int offset) throws ConceptShapeException {
        if (this._y==null || this._y.length!=y.length-offset)
            this._y = new int[y.length-offset];

        if (!set(x, (int[])null, 0))
            return false;
        for (int i = 0; i < _y.length; i++)
            y[i+offset] = _y[i];
        return true;
    }

    private boolean set(Compound x, @Nullable int[] y, int offset) throws ConceptShapeException {
        if (y!=null) _y = y;

        yi = offset;
        x.recurseTermsOrdered(temporalable, this, null);
        var match = yi == _y.length;

        if (y!=null) _y = null;

        return match;
    }

    public static double pctDiff(double[] a, double[] b, int from, int to) {
        double dtDiff = 0;
        for (int i = from; i < to; i++)
            dtDiff += pctDiff(a, b, i);
        return dtDiff;
    }


    private static double pctDiff(double[] a, double[] b, int i) {
        double aDT = a[i], bDT = b[i];
        return aDT == bDT ? 0 :
                Util.pctDiff(aDT, bDT);
    }

    private static final class DTCounter implements Predicate<Term> {
        private int count;

        @Override
        public boolean test(Term x) {
            if (x.TEMPORAL()) //and dt!=DTERNAL?
                count++;
            return true;
        }
    }
}