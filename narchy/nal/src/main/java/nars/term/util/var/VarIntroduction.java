package nars.term.util.var;

import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.atom.Bool;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Random;

abstract class VarIntroduction {

    /** returns null if not applicable */
    @Nullable
    public final Term apply(Compound x, Random rng, @Nullable Map<Term, Term> retransform) {

        if (x.volume() < 3 /*|| x.complexity() < 2*/)
            return null;

        Term[] xx = options(x.subtermsDirect());
        if (xx.length == 0)
            return null;

        Term src = xx.length > 1 ? choose(x, xx, rng) : xx[0];
        Term tgt = introduce(x, src);
        if (tgt != null && !(tgt instanceof Bool)) {
            Term y = x.replace(src, tgt);
            if (y != null && y.CONCEPTUALIZABLE() && !y.equals(x)) {
                if (retransform!=null) {
                    //TODO may not survive normalization
                    retransform.put(y, x);
                    //retransform.put(tgt, src);
                }
                return y;
            }
        }

        return null;
    }

    /** determine the choice of subterms which can be replaced with a variable */
    protected abstract @Nullable Term[] options(Subterms input);

    protected abstract Term choose(Term x, Term[] y, Random rng);

    /**
     * provides the next terms that will be substituted in separate permutations; return null to prevent introduction
     */
    protected abstract @Nullable Term introduce(Term input, Term selection);
}