package nars.term.util.builder;

import jcog.Util;
import jcog.memoize.Memoizers;
import nars.Term;
import nars.term.Compound;

import java.util.function.Function;
import java.util.function.UnaryOperator;

/** memoizes certain target operations in addition to interning */
public class MemoizingTermBuilder extends SeparateInterningTermBuilder {

    private final UnaryOperator<Term> ROOT, NORMALIZE;

    static final class MemoizedUnaryTermFunction  {

        private final Term term;
        private final int hash;
        @Deprecated private final UnaryOperator<Term> f;

        private MemoizedUnaryTermFunction(UnaryOperator<Term> f, Term x) {
            super();
            this.f = f;
            this.term = x;
            this.hash = Util.hashCombine(term, f);
        }
        @Override public int hashCode() { return hash; }

        @Override
        public boolean equals(Object o) {
//            if (this == o) return true;
            //if (!(o instanceof MemoizedUnaryTermFunction)) return false;
            MemoizedUnaryTermFunction that = (MemoizedUnaryTermFunction) o;
            return /*hash == that.hash && */f==that.f && term.equals(that.term);
        }
    }

    private final Function<MemoizedUnaryTermFunction, Term> memo;

    public MemoizingTermBuilder(TermBuilder builder) {
        this(atomCapacityDefault, compoundCapacityDefault, volMaxDefault, builder);
    }

    private MemoizingTermBuilder(int atomCap, int compoundCap, int volInternedMax, TermBuilder builder) {
        super(atomCap, compoundCap, volInternedMax, builder);

        this.ROOT = x -> super.root((Compound) x);
        this.NORMALIZE = x -> super.normalize((Compound) x, (byte)0);

        memo = Memoizers.the.memoize("memo", j -> j.f.apply(j.term), compoundCap);
    }

    @Override
    public Compound normalize(Compound x, byte varOffset) {
		return varOffset == 0 && internable(x) ?
            (Compound) memo.apply(new MemoizedUnaryTermFunction(NORMALIZE, x)) :
            super.normalize(x, varOffset);
    }

    @Override
    public Term root(Compound x) {
		return internable(x) ?
            memo.apply(new MemoizedUnaryTermFunction(ROOT, x)) :
            super.root(x);
    }

    private boolean internable(Compound x) {
        return x.volume() < compoundVolMax && x.internable();
    }


}