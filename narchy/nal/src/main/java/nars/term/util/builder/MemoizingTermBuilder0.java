//package nars.term.util.builder;
//
//import jcog.memoize.byt.ByteKeyExternal;
//import nars.term.Compound;
//import nars.term.Term;
//
//import java.util.function.Function;
//
///** memoizes certain target operations in addition to interning */
//public class MemoizingTermBuilder0 extends SeparateInterningTermBuilder {
//
//    static final class InternedCompoundTransform extends ByteKeyExternal {
//        public final Term term;
//
//        public InternedCompoundTransform(Term x) {
//            super();
//            this.term = x;
//            x.write(key);
//            commit();
//        }
//    }
//
//    //TODO <Term,Term>
//    private final Function<InternedCompoundTransform, Term> normalize;
//    private final Function<InternedCompoundTransform, Term> root;
//
//    public MemoizingTermBuilder0(TermBuilder builder) {
//        this(atomCapacityDefault, compoundCapacityDefault, volMaxDefault, builder);
//    }
//
//    public MemoizingTermBuilder0(int atomCap, int compoundCap, int volInternedMax, TermBuilder builder) {
//        super(atomCap, compoundCap, volInternedMax, builder);
//
//
//        root = memoizer("root", j -> super.root((Compound) j.term), compoundCap);
//
//        normalize = memoizer("normalize", j -> super.normalize((Compound) j.term, (byte) 0), compoundCap);
//
////        concept = newOpCache("concept", j -> super.concept((Compound) j.sub0()), cacheSizePerOp);
//
//    }
//
//    @Override
//    public Compound normalize(Compound x, byte varOffset) {
//		return varOffset == 0 && internable(x) ?
//            (Compound) normalize.apply(new InternedCompoundTransform(x)) :
//            super.normalize(x, varOffset);
//    }
//
//    @Override
//    public Term root(Compound x) {
//		return internable(x) ? root.apply(new InternedCompoundTransform(x)) : super.root(x);
//    }
//
//    private boolean internable(Compound x) {
//        return x.volume() < compoundVolMax && x.internable();
//    }
//
//
//}
