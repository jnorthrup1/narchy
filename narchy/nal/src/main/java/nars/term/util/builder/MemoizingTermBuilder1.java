//package nars.term.util.builder;
//
//import nars.term.Compound;
//import nars.term.Term;
//
//import java.util.function.Function;
//
///** memoizes certain target operations in addition to interning */
//public class MemoizingTermBuilder1 extends SeparateInterningTermBuilder {
//
//
//    static class TermCache {
//        Term root = null;
//        Term normalized = null;
//
//        public Term normalized(Compound x, MemoizingTermBuilder1 B) {
//            Term y = normalized;
//            if (y!=null) return y; else return normalized = B.normalize(x, (byte) 0);
//        }
//        public Term root(Compound x, MemoizingTermBuilder1 B) {
//            Term y = root;
//            if (y!=null) return y; else return root = B.root(x);
//        }
//    }
//
//    private final Function<MemoizingTermBuilder0.InternedCompoundTransform, TermCache> memo;
//
//    public MemoizingTermBuilder1(TermBuilder builder) {
//        this(atomCapacityDefault, compoundCapacityDefault, volMaxDefault, builder);
//    }
//
//    public MemoizingTermBuilder1(int atomCap, int compoundCap, int volInternedMax, TermBuilder builder) {
//        super(atomCap, compoundCap, volInternedMax, builder);
//
//        memo = memoizer("memo", j -> new TermCache(), compoundCap*2);
//    }
//
//    @Override
//    public Term normalize(Compound x, byte varOffset) {
//        if (varOffset != 0 || !internable(x))
//            return super.normalize(x, varOffset);
//        else
////        if (x.PROD()) {
//            return memo.apply(new MemoizingTermBuilder0.InternedCompoundTransform(x)).normalized(x, this);
////        } else {
////            Subterms xx = x.subtermsDirect();
////            Term y = memo.apply(new MemoizingTermBuilder0.InternedCompoundTransform($.pFast(xx))).normalized(x, this);
////            Subterms yy = ((Compound) y).subtermsDirect();
//////            if (xx.equals(yy)) {
//////                return x; //no change
//////            } else {
////                Term z = compoundNNew(x.op(), x.dt(), yy);
////                Subterms zz = ((Compound) z).subtermsDirect();
//////                if (!zz.isNormalized())
//////                    zz.setNormalized(); //HACK?
////
////                return z;
//////            }
////        }
//    }
//
//
//    @Override
//    public Term root(Compound x) {
//		return internable(x) ?
//            memo.apply(new MemoizingTermBuilder0.InternedCompoundTransform(x)).root(x, this) :
//            super._root(x);
//    }
//
//    protected Term _normalize(Compound x) {
//        return interned(super.normalize(x, (byte)0));
//    }
//    protected Term _root(Compound x) {
//        return interned(super._root(x));
//    }
//    private boolean internable(Compound x) {
//        return x.volume() < compoundVolMax && x.internable();
//    }
//
//
//}
