package nars.term.util.transform;

import nars.Term;
import nars.term.Compound;
import nars.term.util.builder.TermBuilder;
import org.jetbrains.annotations.Nullable;

public abstract class MapSubstWithStructFilter extends Subst {

    private final int structure;

    MapSubstWithStructFilter(int structure, TermBuilder B) {
        super(B);
        this.structure = structure;
        assert(structure!=0);
    }

    @Override
    public @Nullable Term applyCompound(Compound x) {
        return x.hasAny(structure) ? super.applyCompound(x) : x;
    }

}