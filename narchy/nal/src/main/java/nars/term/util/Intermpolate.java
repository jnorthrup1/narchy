package nars.term.util;

import jcog.Util;
import jcog.pri.Prioritized;
import jcog.signal.meter.SafeAutoCloseable;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.util.conj.ConjList;
import nars.term.util.transform.RecursiveTermTransform;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

import static java.lang.Float.POSITIVE_INFINITY;
import static nars.Op.*;
import static nars.term.atom.Bool.Null;

/**
 * temporal target intermpolation
 */
public enum Intermpolate {
	;

	public static Term intermpolate(/*@NotNull*/ Compound a, /*@NotNull*/ Compound b, float aProp, NAL nar) {
		return intermpolate(a, b, aProp, nar.dtDither());
	}
	public static Term intermpolate(/*@NotNull*/ Compound a, /*@NotNull*/ Compound b, float aProp, int dtDither) {
		return intermpolate(a, b, aProp, 0, dtDither);
	}


	/**
	 * heuristic representing the difference between the dt components
	 * of two temporal terms.
	 * 0 means they are identical or otherwise match.
	 * 1 means entirely different
	 * between 0 and 1 means some difference
	 * <p>
	 * XTERNAL matches anything
	 */
	static float dtDiff(Term a, Term b) {
		if (a == b) return 0;

		if (a instanceof Neg) {
			if (!(b instanceof Neg))
				return POSITIVE_INFINITY;
			a = a.unneg();
			b = b.unneg();
			if (a == b) return 0;
		}

		if (a instanceof Compound != b instanceof Compound)
			return POSITIVE_INFINITY;

		int ao = a.opID();
		if (ao != b.opID())
			return POSITIVE_INFINITY;

		double dSubterms;
		Subterms as = a.subtermsDirect(), bs = b.subtermsDirect();
		if (ao == CONJ.id && (a.SEQ() || b.SEQ())) {

			boolean ax = a.dt()==XTERNAL, bx = b.dt()==XTERNAL;
			if (ax||bx) {

				dSubterms = dtDiff(as, bs); //compare XTERNAL's as parallel

			} else if (!as.equalTerms(bs))
				return DiffSeq.d((Compound)a, (Compound)b);
			else
				dSubterms = 0;

		} else {
			dSubterms = dtDiff(as, bs);
			if (!Double.isFinite(dSubterms))
				return POSITIVE_INFINITY;
		}

		return (float)(dtDiff(a.dt(), b.dt()) + dSubterms);
	}

	public static float dtDiff(int adt, int bdt) {
		if (adt == bdt) return 0;

		if (adt == XTERNAL || bdt == XTERNAL) {
			//dDT = 0.25f; //undercut the DTERNAL case
			return Prioritized.EPSILON * 2;
		} else {
			if (adt == DTERNAL) adt = 0;
			if (bdt == DTERNAL) bdt = 0; //HACK
			return Math.abs(adt - bdt);
			//double range = Math.max(Math.abs(adt), Math.abs(bdt));
//            float mean = (adt+bdt)/2.0f;
//			return Math.max(Math.abs(adt - mean), Math.abs(bdt - mean));
			//return (float) (maxDelta / Math.max(1, range));
		}
	}

	/** untested */
	private static Term intermpolate1(/*@NotNull*/ Compound a,  /*@NotNull*/ Compound b, float aProp, float curDepth, int dtDither) {
		DTVector v = new DTVector();

		int nMax = 16;

		int[] ad = new int[nMax];  Arrays.fill(ad, XTERNAL);
		v.set(a, ad);

		int n = v.yi;
		int[] bd = new int[n];  Arrays.fill(bd, XTERNAL);
		v.set(b, bd);
		if (v.yi!=n)
			return Null; //throw new WTF(); //shape mismatch

		int[] cd = new int[n];
		for (int i = 0; i < bd.length; i++) {
			double A = ad[i], B = bd[i];
//			if (A!=A) {
//				if (B==B) return Null;//mismatch in # of components
//				else break;
//			}
			cd[i] = chooseDT((int)A, (int)B, aProp, dtDither);
		}
		return new DTVectorTransform(cd).apply(a);
	}

	private static Term intermpolate(/*@NotNull*/ Compound a,  /*@NotNull*/ Compound b, float aProp, float curDepth, int dtDither) {

		if (a instanceof Neg) {
			if (!(b instanceof Neg)) return Null;
			Term au = a.unneg(), bu = b.unneg();
			if (!(au instanceof Compound))
				return !(bu instanceof Compound) && au.equals(bu) ? a : Null;
			else if (!(bu instanceof Compound))
				return Null;
			else
				return intermpolate((Compound) au, (Compound) bu, aProp, curDepth, dtDither).neg();
		}

		if (a.equals(b))
			return a;

		int ao = a.opID;
		if (ao != b.opID)
			return Null; //checked in equalRoot

//		if ((a.structure() & ~CONJ.bit) != (b.structure() & ~CONJ.bit))
//			return Null;

		Subterms aa = a.subterms(), bb = b.subterms();

		int aSubs = aa.subs();
		boolean subsEqual = aa.equals(bb);

		if (!subsEqual) {
			if (ao == CONJ.id)
				return intermpolateSeq(a, b, aProp, dtDither);
			else if (aSubs != bb.subs())
				return Null;
		}

		int dt = ((1<<ao) & Temporals) != 0 ? chooseDT(a, b, aProp, dtDither) : DTERNAL;
		if (dt == XTERNAL)
			return Null;

		if (subsEqual) {
			return a.dt(dt);
		} else {

			Term[] ab = new Term[aSubs];
			boolean change = false;
			for (int i = 0; i < aSubs; i++) {
				Term ai = aa.sub(i), bi = bb.sub(i);
				if ((ai instanceof Compound) != (bi instanceof Compound))
					return Null;
				if (!ai.equals(bi)) {
					if (!(ai instanceof Compound))
						return Null; //HACK
					Term y = intermpolate((Compound) ai, (Compound) bi, aProp, curDepth / 2f, dtDither);
					if (y == Null)
						return Null;
					if (!ai.equals(y)) {
						change = true;
						ai = y;
					}
				}
				ab[i] = ai;
			}

			return change ? Util.maybeEqual(the(ao).the(dt, ab), a, b) : a;
		}
	}

	/** assumes a and b are of the same size */
	private static float diffSeq(ConjList a, ConjList b) {

		int s = a.size();
		if (b.size()!=s) return POSITIVE_INFINITY;
		if (s == 0) return 0;

		return s == 1 ?
				diffSeq1(a, b) :
				diffSeqN(a, b, s);

		//TODO move any negated conjunction sub-events to separate list for closer comparison

	}

	private static float diffSeq1(ConjList a, ConjList b) {
		float ab = dtDiff(a.getFirst(), b.getFirst());
		return ab == POSITIVE_INFINITY ? POSITIVE_INFINITY :
				ab + diffWhen(a.when, b.when, 0);
	}

	private static float diffSeqN(ConjList a, ConjList b, int s) {
		//HACK attempts to arrange the terms for maximum equality, but not full-proof
		a.sortThisByValue();
		b.sortThisByValue();

		double d = 0;
		long[] aw = a.when, bw = b.when;
		for (int i = 0; i < s; i++) {
//			if (!a.get(i).equals(b.get(i))) return POSITIVE_INFINITY;

			float diffTerm = dtDiff(a.get(i), b.get(i));
			if (diffTerm == POSITIVE_INFINITY) return POSITIVE_INFINITY;
			d += diffTerm + diffWhen(aw, bw, i);
		}
		return (float) d;
	}

	private static long diffWhen(long[] aw, long[] bw, int i) {
		return Math.abs(aw[i] - bw[i]);
	}

//	@Deprecated private static float dtDiffSeq(ConjList aa, ConjList bb, int depth) {
////        int v = a.volume();
////        if (v !=b.volume()) {
////            return Float.POSITIVE_INFINITY; //is there a solution here?
////            //return (1+Math.abs(a.eventRange()-b.eventRange())) * (1 + Math.abs(a.volume()-b.volume())) ; //HACK finite, but nonsensical
////        }
//
//		//return (1 + dtDiff(a.eventRange(), b.eventRange()) ) * Math.max(a.subs(), b.subs()); //HACK estimate
//
//		//exhaustive test:
//
//		int n = aa.size();
//		if (n == 1 || n != bb.size())
//			return POSITIVE_INFINITY;
//
//		Term[] aaa = aa.array(), bbb = bb.array();
//
//		MetalBitSet elim = MetalBitSet.bits(n);
//
//		//optimistic optimization: compare start for equality to help reduce the exhaustive computation
//		int start = 0;
//		if (aaa[0].equals(bbb[0])) {
//			start = 1;
//			elim.set(0);
//		}
//
//		int[] shift = null;
//
//		//same events, sum timing differences
//		float subDiff = 0;
//		long dtDiff = 0;
//		long[] aaw = aa.when, bbw = bb.when;
//		for (int i = start; i < n; i++) {
//			int dtABmin = Integer.MAX_VALUE;
//			float snMin = POSITIVE_INFINITY;
//			int J = -1;
//			Term ai = aaa[i];
//			int iShift = i > 0 && shift != null ? shift[i-1] : 0;
//			for (int j = start; j < n; j++) {
//
//				if (elim.test(j)) continue;
//				Term bj = bbb[j];
//				float sn = dtDiff(ai, bj, depth + 1);
//				if (!Float.isFinite(sn))
//					continue;
//				if (sn <= snMin) {
//					int dtAB = Tense.occToDT(Math.abs(aaw[i] - bbw[j] - iShift));
//					if (dtAB <= dtABmin) {
//						J = j;
//						dtABmin = dtAB;
//						snMin = sn;
//					}
//				}
//			}
//
//			if (J==-1)
//				return POSITIVE_INFINITY; //not found:
//
//
//			if ((dtABmin + iShift)!=0) {
//				if (shift==null) shift = new int[n];
//				shift[i] = Tense.occToDT(dtABmin + iShift);
//			}
//
//			if (J==start)
//				start++; //eliminate margin for inner loop
//			elim.set(J);
//			dtDiff += dtABmin; //TODO fail early if dtErr becomes excessive that dtDiff=1
//			subDiff += snMin;
//		}
//
//		//range calculation: assumes sorted
//		//long aar = aa.when[n-1] - aa.when[0], bbr = bb.when[n-1] - bb.when[0]; //save these before changing sort
//		//float dtErr = ((float)dtDiff) / (Math.max(1,Math.max(aar, bbr)));
//
//		return dtDiff + subDiff;
//	}

	/** TODO refine
	 * @param abProp - a to b balance; 0: a (full) ..  1: b (full)
	 * */
	private static Term intermpolateSeq(Compound a, Compound b, float abProp, int dtDither) {
		if (abProp > 0.5f) {
			//swap so that a is dominant (<0.5)
			Compound c = a;
			a = b;
			b = c;
			abProp = 1 - abProp;
		}

		int adt = a.dt();
		int bdt = b.dt();
		boolean ax = adt == XTERNAL;
		boolean bx = bdt == XTERNAL;
		if (ax!=bx)
			return Null; //one xternal, the other isnt

		boolean ad = !ax && !a.SEQ(), bd = !bx && !b.SEQ();

		boolean aFactored = adt == DTERNAL && !ad;
		boolean bFactored = bdt == DTERNAL && !bd;
		if (aFactored!=bFactored)
			return Null;

		ConjList ae, be;
		boolean par;
		if (ax) {
			//HACK special handling for XTERNAL case
			ae = ConjList.eventsParallel(a);
			be = ConjList.eventsParallel(b);
			par = true;
		} else {
			ae = ConjList.events(a);
			be = ConjList.events(b);

			if (!ad || !bd) {
				par = false;
				ae.inhExplode();
				be.inhExplode();
			} else
				par = true;
		}

		Term z;
//		if (aFactored)
//			z = _intermpolateSeqFactored(a, b, abProp, dtDither, ax, ae, be);
//		else
			z = _intermpolateSeq(a, b, abProp, dtDither, ax, ae, be);

		ae.delete();
		be.delete();

		return z;
	}

//	/** compare factored events, and then the embedded sequence */
//	private static Term _intermpolateSeqFactored(Compound a, Compound b, float abProp, int dtDither, boolean ax, ConjList ae, ConjList be) {
//		Term e;
//		int s = ae.size();
//		SortedSet<Term> y = null;
//		int i;
//		for (i = 0; i < s; i++) {
//			long ai = ae.when(i), bi = be.when(i);
//			if (ai == ETERNAL) {
//				if (bi != ETERNAL) return Null;
//
//				Term ei = intermpolate(ae.get(i), be.get(i), abProp, dtDither);
//				if (ei == Null) return Null;
//
//				if (y == null) y = new TreeSet<>();
//				y.add(ei);
//			} else
//				break;
//		}
//		ae.removeBelow(i);
//		be.removeBelow(i);
//		Term seq = _intermpolateSeq(null, null, abProp, dtDither, ax, ae, be);
//		if (seq == Null) return Null;
//		y.add(seq);
//		return CONJ.the(y);
//	}

	private static Term _intermpolateSeq(@Nullable Compound a, @Nullable Compound b, float abProp, int dtDither, boolean ax, ConjList ae, ConjList be) {
		int s = ae.size();
		if (be.size() != s)
			return Null; //?

////		//canonical order
//			ae.sortThis();
//			be.sortThis();
////			ae.sortThisByValue();
		ae.sortThisByValueEternalFirst();
		be.sortThisByValueEternalFirst();

		boolean changedA = false, changedB = false;
		long[] aw = ae.when, bw = be.when;
		Term[] A = ae.array(), B = be.array();
		for (int i = 0; i < s; i++) {
			long ai = aw[i], bi = bw[i];
			if ((ai == ETERNAL) != (bi == ETERNAL))
				return Null;

			Term AI = A[i], BI = B[i];
			Term ABI;
			if (AI!=BI) {
				ABI = intermpolate(AI, BI, abProp, dtDither);
				if (ABI == null || ABI.opID() != AI.opID())
					return Null;
				if (!changedA) changedA = !ABI.equals(AI);
				if (!changedB) changedB = !ABI.equals(BI);
			} else
				ABI = AI;

			long abi = Util.lerpLong(abProp, ai, bi);

			if (abi!=ai || abi!=bi || changedA || changedB) {
				long aiOrig = aw[i], biOrig = bw[i];
				aw[i] = abi;
				bw[i] = abi;
				int abiDur = ABI.seqDur();
				int dABa = abiDur - A[i].seqDur();
				int dABb = abiDur - B[i].seqDur();
				A[i] = ABI;
				if (dABa!=0 || dABb!=0) {
					//shift subsequent times
					//TODO defer shift until current (possibly parallel batch of) events at current time is done, take the maximum shift of them for everything after the parallel
					for (int j = i + 1; j < s; j++) {
						if (aw[j]>aiOrig && dABa!=0) {
							aw[j] += dABa;
							changedA = true;
						}
						/*else
							Util.nop();*/ //TODO
						if (bw[j]>biOrig && dABb!=0) {
							bw[j] += dABb;
							changedB = true;
						}
//						else
//							Util.nop(); //TODO
					}
				}
			}
		}
		return interpolateSeq(a, b, aw, bw, ax, ae, changedA, changedB, dtDither);
	}

	private static Term interpolateSeq(@Nullable Compound a, @Nullable Compound b, long[] aw, long[] bw, boolean ax, ConjList ae, boolean changedA, boolean changedB, int dtDither) {
		int s = ae.size();
		for (int i = 0; i < s; i++) {
			long ai = aw[i], bi = bw[i];

			boolean aie = ai == ETERNAL, bie = bi == ETERNAL;
			if (aie != bie)
				return Null; //one is eternal, the other isnt

			long abi = Tense.dither(aw[i], dtDither);
			changedA |= abi != ai;
			changedB |= abi != bi;
			aw[i] = abi;
		}
		if (!changedA && a!=null) return a;
		if (!changedB && b!=null) return b;

		return ax ?
			CONJ.the(XTERNAL, ae) :
			ae.term();
			//Util.maybeEqual(y, a, b);
	}

	@Nullable
	private static Term intermpolate(Term a, Term b, float ab, int dtDither) {
		if (a==b) return a;
		boolean aic = a instanceof Compound;
		boolean bic = b instanceof Compound;
		if (aic!=bic) return null;
		if (aic && bic)
			return intermpolate((Compound) a, (Compound) b, ab, dtDither);
		else
			return a.equals(b) ? a : null;
	}

//    /**
//     * for merging CONJ or IMPL of equal subterms, so only dt is different
//     */
//    private static Term dtMergeTemporalDirect(/*@NotNull*/ Term a, /*@NotNull*/ Term b, float aProp,
//                                                           float depth, NAR nar) {
//        int dt = chooseDT(a, b, aProp, nar);
//        Subterms aa = a.subterms(), bb = b.subterms();
//        if (aa.equals(bb))
//            return a.dt(dt);
//        else {
//            Term[] ab = Util.map(aa.subs(), Term[]::new, i ->
//                intermpolate(aa.sub(i), bb.sub(i), aProp, depth / 2, nar)
//            );
//
//            return a.op().the(dt, ab);
//        }
//    }

	/**
	 * if returns XTERNAL, it is not possible
	 */
	private static int chooseDT(Term a, Term b, float aProp, int dtDither) {
		return chooseDT(a.dt(), b.dt(), aProp, dtDither);
	}

	static int chooseDT(int adt, int bdt, float aProp, int dtDither) {
		if (adt==XTERNAL || bdt==XTERNAL)
			return XTERNAL;

		if (adt == DTERNAL) adt = 0;
		if (bdt == DTERNAL) bdt = 0; //HACK



		int dt;
		if (adt == bdt)
			dt = adt;

//		else if (adt == XTERNAL || bdt == XTERNAL)
//			dt = adt == XTERNAL ? bdt : adt; //the other one
		else
			dt = merge(adt, bdt, aProp);

		return dt != 0 ? Tense.dither(dt, dtDither) : 0;
	}

	/**
	 * merge delta
	 */
	private static int merge(int adt, int bdt, float aProp) {


		//int range = Math.max(Math.abs(adt), Math.abs(bdt));
		float ab = Util.lerpSafe(aProp, bdt, adt);
		//float delta = Math.max(Math.abs(ab - adt), Math.abs(ab - bdt));
		return Math.round(ab);
//		float ratio = delta / range;
//		return ratio < 1 ? AB : XTERNAL;
	}


	private static double dtDiff(Subterms aa, Subterms bb) {

		int n = aa.subs();
		if (n != bb.subs())
			return Double.POSITIVE_INFINITY;

		double dSubterms = 0;
		for (int i = 0; i < n; i++) {
			float dn = dtDiff(aa.sub(i), bb.sub(i));
			if (!Float.isFinite(dn))
				return Double.POSITIVE_INFINITY;
			dSubterms += dn;
		}

		return dSubterms;
		//return (dSubterms / n);
	}


	/** proportional dt difference calculation */
	static float dtDiffPct(Compound a, Compound b) {
		return DTDiffer.the(a).diff(b);
	}


	private static final class DiffSeq implements SafeAutoCloseable {

//		private static final int DEFAULT_CONJLIST_SIZE = 4;

//		private int ab;
		final ConjList[] neg = new ConjList[2];

		static float d(Compound A, Compound B) {
			ConjList a = ConjList.events(A, 0, true, false, true);
			ConjList b = ConjList.events(B, 0, true, false, true);
			a.symmetricDifference(b);
			float d = d(a, b);
			a.delete(); b.delete();
			return d;
		}

		private static float d(ConjList a, ConjList b) {
			int n = a.size();
			if (n != b.size()) return POSITIVE_INFINITY;

			return n == 1 && a.getFirst().equals(b.getFirst()) ? d1(a, b) : dN(a, b);
		}

		private static float dN(ConjList a, ConjList b) {
			try (DiffSeq d = new DiffSeq()) {
				return d.diff(a, b);
			}
		}

		private static float d1(ConjList a, ConjList b) {
			return diffSeq(a, b);
		}

		@Override public void close() {
			if (neg[0]!=null) neg[0].delete(); neg[0] = null;
			if (neg[1]!=null) neg[1].delete(); neg[1] = null;
		}

//		@Override
//		@Deprecated public boolean accept(long when, Term x) {
//			if (x instanceof Neg) {
//				Term u = x.unneg();
//				if (u.CONJ() /*u.SEQUENCE()*/) { //usually, if not always, a sequence
////					addNegativeEvents(when, (Compound)u);
//					return true;
//				}
//			}
//			return false;
//		}

//		private void addNegativeEvents(long when, Compound u) {
//			int n = this.ab;
//			if (this.neg[n] == null) this.neg[n] = new ConjList(DEFAULT_CONJLIST_SIZE);
//			this.neg[n].addConjEventsDirect(when, u, true, false, true);
//		}


		float diff(ConjList a, ConjList b) {
			//ab=0; //a.removeIf(this);
			//ab=1; //b.removeIf(this);

			if (negs(0) == negs(1)) {
				float abDiffN = diffNeg();
				if (abDiffN != POSITIVE_INFINITY) {
					float abDiffP = diffSeq(a, b);
					return abDiffP + abDiffN;
				}
			}
			return POSITIVE_INFINITY;
		}

		private float diffNeg() {
			ConjList[] neg = this.neg;
			ConjList n0 = neg[0];
			return n0 == null ? 0 : diffSeq(n0, neg[1]);
		}

		int negs(int i) {
			ConjList x = neg[i];
			return x == null ? 0 : x.size();
		}
	}

	private static class DTVectorTransform extends RecursiveTermTransform {
		private final int[] dt;
		int j;

		DTVectorTransform(int[] dt) {
			this.dt = dt;
			j = 0;
		}

		@Override
		protected Term applyCompound(Compound x) {
			int xdt = x.unneg().TEMPORAL() ? dt[j++] : DTERNAL;
			return super.applyCompound(x, xdt);
		}

		@Override
		protected int dtAlign(Op yOp, int yDt, Subterms xx, Subterms yy, boolean xEqY) {
			return yDt;
		}

	}
}