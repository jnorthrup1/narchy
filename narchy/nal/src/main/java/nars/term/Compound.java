/*
 * CompoundTerm.java
 *
 * Copyright (C) 2008  Pei Wang
 *
 * This file is part of Open-NARS.
 *
 * Open-NARS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Open-NARS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-NARS.  If not, see <http:
 */
package nars.term;

import com.google.common.io.ByteArrayDataOutput;
import jcog.Hashed;
import jcog.WTF;
import jcog.data.list.Lst;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.io.TermIO;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.compound.SeparateSubtermsCompound;
import nars.term.util.TermTransformException;
import nars.term.util.builder.TermBuilder;
import nars.term.util.conj.CondMatcher;
import nars.term.util.conj.Conj;
import nars.term.util.conj.ConjBundle;
import nars.term.util.conj.ConjList;
import nars.time.Tense;
import org.eclipse.collections.api.block.function.primitive.IntObjectToIntFunction;
import org.eclipse.collections.api.block.predicate.primitive.LongObjectPredicate;
import org.eclipse.collections.api.block.procedure.primitive.LongObjectProcedure;
import org.eclipse.collections.api.list.primitive.ByteList;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static jcog.Util.hashCombine;
import static nars.Op.*;
import static nars.io.TermIO.outNegByte;
import static nars.time.Tense.occToDT;

/**
 * a compound target
 * TODO make this an interface extending Subterms
 */
public abstract class Compound /*IPair,*/ extends Term implements Subterms {

    public final byte opID;
    /**
     * is this term is its own root
     */
    protected transient boolean root;
    /**
     * is this term is its own concept
     */
    private transient boolean concept;

    protected Compound(int opID) {
        this.opID = (byte) opID;
    }

    @Override public final boolean IMPL() {
        return opID == IMPL.id;
    }

    @Override public final boolean CONJ() {
        return opID == CONJ.id;
    }
    public boolean PROD() {
        return opID == PROD.id;
    }

    public boolean INH() {
        return opID == INH.id;
    }

    public boolean SIM() {
        return opID == SIM.id;
    }

    public boolean STATEMENT() {
        return isAny(Statements);
    }

    public boolean COMMUTATIVE() {
        return isAny(Commutatives) && subs() > 1;
    }

    public boolean EQ() { return opID == EQ.id; }

    public boolean SETe() {
        return opID == SETe.id;
    }

    public boolean SETi() {
        return opID == SETi.id;
    }

    public boolean SET() {
        return isAny(Set);
    }

    public boolean DELTA() {
        return opID == DELTA.id;
    }


    private static String toString(Compound c) {
        StringBuilder sb = new StringBuilder(
                /* estimate */
                c.volume() * 4
        );
        return c.appendTo(sb).toString();
    }

    private static int hash(int opID, int subtermsHash) {
        return hashCombine(subtermsHash, opID);
    }

    public static int hash1(int opID, Term onlySubterm) {
        return hash(opID, Subterms.hash(onlySubterm));
    }

    /**
     * whether a term is 'temporal' and its derivations need analyzed by the temporal solver:
     * if there is any temporal terms with non-DTERNAL dt()
     */
    @Deprecated public static boolean _temporal(Term x) {
        if (!(x instanceof final Compound c) || x instanceof CondAtomic)
            return false;

        int o = c.opID;
        if (o == CONJ.id || o == IMPL.id) { //x.TEMPORAL()
            int dt = x.dt();
            if (dt != DTERNAL && dt != XTERNAL) return true;
        }

        return Op.hasAny(c.structureSubs(), Temporals) && c.OR(Compound::_temporal);
    }

    private static boolean decomposeConj(int dt, boolean decomposeParallel, boolean decomposeXternal) {
        return switch (dt) {
            case DTERNAL -> decomposeParallel;
            case XTERNAL -> decomposeXternal;
            default -> true;
        };
    }

    /**
     * gets temporal relation value
     */
    @Override
    public abstract int dt();


    @Override
    public final int opID() {
        return opID;
    }

    @Override
    public final String toString() {
        return toString(this);
    }

    @Override
    public boolean equalConcept(Term x) {
        if (this == x) return true;
        if (!(x instanceof Compound)) return false;
        return opID() == x.opID() && concept().equals(x.concept());
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;

        return o instanceof Compound && equalsCompound((Compound) o);
    }

    private boolean equalsCompound(Compound B) {
        int ao = opID; if (B.opID!=ao) return false;

        if (!(this instanceof Hashed && B instanceof Hashed) || hashCode() == B.hashCode()) {
            return
                ((Temporals & (1 << ao)) == 0 || dt() == B.dt())
                &&
                subtermsDirect().equalTerms(B.subtermsDirect())
                ;
        }
        return false;
    }

    @Override
    public final void write(ByteArrayDataOutput out) {
        if (this instanceof Neg) {
            outNegByte(out);
            unneg().write(out);
        } else {
            TermIO.writeCompoundPrefix(opID, dt(), out);
            if (this instanceof SeparateSubtermsCompound)
                subtermsDirect().write(out);
            else
                Subterms.super.write(out);
        }

    }

    /**
     * Compound default hashcode procedure
     */
    @Override
    public int hashCode() {
        int b = hash(opID, hashCodeSubterms());
        int dt = dt();
        return dt == DTERNAL ? b : hashCombine(b, dt);
    }

    @Override
    public int intifyRecurse(int vIn, IntObjectToIntFunction<Term> reduce) {
        return reduce.intValueOf(
                this instanceof SeparateSubtermsCompound ?
                        subterms().intifyRecurse(vIn, reduce)
                        : Subterms.super.intifyRecurse(vIn, reduce),
                this);
    }


//    /**
//     * TODO test
//     */
//    public boolean unifiesRecursively(Term x, Predicate<Term> preFilter) {
//
//        if (x instanceof Compound) {
////            int xv = x.volume();
//            if (!hasAny(Op.Variables) /*&& xv > volume()*/)
//                return false; //TODO check
//
//            //if (u.unifies(this, x)) return true;
//
//            int xOp = x.opID();
//            return !subterms().ANDrecurse(s -> s.hasAny(1 << xOp)/*t->t.volume()>=xv*/, s -> !(s instanceof Compound && s.opID() == xOp && preFilter.test(s) &&
//                    new UnifyAny().unify(x, s)), this);
//        } else {
//            return x instanceof Variable || containsRecursively(x);
//        }
//    }

    public boolean recurseTermsOrdered(Predicate<Term> inSuperCompound, Predicate<Term> whileTrue, Compound parent) {
        return Subterms.super.recurseTermsOrdered(inSuperCompound, whileTrue, parent);
    }

    /**
     * very fragile be careful here
     */
    @Override
    public /* final */ boolean containsRecursively(Term x, @Nullable Predicate<Compound> inSubtermsOf) {
        return (inSubtermsOf == null || inSubtermsOf.test(this))
                &&
                (this instanceof SeparateSubtermsCompound ?
                        this.subterms().containsRecursively(x, inSubtermsOf) :
                        Subterms.super.containsRecursively(x, inSubtermsOf)
                );
    }

    @Override
    public boolean internables() {
        return subtermsDirect().internables();
    }

    public TermList events(@Nullable TermList x, boolean decomposeRootAuto, boolean decomposeE, boolean decomposeX) {
        if (!CONJ())
            throw new WTF();

        switch (dt()) {
            case DTERNAL, XTERNAL, 0 -> {
                if (decomposeRootAuto) {
                    Subterms X = subtermsDirect();
                    if (x == null) {
                        return X.toList();
                    } else {
                        X.addAllTo(x);
                        return x;
                    }
                }
            }
        }
        TermList X = x == null ?
            new TermList(4 /* estimate */) :
            x;
        events(X::add, decomposeE, decomposeX);
        return X;
    }

    @Override
    public boolean SEQ() {
        return
            CONJ()
            &&
            (!Tense.parallel(dt()) || subtermsDirect().hasSeq());
    }

    @Override
    public final int seqDur(boolean xternalSensitive) {

        if (this instanceof Neg/* || DELTA()*/) return sub(0).seqDur(xternalSensitive);
        if (this instanceof CondAtomic || !CONJ()) return 0;

        int dt = dt();
        if (xternalSensitive && dt == XTERNAL)
            return XTERNAL;

        Subterms s = this.subtermsDirect();
        return switch (dt) {
            case DTERNAL, 0, XTERNAL -> s.seqDur(xternalSensitive);
            default -> seqDurSeq(dt, s, xternalSensitive);
        };
    }

    private static int seqDurSeq(int dtInner, Subterms ab, boolean xternalSensitive) {
        int a = ab.seqDurSub(0, xternalSensitive);
        if (xternalSensitive && a == XTERNAL) return XTERNAL;

        int b = ab.seqDurSub(1, xternalSensitive);
        if (xternalSensitive && b == XTERNAL) return XTERNAL;

        return a + Math.abs(dtInner) + b;
    }

    public final boolean eventsOR(Predicate<Term> each, boolean decomposeDTernal, boolean decomposeXternal) {
        return eventsOR((w,x)->each.test(x), 0, decomposeDTernal, decomposeXternal);
    }

    public final boolean eventsOR(LongObjectPredicate<Term> each, long offset, boolean decomposeDTernal, boolean decomposeXternal) {
        return opID == CONJ.id ? !eventsAND(
                (when, what) -> !each.accept(when, what)
                , offset, decomposeDTernal, decomposeXternal) :
            each.accept(offset, this);
    }

    public final void events(Consumer<Term> each, boolean decomposeConjDTernal, boolean decomposeXternal) {
        eventsAND((subWhen, subWhat) -> {
            each.accept(subWhat);
            return true;
        }, 0, decomposeConjDTernal, decomposeXternal);
    }

    public final void events(LongObjectProcedure<Term> each, long when, boolean decomposeConjDTernal, boolean decomposeXternal) {
        eventsAND((subWhen, subWhat) -> {
            each.value(subWhen, subWhat);
            return true;
        }, when, decomposeConjDTernal, decomposeXternal);
    }

    /**
     * iterates contained events within a conjunction
     */
    public boolean eventsAND(LongObjectPredicate<Term> each, long offset, boolean decomposeParallel, boolean decomposeXternal) {
        if (opID == CONJ.id) {
            int dt = dt();
            if (decomposeConj(dt, decomposeParallel, decomposeXternal))
                return subtermsDirect().conjDecompose(each, offset, dt, decomposeParallel, decomposeXternal);
        }

        return each.accept(offset, this); //default: singular event
    }

    public boolean condFirst(Term event) {
        int x = when(event, true);
        return x == DTERNAL || x == 0;
    }
    public boolean condLast(Term event) {
        int w = when(event, false);
        if (w == XTERNAL) return false;
        return w == DTERNAL || w == seqDur();
    }

    @Override
    public boolean CONDS() {
        return CONJ() || ConjBundle.bundled(this);
    }

    /** @return offset of a matching event if in a sequence
     *    XTERNAL if not found.
     *    DTERNAL if equal to the event, or is a parallel/factored component */
    public int when(Term e, boolean fromStartOrEnd) {
        if (this == e) return DTERNAL;
        int vThis = volume(), vE = e.volume();
        if (vThis > vE) {
            if (CONDS()) {
                if (SEQ()) {
                    return eventTimeSeq(e, fromStartOrEnd);
                } else {
                    /*if (dt()!=XTERNAL) {*/
                    return condOf(e) ? DTERNAL : XTERNAL;
                }
            }
        } else if (vThis == vE && equals(e))
            return DTERNAL;

        return XTERNAL;
    }

    private int eventTimeSeq(Term e, boolean fromStartOrEnd) {
        //certain sequence representations may be lengthier but still match, so dont test for volume containment here.
        if (impossibleSubStructure(e.structure() & ~CONJ.bit))
            return XTERNAL;

        Set<Term> common = Conj.factoredEternalEventsCommon(this, e);
        if (common!=null) {
            return ((Compound)(Conj.eternalEventsRemove(this, common)))
                    .eventTimeSeq(Conj.eternalEventsRemove(e, common), fromStartOrEnd);
        }


        boolean explode = e.hasAny(INH);
        var C = ConjList.events(this, 0, true, false, explode);
        var E = ConjList.events(e, 0, true, false, explode);
        try (CondMatcher cc = new CondMatcher(C)) {
            return cc.match(E, fromStartOrEnd) ?
                occToDT(cc.matchStart) : XTERNAL;
        } finally {
            C.delete();
            E.delete();
        }
    }

    @Override
    @Nullable
    public final Term normalize(byte varOffset) {

        boolean v0 = varOffset == 0;
        if (v0 && this.NORMALIZED())
            return this;

        if (this instanceof Neg)
            return _normalizeNeg(varOffset);

        Compound y = terms.normalize(this, varOffset);

        if (v0) y.setNormalized();

        return y;
    }

    private Term _normalizeNeg(byte varOffset) {
        Term u = unneg();
        if (u instanceof Compound) {
            return _normalizeNegCompound(varOffset, u);
        } else if (u instanceof Variable){
            return _normalizeNegAtomic((Variable) u, varOffset);
        } else
            return this;
    }

    private Term _normalizeNegCompound(byte varOffset, Term u) {
        Term uu = u.normalize(varOffset);
        Compound y;
        if (u.equals(uu)) {
            y = this; //same
        } else {
            y = (Compound) uu.neg(); //different
        }
        if (varOffset == 0) y.setNormalized();
        return y;
    }

    private Term _normalizeNegAtomic(Variable x, byte varOffset) {

        Term y = x.normalize(varOffset);
        if (y == x) {
            //negated already normalized variable
            setNormalized();
            return this;
        } else
            return y.neg();

    }

    @Override
    public Term root() {
        if (this.root) // || this instanceof Atemporal)
            return this;

        boolean neg = this instanceof Neg;
        Term x;
        if (neg) {
            x = unneg();
            if (x instanceof Compound && ((Compound) x).root) {
                this.root = true;
                return this;
            }
        } else {
            x = this;
        }

        if (!(x instanceof Compound)) // || !x.TEMPORALABLE())
            return this;

        Term y = terms.root((Compound) x);
        if (!(y instanceof Compound))
            throw new TermTransformException("root fault", x, y);

//        if (x.equals(y)) {
//            ((Compound) x).root = true;
//            if (neg)
//                this.root = true;
//            return this; //unchanged
//        } else {
            ((Compound) y).root = true;

            if (neg) {
                Term yn = y.neg();
                ((Compound) yn).root = true;
                return yn;
            } else
                return y;
//        }
    }

    @Override
    public final Term concept() {

        if (this.concept) return this;

        if (this instanceof Neg)
            return unneg().concept();

        Term y = this.root().normalize();

        validateConcept(y);

        if (!(y instanceof Compound))
            throw new TermTransformException("concept()", this, y);

        ((Compound) y).concept = true;

        return y;

    }

    private static void validateConcept(Term y) {
//        if (this != y && opID != y.opID())
//            throw new TermTransformException(Op.UNCONCEPTUALIZABLE, this, y); //TODO other tests

        assert !NAL.DEBUG || ((Compound) y).NORMALIZED();
    }


    @Override
    public int structure() {
        return opBit() | structureSubs();
    }

    @Override
    public final boolean hasAny(int struct) {
        return Op.hasAny(struct, opBit()) | Op.hasAny(struct, structureSubs());
    }

    @Deprecated public final Term dt(int dt) {
        return dt(dt, terms);
    }

    @Deprecated public final Term dt(int nextDT, TermBuilder b) {

        if (!(this instanceof CondAtomic)) {
            if (nextDT == 0) nextDT = DTERNAL; //HACK
            if (nextDT != dt())
                return op().build(b, nextDT, subterms());
        }

        return this;
    }

    @Override
    public boolean equalsRoot(Term y) {
        if (this == y) return true;

        if (!(y instanceof Compound) || opID != ((Compound) y).opID)
            return false;

        if (this.equals(y))
            return true;

        Term xx = root();
        Term yy = y.root();
        return (xx != this || yy != y) && xx.equals(yy);
    }

    public final Lst<ByteList> pathsToList(Term x) {
        Lst<ByteList> paths = new Lst<>(2);
        //int minPathLength = STATEMENT() ? 2 : 0;
        pathsTo(x, (path, t) -> {
            //if (path.size() >= minPathLength)
            paths.add(path.toImmutable());
            return true;
        });
        return paths;
    }

    private boolean pathsTo(Term target, BiPredicate<ByteList, Term> receiver) {
        return pathsTo(target.equals(),
            x -> !x.impossibleSubTerm(target),
            receiver);
    }

//    public Predicate<Term> condOf(int polarity) {
//        throw new TODO();
//    }

//    public boolean eventOfOrEqual(Term x, int polarity) {
//        if (polarity >= 0 && equals(x) || polarity == -1 && equalsNeg(x))
//            return false;
//
//        return eventOf(x, polarity);
//    }

    public boolean condOf(Term x, int polarity) {
        return switch (this.op()) {
            case INH -> condOfInh(x, polarity);
            case CONJ -> condOfConj(x, polarity);
            default -> false;
        };
    }

    private boolean condOfInh(Term x, int polarity) {
        boolean xNeg = x instanceof Neg;
        Term xu = xNeg ? x.unneg() : x;
        if (!xu.INH())
            return false;
        if (!hasAll(xu.structure() & ~(CONJ.bit | NEG.bit)))
            return false;
        if (volume() + (polarity == 1 ? 0 : +1) <= x.volume())
            return false;

        //x = Image.imageNormalize(x); //assert (C.INH());

        return subtermsDirect().condOfInh(xu.subtermsDirect(), xNeg ? -polarity : polarity);
    }


    public final boolean condOf(Term event) {
        return condOf(event, +1);
    }

    /* TODO private */ public boolean condOfConj(Term x, int polarity) {
        assert(CONJ());
        return
                polarity >= 0 && condOfConj(x)
                ||
                polarity <= 0 && condOfConj(x.neg());

    }

    private boolean condOfConj(Term x) {
        if (volume() <= x.volume())
            return false;

        boolean xConj = x.CONJ();

        if (impossibleSubStructure(
            xConj ? x.structureSubs() /*& ~CONJ.bit*/ :
                x.structure()
        ))
            return false;

        int dt = dt();
        boolean dtXternal = dt == XTERNAL;
        if (dtXternal && !x.TEMPORAL_VAR()) {
            return OR(x.equalsOrInCond());
        }

        boolean seq = this.SEQ(), xSeq = xConj && x.SEQ();
        if (!seq) {
            if (xSeq) return false;
            //else if (!xConj) OR(x.equalsOrInCond());
        }

        if (seq && xSeq && !dtXternal) {
            return condOfConjSeq(x);
        } else if (xConj && !xSeq && (!seq || x.dt()==XTERNAL)) {
            //par in par
            return ((Compound)x).AND(this::condOfConj);
        } else {
            //HACK eliminate shared eternal components in factored sequence
            Set<Term> common = Conj.factoredEternalEventsCommon(this, x);
            if (common!=null) {
                Term t2 = Conj.eternalEventsRemove(this, common);
                return t2.CONDS() && ((Compound) t2).condOf(
                        Conj.eternalEventsRemove(x, common)
                );
            }

            Predicate<Term> xMatch = x.equalsOrInCond();

            if (dtXternal || dt == DTERNAL) {
                return OR(xMatch);
            } else {
                return eventsOR(xMatch, !(xConj && x.dt()==DTERNAL), false);
            }
        }
    }

    private boolean condOfConjSeq(Term x) {
        //exhaustive
        //TODO optimize
        if (seqDur() < x.seqDur())
            return false; //impossible
        ConjList C = ConjList.events(this);
        ConjList X = ConjList.events(x);
        if (C.contains(X))
            return true;
        else {
            if (x.hasAll(CONJ.bit|INH.bit) || hasAll(CONJ.bit | INH.bit)) {
                if (C.inhExplode(terms) == 1 || X.inhExplode(terms) == 1)
                    return C.contains(X);
            }
            return false;
        }
    }

}























































































    /*
    @Override
    public boolean equals(final Object that) {
        return (that instanceof Term) && (compareTo((Term) that) == 0);
    }
    */









































































































































































    /* UNTESTED
    public Compound clone(VariableTransform t) {
        if (!hasVar())
            throw new RuntimeException("this VariableTransform clone should not have been necessary");

        Compound result = cloneVariablesDeep();
        if (result == null)
            throw new RuntimeException("unable to clone: " + this);

        result.transformVariableTermsDeep(t);

        result.invalidate();

        return result;
    } */


/**
 * override in subclasses to avoid unnecessary reinit
 */
    /*public CompoundTerm _clone(final Term[] replaced) {
        if (Terms.equals(target, replaced)) {
            return this;
        }
        return clone(replaced);
    }*/





















































    /*static void shuffle(final Term[] list, final Random randomNumber) {
        if (list.length < 2)  {
            return;
        }


        int n = list.length;
        for (int i = 0; i < n; i++) {
            
            int r = i + (randomNumber.nextInt() % (n-i));
            Term tmp = list[i];    
            list[i] = list[r];
            list[r] = tmp;
        }
    }*/

/*        public static void shuffle(final Term[] ar,final Random rnd)
        {
            if (ar.length < 2)
                return;



          for (int i = ar.length - 1; i > 0; i--)
          {
            int index = randomNumber.nextInt(i + 1);
            
            Term a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
          }

        }*/