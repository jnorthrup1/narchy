package nars.term.buffer;

import com.google.common.primitives.Ints;
import jcog.data.byt.DynBytes;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.atom.Atomic;
import nars.term.util.TermException;
import nars.term.util.builder.TermBuilder;
import nars.term.util.map.ByteAnonMap;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.function.UnaryOperator;

import static nars.Op.DTERNAL;
import static nars.Op.NEG;
import static nars.term.atom.Bool.Null;
import static nars.term.atom.Bool.True;

//import nars.term.var.ellipsis.Fragment;

/**
 * memoizable supplier of compounds
 * fast to construct but not immediately usable (or determined to be valid)
 * without calling the .the()
 * consists of a tape flat linear tape of instructions which
 * when executed construct the target
 */
public class TermBuffer {
	private static final int INITIAL_CODE_SIZE = 16;
	private static final byte MAX_CONTROL_CODES = (byte) Op.count;
	public final ByteAnonMap sub;
	final DynBytes code = new DynBytes(INITIAL_CODE_SIZE);
	private final TermBuilder builder;
	/**
	 * when true, non-dynamic constant sequences will be propagated to inline future repeats in instantiation.
	 * if a non-deterministic functor evaluation occurrs, it must not propagate
	 * because that will just cause the same value to be assumed when it should not be.
	 */
	int volRemain;
	/**
	 * incremental mutation counter to detect unmodified segments that can be inlined / interned atomically
	 */
	private int change;
	private transient byte[] bytes;
	private transient int readPtr;

	public TermBuffer() {
		this(Op.terms);
	}

	private TermBuffer(TermBuilder builder) {
		this(builder, new ByteAnonMap());
	}

	private TermBuffer(TermBuilder builder, ByteAnonMap sub) {
		this.builder = builder;
		this.sub = sub;
	}

	/**
	 * expand a fragment
	 */
	private static Term[] nextFrag(Term[] subterms, int i, Term fragment, byte len, int fragmentLen) {
		if (subterms == null)
			subterms = new Term[len];
		else if (subterms.length != len)
			subterms = Arrays.copyOf(subterms, len); //used to grow OR shrink
		if (fragmentLen > 0)
			fragment.addAllTo(subterms, i); //TODO recursively evaluate?
		return subterms;
	}

    private static Term emptySubterms(Op op) {
        return switch (op) {
            case PROD -> Op.EmptyProduct;
            case CONJ -> True;
            default -> throw new TermException(op + " does not support empty subterms");
        };
    }

    public TermBuffer clone() {
		TermBuffer b = new TermBuffer(builder, sub);
		b.code.write(code);
		//TODO volRemain ?
		return b;
	}

	public final void clear() {
		clear(true, true);
	}

	public void clear(boolean code, boolean uniques) {
		if (uniques) {
			sub.clear();
		}
		if (code) {
			this.code.clear();
			change = 0;
		}
	}

	public boolean replace(UnaryOperator<Term> m) {
		return sub.updateMap(m);
	}

	final TermBuffer compoundStart(Op o) {
		return compoundStart(o.id);
	}

	private TermBuffer compoundStart(byte oid) {
		DynBytes c = this.code;
		c.writeByte(oid);
		if (((1 << oid) & Op.Temporals) != 0)
			c.writeByteInt(oid, DTERNAL);
		return this;
	}

	/**
	 * append compound
	 */
	final TermBuffer appendCompound(Op o, Term... subs) {
		return appendCompound(o, DTERNAL, subs);
	}

	/**
	 * append compound
	 */
	final TermBuffer appendCompound(Op o, int dt, Term... subs) {
		return compoundStart(o, dt).appendSubterms(subs).compoundEnd();
	}

	final TermBuffer compoundEnd() {
		return this;
	}

	final TermBuffer subsStart(byte subterms) {
		code.writeByte(subterms);
		return this;
	}

	private TermBuffer compoundStart(Op o, int dt) {
		DynBytes c = this.code;

		byte oid = o.id;

		if (!o.temporal)
			c.writeByte(oid);
		else
			c.writeByteInt(oid, dt);

		return this;
	}

	/**
	 * negEnd() is not necessary to call, since NEG can only have 1 argument, whatever the next term is will be it.
	 */
	public final TermBuffer negStart() {
		//compoundStart(NEG);
		code.writeByte(NEG.id);
		return this;
	}

	private TermBuffer negEnd() {
		compoundEnd();
		return this;
	}

	/**
	 * add an already existent sub
	 */
	public final TermBuffer append(Term x) {
		return x instanceof Atomic ? appendInterned(x) : append((Compound) x);
	}

	private TermBuffer append(Compound x) {
		if (x instanceof Neg) {
			return negStart().append(x.unneg()).negEnd();
		} else {
			Op o = x.op();
			return compoundStart(o, x.dt()).appendSubterms(x.subtermsDirect()).compoundEnd();
		}
	}

	private TermBuffer appendSubterms(Subterms s) {
		subsStart((byte) s.subs());
		s.appendTo(this);
		return subsEnd();
	}

	/**
	 * interns the term as-is, encapsulated as an atomic symbol
	 */
	private TermBuffer appendInterned(Term x) {
		return appendInterned(intern(x));
	}

	private TermBuffer appendInterned(byte i) {
		//assert (i < Byte.MAX_VALUE);
		code.writeByte(MAX_CONTROL_CODES + i);
		return this;
	}

	private byte intern(Term x) {
		//assert(!(x == null || x == Null || x.op() == NEG));
		return sub.intern(x);
	}


	final TermBuffer appendSubterms(Term... subs) {
		return subsStart((byte) subs.length).appendAll(subs).subsEnd();
	}

	private TermBuffer appendAll(Term[] subs) {
		for (Term x : subs)
			append(x);
		return this;
	}

	public Term term() {
		return term(NAL.term.COMPOUND_VOLUME_MAX);
	}

	/**
	 * run the construction process
	 */
	public Term term(int volMax) {
		this.volRemain = volMax;
		readPtr = 0;
		bytes = code.arrayDirect();
		Term z = nextTerm();
		bytes = null;
		return z;
	}

	private Term nextTerm() {

		int start = readPtr++;
		byte ctl = bytes[start];
		if (ctl >= MAX_CONTROL_CODES)
			return nextInterned(ctl);
		else if (ctl == NEG.id)
			return nextTerm().neg();

		Op op = Op.the(ctl);

		if (op.atomic)  //alignment error or something
			throw new TermException(TermBuffer.class + ": atomic found where compound op expected: " + op);

		int dt = op.temporal ? dt(bytes) : DTERNAL;

		int volRemaining = this.volRemain - 1 /* one for the compound itself */;

		byte len = bytes[readPtr++];
		if (len == 0)
			return emptySubterms(op);

		Term[] subterms = null;

		for (int i = 0; i < len; ) {
			//assert(range[0] <= range[1]) //check if this is < or <=

			Term nextSub = nextTerm();

			if (nextSub == Null)
				return Null;

			//append next subterm
			if (subterms == null)
				subterms = new Term[len];

			volRemaining -= nextSub.volume();

			subterms[i++] = nextSub;

			if ((this.volRemain = volRemaining) <= 0)
				return Null; //capacity reached
		}

		return nextCompound(op, dt, subterms, start);
	}

	private Term nextCompound(Op op, int dt, Term[] subterms, int from) {
		return newCompound(op, dt, subterms); //assert (next != null);

//		if (next != Null) {
//			replaceAhead(bytes, from, next);
//		}
	}

	/**
	 * constructs a new compound term
	 */
	private Term newCompound(Op op, int dt, Term[] subterms) {
        return op.build(builder, dt, subterms);
    }


//    private Term nextInterned(byte ctl, byte[] ii) {
//        Term next = nextInterned(ctl);
//        if(next==null)
//            throw new NullPointerException();
//
//        return next;
//    }

	private int uniques() {
		return sub.idToTerm.size();
	}

	public final byte term(Term x) {
		return sub.interned(x);
	}

	private int dt(byte[] ii) {
		int p = readPtr;
		readPtr += 4;
		return Ints.fromBytes(ii[p++], ii[p++], ii[p++], ii[p/*++*/]);
	}

	/**
	 * constant propagate matching spans further ahead in the construction process
	 * TODO test
	 */
	private void replaceAhead(byte[] ii, int from, Term next) {
		int to = readPtr;

		int span = to - from;
		if (span <= NAL.term.TERM_BUFFER_MIN_REPLACE_AHEAD_SPAN)
			return;

		int end = length();

		if (end - to >= span) {
			//search for repeat occurrences of the start..end sequence after this
			int afterTo = to;
			byte n = 0;
			do {
				int match = ArrayUtil.nextIndexOf(ii, afterTo, end, ii, from, to);

				if (match != -1) {
					//System.out.println("repeat found");
					if (n == 0)
						n = (byte) (MAX_CONTROL_CODES + intern(next)); //intern for re-substitute
					code.set(match, n);

					code.fillBytes((byte) 0, match + 1, match + span); //zero padding, to be ignored
					afterTo = match + span;
				} else
					break;

			} while (afterTo < end);
		}

	}

	private Term nextInterned(byte ctl) {
		return sub.interned((byte) (ctl - MAX_CONTROL_CODES));
	}

	public int length() {
		return code.length;
	}


	private void rewind(int codePos) {
		code.length = codePos;
	}

	public final TermBuffer subsEnd() {
		return this;
	}

	public boolean isEmpty() {
		return code.length == 0 && (sub == null || sub.isEmpty());
	}

	public boolean append(Term x, UnaryOperator<Term> f) {
		return x instanceof Compound ? appendCompound((Compound) x, f) : appendAtomic(x, f);
	}

	private boolean appendAtomic(Term x, UnaryOperator<Term> f) {
		if (x == Null)
			return false;

		@Nullable Term y = f.apply(x);
		if (y == Null)
			return false;
		else {
			if (x!=y) change++;
			this.append(y);
			return true;
		}
	}

	private boolean appendCompound(Compound x, UnaryOperator<Term> f) {
		if (x instanceof Neg) {
			negStart();
			if (!append(x.unneg(), f))
				return false;
			negEnd();
			return true;
		} else {

			byte interned = term(x);
			if (interned != Byte.MIN_VALUE) {
				appendInterned(interned);
				return true;
			} else
				return appendCompoundPos(x, f);
		}

	}

	private boolean appendCompoundPos(Compound x, UnaryOperator<Term> f) {
		int c = this.change;
//		final int u = this.uniques();
		int p = this.length();

		Op o = x.op();
//		if (o==FRAG)
//			Util.nop(); //TODO?

		this.compoundStart(o, o.temporal ? x.dt() : DTERNAL);

		Subterms xx = x.subtermsDirect();
		if (!append(xx, f))
			return false;

		this.compoundEnd();

		if (change == c && constant(xx)) {
			//int TERM_BUFFER_MIN_INTERN_VOL = 5;

			//unchanged constant; rewind and pack the exact Term as an interned symbol
			this.rewind(p);
			this.appendInterned(x);
		}
		return true;
	}

	/**
	 * can override to determine what is internable
	 */
	protected boolean constant(Subterms xx) {
		return xx.isConstant();
	}


	//	@Deprecated private transient int subTermCount;

	private boolean append(Subterms s, UnaryOperator<Term> t) {
		byte n = (byte) s.subs();
		this.subsStart(n);
		if (!s.ANDwithOrdered(this::append, t))
			return false;
		this.subsEnd();
		return true;
	}

	public TermBuffer volRemain(int v) {
		volRemain = v;
		return this;
	}
}