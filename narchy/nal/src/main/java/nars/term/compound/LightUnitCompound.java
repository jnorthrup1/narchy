package nars.term.compound;

import nars.Op;
import nars.Term;

public class LightUnitCompound extends UnitCompound {

	protected final Term sub;

	public LightUnitCompound(Op op, Term sub) {
		this(op.id, sub);
	}

	public LightUnitCompound(int op, Term sub) {
		super(op);
		this.sub = sub;
	}

	@Override
	public final Term sub() {
		return sub;
	}


}