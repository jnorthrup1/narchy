package nars.term.compound;

import jcog.Hashed;
import jcog.The;
import nars.Term;
import nars.term.Compound;

public abstract class SemiCachedUnitCompound extends LightUnitCompound implements The, Hashed {


    /** hash including this compound's op (cached) */
    protected final int chash;

    protected SemiCachedUnitCompound(int opID, Term sub) {
        this(opID, sub, Compound.hash1(opID, sub));
    }

    protected SemiCachedUnitCompound(int opID, Term sub, int chash) {
        super(opID, sub);
        this.chash = chash;
    }

    @Override
    public final int hashCode() {
        return chash;
    }

}