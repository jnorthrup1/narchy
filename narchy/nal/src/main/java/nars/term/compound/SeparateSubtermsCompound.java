package nars.term.compound;

import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import org.eclipse.collections.api.block.function.primitive.IntObjectToIntFunction;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nars.Op.XTERNAL;

/** delegates certain methods to a specific impl */
public abstract class SeparateSubtermsCompound extends Compound {

    private final Subterms subterms;

    protected SeparateSubtermsCompound(int op, Subterms subterms) {
        super(op);
        this.subterms = subterms;
    }
    @Override
    public final Subterms subterms() {
        return subterms;
    }

    @Override
    public final boolean NORMALIZED() {
        return subterms.NORMALIZED();
    }

    @Override
    public final void setNormalized() {
        subterms.setNormalized();
    }

    public final int structureSubs() {
        return subterms.structureSubs();
    }

    @Override
    public final int height() {
        return subterms.height();
    }

    @Override
    public int structureSurface() {
        return subterms.structureSurface();
    }

    @Override
    public int structureSurfaceUnneg() {
        return subterms.structureSurfaceUnneg();
    }


    @Override
    public Term subUnneg(int i) {
        return subterms.subUnneg(i);
    }

    @Override
    public boolean subEquals(int i, Term x) {
        return subterms.subEquals(i, x);
    }


    @Override
    public Subterms negated() {
        return subterms.negated();
    }

    @Override
    public Subterms reversed() {
        return subterms.reversed();
    }

    @Override
    public final boolean equalTerms(Subterms b) {
        return subterms.equalTerms(b instanceof SeparateSubtermsCompound ? ((SeparateSubtermsCompound)b).subterms : b);
    }

    @Override
    public boolean containsInstance(Term t) {
        return subterms.containsInstance(t);
    }

    @Override
    public boolean contains(Term x) {
        return subterms.contains(x);
    }
    @Override
    public Predicate<Term> contains(boolean pn, boolean preFilter) {
        return subterms.contains(pn, preFilter);
    }

    @Override
    public final boolean containsNeg(Term x) {
        return subterms.containsNeg(x);
    }

    @Override
    public boolean containsPN(Term x) {
        return subterms.containsPN(x);
    }

    @Override
    public int indexOf(Term t, int after) {
        return subterms.indexOf(t, after);
    }


    //    @Override
//    public final boolean equals(Object obj) {
//        if (Compound.equals(this, obj, true)) {
////            if (obj instanceof SeparateSubtermsCompound)
////                share(this, ((SeparateSubtermsCompound)obj));
//            return true;
//        }
//        return false;
//    }

//  //dangerous if contains equal but different classes, like in Functor resolution
//    private static void share(SeparateSubtermsCompound x, SeparateSubtermsCompound y) {
//        Subterms xs = x.subterms, ys = y.subterms;
//        if (xs!=ys) {
//            if (System.identityHashCode(x) > System.identityHashCode(y)) {
//                SeparateSubtermsCompound z = x; x = y; y = z;
//            }
//            y.subterms = x.subterms;
//        }
//    }


    @Override
    public final int hashCodeSubterms() {
        return subterms.hashCodeSubterms();
    }


    @Override
    public final boolean recurseTermsOrdered(Predicate<Term> inSuperCompound, Predicate<Term> whileTrue, Compound parent) {
        return !inSuperCompound.test(this) ||
                whileTrue.test(this) &&
                subterms.recurseTermsOrdered(inSuperCompound, whileTrue, this);
    }

    @Override
    public boolean boolRecurseSubterms(Predicate<Compound> inSuperCompound, BiPredicate<Term, Compound> whileTrue, Compound parent, boolean andOrOr) {
        return subterms.boolRecurseSubterms(inSuperCompound, whileTrue, parent, andOrOr);
    }

    /*@NotNull*/
    @Override
    public final Term[] arrayClone() {
        return subterms.arrayClone();
    }

    @Override
    public final Term[] arrayShared() {
        return subterms.arrayShared();
    }

    @Override
    public final Term[] arrayClone(Term[] x, int from, int to) {
        return subterms.arrayClone(x, from, to);
    }

    @Override
    public final TermList toList() {
        return subterms.toList();
    }

    @Override
    public final int subs() {
        return subterms.subs();
    }

    @Override
    public final Term sub(int i) {
        return subterms.sub(i);
    }

    @Override
    public final Term sub(int i, Term ifOutOfBounds) {
        return subterms.sub(i, ifOutOfBounds);
    }

    @Override
    public final boolean subIs(int i, Op o) {
        return subterms.subIs(i, o);
    }



    @Override
    public Iterator<Term> iterator() {
        return subterms.iterator();
    }

    @Override
    public final Stream<Term> subStream() {
        return subterms.subStream();
    }

    @Override
    public final int count(Op matchingOp) {
        return subterms.count(matchingOp);
    }

    @Override
    public final int count(Predicate<? super Term> match) {
        return subterms.count(match);
    }

    @Override
    public final void forEach(/*@NotNull*/ Consumer<? super Term> c) {
        subterms.forEach(c);
    }

    @Override
    public final void forEach(/*@NotNull*/ Consumer<? super Term> action, int start, int stop) {
        subterms.forEach(action, start, stop);
    }
//    @Override
//    public final void forEachI(ObjectIntProcedure<Term> t) {
//        subterms.forEachI(t);
//    }

//    @Override
//    public final <X> void forEachWith(BiConsumer<Term, X> t, X argConst) {
//        subterms.forEachWith(t, argConst);
//    }

    @Override
    public int addAllTo(Term[] t, int offset) {
        return subterms.addAllTo(t, offset);
    }

    @Override
    public void addAllTo(Collection target) {
        subterms.addAllTo(target);
    }



    @Override
    public boolean TEMPORAL_VAR() {
        return dt()==XTERNAL || subterms.TEMPORAL_VAR();
    }

    @Override
    public final int complexity() {
        return subterms.complexity();
    }

    @Override
    public final int volume() {
        return subterms.volume();
    }

    @Override
    public final int varQuery() {
        return subterms.varQuery();
    }

    @Override
    public final int varPattern() {
        return subterms.varPattern();
    }

    @Override
    public final int varDep() {
        return subterms.varDep();
    }

    @Override
    public final int varIndep() {
        return subterms.varIndep();
    }

    @Override
    public final int vars() {
        return subterms.vars();
    }


    @Override
    public final int intifyShallow(int v, IntObjectToIntFunction<Term> reduce) {
        return subterms.intifyShallow(v, reduce);
    }



    @Override
    public boolean BOOL(Predicate<? super Term> t, boolean andOrOr) {
        return subterms.BOOL(t, andOrOr);
    }

    @Override
    public <X> boolean ORwith(BiPredicate<Term, X> p, X param) {
        return subterms.ORwith(p, param);
    }

    @Override
    public <X> boolean ANDwith(BiPredicate<Term, X> p, X param) {
        return subterms.ANDwith(p, param);
    }

    @Override
    public <X> boolean ANDwithOrdered(BiPredicate<Term, X> p, X param) {
        return subterms.ANDwithOrdered(p, param);
    }


}