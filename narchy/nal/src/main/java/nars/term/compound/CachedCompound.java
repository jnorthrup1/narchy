package nars.term.compound;

import jcog.Hashed;
import jcog.The;
import nars.Op;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.util.TermException;

import static nars.Op.DTERNAL;


/**
 * on-heap, caches many commonly used methods for fast repeat access while it survives
 */
public final class CachedCompound extends SeparateSubtermsCompound implements The, Hashed {

    private final int dt;

    private transient int hash;

    public static Compound the(Op op, Subterms subterms) {
        return the(op, DTERNAL, subterms);
    }

    public static Compound the(Op op, int dt, Subterms s) {
        if (s instanceof Compound)
            throw new TermException("subterms should be a pure Subterms instance", op, dt, s);
        if (s instanceof TermList)
            throw new TermException(s.getClass() + " mutable Subterms", op, dt, s);

        return new CachedCompound(op.id, dt, s);
    }

    private CachedCompound(int op, int dt, Subterms s) {
        super(op, s);
        this.dt = dt;

//        TEMPORARY
//        if (dt!=DTERNAL && dt < -2147464360+1)
//        if (dt!=DTERNAL && dt!=XTERNAL && Math.abs(dt) > 1_000_000)
//            throw new WTF();
//        if (op==IMPL.id && dt==DTERNAL) {
//            Util.nop();
////            if (s.subs()!=2 || s.sub(0).compareTo(s.sub(1)) > 0)
////                throw new WTF();
//        }

        //dtDither test
//        if (dt!=DTERNAL && dt!=Op.XTERNAL && dt%10!=0)
//            throw new WTF(); //TEMPORARY

        //ConjBundle.assertReduced(this); //TEMPORARY
    }


    @Override
    public final int dt() {
        return dt;
    }

    @Override
    public final int hashCode() {
        int h = hash;
        return (h == 0) ? hash = super.hashCode() : h;
    }
}