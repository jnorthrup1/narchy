package nars.term.compound;

import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.UnitSubterm;
import nars.term.Compound;
import org.eclipse.collections.api.block.function.primitive.IntObjectToIntFunction;

import static nars.Op.DTERNAL;

public abstract class UnitCompound extends SameSubtermsCompound {

    protected UnitCompound(int op) {
        super(op);
    }

    protected abstract Term sub();

    public final Term[] arrayClone() {
        return new Term[]{sub()};
    }

    @Override
    public void setNormalized() {
        super.setNormalized();
        Term s = sub();
        if (s instanceof Compound)
            ((Compound)s).setNormalized();
    }

    @Override
    public boolean containsInstance(Term t) {
        return sub()==t;
    }

    @Override
    public final Term sub(int i) {
        if (i!=0)
            throw new ArrayIndexOutOfBoundsException();
        return sub();
    }


    @Override
    public boolean internables() {
        return sub().internable();
    }

    @Override
    public boolean containsNeg(Term x) {
        return sub().equalsNeg(x);
    }

    @Override
    public int hashCode() {
        //return Compound.hash(this);
        return Compound.hash1(opID, sub());
    }

    @Override
    public final int hashCodeSubterms() {
        return Subterms.hash(sub());
    }

    @Override
    public final int subs() {
        return 1;
    }

    @Override
    public final Subterms subterms() {
        return new UnitSubterm(sub());
    }

    @Override
    public final int dt() {
        return DTERNAL;
    }

    @Override
    public int volume() {
        return sub().volume()+1;
    }

    @Override
    public final int complexity() {
        return sub().complexity()+1;
    }

    @Override
    public final float voluplexity() {
        return sub().voluplexity()+1;
    }

    @Override
    public final int structureSurface() {
        return sub().opBit();
    }

    @Override
    public final int structureSubs() {
        return sub().structure();
    }

    @Override
    public int varPattern() {
        return sub().varPattern();
    }

    @Override
    public int varDep() {
        return sub().varDep();
    }

    @Override
    public int varIndep() {
        return sub().varIndep();
    }

    @Override
    public int varQuery() {
        return sub().varQuery();
    }

    @Override
    public int vars() {
        return sub().vars();
    }

    @Override
    public final int intifyShallow(int v, IntObjectToIntFunction<Term> reduce) {
        return reduce.intValueOf(v, sub());
    }

    @Override
    public final int intifyRecurse(int v, IntObjectToIntFunction<Term> reduce) {
        return reduce.intValueOf(sub().intifyRecurse(v, reduce), this);
    }
}