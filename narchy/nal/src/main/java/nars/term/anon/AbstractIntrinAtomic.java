package nars.term.anon;

import nars.Op;
import nars.Term;
import nars.term.atom.AbstractAtomic;

import java.util.function.Predicate;

public abstract class AbstractIntrinAtomic extends AbstractAtomic implements IntrinAtomic {

    protected  AbstractIntrinAtomic(Op o, byte num) {
        super(o, num);
    }

    protected AbstractIntrinAtomic(short intrin, byte... bytes) {
        super(intrin, bytes);
    }
    
    @Override
    public final short intrin() {
        return (short)hash;
    }

    @Override
    public final Predicate<Term> equals() {
        return x -> x == AbstractIntrinAtomic.this;
    }
    @Override
    public final Predicate<Term> equalsPN() {
        return x -> x.unneg() == AbstractIntrinAtomic.this;
    }

    @Override
    public final boolean equals(Object x) {
        return x == this;
    }

}