package nars.util;

import jcog.Is;
import jcog.random.RandomBits;

import java.util.Random;

/** implementations provide a time-bound context which supports
 *  awareness of temporal determinism */
public interface Timed {

    /** time units (cycles) constituting a "duration" */
    @Is("Time_constant") float dur();

    @Deprecated Random random();

    default RandomBits rng() {
        return new RandomBits(random());
    }

    /** absolute current time, in time units */
    long time();

}