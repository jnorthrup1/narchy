package nars.func;

import nars.$;
import nars.NAR;
import nars.NARS;
import nars.Narsese;
import nars.action.resolve.TaskResolve;
import nars.action.resolve.TaskResolver;
import nars.concept.Concept;
import nars.derive.Deriver;
import nars.derive.impl.BagDeriver;
import nars.derive.reaction.Reactions;
import nars.focus.time.NonEternalTiming;
import nars.func.stm.CompoundClustering;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.test.TestNAR;
import nars.time.Tense;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static nars.Op.BELIEF;
import static nars.Op.GOAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CompoundClusteringTest {

    private final NAR n = NARS.shell();

    @Test
    void testSameTruthSameTime() throws Narsese.NarseseException {


        int ccap = 3;
        conjCluster(n, ccap*2, BELIEF);

		n.run(1); //HACK

        for (int i = 0; i < ccap*2; i++)
            n.believe($.the("x" + i), Tense.Present);
        n.run(64);

        Concept c = n.concept($.$(
                "(&&,x0,x1)"
        ));
        assertNotNull(c);
        TaskTable b = c.beliefs();
//        assertEquals(1, b.taskCount());

        assertStamp("[1, 2]", b);
    }

    @Test
    void testNeg() throws Narsese.NarseseException {


        int ccap = 3;
        conjCluster(n, ccap*2, BELIEF);
//        n.log();
        n.run(1); //HACK

        for (int i = 0; i < ccap; i++)
            n.believe($.the("x" + i).neg(), Tense.Present);
        n.run(64);

        Concept c = n.concept($.$("(&&,--x0,--x1)"));
        assertNotNull(c);
        TaskTable b = c.beliefs();

        assertStamp("[1, 2]", b);
    }

    private static CompoundClustering conjCluster(NAR n, int ccap, byte punc) {
        return conjCluster(n, 2, ccap, punc);
    }

    private static CompoundClustering conjCluster(NAR n, int centroids, int ccap, byte punc) {
        CompoundClustering c = new CompoundClustering(punc, centroids, ccap, NALTask::isInput);
        c.eventMax.set(2);
		Deriver e = new BagDeriver(new Reactions().addAll(
            c, new TaskResolve(new NonEternalTiming(), TaskResolver.AnyTaskResolver)
        ), n);
        n.onCycle(()-> e.next(n.main()));
        return c;
    }

    private static void assertStamp(String stamp, TaskTable b) {
        NALTask the = b.taskStream().findFirst().get();
        float p = the.pri();
        assertEquals(p, p);
        assertEquals(stamp, Arrays.toString(the.stamp()));
     }

    @Test void DimensionalDistance1() {

        n.time.dur(4);

        conjCluster(n, 8, BELIEF);

        n.inputAt(1, "$1.0 x. |");
        n.inputAt(2, "$1.0 y. |");
        n.inputAt(1, "$0.1 z. |");
        n.inputAt(3, "$0.1 w. |");


        n.run(2);
        //TODO
//        assertEquals(1, n.concept($.$("")).beliefs().size());

    }
    @Test void ConjGoal() {
        int cycles = 10;

        TestNAR t = new TestNAR(n);

        t.volMax(7);
        n.time.dur(1);

        CompoundClustering c = conjCluster(n, 8, GOAL);

        n.inputAt(1, "x! |");
        n.inputAt(2, "y! |");

        n.inputAt(6, "a! |");
        n.inputAt(7, "b! |");

        t.mustGoal(cycles, "(x &&+1 y)", 1f, 0.9f*0.9f, (s,e)->s==1 && e==1);
        t.mustGoal(cycles, "(a &&+1 b)", 1f, 0.9f*0.9f, (s,e)->s==6 && e==6);
        t.run();
    }
    
}