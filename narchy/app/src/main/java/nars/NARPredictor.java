package nars;

import jcog.TODO;
import jcog.math.FloatSupplier;
import jcog.predict.Predictor;
import jcog.signal.FloatRange;
import nars.game.Game;
import nars.game.sensor.ScalarSensor;

import java.util.Random;

/** general purpose vector -> vector associative memory.
 *  uses an embedded NAR and (TODO optional) eternal reasoning.
 *
 *  TODO
 *  */
public class NARPredictor implements Predictor {
    final NAR nar;
    private final int inputs;
    private final Term[] xt, yt;
    private final IntegratingScalarSensor[] x;
    private final IntegratingScalarSensor[] y;

    public NARPredictor(int inputs, int outputs) {
        this(inputs, outputs, NARS.tmp());
    }

    public NARPredictor(int inputs, int outputs, NAR nar) {
        this.nar = nar;
        this.inputs = inputs;

        this.x = new IntegratingScalarSensor[inputs];
        xt = new Term[inputs];
        this.y = new IntegratingScalarSensor[outputs];
        yt = new Term[inputs];
//        this.game = new Game($.the("xy")) {
        {
            for (int i = 0; i < inputs; i++) {
                x[i] = new IntegratingScalarSensor(xt[i] = x(i));
            }
            for (int i = 0; i < inputs; i++) {
                y[i] = new IntegratingScalarSensor(yt[i] = y(i));
            }
        }
//        };
    }

    @Override
    public double[] put(double[] x, double[] y, float pri) {
        throw new TODO();
    }

    @Override
    public double[] get(double[] x) {
        return new double[0];
    }

    static class IntegratingScalarSensor extends ScalarSensor {

        final FloatSupplier x = new FloatRange(0.5f, 0, 1);

        public IntegratingScalarSensor(Term id) {
            super(id);
        }

        public void addDelta(float d) {
            x.plus(d);
        }

        @Override
        public void accept(Game g) {
            accept(x.asFloat(), g);
        }

    }


    /** TODO cache */
    private Term x(int i) {
        return $.the("x" + i);
    }
    /** TODO cache */
    private Term y(int i) {
        return $.the("y" + i);
    }


    @Override
    public void clear(Random rng) {
        nar.main.clear();
    }
}