package nars.game;

import jcog.Fuzzy;
import jcog.Is;
import jcog.Research;
import jcog.TODO;
import jcog.event.ListTopic;
import jcog.event.Off;
import jcog.event.Topic;
import jcog.signal.FloatRange;
import jcog.thing.Part;
import nars.*;
import nars.concept.Concept;
import nars.exe.NARLoop;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.focus.PriAmp;
import nars.focus.PriNode;
import nars.focus.util.PriTree;
import nars.game.action.AbstractAction;
import nars.game.action.AbstractGoalAction;
import nars.game.action.CompoundAction;
import nars.game.reward.LambdaScalarReward;
import nars.game.reward.MultiReward;
import nars.game.reward.Reward;
import nars.game.reward.ScalarReward;
import nars.game.sensor.FocusLoop;
import nars.game.sensor.ScalarSensor;
import nars.game.sensor.Sensor;
import nars.game.sensor.VectorSensor;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.time.When;
import nars.truth.Truther;
import nars.util.Timed;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static nars.Op.BELIEF;

/**
 * an integration of sensor concepts and motor functions
 * interfacing with an environment forming a sensori-motor loop.
 * <p>
 * these include all forms of problems including
 * optimization
 * reinforcement learning
 * etc
 * <p>
 * the name 'Game' is used, in the most general sense of the
 * word 'game'.
 *
 * TODO min required vocabulary volume test
 */
@Research
@Is({"General_game_playing", "Game_studies", "Game_theory"})
public class Game extends NARPart /* TODO extends ProxyWhat -> .. and commit when it updates */ implements NSense, NAct, NReward, Timed, NARLoop.Pausing {

	/**
	 * the context representing the experience of the game
	 */
	private Focus focus;

	public final Sensors sensors = new Sensors();
	public final Actions actions = new Actions();
	public final Rewards rewards = new Rewards();

	/** global curiosity rate */
	public final FloatRange curiosityDurs = new FloatRange(NAL.CURIOSITY_DURS, 0, 200);

	/** probability of disabling an enabled curiosity goal */
	public final FloatRange curiosityOffDurs = new FloatRange(Math.max(1,NAL.CURIOSITY_DURS/4f), 0, 100);

	public final AtomicBoolean enable = new AtomicBoolean(true);

	/** frame timing */
	private final GameTime clock;

	public final When<Focus> time = new When(Long.MIN_VALUE, Long.MIN_VALUE);

	private final Topic<Game> beforeFrame = new ListTopic<>();
	private final Topic<Game> afterFrame = new ListTopic<>();


	/** populated after initialization */
	public final Set<Term> vocabulary = new HashSet<>();

	private final Truther truther = new Truther.FreqTruther();

	public Game(String id) {
		this($.$$(id));
	}

	public Game(Term id) {
		this(id, GameTime.durs(
			1
			//2 /* nyquist */
		));
	}

	public Game(String id, GameTime clock) {
		this($.$$(id), clock);
	}

	/** HACK game ID, overrides superclass's ID field */
	public final Term id;

	public Game(Term id, GameTime clock) {
		super($.inh(id,GAME));

		this.id = id;
		this.clock = clock;

		this.focus = newFocus();

	}


	@Override
	public final Focus focus() {
		return focus;
	}


	/**
	 * dexterity = mean(conf(action))
	 * evidence/confidence in action decisions, current measurement
	 */
	public final double dexterity() {
		return enabled() ? actions.dexterity() : Double.NaN;
	}

	public final double coherency() {
		return enabled() ? actions.coherency(time.s, time.e, time.dur, nar) : Double.NaN;
	}


	/**
	 * avg reward satisfaction, current measurement
	 * happiness metric applied to all reward concepts
	 */
	public final double happiness(long start, long end, float dur) {
		return enabled() ? rewards.happiness(start, end, dur) : Double.NaN;
	}

	public final double happiness() {
		return happiness(time.s, time.e, time.dur);
	}

//	public final double frustration() {
//		return enabled() ? actions.frustration(nowLoop.s, nowLoop.e, nowLoop.dur, nar) : Double.NaN;
//	}

	@Override
	public final <A extends FocusLoop<Game>> A addAction(A c) {
		return actions.addAction(c);
	}

	@Override
	public final <S extends Sensor> S addSensor(S s) {
		return sensors.addSensor(s);
	}

	private void init(PriNode target, Object s) {

		if (s instanceof Controlled)
			((Controlled) s).controlStart(this);

		if (s instanceof FocusLoop) {
			((FocusLoop<Game>)s).start(this);
		}

		PriTree pri = nar.pri;

		if (s instanceof VectorSensor ss) {
			pri.link(ss.pri, target);
		} else if (s instanceof ScalarSensor ss) {
			pri.link(ss.pri, target);
		} else if (s instanceof Reward ss) {
			if (ss instanceof ScalarReward sss)
				pri.link(sss.sensor.pri, target);
			else if (ss instanceof MultiReward sss) {
				for (LambdaScalarReward rr : sss.r)
					pri.link(rr.sensor.pri, target); //HACK
			} else
				throw new TODO();
		} else if (s instanceof PriNode ss)
			pri.link(ss, target);
		else if (s instanceof AbstractAction ss) {
			if (ss instanceof AbstractGoalAction sss)
				pri.link(sss.pri, target);
			else
				throw new TODO();
		} else if (s instanceof CompoundAction ss) {
			pri.link(ss.pri, target);
		} else
			throw new TODO();

		if (s instanceof NARPart ss)
			nar.add(ss);

		if (s instanceof FocusLoop ss)
			vocabularize(ss);


	}

	@Deprecated public Random random() {
		Focus w = this.focus;
		return w != null ? w.random() : ThreadLocalRandom.current();//HACK
	}

	@Override
	public long time() {
		return time.mid();
	}

	public String summary() {
		return id +
			" dex=" + /*n4*/(actions.dexterity()) +
			" hapy=" + /*n4*/(happiness()) +

			/*" var=" + n4(varPct(nar)) + */ '\t' + nar.memory.summary() + ' ' +
			nar.emotion.summary();
	}

	/**
	 * registers sensor, action, and reward concepts with the NAR
	 * TODO call this in the constructor
	 */

	protected final void starting(NAR nar) {
		super.starting(nar);

		focus.nar = nar; //HACK early
		nar.add(focus);
		focus.focusLocal(Game.class, z->this);
		focus.focusLocal(NARPart.class, z->this);
		time.the(focus);


		PriTree tree = nar.pri;

		tree.link(actions.pri = new PriAmp($.p(focus.id, ACTION),
			nar.goalPriDefault.pri()
		), focus.pri);
		tree.link(sensors.pri = new PriAmp($.p(focus.id, SENSOR),
			nar.beliefPriDefault.pri()
		), focus.pri);
		tree.link(rewards.pri = new PriAmp($.p(focus.id, REWARD),
//	1
//			Math.max
			Fuzzy.or
				(nar.beliefPriDefault.pri(), nar.goalPriDefault.pri())
		), focus.pri);


		init();

		vocabularize();

		clock.start(this);

		//HACK possible race condition if clock starts asynchronous
		focus.dur(clock.dur()); //default/initial dur
	}

	private void vocabularize() {
		for (FocusLoop s : sensors.sensors)
			init(sensors.pri, s);

		for (Reward r : rewards)
			init(rewards.pri, r);

		for (FocusLoop<Game> a : actions._actions)
			init(actions.pri, a);

		for (AbstractAction a : actions)
			init(actions.pri, a);

		nar.pri.update();
	}

	private void vocabularize(FocusLoop<Game> s) {
		s.components().forEach(ss -> vocabularize(ss.term()));
	}

	private void vocabularize(Term t) {
		if (t instanceof Compound) {
			for (Term tt : t.subterms())
				vocabularize(tt);
		} else if (t.ATOM())
			vocabulary.add(t);
	}

	/** constructs a new focus context */
	protected Focus newFocus() {
		return new BagFocus(id, NAL.DEFAULT_GAME_CAPACITY);
	}

	protected void stopping(NAR nar) {
		clock.stop();

		sensors.sensors.stream().filter(x -> x instanceof NARPart).forEach(s -> nar.remove((NARPart) s));

		nar.pri.removeAll(actions.pri, sensors.pri, rewards.pri);

		focus.focusLocal(Game.class, (g)->null);

		this.focus.close();
		//this.focus = null;

		//this.nar = null;
	}

	/**
	 * subclasses can safely add sensors, actions, rewards by implementing this.  NAR and What will be initialized prior
	 */
	protected void init() {

	}



	/**
	 * default reward module
	 */
	@Override public final synchronized void reward(Reward r) {
		rewards.add(r);
	}

	@Override
	@Deprecated public final Term rewardTerm(String r) {
		return $.inh(id, $.$$(r));
	}

	/**
	 * loop duration
	 */
	public final float dur() {
		return time.dur;
	}

	@Override
	public final void pause(boolean pause) {
		//clock.pause(pause);
		enable(!pause);
	}

	public final void enable(boolean e) {
		enable.setOpaque(e);
	}
	public final boolean enabled() {
		return enable.getOpaque();
	}


	/**
	 * iteration
	 */
	protected final void next() {

		if (!isOn() || !enabled() || !setTime())
			return;

		setTruther();

		//TODO what is the right order for these?

		beforeFrame.emit(this);



		sense();

		actions.actBefore(this);

		afterFrame.emit(this);

		actions.actAfter(this);

		reward();
	}

	private boolean setTime() {
		long prev = time.s;

		//center aligned
		//nowLoop.setCenterDur(nar.time(), clock.dur(), nar.dtDither());

		long now = nar.time(); //left aligned

		float dur = clock.dur();

		long s = time.when(now, Math.max(now, now + Math.round(dur) - 1)/* dither:, nar */).dur(dur).s;
		if (s <= prev)
			return false; //not yet

		return true;
	}

	private void setTruther() {
		((Truther.FreqTruther)truther)
				.freqRes(nar.freqResolution.floatValue())
				.conf(nar.confDefault(BELIEF));
	}

	@Override
	public final NAR nar() {
		return nar;
	}

	private void beforeFrame() {
		beforeFrame.emit(this);
	}

	private void sense() {
		sensors.sensors.forEach(this::update);
	}

	private void reward() {
		rewards.update(this);
	}



	void update(FocusLoop s) {
		if (s instanceof Part && !((Part) s).isOn()) return;

		s.accept(this);
	}

	/** add before-phase of the frame callback */
	public Off onFrame(Consumer<Game> eachFrame) {
		return beforeFrame.on(eachFrame);
	}

	/** add before-phase of the frame callback */
	public Off onFrame(Runnable eachFrame) {
		return beforeFrame.on(eachFrame);
	}

//	public Off onFrameWeak(Runnable eachFrame) {
//		return eventFrame.onWeak(eachFrame);
//	}

	/** add after-phase of the frame callback */
	public Off afterFrame(Runnable eachFrame) {
		return afterFrame.on(eachFrame);
	}
	public Off afterFrame(Consumer<Game> eachFrame) {
		return afterFrame.on(eachFrame);
	}
//	public Off logActions() {
//		return onFrame(new Runnable() {
//			DataTable data = null;
//			Object[] row = null;
//
//			{
//				Runtime.getRuntime().addShutdownHook(new Thread(()->{
//					System.out.println(data);
//					String file = "/tmp/" + id + ".actions.csv";
//					try {
//						data.write().csv(file);
//						System.err.println("wrote: " + file + " (" + data.rowCount() + " rows)");
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//					data.columns().forEach(c -> {
//						//StreamingStatistics s = new StreamingStatistics(true);
//						DescriptiveStatistics s = new DescriptiveStatistics(4096);
//						((DoubleColumn)c).forEach(_d -> {
//							double d = _d;
//							if (d == d)
//								s.addValue(d);
//						});
//						System.out.println(c.name() + "\t" + s.toString().replace("\n", ", "));
//
//					});
//				}));
//			}
//
//			@Override
//			public void run() {
//				int n = actions.size();
//				if (data == null || data.columnCount()!=n) {
//					Table t = Table.create(actions.stream().map(x -> DoubleColumn.create(x.term.toString())));
//					row = new Object[n];
//					data = new DataTable(t);
//				}
//				int i = 0;
//				for (AbstractAction a : actions) {
//					row[i++] = a.freq();
//				}
//				data.add(row);
//
//			}
//		});
//	}

	public Game capacity(int c) {
		((BagFocus)focus()).capacity.set(c);
		return this;
	}

	/** streams the NAR's relevant beliefs for this Game */
	public Stream<NALTask> knowledge() {
		return nar().concepts().filter(x -> relevant(x.term())).flatMap(Concept::tasks);
	}

	public boolean relevant(Term x/* TODO , boolean allowOutsiders */) {
		if (x instanceof Compound) {
			return ((Compound) x).AND(this::relevant);
		} else if (x.ATOM()) {
			return vocabulary.contains(x);
		} else
			return true; //variable or special
	}

	/** default truther used by signals */
	public Truther truther() {
		return truther;
	}

	public final double rewardMean() {
		return rewards.rewardMean();
	}

	public final float durFocus() {
		return focus().time.dur();
	}


	private static String lClass(Class c) {
		return c.getSimpleName().toLowerCase();
	}

	private static final Atom GAME = Atomic.atom(lClass(Game.class));

	private static final Atom ACTION = Atomic.atom("action");
	private static final Atom SENSOR = Atomic.atom(lClass(Sensor.class));
	private static final Atom REWARD = Atomic.atom(lClass(Reward.class));

}