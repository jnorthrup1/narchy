package nars.game.action.util;

import com.google.common.collect.Streams;
import jcog.Is;
import jcog.Log;
import jcog.Research;
import jcog.Util;
import jcog.agent.Agent;
import jcog.agent.SensorBuilder;
import jcog.agent.SensorTensor;
import jcog.data.list.Lst;
import jcog.event.Off;
import jcog.math.Digitize;
import jcog.math.FloatSupplier;
import jcog.ql.dqn.ValuePredictAgent;
import jcog.random.XoRoShiRo128PlusRandom;
import jcog.signal.FloatRange;
import jcog.signal.anomaly.ewma.Ewma;
import jcog.thing.SubPart;
import nars.NAR;
import nars.concept.Concept;
import nars.concept.TaskConcept;
import nars.control.Cause;
import nars.focus.Focus;
import nars.game.Game;
import nars.game.Rewards;
import nars.game.action.AbstractAction;
import nars.game.action.AbstractGoalAction;
import nars.game.sensor.FocusLoop;
import nars.table.BeliefTables;
import nars.table.dynamic.MutableTasksBeliefTable;
import nars.task.SerialTask;
import nars.term.Termed;
import nars.time.Moment;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.lang.Float.NaN;
import static java.lang.Math.max;
import static java.lang.Math.round;
import static jcog.Util.*;
import static nars.Op.GOAL;
import static nars.truth.func.TruthFunctions.*;

/**
 * Low-level/instinct sensorimotor reflexes.
 * NAgent Reinforcement Learning Algorithm Accelerator
 * trains and is trainable in cooperation with other action generators.
 * (multiple instances of this can cooperate with each other)
 * <p>
 * Default timing model:
 * <p>
 * [Previous Dur]			[This Dur]				[Next Dur]
 * prevStart...nowStart	nowStart...nextStart	nextStart...nextEnd
 * * reward				*sensorsNow
 * * actionsPrev			*actionsNow
 */
@Research
@Is("Reinforcement_learning")
public class Reflex implements SubPart<Game>, Consumer<Game> {



    /*
     * TODO use AgentBuilder
     * TODO auto-confidence in proportion to NAL beliefs
     * TODO overlay belief table like BeliefPrediction?
     */

    public static final Logger logger = Log.log(Reflex.class);

    /**
     * >=1, TODO constructor param
     */
    private static final int trainGoals = 1;
    private static final float digitizedActionSpecificity =
            //4;
            2;
    @Deprecated
    private static final boolean temporalSuperSampling = false;

    /**
     * NAR goal training rate, in proportion to action's measured dexterity
     */
    public final FloatRange train = new FloatRange(0.25f, 0, 2);

//	public final Agenterator agentMeta;
    /**
     * relative durs of the game which this runs
     */
    public final FloatRange durs = new FloatRange(1, 0.5f, 8f);
    public final AtomicBoolean enabled = new AtomicBoolean(true);
    private final Game game;

    ///** action DAC low-pass smoothing */
    //public final FloatRange actionMomentum = new FloatRange(0f, 0, 1);

    final ReflexBuilder builder;

    private final Lst<Consumer<SensorBuilder>> sensorBuilders = new Lst<>();

    @Deprecated private final List<AbstractAction> _actions = new Lst<>();
    @Deprecated private final XoRoShiRo128PlusRandom rng = new XoRoShiRo128PlusRandom();

    /**
     * TODO potentially mutable
     */
    public Agent agent;
    public double[] actionIn = null;
    @Deprecated private int inD, outD;
    private transient SensorTensor sensors;
    private Cause cause;
    private ReflexActionTable[] actionTables;


    private double reward = NaN;
    //1;
    //0.25f;
    private transient double[] input;
    private transient float durReflex;
    /**
     * duration across which truth is sampled for sensors, actions, rewards
     */
    private transient float durTruth;
    private transient long now = Long.MIN_VALUE;
    private Off on, onAfter;
    private long nowStart, nextStart;

    public static final class ReflexBuilder {

        public final static ReflexBuilder RANDOM = new ReflexBuilder("Random", ValuePredictAgent::Random, 1);

        String name;
        private final IntIntToObjectFunction<Agent> agent;
        private final float[] history;

        private static final boolean senseSensors = true;

        final boolean senseActions =
            true;
            //false;

        /** especially important to be true if multiple rewards are present,
         *  then at least as sensors they can be separated -
         *  since RL/MDP must scalarize them as the reward signal. */
        final boolean senseRewards =
            true;
            //false;

        public ReflexBuilder(IntIntToObjectFunction<Agent> agent, float[] history) {
            this(null, agent, history);
        }

        public ReflexBuilder(@Nullable String name, IntIntToObjectFunction<Agent> agent, float... history) {
            this.name = name;
            this.agent = agent;
            this.history = history;

            if (history.length < 1)
                throw new UnsupportedOperationException();
        }

        @Override
        public String toString() {
            return (name!=null ? name : agent) +
                    "_H=" + Arrays.toString(history);
        }

        /**
         * TODO abstract history model, with non-uniform timesteps
         */
        void add(Game g, Reflex r) {

            Lst<Consumer<SensorBuilder>> sensorBuilders = r.sensorBuilders;

            /* warning: this might cause unsafe feedback loops (as it is, unclamped) */
            @Deprecated boolean senseActionGoals = false;

            g.actions.forEach(r._actions::add);

            sensorBuilders.add((s) -> {
                //false;
                //pastDurs.length==1 && g.rewards.size() > 1; //use individual rewards as sensors if > 1 reward

                Stream<Termed> ins = Stream.concat(Stream.<FocusLoop>concat(
                                    senseSensors ? g.sensors.stream() : Stream.empty(),
                                    (senseActions || g.sensors.sensors.isEmpty()) ? g.actions.stream() : Stream.empty()
                                ),
                                senseRewards ? g.rewards.stream() : Stream.empty())
                        .flatMap(x -> Streams.stream(x.components()));

                r.sensors(s, ins, history);

                if (senseActionGoals) {
                    //s.in(new ArrayTensor(this.agent.actionNext))
                    int aa = g.actions.size();
                    for (int i = 0; i < aa; i++) {
                        int I = i;
                        s.in(() -> (float) r.agent.actionNext[I]);
                    }
                }
            });
        }

    }

    @Deprecated public Reflex(IntIntToObjectFunction<Agent> agentBuilder, Game g, float... history) {
        this(new ReflexBuilder(agentBuilder, history),g);
    }

    public Reflex(ReflexBuilder b, Game g) {
        this.builder = b;

        this.game = g;

        try {
            g.add(this);
        } catch (Throwable ignore) {
            //HACK
        }


    }

    private static float truthFeedback(Truth t) {
        return t.freq();
        //return t.expectation();
    }
//
//    /**
//     * abstracted API
//     *  TODO NOT WORKING YET abstract history model, with non-uniform timesteps
//     */
//    @Deprecated
//    public void _add(Game g, float[] timeLens) {
//        assert (timeLens.length >= 1);
//
//        g.actions.forEach(_actions::add);
//
//        sensorBuilders.add((s) -> {
//
//
//            if (senseSensors) {
//                SensorInput SENSORS = new SensorInput(timeLens, s, 0);
//                components(g.sensors.stream()).forEach(SENSORS);
//            }
//            if (senseRewards)
//                components(g.rewards.stream()).forEach(
//                        new SensorInput(timeLens, s, Rewards.REWARD_OFFSET_DURS));
//
//            if (senseActions) {
//                //offset: -1 because we are still deciding actions for now
//                components(g.actions.stream()).forEach(
//                        new SensorInput(timeLens, s, -1));
//            }
//        });
//    }


    /**
     * cartesian product of concepts and times (in past durations ago)
     * <p>
     * aggregates (avg) the span between the specified durations, which can be non-uniform:
     * <p>
     * ex: 1,2,4,16
     * [         16...4        ] [  4..2  ] [2..1] [1..0] NOW
     * <-- past
     */
    private void sensors(SensorBuilder s, Stream<Termed> ins, float[] pastDurs) {
        ins.forEach(t -> {

            float pastpast = 0; //start at now
            final Concept[] c = {null};

            for (int p = 0; p < pastDurs.length; p++) {
                float pastEnd = pastpast;
                float pastStart = pastDurs[p];

                s.in(new SensorInputter(
                        c, t, pastStart, pastEnd, p));

                pastpast = pastStart;
            }
        });
    }

    @Override
    public void startIn(Game g) {

        this.cause = g.nar.control.newCause(this);

        builder.add(g, this);

        SensorBuilder s = new SensorBuilder();
        for (Consumer<SensorBuilder> sb : sensorBuilders)
            sb.accept(s);

        this.sensors = s.sensor();

        this.inD = this.sensors.volume();
        this.input = new double[inD];

        var actions = _actions.stream()
                .filter(z -> z instanceof AbstractGoalAction)
                .toArray(AbstractGoalAction[]::new);
        assert (actions.length > 0);

        this.actionTables = Stream.of(actions).map(new Function<AbstractGoalAction, MutableTasksBeliefTable>() {
            int from = 0, to = -1;

            @Override
            public MutableTasksBeliefTable apply(AbstractGoalAction a) {

                int poles = a.poles();

                to = from + poles;

                MutableTasksBeliefTable t = new ReflexActionTable(a, from, to, game);

                ((BeliefTables) a.concept.goals()).add(t);

                from = to;

                return t;
            }
        }).toArray(ReflexActionTable[]::new);

        actionIn = new double[outD = Stream.of(actionTables).mapToInt(ReflexActionTable::poles).sum()];
        assert (inD > 0 && outD > 0);

        agent(builder.agent.value(inD, outD));

        assert (this.on == null);

        this.on = game.afterFrame(this);

    }

    @Override
    public void stopIn(Game g) {
        this.on.close();
        this.on = null;
    }

//	private float sensorValueIfMissing() {
//		//return 0.5f;
//		return noise();
//	}

    /**
     * set the agent
     */
    public void agent(Agent a) {
        if (agent != a) {
            this.agent = a;
            logger.info("{} {} in={} out={}", agent, game, inD, outD);
        }
    }

    /**
     * uniform [0,1] noise
     */
    @Deprecated private float noise() {
        return rng.nextFloat();
    }

//	private void _feedback(long prev, long now, double[] z) {
//		int k = 0;
//		NAR n = game.nar();
//
//		for (AbstractGoalActionConcept s : actions) {
//			Truth t = s.beliefs().truth(prev, now, durReflex, n);
//			float Y = t != null ? truthFeedback(t) : Float.NaN;
//			float y = Y == Y ? Y : noise();
//			if (actionDiscretization > 1) {
//				//DIGITIZE
//				for (int d = 0; d < actionDiscretization; d++) {
//					float yd = (d + 0.5F) / actionDiscretization;
//					z[k++] = (float)pow(1 - abs(yd - y), digitizedActionSpecificity);
//				}
//				Util.normalizeSubArraySum1(z, k-actionDiscretization, k);
//			} else {
//				z[k++] = y;
//				//z[k++] = polarize(y);
//			}
//		}
//
//		if (this.nothingAction)
//			z[z.length - 1] =
//					(float) (1 - Util.sum(z, 0, z.length-1)/(z.length-1));
//					//(1 - Util.max(fb)) * 1f / (1 + Util.variance(fb)); //HACK TODO estimate better
//
//
//	}


    @Override
    public final void accept(Game g) {

        if (!enabled.getOpaque() || !updateTime())
            return;

        int gDur = round(g.dur());
        int rDur = round(durReflex);

//        if (iDur <= 1)
//            throw new TODO("verify that " + Reflex.class.getSimpleName() + " works with dur=1");

        long now = this.now;
        nowStart = now - gDur/2;
        long prevStart = nowStart - rDur;
        nextStart = nowStart + rDur;


        senseIn(); //sensors first, since it might include previous cycle's actionIn not to be overwritten yet
        actionIn(
                //prevStart, nowStart,
                //prevStart + gDur/2, nowStart + gDur/2,
                prevStart, nowStart,
                actionIn);

        /* coordinate with ScalarReward.RewardScalarSensor.rewardOccurenceShiftDurs */
        rewardIn(
                nowStart + Rewards.REWARD_OFFSET_DURS*rDur, nowStart
                //prevStart, nowStart
                //nowStart, nextStart
                //prevStart + gDur/2, nowStart + gDur/2
                //nowStart, nextStart
        );

        predict();

        actionOut(
                nowStart, nextStart
                //nextStart, nextStart + (nextStart - nowStart)
        );
    }

    private void actionIn(long s, long e, double[] actions) {
        NAR nar = game.nar();
        for (ReflexActionTable table : actionTables)
            table.actionIn(s, e, actions, nar);
    }

    private void rewardIn(long s, long e) {
        double h = game.happiness(s, e, durTruth);
        this.reward =
                h;
        //polarize(h);
    }


    private void senseIn() {
        sensors.update().writeTo(input);
    }

    //private final Ewma rewardNormalized = new Ewma(1,0.01f);

    private void predict() {

        double r0 = this.reward;
        double r = r0;
        //double r = rewardPolarize ? Fuzzy.polarize(r0) : r0;
        //double r = -1 + r0;
        agent.act(actionIn, (float) r, input,
                agent.actionNext);

    }

    private void actionOut(long nextStart, long nextEnd) {

        float train = this.train.floatValue();
//		if (train <= Float.MIN_NORMAL)
//			return; //disabled

        float pri =
                //Prioritizable.EPSILON;
                //0;
                game.nar.priDefault(GOAL); // * train;
        // /actionTables.length;

        //TODO compute freq[] array using abstract method, then apply freq[i] to each table

        var f = game.focus();
        double[] actionOut = agent.actionNext;
        for (ReflexActionTable t : actionTables)
            t.goal(actionOut, nextStart, nextEnd, train, pri, f);
    }

    /**
     * DIGITAL -> ANALOG
     */
    private double dac(double[] x, int i) {
        double xi = x[i];
        if (xi != xi) {
			//xi = noise(); //HACK
			throw new UnsupportedOperationException();
		} else
            return unitize(xi);
    }

    private boolean updateTime() {
        Moment NOW = game.time;
        float gameDur = game.dur();
        float durRL = gameDur * durs.floatValue(); //w = w.add(Math.round(nowDur/2f));
        long now =
                NOW.mid();
        //NOW.e; //late edge of now
        //Util.mean(NOW.s, NOW.e);
        long _now = this.now;
        int iDurRL = round(durRL);
        if (now < _now + (temporalSuperSampling ? 0 : iDurRL - game.nar.dtDither() / 2))
            return false;


        this.now = now;
        this.durReflex = durRL;

        this.durTruth =
                1;
        //0;
        //durReflex;

        for (ReflexActionTable t : actionTables)
            t.stretchDurs = durRL;

        return true;

    }

    @Deprecated
    public void afterFrame(Runnable runnable) {
        game.afterFrame(runnable);
    }

    /**
     * past is number of durations ago to begin (left-aligned) sensed frame
     */
    private Truth input(Concept c, float pastStart, float pastEnd, float o) {
        long now = nextStart;
        float dur = durReflex;
        long s = now + round((o - pastStart) * dur);
        long e = now + round((o - pastEnd) * dur);
        return c.beliefs().truth(s, e, durTruth, game.nar);
    }

    private float scalar(Truth t) {
        return t != null ?
                t.freq() :
                0.5f /*NaN*/ /*noise()*/;
    }

    private class ReflexActionTable extends MutableTasksBeliefTable {

        final Ewma dex = new Ewma().period(4, 16).with(0);
        private final AbstractAction action;

        /** action vector index range */
        final int from, to;


        ReflexActionTable(AbstractAction a, int from, int to, Game g) {
            super(a.term(), false, trainGoals);
            this.action = a;
            sharedStamp = g.nar.evidence();
            this.from = from; this.to = to;
        }

        @Override
        public SerialTask task(long start, long end, float f, double evi, long[] stampIgnored) {
            SerialTask x = super.task(start, end, f, evi, sharedStamp);
            x.why(cause.ID);
            return x;
        }

        @Override
        protected Truth taskTruth(float f, double evi) {
            return new MutableTruth(f, evi);
        }

        void adc(double freq, double[] action) {
            int poles = poles();
            assertUnitized(freq);

            switch(poles) {
                case 1: { action[from] = freq; break; }
                case 2: {


                    Digitize digitizer =
                        Digitize.FuzzyNeedle;
                        //Digitize.BinaryNeedle;

                    float FREQ = (float) freq;
                    for (int d = 0; d < poles; d++)
                        action[from + d] = digitizer.digit(FREQ, d, poles);

//                    //contrast exponent curve
//                    if (actionContrast!=1) {
//                        for (int d = 0; d < actionDigitization; d++)
//                            y[j + d] = Math.pow(y[j + d], actionContrast);
//                    }

                    //normalize so each action's components sum to 1
                    Util.normalize(action, from, to, 0, Util.sum(action, from, to));

                    //normalize to max component value
                    //Util.normalize(action, from, to, 0, Util.max(from, to, action));

                    break;
                }
                default: throw new UnsupportedOperationException();
            }
        }

        double dac(double[] action) {
            return switch (poles()) {
                case 1 -> action[from];
                case 2 -> undigitizeWeightedMean(action, from, poles());
                default -> throw new UnsupportedOperationException();
            };
        }

        /**
         * digital -> analog
         */
        static double undigitizeWeightedMean(double[] y, int i, int poles) {
            double x = 0, sum = 0;
            for (int d = 0; d < poles; d++) {
                double D = y[i + d];
                //D = Fuzzy.unpolarize(D);
//                D = Util.unitize(D);
                D = (D < 0) ? 0 : D; //D = Math.max(0, D);

                //D = Math.max(0, D)/max;
                //D = Util.normalize(D, min, max);
                float value = ((float) d) / (poles - 1);
                x += value * D;
                sum += D;
            }
            if (sum > Float.MIN_NORMAL)
                x /= sum;
            else
                x = 0.5f;
            return x;
        }
//        public double[] joinSoftmax(double[] y, double[] tgt) {
//            final DecideSoftmax decide = new DecideSoftmax(0.1f, rng);
//            for (int i = 0, k = 0; k < tgt.length; ) {
//                int index = decide.applyAsInt(Util.toFloat(y, i, i + actionDigitization));
//                tgt[k++] = ((float) index) / (actionDigitization - 1);
//                i += actionDigitization;
//            }
//            return tgt;
//        }

        private int poles() {
            return to-from;
        }

        public void goal(double[] action, long s, long e, float strength, float pri, Focus f) {
            double a = dac(action);
            if (a==a) {
                goal(unitizeSafe(a), s, e, strength, pri, f);
            }
        }

        private void goal(double f, long s, long e, float strength, float pri, Focus g) {
            double F = Util.round(f, action.resolution());

            double dex = action.dexterity();
            double dexMean = this.dex.acceptAndGetMean(dex);
            double C = e2c(max(AbstractGoalAction.CURI_EVI_THRESHOLD, strength * c2e(dexMean)));

            g.link(add(tt(F, C), s, e, durReflex).withPri(pri),
                    action instanceof TaskConcept ? ((TaskConcept) action) : null);
        }

        void actionIn(long s, long e, double[] actions, NAR nar) {
            AbstractGoalAction a = (AbstractGoalAction)action;
            Truth t = a.concept.beliefs().truth(s, e, durTruth, nar);
            adc(t != null ? truthFeedback(t) : noise(), actions);
        }

//		private double f(float momentum, double next, MutableTasksBeliefTable t) {
//			if (momentum <= 0) return next;
//
//			SerialTask prev = t.last();
//			return momentum > 0 ? lerpSafe(momentum, next, prev != null ? prev.freq() : next) : next;
//		}

    }

    private class SensorInput implements Consumer<Termed> {
        private final float[] timeLens;
        private final SensorBuilder s;
        private final float offset;
        private Concept c;

        SensorInput(float[] timeLens, SensorBuilder s, float offset) {
            this.timeLens = timeLens;
            this.s = s;
            this.offset = offset;
        }

        @Override
        public void accept(Termed t) {

            float pastpast = 0; //start at now

            for (int p = 0; p < timeLens.length; p++) {
                float pastEnd = pastpast;
                float pastStart = timeLens[p];

                s.in(new SensorHistoryInput(
                        t, pastStart, pastEnd, p));

                pastpast = pastStart;
            }
        }

        public final int size() {
            return s.size();
        }

        private class SensorHistoryInput implements FloatSupplier {
            final int p;
            private final Termed t;

            /**
             * in durs ago
             */
            private final float pastStart, pastEnd;

            SensorHistoryInput(Termed t, float pastStart, float pastEnd, int p) {
                this.t = t;
                this.p = p;
                assert (pastStart > pastEnd);
                this.pastStart = pastStart;
                this.pastEnd = pastEnd;
            }

            @Override
            public float asFloat() {
                Concept C = c;
                if (p == 0) {
                    if ((C == null || C.isDeleted()))
                        c = C = game.nar.conceptualizeDynamic(t);
                }

                return scalar(C != null ?
                        input(C, pastStart, pastEnd, offset) :
                        null);
            }


        }


    }

    private class SensorInputter implements FloatSupplier {
        final int p;
        private final Concept[] c;
        private final Termed t;
        private final float pastStart;
        private final float pastEnd;

        SensorInputter(Concept[] c, Termed t, float pastStart, float pastEnd, int p) {
            this.c = c;
            this.t = t;
            this.p = p;
            assert (pastStart > pastEnd);
            this.pastStart = pastStart;
            this.pastEnd = pastEnd;
        }

        @Override
        public float asFloat() {
            Concept C = c[0];
            if (p == 0) {
                if ((C == null || C.isDeleted()))
                    c[0] = game.nar.conceptualizeDynamic(t);
            }

            return scalar(C != null ?
                    Reflex.this.input(c[0], pastStart, pastEnd, 0) :
                    null);
        }
    }
}