package nars.game.sensor;

import com.google.common.collect.Streams;
import nars.term.Termed;

import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * base interface for a repeatedly invoked procedure context
 * consisting of one or a group of concepts, sharing:
 *          resolution
 *          priority
 *          cause channel
 **/
public interface FocusLoop<X> extends Termed, Consumer<X> {

    static Stream<Termed> components(Stream<? extends FocusLoop> s) {
        return s.flatMap(x -> Streams.stream(x.components()));
    }

    /** the components of the sensor, of which there may be one or more concepts*/
    Iterable<? extends Termed> components();
//  default Iterable<? extends Termed> components() {
//        return Collections.EMPTY_LIST;
//    }

    default float resolution() { return 0; }

    /** initialization procedure */
    default void start(X x) { }

}