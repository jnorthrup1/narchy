//package nars.game.sensor.attn;
//
//import nars.Op;
//import nars.Term;
//import nars.focus.Focus;
//import nars.game.sensor.ComponentSignal;
//import nars.game.sensor.VectorSensor;
//import nars.term.util.conj.ConjList;
//import nars.truth.Truth;
//
//public class ChunkedVectorSensorAttention extends QueueVectorSensorAttention {
//
//    private final int chunkSizeMax;
//
//    public ChunkedVectorSensorAttention(VectorSensor v, Term why, float inputRate, int chunkSizeMax) {
//        super(v, why, inputRate);
//        this.chunkSizeMax = chunkSizeMax;
//    }
//
//    @Override
//    protected void activate(float priEach, int toActivate, Focus w) {
//        ConjList c = new ConjList();
//        for (int i = toActivate; i > 0; i--) {
//            ComponentSignal t = pop();
//            t.remember(priEach, false, w);
//            Truth tt = t.truth();
//            if (tt!=null) {
//                c.add(0L, t.term().negIf(tt.NEGATIVE()));
//                int cn = c.size();
//                if (i <= 1 || cn >= chunkSizeMax) {
//                    c.inhBundle(Op.terms);
//                    Term cc = c.term();
//                    w.link(cc, t.punc(), priEach * cn);
//                    //w.log(cc);
//                    c.clear();
//                }
//            }
//        }
//        assert(c.isEmpty());
//    }
//}