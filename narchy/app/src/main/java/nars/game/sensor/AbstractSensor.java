package nars.game.sensor;

import jcog.signal.FloatRange;
import nars.NAL;
import nars.Term;
import nars.focus.PriAmp;
import nars.truth.Truther;
import org.jetbrains.annotations.Nullable;

import static nars.NAL.signal.SIGNAL_SLEEP_DURS_MAX;

public abstract class AbstractSensor implements Sensor {
    public final PriAmp pri;
    public final Term id;
    @Nullable public Term why;
    public Sensing sensing;

    public AbstractSensor(Term id) {
        this.id = id;
        this.pri = new PriAmp(id);
        this.sensing = new Sensing();
    }

    @Override
    public final Term term() {
        return id;
    }

    @Override public final AbstractSensor resolution(float v) {
        sensing.res.set(v);
        return this;
    }

    @Override public final float resolution() {
        return sensing.res.floatValue();
    }

    @Override public final Truther truther() { return sensing.truther; }

    public final Sensor truther(@Nullable Truther t) {
        sensing.truther = t;
        return this;
    }


//    private float pri(float pri, Truth prev, Truth next, float fRes) {
//        float priIfNoChange =
//                //ScalarValue.EPSILON; //min pri used if signal value remains the same
//                pri * pri;
//
//        if (prev == null)
//            return pri;
//        else {
////            float fDiff = next!=null ? Math.abs(next.freq() - prev.freq()) : 1f;
////            return Util.lerp(fDiff, priIfNoChange, pri);
//            if (next == null || Math.abs(next.freq()-prev.freq()) > fRes)
//                return pri;
//            else
//                return priIfNoChange;
//        }
//    }



    /** sensor parameter object */
    public final class Sensing {
        public final FloatRange res = new FloatRange(
                //0,
                NAL.truth.TRUTH_EPSILON,
                0, 1);
        public float sleepDurs = SIGNAL_SLEEP_DURS_MAX;


        @Nullable public Truther truther = null;

        public Sensing() {

        }

        /** sets priority post-filter */
        public void amp(float a) {
            pri.amp(a);
        }

        public float pri() {
            return pri.pri();
        }

        public void why(Term x) {
            why = x;
        }

        public Term why() {
            return why;
        }


        /*

    //        this.why = why == null ? n.control.newCause(term).ID : why;
    //
    //		this.res = FloatRange.unit(n.freqResolution);
    //
    //		this.pri = pri!=null ? pri : new PriAmp(term);
         */
    }
}