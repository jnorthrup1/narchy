package nars.game.sensor;

import jcog.data.list.Lst;
import nars.$;
import nars.NAR;
import nars.Term;
import nars.game.Game;
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction;
import org.eclipse.collections.api.block.function.primitive.IntToIntFunction;

import java.util.Iterator;
import java.util.List;
import java.util.function.IntSupplier;

/** the selector lambda can return -1 to choose none, or Integer.MAX_VALUE to choose all
 * TODO mode for bitvector choice
 * */
public class SelectorSensor extends VectorSensor {

    private final List<SubSignalComponent> choices;
    private final IntIntToObjectFunction<Term> pattern;
    private final IntToIntFunction value;
    private final int values;
    private final int arity;

    public SelectorSensor(Term id, Term[] values, IntSupplier value, Term pattern, NAR nar) {
        this(id,
                values.length, 1,
                (ignored)->value.getAsInt(),
                (a,v)->pattern.replace($.varDep(1), values[v]),
                nar);
    }

    public SelectorSensor(Term id, int values, int arity, IntToIntFunction value, IntIntToObjectFunction<Term> pattern, NAR n) {
        super(id);

        assert(values > 1);
        assert(arity >= 1);

        choices = new Lst<>(arity * values);
        this.arity = arity;
        this.pattern = pattern;
        this.value = value;
        this.values = values;

    }

    @Override
    public void start(Game game) {
        super.start(game);

        for (int a = 0; a < arity; a++) {
            for (int v = 0; v < values; v++) {

                int V = v, A = a;
//                Term id = pattern.replace($.varDep(1), e);
//                if (arity > 1)
//                    id = id.replace($.varDep(2), $.the(a));

                choices.add(component(
                        pattern.value(a, v),
                        () -> {
                            int y = value.applyAsInt(A);
                            return y == V || y == Integer.MAX_VALUE ? 1 : 0;
                        }, game.nar)
                );
            }
        }

    }

    @Override
    public int size() {
        return choices.size();
    }

    @Override
    public Iterator<SubSignalComponent> iterator() {
        return choices.iterator();
    }
}