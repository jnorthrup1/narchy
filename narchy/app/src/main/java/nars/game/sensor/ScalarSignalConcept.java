package nars.game.sensor;

import nars.NAR;
import nars.Term;
import nars.focus.Focus;
import nars.focus.PriAmp;
import nars.table.BeliefTable;
import nars.time.When;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

/** an independent scalar (1-dimensional) signal
 * TODO impl that takes a lambda Supplier<Truth> for the signal and base ScalarSignal off that to remove its truth() method
 * */
public class ScalarSignalConcept extends SignalConcept {

	public ScalarSignalConcept(Term term, BeliefTable beliefTable, BeliefTable goalTable, NAR n) {
		this(term, beliefTable, goalTable, null, n);
	}

	protected ScalarSignalConcept(Term term, BeliefTable beliefTable, BeliefTable goalTable, PriAmp pri, NAR n) {
		super(term, beliefTable, goalTable, n);
	}

	public ScalarSignalConcept(Term term, NAR n) {
		this(term, beliefTable(term, true, true, n), beliefTable(term, false, false, n), n);
	}

	public final void inputCommit(@Nullable Truth next, AbstractSensor.Sensing s, When<Focus> w) {
		if (input(next, s.why(), s.sleepDurs, w))
			remember(s.pri(), w.x);
	}


}