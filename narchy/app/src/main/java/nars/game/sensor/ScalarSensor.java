package nars.game.sensor;

import jcog.Util;
import nars.Term;
import nars.focus.Focus;
import nars.game.Game;
import nars.term.Termed;
import nars.time.When;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class ScalarSensor extends AbstractSensor {

    public ScalarSignalConcept concept;

    public ScalarSensor(Term id) {
        super(id);
    }

    @Override
    public void start(Game g) {
        if (concept!=null)
            throw new UnsupportedOperationException();
        g.nar.add(this.concept = conceptNew(g));
        super.start(g);
    }

    protected ScalarSignalConcept conceptNew(Game g) {
        return new ScalarSignalConcept(term(), g.nar);
    }

    @Override
    public abstract void accept(Game g);

    protected void accept(float x, Game g) {
        concept.inputCommit(updateTruth(x == x ?
                        Util.round(x, resolution()) : Float.NaN, g),
                sensing,
                when(g));
    }

    /** determines what time interval the next sensor input will occurr */
    protected When<Focus> when(Game g) {
        return g.time;
    }

    @Nullable private Truth updateTruth(float nextValue, Game g) {
        return nextValue == nextValue ?
                truth(nextValue, g)
                :
                null;
    }


    @Override
    public final Iterable<? extends Termed> components() {
        return List.of(concept);
        //return List.of(id);
    }

}