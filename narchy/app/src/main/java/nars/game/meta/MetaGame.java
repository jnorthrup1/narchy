package nars.game.meta;

import jcog.Fuzzy;
import jcog.Is;
import jcog.data.list.Lst;
import jcog.math.FloatSupplier;
import jcog.math.v2;
import jcog.ql.dqn.ValuePredictAgent;
import jcog.signal.FloatRange;
import nars.$;
import nars.Op;
import nars.Term;
import nars.focus.BagFocus;
import nars.focus.BagSustain;
import nars.focus.NARTimeFocus;
import nars.focus.PriAmp;
import nars.game.Game;
import nars.game.GameTime;
import nars.game.action.AbstractAction;
import nars.game.action.AbstractGoalAction;
import nars.game.action.util.Reflex;
import nars.game.reward.LambdaScalarReward;
import nars.task.util.OpPri;
import nars.task.util.PuncBag;
import nars.term.atom.Atomic;
import nars.time.Moment;
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure;

import java.util.stream.Stream;

import static jcog.Util.*;
import static nars.Op.*;

/**
 * self metaprogrammer - agent metavisor - homunculus
 */
@Is({"Homunculus", "Metagaming", "Human_biocomputer", "Variety_(cybernetics)"})
public abstract class MetaGame extends Game {

	protected static final float resAction = 0.05f;
	protected static final float resReward = resAction;

	/** action's dynamic range exponent.
	 *  higher values pack more dynamic range "octaves" or "decibels",
	 *  thus allow more sensitivity and expressiveness
	 *
	 *  set to NaN to disable.
	 *  */
	static float priActionSharp =
		3
		//4
		//2
		//Float.NaN
	;

	static float priActionMin = 0.01f;

	static float priActionMax = 1;


	static final Atomic OP = Atomic.atom("op");
	static final Atomic forget = Atomic.atom("forget");
	static final Atomic simple = Atomic.atom("simple");

	static final Atomic clear = Atomic.atom("clear");
	static final Atomic effort = Atomic.atom("effort");

	static final Atomic meta = Atomic.atom("meta");
	static final Atomic focus = Atomic.atom("focus");

//	static final Atomic grow = Atomic.atom("grow");
	/**
	 * internal truth frequency precision
	 */
	static final Atomic precise = Atomic.atom("precise"); //frequency resolution
//	static final Atomic ignorant = Atomic.atom("ignorant"); //min conf
//	static final Atomic careful = Atomic.atom("careful"); //conf resolution

	static final Atomic CURIOSITY = Atomic.the("curi");

//	static final Atomic belief = Atomic.atom("belief");
//	protected static final Atomic goal = Atomic.atom("goal");
//
//	static final Atomic question = Atomic.atom("question");
//	static final Atomic quest = Atomic.atom("quest");

	protected static final Atomic freq = Atomic.atom("freq");
	protected static final Atomic conf = Atomic.atom("conf");
	static final Atomic pri = Atomic.atom("pri");
	static final Atomic mean = Atomic.atom("mean");

	static final Atomic play = Atomic.atom("play");
	static final Atomic input = Atomic.atom("input");
	static final Atomic dur = Atomic.atom("dur");
	static final Atomic happy = Atomic.atom("happy");
	static final Atomic optimistic = Atomic.atom("optimistic");
	public static final Atomic dex = Atomic.atom("dex");
	static final Atomic now = Atomic.atom("now");
	static final Atomic past = Atomic.atom("past");
	static final Atomic future = Atomic.atom("future");
	static final Atomic on = Atomic.atom("on");


	protected MetaGame(Term id, GameTime t) {
		super(id, t);

		focus().time = new NARTimeFocus();

		((BagSustain)((BagFocus)focus()).updater).sustain.set(0); //HACK
	}


	public LambdaScalarReward rewards(Game g, boolean normalized) {
		Term id = $.inh(g.id, happy);
		FloatSupplier f = () -> {
			//happiness in the past meta-dur
			Moment moment =
					this.time; //metaNow
					//g.nowLoop;
			long range = moment.e - moment.s;
			long now = Fuzzy.mean(moment.s, moment.e);
			float dur =
					moment.dur;
					//0;
			return (float) g.happiness(
				now - range, now, dur
			);
		};
		var r = normalized ? rewardNormalized(id, f) : reward(id, f);
		r.resolution(resReward);
		r.eternal = true;
		return r;
	}

	public void durOctaves(Term id, FloatSupplier baseDur, float octaves, FloatProcedure durSetter) {
		floatAction($.inh(id, dur), x -> {
			double d = baseDur.asFloat() * Math.pow(2, x * octaves);
			durSetter.value((float) d);
		});
	}
	@Deprecated public void dur(Term id, FloatSupplier baseDur, float octaves, FloatProcedure durSetter) {

//		float sharpness = 3; //some curve to give finer precision at smaller durs
//
//		floatAction($.inh(id, dur), x -> {
//			//x = (float)Math.pow(x, sharpness);
//			x = (float) expUnit(x, sharpness);
//			float d = baseDur.asFloat();
//			durSetter.accept(lerp(x, d, d * maxDurs));
//		});

		//TODO durOctaves
		floatAction($.inh(id, dur), x -> {
			//x = (float)Math.pow(x, sharpness);
			float d = baseDur.asFloat();
			double dur = d * Math.pow(2, x * octaves);
			//System.out.println(d + " * " + x + " => " + dur);
			durSetter.value((float) dur);
		});
	}


	public AbstractAction priAction(PriAmp node) {
		return priAction($.inh(node.id, pri), node::amp);
	}

	public AbstractAction priAction(Term id, FloatProcedure action) {
		return action(id, action,
				priActionMin, priActionMax, priActionSharp);
	}

	public AbstractAction action(Term id, FloatProcedure action, float priMin, float priMax, float sharpness) {
		AbstractAction a = floatAction(id, x->{
			float y = lerpSafe(
					(sharpness == sharpness) ?
						(float) expUnit(x, sharpness) //EXPONENTIAL
						:
						x //LINEAR
					, priMin, priMax);

//			/* quadratic concave */ float a = Util.lerp(x*x, priMin, priMax);

//			/* linear */ float a = Util.lerpSafe(x, priMin, priMax);

			action.value(y);
		});
		return a;
	}


	protected AbstractAction actFloat(Term t, FloatRange r) {
		return floatAction(t, 1, r);
	}

	@Deprecated protected AbstractAction floatAction(Term t, float exp, FloatRange r) {
		return floatAction(t, r.min, r.max, exp, r::set);
	}

	protected AbstractGoalAction floatAction(Term t, FloatProcedure r) {
		return floatAction(t, 0, 1, 1, r);
	}

	protected AbstractGoalAction floatAction(Term t, float min, float max, float exp, FloatProcedure r) {
		//FloatAveraged f = new FloatAveraged(/*0.75*/ 1);
		//FloatToFloatFunction f = (z)->z;
		var a = action(t, x -> {
			//float y = f.valueOf(x);
			if (x == x)
				r.value(lerp((float) Math.pow(x, exp), min, max));
			return x;
		});
		a.resolution(resAction);
		return a;
	}

	public OpPri opPri(Term root, OpPri pri, float min, float max) {
		//TODO check any other misssing ops that may name tasks
		Lst<FloatRange> o = pri.op;
		java.util.Set<Op> mgd = java.util.Set.of(
			opPri(INH, root, min, max, o),
			opPri(SIM, root, min, max, o),
			opPri(IMPL, root, min, max, o),
			opPri(CONJ, root, min, max, o)
			//opPri(PROD, root, min, max, o) //optional?
		);

		Op[] others = Stream.of(Op.values()).filter(j -> j.taskable && !mgd.contains(j)).toArray(Op[]::new);
		floatAction($.inh(root, $.p(OP, "other")),  v->{
			for (Op k : others)
				o.get(k.id).setLerp(v, min, max);
		});

		return pri;
	}

	private Op opPri(Op x, Term root, float min, float max, Lst<FloatRange> o) {
		floatAction($.inh(root, $.p(OP, x.atom)),
				v-> o.get(x.id).setLerp(v, min, max));
		return x;
	}



	/** 4 punctuation controlled by 2 actions:
	 *     belief to goal ratio, belief/goal to question/quest ratio
	 * @param ampAction if true, adds an extra action for controlling the max amplitude.
	 * */
	public void puncPriSimple(Term root, float min, float max, boolean ampAction, PuncBag... pri) {
		assert(pri.length>0);
		FloatRange A = new FloatRange(0.5f, 0, 1);
		FloatRange Q = new FloatRange(0.5f, 0, 1);

		FloatRange AMP = new FloatRange(max, 0, max);

		Term r = $.p(root, MetaGame.pri);
		if (ampAction) {
			priAction($.inh(r, "amp"),
					AMP::set);
		}
		floatAction($.inh(r,
			CONJ.the(punc(GOAL), punc(QUEST))),
				A::set);
		floatAction($.inh(r,
			CONJ.the(punc(QUESTION), punc(QUEST))),
				Q::set);
		afterFrame(()->{
			float a = (A.floatValue() - 0.5f)*2;
			float q = (Q.floatValue() - 0.5f)*2;

			v2 v = new v2(a, q); //v.normalize();
			float an = (v.x+1)/2;
			float qn = (v.y+1)/2;
			float ap = 1 - an, qp = 1 - qn;
			float[] y = {
					mean(an, qn), mean(ap, qn),
					mean(an, qp), mean(ap, qp)
			};

			//normalize:
			float yMax = max(y); if (yMax > 0.001f) mul(1 / yMax, y);

			float amp = AMP.asFloat();
			float BB = lerpSafe(y[0], min, amp);
			float GG = lerpSafe(y[1], min, amp);
			float QQ = lerpSafe(y[2], min, amp);
			float qq = lerpSafe(y[3], min, amp);

			for (PuncBag p : pri)
				p.set(BB, GG, QQ, qq);
		});
	}

	public void puncPri(Term root, PuncBag p, float min, float max) {
		Term amp = $.p(root, pri);
		floatAction(punc(amp, BELIEF), v -> p.belief  .setLerp(v, min, max));
		floatAction(punc(amp, QUESTION), v -> p.question.setLerp(v, min, max));
		floatAction(punc(amp, GOAL), v -> p.goal    .setLerp(v, min, max));
		floatAction(punc(amp, QUEST), v -> p.quest   .setLerp(v, min, max));
	}


	public Reflex rlBoost() {
		return rlBoost(1/*,2,4*/);
	}

	public Reflex rlBoost(float... history) {
		Reflex reflex = new Reflex(
				//ValuePredictAgent::DQrecurrent,
				ValuePredictAgent::DQN,
				//ValuePredictAgent::DQNmini,
				//ValuePredictAgent::DQN_NTM,
				this,
			history
		);
//		reflex.durs.set(2);
		return reflex;
	}

	//	private float dur(int initialDur, float d) {
//		return Math.max(1, ((d + 0.5f) * 2 * initialDur));
//	}

	//    public GoalActionConcept[] dial(Game a, Atomic label, FloatRange var, int steps) {
//        GoalActionConcept[] priAction = actionDial(
//                $.inh(id, $.p(label, $.the(-1))),
//                $.inh(id, $.p(label, $.the(+1))),
//                var,
//                steps);
//        return priAction;
//    }

	//    /** creates a base agent that can be used to interface with external controller
//     *  it will be consistent as long as the NAR architecture remains the same.
//     *  TODO kill signal notifying changed architecture and unwiring any created WiredAgent
//     *  */
//    public Agenterator agent(FloatSupplier reward, IntIntToObjectFunction<Agent> a) {
//        AgentBuilder b = new AgentBuilder(reward);
//        for (MetaGoal m : MetaGoal.values()) {
//            b.out(5, i->{
//                float w;
//                switch(i) {
//                    default:
//                    case 0: w = -1; break;
//                    case 1: w = -0.5f; break;
//                    case 2: w = 0; break;
//                    case 3: w = +0.5f; break;
//                    case 4: w = +1; break;
//                }
//                nar.emotion.want(m, w);
//            });
//        }
//
//        for (Why c : why) {
//
//            b.in(() -> {
//                float ca = c.amp();
//                return ca==ca ? ca : 0;
//            });
//
////            for (MetaGoal m : MetaGoal.values()) {
////                Traffic mm = c.credit[m.ordinal()];
////                b.in(()-> mm.current);
////            }
//            //TODO other data
//        }
//
//        for (How c : nar.how) {
//            b.in(() -> {
//                PriNode cp = c.pri;
//                return Util.unitize(cp.priElseZero());
//            });
//            //TODO other data
//        }
//
//        return b.get(a);
//    }


//    private static NAgent metavisor(NAgent a) {
//
////        new NARSpeak.VocalCommentary( nar());
//
//        //
////        nar().onTask(x -> {
////           if (x.isGoal() && !x.isInput())
////               System.out.println(x.proof());
////        });
//
//        int durs = 4;
//        NAR nar = nar();
//
//        NAgent m = new NAgent($.func("meta", id), FrameTrigger.durs(durs), nar);
//
//        m.reward(
//                new SimpleReward($.func("dex", id),
//                        new FloatNormalized(new FloatFirstOrderDifference(nar()::time,
//                                a::dexterity)).relax(0.01f), m)
//        );
//
////        m.actionUnipolar($.func("forget", id), (f)->{
////            nar.memoryDuration.setAt(Util.lerp(f, 0.5f, 0.99f));
////        });
////        m.actionUnipolar($.func("awake", id), (f)->{
////            nar.conceptActivation.setAt(Util.lerp(f, 0.1f, 0.99f));
////        });
//        m.senseNumber($.func("busy", id), new FloatNormalized(() ->
//                (float) Math.log(1 + m.nar().emotion.busyVol.getMean()), 0, 1).relax(0.05f));
////
//
////        actionUnipolar($.inh(this.nar.self(), $.the("deep")), (d) -> {
////            if (d == d) {
////                //deep incrases both duration and max target volume
////                this.nar.time.dur(Util.lerp(d * d, 20, 120));
////                this.nar.termVolumeMax.setAt(Util.lerp(d, 30, 60));
////            }
////            return d;
////        });
//
////        actionUnipolar($.inh(this.nar.self(), $.the("awake")), (a)->{
////            if (a == a) {
////                this.nar.activateConceptRate.setAt(Util.lerp(a, 0.2f, 1f));
////            }
////            return a;
////        });
//
////        actionUnipolar($.prop(nar.self(), $.the("focus")), (a)->{
////            nar.forgetRate.setAt(Util.lerp(a, 0.9f, 0.8f)); //inverse forget rate
////            return a;
////        });
//
////        m.actionUnipolar($.func("curious", id), (cur) -> {
////            curiosity.setAt(lerp(cur, 0.01f, 0.25f));
////        });//.resolution(0.05f);
//
//
//        return m;
//    }
//    public static AgentBuilder newController(NAgent a) {
//        NAR n = a.nar;
//
//        Emotion ne = n.emotion;
//        Arrays.fill(ne.want, 0);
//
//        AgentBuilder b = new AgentBuilder(
//
//                HaiQae::new,
//
//                () -> a.enabled.get() ? (0.1f + a.dexterity()) * Util.tanhFast(a.reward) /* - lag */ : 0f)
//
//                .in(a::dexterity)
//                .in(a.happy)
//
//
//
//
//                .in(new FloatNormalized(
//
//                        new FloatFirstOrderDifference(n::time, () -> n.emotion.deriveTask.getValue().longValue())
//                ).relax(0.1f))
//                .in(new FloatNormalized(
//
//                                new FloatFirstOrderDifference(n::time, () -> n.emotion.premiseFire.getValue().longValue())
//                        ).relax(0.1f)
//                ).in(new FloatNormalized(
//                                n.emotion.busyVol::getSum
//                        ).relax(0.1f)
//                );
//
//
//        for (MetaGoal g : values()) {
//            final int gg = g.ordinal();
//            float min = -2;
//            float max = +2;
//            b.in(new FloatPolarNormalized(() -> ne.want[gg], max));
//
//            float step = 0.5f;
//
//            b.out(2, (w) -> {
//                float str = 0.05f + step * Math.abs(ne.want[gg] / 4f);
//                switch (w) {
//                    case 0:
//                        ne.want[gg] = Math.min(max, ne.want[gg] + str);
//                        break;
//                    case 1:
//                        ne.want[gg] = Math.max(min, ne.want[gg] - str);
//                        break;
//                }
//            });
//        }
//
//
//
//
//
//
//
//
//
//
//
//
//        return b;
//    }

}