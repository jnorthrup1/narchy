package nars.game.meta;

import jcog.Is;
import jcog.Research;
import jcog.Util;
import jcog.math.FloatDifference;
import jcog.math.FloatSupplier;
import jcog.normalize.FloatNormalized;
import jcog.pri.bag.impl.ArrayBag;
import jcog.signal.FloatRange;
import jcog.signal.anomaly.ewma.Ewma;
import nars.$;
import nars.*;
import nars.derive.pri.DefaultBudget;
import nars.focus.BagFocus;
import nars.focus.BagSustain;
import nars.focus.BasicTimeFocus;
import nars.focus.Focus;
import nars.game.Game;
import nars.game.GameTime;
import nars.game.action.AbstractAction;
import nars.game.reward.LambdaScalarReward;
import nars.game.reward.ScalarReward;
import nars.game.sensor.AbstractSensor;
import nars.game.sensor.FocusLoop;
import nars.game.sensor.VectorSensor;
import nars.game.sensor.attn.DirectVectorSensorAttention;
import nars.game.sensor.attn.QueueVectorSensorAttention;
import nars.link.AtomicTaskLink;
import nars.link.TaskLink;
import nars.memory.HijackMemory;
import nars.premise.Premise;
import nars.time.ScheduledTask;
import nars.truth.util.ConfRange;
import org.eclipse.collections.api.block.predicate.primitive.BooleanPredicate;
import org.eclipse.collections.api.block.procedure.primitive.BooleanProcedure;
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;
import static jcog.Util.lerpSafe;
import static jcog.Util.round;
import static nars.$.the;
import static nars.$.*;
import static nars.Op.*;

/**
 * core metavisor
 */
public abstract class SelfMetaGame extends MetaGame {


    @Deprecated private static final int META_FRAME_DIVISOR =
        //2;
        1;

    /**
     * input gain
     */
    @Deprecated
    final float gain = 1;
    private final Term SELF;
    public List<SubMetaGame> game;


    public SelfMetaGame(NAR nar, float dur) {
        super($.p(nar.self(), meta), GameTime.durs(dur));
        SELF = nar.self();
    }

//    static final Logger logger = Log.logger(SelfMetaGame.class);
//
//    private void load() {
//        byte[] prev = User.the().blob(SelfMetaGame.class.getName());
//        if (prev != null) {
//            try {
//                int numRead = IO.readTasks(prev, what());
//                logger.info("{} read {} tasks ({} bytes)", this, numRead, prev.length);
//            } catch (IOException e) {
//                logger.error("{}", e);
//            }
//        }
//    }
//
//    public void save() {
//
//        DynBytes b = new DynBytes(1024 * 128);
//        what().concepts()
//                .flatMap(Concept::tasks)
//                .filter(Objects::nonNull)
//                .map(x -> NALTask.eternalize(x, nar))
//                .filter(Objects::nonNull)
//                .distinct()
//                .map(IO::taskToBytes)
//                .forEach(b::write);
//
//        User.the().put(SelfMetaGame.class.getName(), b.compact());
//        logger.info("{} saved tasks ({} bytes)", this, b.length);
//    }

    @Override
    protected final void init() {
        super.init();

        nar.beliefPriDefault.pri(gain);
        nar.goalPriDefault.pri(gain);
        nar.questionPriDefault.pri(gain);
        nar.questPriDefault.pri(gain);

        initMeta();

        metagames();

        rewards();

        initMetaEnd();

    }

    @Deprecated protected void initMetaEnd() {
    }

    protected abstract void initMeta();

    protected abstract void initMeta(SubMetaGame subMetaGame);


    /**
     * TODO this needs a fix in GameTime.Durs because MetaGame could set the dur longer than the game's dur, and never react to changing curiosity
     */
    @Deprecated
    public void durMeta(float octaves, float subOctaves) {
        Ewma target = new Ewma(0.3f);
        durOctaves(SELF, ()->(float) (nar.dur() * Math.pow(2, -subOctaves)), octaves + subOctaves, d -> nar.time.dur((float) target.acceptAndGetMean(d)));
    }

    protected void volMax(int min, int max) {
        action($.inh(SELF, "volMax"), (org.eclipse.collections.api.block.procedure.primitive.FloatProcedure) r ->
                nar.volMax.set(jcog.Util.lerpInt(r, min, max)));
    }
//
//    private void governor(MLPGovernor governor) {
//        actionUnipolar($.inh(SELF, $.p("exe", "exploration")), (float r) ->
//                governor.explorationRate.setLerp(r, 0.01f, 1f));
//
//    }


    private void memoryControl(HijackMemory m) {
        //TODO refine
        //TODO refine
        floatAction(inh(SELF, "memTemp"), m.add::set);
    }

    @Research
    @Is("Energy_proportional_computing")
    public void cpuThrottle(float min, float max) {
        floatAction(inh(SELF, effort), min, max, 1, nar.loop.throttle::set);

//TODO feedback -> powersave mode
//        ThreadCPUTimeTracker.getCPUTime()
//        reward("lazy", 1, ()->{
//            return 1-nar.loop.throttle.floatValue();
//        });
    }


    public void metagames() {
        List<Game> x = nar.parts(Game.class).filter(g -> !(g instanceof MetaGame)).collect(toList());
        if (x.size() == 1) {
            //..

        }
        this.game = x.stream().map(g -> {
            //logger.info(this -)
            SubMetaGame gMeta = new SubMetaGame(g, GameTime.frames(g, META_FRAME_DIVISOR));
            nar.add(gMeta);


            focusPri(g, 0.01f, 1);
            focusPri(gMeta, 0.01f, 1);

            return gMeta;
        })
                .collect(toList());
    }

    private void focusPri(Game g, float min, int max) {
        floatAction(inh(g.id, freq), min, max, 1, g.focus()::
                //freqAndPri
                freq
        );
    }

    private void rewards() {
        game.forEach(subMeta -> rewards(subMeta.game, false));
    //        game.forEach(s -> rewards(s, false));
    }


//    private void rewardsAggregate() {
//        reward($.inh(happy, SELF), () -> {pri
//            return (float) happiness(nowPercept.s, nowPercept.e, nowPercept.dur, nar);
//            //return (float)happinessSigma(nowPercept.s, nowPercept.e, nowPercept.dur, nar);
//        });
//
//        // /*$.p(SELF, now)*/
//
//        //
////		/** past happiness memory */
////		sense($.inh($.p(SELF, past), happy), () -> {
////			float durProjected = dur();
////			return happiness(Math.round(nowPercept.start - dur() * emotionalMomentumDurs), nowPercept.start, durProjected, nar);
////		});
////
////		/** optimism */
////		reward($.inh(SELF, optimistic), () -> {
////			float dur = nowLoop.dur * 2;
////			float shift = 1 * dur;
////			return happiness(Math.round(nowLoop.start + shift), Math.round(nowLoop.end + shift), dur, nar);
////		});//.conf(0.5f*nar.confDefault(GOAL));;
//
//    }

    private void cpu() {

    }

    public void heapSense() {

        Runtime rt = Runtime.getRuntime();

        FloatSupplier freeHeap = () -> (float) (((double) rt.freeMemory()) / rt.totalMemory());
        sense(inh(SELF, p("free", "heap")), freeHeap);
        sense(inh(SELF, p("free", "heapDelta")), normalizedDelta(freeHeap));
    }

    public void emotionSense() {


        Emotion e = nar.emotion;

        List.of(
                sense(inh(SELF, "derived"), normalizedDelta(e.derivedTask)),
                sense(inh(SELF, "premised"), normalizedDelta(e.derivedPremise)),
//        sense($.inh(SELF,$.p( "derive", "task", "drop")), deltaNormalized(e.derivedTaskDrop));
//        sense($.inh(SELF,$.p( "derive", "task", "dup")), deltaNormalized(e.derivedTaskDup));

                sense(inh(SELF, "conceptualize"), normalizedDelta(e.conceptConstruct)),
//        sense($.inh(SELF, "unceptualize"), deltaNormalized(e.conceptDelete)),

                sense(inh(SELF, "busy"), new FloatNormalized(e.busyVol)),
                sense(inh(SELF, p("nar", "lag")), new FloatNormalized(e.narLoopLag.abs())),
                sense(inh(SELF, p("loop", "lag")), new FloatNormalized(e.durLoopLag))
                //sense(inh(SELF, p("loop", "slow")), new FloatNormalized(e.durLoopSlow)),
        );
          //      .forEach(s -> s.resolution(resSense));
    }

//    public void metaGoals(CreditControlModel model) {
//        for (MetaGoal mg : MetaGoal.values()) {
//            actionUnipolar($.inh(SELF, $.the(mg.name())), x -> {
//                model.want(mg,
//
//                        //x
//                        (x * 2) - 1
//
////					x >= 0.5f ?
////						(float) Util.lerp(Math.pow((x - 0.5f) * 2, 1 /* 2 */), 0, +1) //positive (0.5..1)
////						:
////						//Util.lerp((x) * 2, -0.02f, 0) //negative (0..0.5): weaker
////						0f
//                );
//            });
//        }
//    }

    private FloatNormalized normalizedDelta(FloatSupplier in) {
        return new FloatNormalized(new FloatDifference(in, nar::time)).polar();
    }

    private void confRes(Term label, ConfRange c, float min, float max) {
        floatAction(inh(SELF, p(conf, label)), x -> c.set(round(lerpSafe(x, min, max), nar.confResolution.floatValue())));
    }

    /**
     * increasing decreases the max confMin
     */
    public void confMin(float confPower) {
        double confMin = NAL.truth.CONF_MIN;
        floatAction(inh(SELF, "ignore"), x -> {
            double confFirstOrder = Math.pow(Math.min(nar.confDefault(BELIEF), nar.confDefault(GOAL)), confPower);
            double cMin = lerpSafe(x, confMin, confFirstOrder);
            nar.confMin.set(cMin);
        });
    }

    public void conf(Term id, ConfRange c, float confMin, float confMax) {
        floatAction(inh(SELF, p(id, conf)), x -> c.set(lerpSafe(x, confMin, confMax)));
    }

//    private void confMin(double min, double max) {
//        actionUnipolar($.inh($.p(conf, "min"), SELF), x -> {
//            nar.confMin.set(Util.round(lerpSafe(x, min, max), nar.confResolution.floatValue()));
//        });
//    }

    public void truthPrecision(boolean freq, boolean conf) {
        assert (freq || conf);

        floatAction(inh(SELF, precise), x -> {
            float b, fRes, cRes;
            //double cMin;
            if (x >= 0.8f) {
                fRes = 0.0125f;
                cRes = 0.01f;
                //cMin = NAL.truth.CONF_MIN;
                b = 1f;
            } else if (x >= 0.6f) {
                fRes = 0.025f;
                cRes = 0.01f;
                //cMin = 0.005;
                b = 0.75f;
            } else if (x >= 0.4f) {
                fRes = 0.05f;
                cRes = 0.01f;
                //cMin = 0.01;
                b = 0.50f;
            } else if (x >= 0.2f) {
                fRes = 0.1f;
                cRes = 0.02f;
                //cMin = 0.02;
                b = 0.25f;
            } else {
                fRes = 0.2f;
                cRes = 0.04f;
                //cMin = 0.02;
                b = 0;
            }
            if (freq)
                nar.freqResolution.set(fRes);
            if (conf)
                nar.confResolution.set(cRes);

//            return b; //TODO
        });
    }

//    private void confPrecision() {
//        actionUnipolar($.inh(SELF, ignorant), (i) -> {
//            nar.confMin.conf(Util.lerp(i, NAL.truth.CONF_MIN, nar.beliefConfDefault.get() / 8f));
//        });
//    }

//    private DoubleStream happinesses(long start, long end, float dur, NAR nar) {
//        return nar.parts(MetaGame.class)
//                .filter(g -> g != SelfMetaGame.this)
//                .filter(Part::isOn)
//                .mapToDouble(g -> g.happiness(start, end, dur));
//    }

//	@NotNull
//	private Stream<What> others() {
//		return nar.what.stream()
//			.map(What.Much::the)
//			.filter(w -> w != this.what());
//	}


//    float happiness(long start, long end, float dur, NAR nar) {
//        //System.out.println(Joiner.on(", ").join(happinesses(start, end, dur, nar).iterator()));
//        return (float) happinesses(start, end, dur, nar)
//                .filter(x -> x == x) //not NaN
//                /*.map(gg ->
//                    (((gg - 0.5f) * 2f
//                        * g.what().pri() //weighted by current priority
//                    ) / 2f) + 0.5f
//                )*/
//                .average().orElse(Float.NaN);
//    }
//
//    private DoubleStream happinessesFinite(long start, long end, float dur, NAR nar) {
//        return happinesses(start, end, dur, nar).filter(x -> x == x);
//    }
//
//    public double happinessAverage(long start, long end, float dur, NAR nar) {
//        return happinessesFinite(start, end, dur, nar).average().orElse(Double.NaN);
//    }
//
//    public double happinessSigma(long start, long end, float dur, NAR nar) {
//        return happinessesFinite(start, end, dur, nar).reduce(1, (x, y) -> x * y);
//    }

    /**
     * TODO prevent duplicate add
     */
    public LambdaScalarReward addEncouragement(FloatRange enc) {
        LambdaScalarReward e = reward(inh(SELF, "encouragement"), () -> {
            float v = enc.asFloat();
            if (Util.equals(v, 0.5f, nar.freqResolution.floatValue()))
                return Float.NaN; //neutral

            enc.setLerp(0.01f, v, 0.5f); //fade to 0.5
            return v;
        });

//        Exe.runLater(() -> { //HACK
//            e.usually(0.5f);
//        });

        return e;
    }

//    public void exeControl(Focus f, boolean puncProb) {
//            if (f.budget instanceof DefaultBudget) {
//
//            }
////        actionUnipolar($.inh(exe.SELF, "subCycles"), (i) ->{
////            //long subCycleNS = Math.round(Util.lerpSafe(i, 0.05, 0.45) * exe.nar.loop.periodNS());
////            //System.out.println(Texts.timeStr(subCycleNS));
////            //exe.subCycleNS = subCycleNS;
////            exe.subCycles = lerpSafe(i, 2, 8);
////        });
//
////        Term SELF = exe.nar.self();
//
////        if (exe.deriveParam!=null)
////            bagDeriver(SELF, exe.deriveParam);
//
//
////        float gainMin = 0.5f, gainMax = 1.5f;
//
////        workerUnipolar($.inh($.p(SELF,"derive"), "detail"), (e, v) -> {
////            ((DefaultDerivePri) (e.deriver.derivePri)).complexityAbsCost.setProportionally(1-v);
////        });
////        workerUnipolar($.inh($.p(SELF,"derive"), "detailRel"), (e, v) -> {
////            ((DefaultDerivePri) (e.deriver.derivePri)).complexityRelCost.setProportionally(1-v);
////        });
//
//
//    }

//    private void bagDeriver(Term x, BagDeriver.BagDeriverParam p) {
//
//        /* TODO iterations limits needs auto-tuned for system's CPU speed */
//        floatAction(inh(p("derive", x), "depth"),
//                (float v) -> p.iterations.set(Math.round(lerpSafe(v, 64, 256))));
//
//        floatAction(inh(p("derive", x), "breadth"),
//                (float v) -> p.tasklink.set(1f / Util.lerpSafe(v, 16, 4)));
//
////        actionUnipolar($.inh(DERIVE, "bounce"),
////                (float v) -> p.bounce.setLerp(v, 0.0f, 0.9f));
//
////        //TODO DISABLE automatically when output = Direct
////        actionUnipolar($.inh(DERIVE, "output"),
////                (float v) -> p.out.setLerp(v, 0.01f, 1));
//
//
//    }

//    private void simplicity(Term DERIVE, DefaultDerivePri derivePri) {
//        //        actionUnipolar($.inh("certainty", SELF_DERIVE), (v) -> {
////            //pri.simplicity.setProportionally(v);
////            pri.eviLossCost.set(lerpSafe(v, 0.1f, 1.0f));
////        });
//
////        int volMaxOriginal = nar.volMax();
//        actionUnipolar($.inh(DERIVE, "simple"), v -> {
//            //nar.volMax.setLerp(v, volMaxOriginal, 24);
//
//            derivePri.simplicity.setLerp(v);
//            //derivePri.complexityCost.setLerp(v);
//
//            //derivePri.complexityRelCost.set(0);
////            derivePri.complexityAbsCost.set(lerpSafe(v, 0f, 0.5f));
//
//        });
//    }

//    private void overderive(Term DERIVE, DefaultDerivePri derivePri, float minGain, float maxGain) {
//        /** "overderive": afterburner distortion pedal */
//        actionUnipolar($.inh(DERIVE, "gain"), v->{
//            derivePri.nalPri.set(lerpSafe(v, minGain, maxGain));
//        });
//    }
//
//    private void workerUnipolar(Term id, ObjectFloatProcedure<WorkerExec.WorkPlayLoop> ww) {
//        WorkerExec e = (WorkerExec) nar.exe;
//        actionUnipolar(id, (rate) -> {
//            e.loops.forEach(w -> ww.value(w, rate));
//        });
//    }


    public class SubMetaGame extends MetaGame {

        /**
         * the game that this controls
         */
        public final Game game;

        public SubMetaGame(Game g, GameTime timing) {
            super($.p(g.id, meta), timing);
            this.game = g;
        }

        @Override
        protected final void init() {

            super.init();

            initMeta(this);
        }

        @Deprecated
        private void pausing0() {
            float playThresh = 0.25f;
            actionPushButton(inh(game.id, MetaGame.play), new BooleanProcedure() {

                private final AtomicInteger autoResumeID = new AtomicInteger();
                private volatile ScheduledTask autoResume;
                private volatile Runnable resume;

                @Override
                public void value(boolean e) {

                    //enableAction = n.actionToggle($.func(enable, n.id), (e)->{
                    //TODO integrate and threshold, pause for limited time
                    synchronized (this) {
                        if (e) {
                            tryResume();
                        } else {
                            tryPause();
                        }
                    }

                }

                void tryPause() {

                    if (resume == null) {

                        resume = game.pause();
                        NAR n = nar();

                        int a = autoResumeID.get();

                        final long autoResumePeriod = 256;

                        autoResume = n.runAt(Math.round(n.time() + autoResumePeriod * n.dur()), () -> {
                            if (autoResumeID.get() == a)
                                tryResume();
                            //else this one has been cancelled
                        });

                    }
                }

                void tryResume() {

                    if (resume != null) {
                        autoResumeID.getAndIncrement();
                        resume.run();
                        autoResume = null;
                        resume = null;
                    }

                }
            }, () -> playThresh);

        }

        public AbstractAction pausing() {
            final int MAX_SLEEP_DURS = 80, MIN_WAKE_DURS = 20;
            return actionPushButton(inh(game.id, on), new BooleanPredicate() {

                long mustWakeAt = Long.MIN_VALUE;
                long maySleepAt = Long.MIN_VALUE;

                {
                    nar.onDur(() -> {
                        if (mustWakeAt > Long.MIN_VALUE) {
                            long now = nar.time();
                            if (now >= mustWakeAt) {
                                mustWakeAt = Long.MIN_VALUE;
                                maySleepAt = now + Math.round(game.dur() * MIN_WAKE_DURS);
                                game.enable(true); //force restart
                            }
                        }
                    });
                }

                @Override
                public boolean accept(boolean on) {
                    if (on) {
                        mustWakeAt = Long.MIN_VALUE;
                    } else {
                        long now = nar.time();
                        if (now < maySleepAt)
                            on = true;//force wake
                        else {
                            if (mustWakeAt == Long.MIN_VALUE) {
                                //start sleep
                                mustWakeAt = now + Math.round(game.dur() * MAX_SLEEP_DURS);
                            } else {
                                //continue sleep
                            }
                        }
                    }

                    game.enable(on);

                    return on;
                }
            });
            //pauser.goalDefault($.t(1, 0.0001f), nar);
            //        Reward enableReward = reward("enable", () -> enabled.getOpaque() ? +1 : 0f);
        }

        private void senseGameRewards() {
            //sense game's rewards individually
            if (game.rewards.size() > 1) {
                game.rewards.forEach(r -> {
                    if (r instanceof ScalarReward)
                        sense(r.id, () -> ((ScalarReward) r).reward);
                });
            }
        }

        public void focusShare(Focus from, float pct) {
            //                //this.what().accept(new EternalTask($.inh(aid,this.id), BELIEF, $.t(1f, 0.9f), nar));
            focusShare(from, focus(), pct);
        }

        private void focusShare(Focus from, Focus to, float pct) {

            onFrame(() -> {
                int links = (int) Math.ceil(pct * ((BagFocus) to).bag.capacity());
                from.sample(random(), links, t -> {
                    to.link(((AtomicTaskLink) t).clone());
                });
            });
        }

//        public void timeFocus0(Term id, BasicTimeFocus focus, FloatSupplier baseDur, float octaves, float range) {
//            durOctaves(id, baseDur, octaves, focus::dur);
//
//            floatAction(inh(id, "shift"), x ->
//                focus.shift.setLerp(x, -range, +range)
//            );
//
////            if (focus instanceof FuzzyTimeFocus) {
////                floatAction(inh(id, "whenever"), x -> {
////                    ((FuzzyTimeFocus) focus).jitter.setLerp(x, 0, 1);
////                });
////            }
//        }

        public void focusSharpness(float min, float max) {
            floatAction($.inh(game.id, "sharp"), (float s) ->
                ((ArrayBag) ((BagFocus) game.focus()).bag).sharp =
                    lerpSafe(s, min, max));
        }

        public void timeFocus(Term id, BasicTimeFocus focus, FloatSupplier baseDur, float rangeOctaves, float shiftDurs) {

            dur(id, baseDur, rangeOctaves, focus::dur);

            floatAction(inh(id, "shift"), x -> {
                float bd = baseDur.asFloat();
                double shiftMax = shiftDurs * Math.pow(2, rangeOctaves) * bd;
                double t = Util.lerp(x, -shiftMax, shiftMax);
                long s = Math.round(t / bd);
                focus.shiftDurs(s);
            });

        }

//        private void memoryPreAmp(GraphBagFocus w, boolean punc, boolean op) {
//            PuncPri byPunc = punc ? puncPri(game.id, new PuncPri()) : null;
//            OpPri byOp = op ? opPri(game.id, new OpPri()) : null;
//
//            FloatRange amp = new FloatRange(1, 0.1f, 1);
////            actionUnipolar($.inh(game.id, "amp"), (float a) -> amp.setLerp(Util.sqr(a)));
//
//            w.priTask = t -> (float) (
//                amp.doubleValue() *
//                t.priElseZero() *
//                (byPunc!=null ? byPunc.floatValueOf(t) : 1) *
//                (byOp!=null ? byOp.floatValueOf(t) : 1)
//            );
//        }


        public void focusClear(Focus w) {
            final float minDurations = 2;
            actionPushButton(inh(game.id, clear),
                    debounce(w::clear, minDurations));
        }



        public void curiosity(Game g, float onMin, float onMax, float offMin, float offMax) {
            //float ratio = g.curiosityOffDurs.floatValue() / g.curiosityDurs.floatValue();
            //g.curiosityOffDurs.set(t * ratio);
            floatAction(inh(g.id, $.p(CURIOSITY, "on")),
                x -> g.curiosityDurs.set(lerpSafe(x, onMin, onMax)));
            floatAction(inh(g.id, $.p(CURIOSITY, "off")),
                x -> g.curiosityOffDurs.set(lerpSafe(x, offMin, offMax)));
        }


        public void focusSharp(BagFocus w, float min, float max) {
            floatAction(inh(game.id, "sharp"), x -> {
                double s = ((ArrayBag) w.bag).sharp = lerpSafe(x, min, max);
//                System.out.println(s);
            });
        }

        //        public void focusGrow(BagFocus w, float min, float max) {
//            actionUnipolar($.inh(game.id, grow), (float x) -> {
//                ((BasicActivator) w.activator).feedback.set(lerpSafe(x, min, max));
//            });
//        }
        public void simplicity(DefaultBudget B, float min, float max) {
            floatAction(inh(game.id, simple), x ->
                B.simple.setLerp(x, min, max));
        }

        public void simplicityExtended(DefaultBudget B, float min, float max) {
            floatAction(inh(game.id, $.p(simple, "in")), x ->
                B.simpleIn.setLerp(x, min, max));

            floatAction(inh(game.id, $.p(simple, "out")), x ->
                B.simpleOut.setLerp(x, min, max));
        }

//        public void focusAmp(BagFocus w, float min, float max) {
//            actionUnipolar(inh(game.id, amp), (float x) -> {
//                ((BasicActivator) w.budget).amp.set(lerpSafe(x, min, max));
//            });
//        }

        public void focusSustainBasic(BagFocus w, float min, float max) {
            if (!(w.updater instanceof BagSustain)) {
                //HACK
                w.updater = new BagSustain();
            }
            floatAction(inh(game.id, forget), min, max, 1, x -> ((BagSustain) w.updater).sustain.set(x));
        }

        private void memoryControlPrecise(BagFocus w, boolean base, boolean byPunc, boolean byOp) {

            BagFocus.BagSustainByOp u = new BagFocus.BagSustainByOp();
            w.updater = u;

            if (base)
                focusSustainBasic(w, -1, +1);

            if (byPunc) {
                priAction(punc(BELIEF), u.belief::set);
                priAction(punc(GOAL), u.goal::set);
                priAction(punc(QUESTION), u.question::set);
                priAction(punc(QUEST), u.quest::set);
            }
            if (byOp) {
                /* Op.ATOM.atom conflicts with BELIEF */
                //TODO fix the labeling:
                actFloat(inh(MetaGame.pri, p(OP, the("atom"))), u.atom);
                actFloat(inh(MetaGame.pri, p(OP, INH.atom)), u.inh);
                actFloat(inh(MetaGame.pri, p(OP, SIM.atom)), u.sim);
                actFloat(inh(MetaGame.pri, p(OP, IMPL.atom)), u.impl);
                actFloat(inh(MetaGame.pri, p(OP, CONJ.atom)), u.conj);
                actFloat(inh(MetaGame.pri, p(OP, PROD.atom)), u.prod);
            }
//                if (byCmpl) {
//                    floatAction($.inh(gid, $.p(forget, "simple")), u.simple);
//                    floatAction($.inh(gid, $.p(forget, "complex")), u.complex);
//                }


            //		WhatThe.BagDecayByVolume updater = new WhatThe.BagDecayByVolume();
            //		((WhatThe) w).updater = updater;
            //		floatAction($.inh(gid, forget), updater.decayRate.subRange(0.5f, 1.5f));
            //		floatAction($.inh(gid, Atomic.the("forgetCmpl")),  -1 /* dead zone half */, +1, 1, updater.volumeFactor::set);


        }

        public void focusSimpleSensor(BagFocus w) {
            sense(inh(p(w.id, focus), simple), () -> {
                double vSum = 0;
                int n = 0;
                for (Premise t : w.bag) {
                    vSum += ((TaskLink) t).volMean();
                    n++;
                }
                if (n == 0) return Float.NaN;
                double vMean = vSum / n;
                double vMeanNormalized = Math.min(1, vMean / w.volMax());
                return (float) (1 - vMeanNormalized);
            });
        }

        public void focusPuncSensor(BagFocus w) {
            TaskLinkPuncVectorSensor vs = new TaskLinkPuncVectorSensor(w);
//            vs.resolution(resSense);
            sensors.addSensor(vs);
            sense(inh(p(w.id, focus), p(mean,pri)), () -> (float) vs.mean);
                    //.resolution(resSense);
        }

        public LambdaScalarReward dexReward() {
//            FloatAveragedWindow dexSum = new FloatAveragedWindow(8 /* TODO calculate */)
//                    .mode(FloatAveragedWindow.Mode.Mean);
//            Ewma dexMean = new Ewma(0.5f);
//            game.onFrame(() -> dexMean.accept((float) game.dexterity()));

            final FloatSupplier dex = () -> (float) game.dexterity();
            FloatNormalized dexNorm = new FloatNormalized(dex)
                .period(0, 256)
                .minLimit(0, 0);

            //TODO this needs to be sampled each game frame, esp when the self runs at a lower duration it will not capture all the dex
            var r = reward(inh(game.id, MetaGame.dex), dexNorm);
            r.resolution(resReward);
            r.eternal = true;
            return r;
        }



        public void priRewards() { priAction(game.rewards.pri); }

        public void priActions() {
            priAction(game.actions.pri);
        }

        public void priSensors() {
            priAction(game.sensors.pri);
        }

        public void priVectorSensors(boolean individualSensorPri, boolean vectorSensorPri, boolean vectorSensorRate) {

            for (FocusLoop s : game.sensors.sensors) {
                if (s instanceof VectorSensor v) {
                    if (vectorSensorPri)
                        priAction(v.pri);

                    if (vectorSensorRate && v.size() > 16) {
                        FloatProcedure updater;
                        if (v.model instanceof DirectVectorSensorAttention)
                            updater = r -> ((DirectVectorSensorAttention) v.model).activateRate.setLerp(r, 0, 1);
                        else if (v.model instanceof QueueVectorSensorAttention)
                            updater = r -> ((QueueVectorSensorAttention) v.model).inputRate.setLerp(r, 0, 1);
                        else
                            updater = null;

                        if (updater!=null)
                            floatAction(inh(v.id, "rate"), updater);
                    }

                } else if (s instanceof AbstractSensor ss) {
                    if (individualSensorPri)
                        priAction(ss.pri);
                } else
                    throw new UnsupportedOperationException();
            }

        }
        public LambdaScalarReward gameReward() {
            //senseGameRewards();
            return rewards(game, false);
            //return rewards(game, true);
        }

        public void certainty(DefaultBudget B, float min, float max) {
            floatAction($.inh(game.id, "certain"),
                z -> B.certain.setLerp(z, min, max));
        }
//        public void amp(DefaultBudget B, float min, float max) {
//            floatAction($.inh(game.id, "amp"),
//                    z -> B.amp.setLerp(z, min, max));
//        }

//        public void rewardConf(float rConfMin, float rConfMax) {
//            if (game.rewards.size() > 1) {
//                game.rewards.forEach(r -> {
//                    floatAction(inh(r.id, "conf"), (c) -> {
//                        ((ScalarReward) r).goalTruth.conf(lerpSafe(c, rConfMin, rConfMax));
//                    }).resolution(0.1f);
//                });
//            }
//        }
    }


}