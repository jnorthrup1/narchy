package nars.func;

import jcog.deep.Autoencoder;
import jcog.random.XoRoShiRo128PlusRandom;
import jcog.sort.QuickSort;
import nars.$;
import nars.NAR;
import nars.Term;
import nars.focus.Focus;
import nars.game.Game;
import nars.game.sensor.SubSignalComponent;
import nars.game.sensor.VectorSensor;
import nars.table.BeliefTable;
import nars.task.SerialTask;
import nars.term.Neg;
import nars.truth.Truth;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static nars.Op.*;

/**
 * decompiles a continuously trained autoencoding of an input concept vector
 * TODO make DurService
 * TODO extend VectorSensor
 */
public class AutoConceptualizer extends VectorSensor {

    public final Autoencoder ae;

    private final List<SubSignalComponent> concepts;

    private final boolean beliefOrGoal;
    private final float[] x;
    float learningRate = 0.05f;

    public AutoConceptualizer(Term id, List<SubSignalComponent> concepts, boolean beliefOrGoal, int features, NAR n) {
        super(id);
        this.concepts = concepts;
        this.beliefOrGoal = beliefOrGoal;
        this.ae = new Autoencoder(concepts.size(), features, new XoRoShiRo128PlusRandom(n.random().nextLong()));
        this.x = new float[concepts.size()];
        ae.noise.set(0.0002f);
    }

    @Override
    public int size() {
        return concepts.size();
    }


    @Override
    public void accept(Game g) {

        NAR n = g.nar;
        long now = n.time();
        float[] x = this.x;
        int inputs = concepts.size();
        for (int i = 0, inSize = inputs; i < inSize; i++) {
            BeliefTable beliefTable = ((BeliefTable) concepts.get(i)
                    .table(beliefOrGoal ? BELIEF : GOAL));
            Truth t = beliefTable.truth(now, now, n);
            x[i] = t == null ? 0.5f : t.freq();
        }

        //ae.noise.set(noiseLevel);
        ae.put(x, learningRate);
        
        int outputs = ae.outputs();
        float[] b = new float[outputs];

        float thresh = n.freqResolution.floatValue();

        Focus w = g.focus();

        float dur = w.dur();
        long start = Math.round(now - dur / 2), end = Math.round(now + dur / 2);
        int[] order = new int[inputs];
        Truth truth = $.t(1f, 0.9f);
        for (int i = 0; i < outputs; i++) {
            b[i] = 1; 

            float[] a = ae.decode(b);
            
            Term feature = conj(order, a /* threshold, etc */, 3 /*a.length/2*/,
                    thresh);
            if (feature != null)
                w.remember(onFeature(feature, truth, start, end, n.evidence()));

            b[i] = 0; 
        }
    }

    protected static SerialTask onFeature(Term feature, Truth truth, long start, long end, long[] evi) {
        if (feature instanceof Neg) {
            feature = feature.unneg();
            truth = truth.neg();
        }
        return new SerialTask(feature, BELIEF, truth, start, end, evi);
    }

    private Term conj(int[] order, float[] a, int maxArity, float threshold) {

        
        int n = a.length;
        for (int i = 0; i < n; i++)
            order[i] = i;

        float finalMean = 0.5f; 
        QuickSort.sort(order, (i) -> Math.abs(finalMean - a[i]));

        Set<Term> x = new UnifiedSet<>(maxArity);
        int j = 0;
        for (int i = 0; i < order.length && j < maxArity; i++) {
            int oi = order[i];
            float aa = a[oi];
            if (Math.abs(aa - 0.5f) < threshold)
                break; 

            x.add(concepts.get(oi).term().negIf(aa < finalMean));
            j++;
        }

        return x.isEmpty() ? null : CONJ.the(0, x);
    }

    @Override
    public Iterator<SubSignalComponent> iterator() {
        return concepts.iterator();
    }
}