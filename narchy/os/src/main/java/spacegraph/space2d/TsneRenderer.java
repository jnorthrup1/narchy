package spacegraph.space2d;

import com.jogamp.opengl.GL2;
import jcog.Util;
import jcog.data.bit.MetalBitSet;
import jcog.table.DataTable;
import jcog.tree.rtree.rect.RectF;
import spacegraph.space2d.container.graph.Graph2D;
import spacegraph.space2d.container.graph.NodeVis;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.video.Draw;
import tech.tablesaw.api.NumericColumn;
import tech.tablesaw.columns.Column;

import java.util.DoubleSummaryStatistics;

public class TsneRenderer implements Graph2D.Graph2DRenderer<DataTable.Instance> {

    public static Surface view(DataTable data, MetalBitSet vectorCols) {
        TsneModel m = new TsneModel(vectorCols);
        TsneRenderer r = new TsneRenderer() {

            final int scoreColumn = 0;

            double min, max;
            {
                Column<?> scoreCol = data.column(scoreColumn);
                var scoreStats = stats((NumericColumn<?>) scoreCol);
                min = scoreStats.getMin();
                max = scoreStats.getMax();
            }

            static DoubleSummaryStatistics stats(final NumericColumn<?> values) {
                DoubleSummaryStatistics s = new DoubleSummaryStatistics();
                final int n = values.size();
                for (int i = 0; i < n; i++)
                    s.accept(values.getDouble(i));

                return s;
            }


            @Override
            protected void paintNode(GL2 gl, Surface surface, DataTable.Instance id) {
                double score = ((Double) (id.data.get(scoreColumn)));
                double scoreNormal = Util.normalizeSafer(score, min, max);
                //Draw.colorGrays(gl, (float)(0.1f + 0.9f * scoreNormal));
                Draw.hsl(gl, (float) scoreNormal, 0.9f, 0.5f, 0.9f);
                Draw.rect(surface.bounds, gl);
            }
        };

        return new Graph2D<DataTable.Instance>()
            .update(m)
            .render(r)
            .set(data.stream().map(data::instance))
            .widget();
    }

    @Override
    public void node(NodeVis<DataTable.Instance> node, Graph2D.GraphEditor<DataTable.Instance> graph) {
        node.set(new PushButton() {
            @Override
            protected void paintWidget(RectF bounds, GL2 gl) {

            }

            @Override
            protected void paintIt(GL2 gl, ReSurface r) {
                paintNode(gl, this, node.id);
            }
        }.clicked(() -> System.out.println(node.id.data)));
    }

    protected void paintNode(GL2 gl, Surface surface, DataTable.Instance id) {
        Draw.colorHash(gl, id.hashCode(), 0.8f);
        Draw.rect(surface.bounds, gl);
    }
}