package nars

import com.github.benmanes.caffeine.cache.Weigher
import jcog.Config
import jcog.Util
import jcog.data.list.Lst
import jcog.exe.Loop
import jcog.math.FloatSupplier
import jcog.pri.bag.impl.ArrayBag
import jcog.pri.op.PriMerge
import jcog.ql.dqn.ValuePredictAgent
import jcog.signal.FloatRange
import nars.action.answer.AnswerQuestionsFromConcepts
import nars.action.link.LinkFlow
import nars.action.link.STMLinker
import nars.action.link.TermLinking
import nars.action.link.index.CachedAdjacenctTerms
import nars.action.link.index.EqualTangent
import nars.action.resolve.BeliefResolve
import nars.action.resolve.TaskResolve
import nars.action.resolve.TaskResolver
import nars.action.transform.Abbreviate
import nars.action.transform.Arithmeticize
import nars.action.transform.ExplodeAction
import nars.action.transform.ImplTaskifyAction
import nars.action.transform.Inperience.BeliefInperience
import nars.action.transform.Inperience.QuestionInperience
import nars.concept.Concept
import nars.derive.Deriver
import nars.derive.impl.BagDeriver
import nars.derive.impl.MixDeriver
import nars.derive.pri.DefaultBudget
import nars.derive.reaction.NativeReaction
import nars.derive.reaction.ReactionModel
import nars.derive.reaction.Reactions
import nars.exe.Exec
import nars.exe.impl.UniExec
import nars.exe.impl.WorkerExec
import nars.focus.BagFocus
import nars.focus.BasicTimeFocus
import nars.focus.time.ActionTiming
import nars.focus.time.PresentTiming
import nars.func.Commentary
import nars.func.Factorize
import nars.func.stm.CompoundClustering
import nars.game.Game
import nars.game.action.util.Reflex
import nars.game.meta.MetaGame
import nars.game.meta.SelfMetaGame
import nars.game.reward.LambdaScalarReward
import nars.game.reward.Reward
import nars.game.reward.ScalarReward
import nars.game.sensor.FocusLoop
import nars.game.sensor.VectorSensor
import nars.gui.NARui
import nars.link.TaskLink
import nars.link.flow.LinkFlows
import nars.memory.CaffeineMemory
import nars.memory.HijackMemory
import nars.memory.Memory
import nars.memory.TierMemory
import nars.task.NALTask
import nars.task.util.Eternalization.Flat
import nars.task.util.PuncBag
import nars.term.Termed
import nars.term.atom.Atomic
import nars.time.Time
import nars.time.clock.RealTime.MS
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure
import spacegraph.SpaceGraph
import spacegraph.space2d.container.grid.Gridding
import spacegraph.space2d.widget.button.PushButton
import spacegraph.space2d.widget.text.BitmapLabel
import java.util.function.*
import java.util.stream.Stream

/**
 * builder for NAR's that play games
 */
open class Player() {
    @JvmField
    var threads = Config.INT("THREADS", Util.concurrencyExcept(1))

    /**
     * fundamental system framerate
     * TODO auto-determine by estimating from lowest common denominator of the added parts duration's
     */
    var fps = 30f
    var eternalizationControl = false
    var beliefConfControl = false
    var goalConfControl = false

    /** thread affinity, can improve latency
     * WARNING untested. SUSPECT: threads may not respawn correctly
     */
    var exeAffinity = false
    var ready = Consumer { n: NAR? -> }

    @JvmField
    public var nal = true
    public var meta = true

    @JvmField
    var inperience = true

    /** global volmax control
     * warning: may interfere with other simplicity controls  */
    private val volMaxControl = false
    private val durMeta = false
    private val selfFreq = false
    var gamePri = true
    var autoReward = true

    //    public boolean rewardConf = false;
    //    /** @see DefaultDerivePri */
    //    public boolean puncProb = false;
    //    public boolean subMetaPuncPri = false;
    //    public boolean subMetaOpPri = false;
    @JvmField
    var motivation = true
    var xor = false
    var answerQuestionsFromTaskLinks = false

    @JvmField
    var goalSpread = false
    var actionRewardQuestion = false

    @JvmField
    var arith = true

    @JvmField
    var explode = true

    @JvmField
    var factorize = false
    var abbreviation = false
    var globalAdjacency = false
    var ifThenElse = false
    var goalInduction = false

    /** TODO split into separate 'dur' and 'shift' flags  */
    var timeFocus = true
    //false;
    /** in durs  */
    var timeOctaves = 6f
    //7;
    //3;
    //4;
    //2;
    //5;
    //8;
    //10;
    //16;
    /** scannable horizon duration  */
    var shiftEpochs = 8f

    //2;
    //4;
    //1;
    @JvmField
    var deltaIntro = true
    var deltaGoal = false
    //    static final public boolean deltaGoals = false;
    //    static final boolean answerLogging = true;
    /** TODO maybe broken  */
    @JvmField
    @Deprecated("")
    var gameReflex = false
    var selfMetaReflex = true
    var subMetaReflex = true
    //    public boolean subMetaReflexMerged = false;
    /**
     * extra task resolver for present focus
     */
    var taskResolveAction = false

    /**
     * extra task resolver for present focus
     */
    var beliefResolveAction = false
    var encourager = false
    var commentary = false

    @JvmField
    var conjClusterBelief = true

    @JvmField
    var conjClusterGoal = false

    @JvmField
    var stmLinker = true
    //!(conjClusterBelief && conjClusterGoal);
    //!conjClusterBelief;
    /** ability for a submeta to pause its game  */
    var pausing = false

    @JvmField
    var uiTop = true
    var uiStats = false
    var focusSamplingExponent = ArrayBag.SHARP_DEFAULT //4

    //1
    //2
    //3
    var cpuThrottle = true
    //    public ControlModel control = null;
    /** -1 for auto-size to heap (-Xmx)  */
    @JvmField
    var conceptsMax = -1.0

    //    /** default focus capacity TODO */
    //    public int focusCapacity =
    //            //128
    //            //256
    //            512
    //            //1024
    //            //2048
    //    ;
    var beliefConf = 0.3f
    var goalConf = beliefConf
    //public float beliefConf = 0.5f, goalConf   = 0.75f;
    //public float beliefConf = Util.PHI_min_1f, goalConf   = 0.75f;
    //public float beliefConf = Util.PHI_min_1f, goalConf   = beliefConf * 1.1f;
    //    public float beliefConf = Util.PHI_min_1f, goalConf   = beliefConf;
    //public float beliefConf = 0.85f, goalConf   = 0.9f;
    //    public float beliefConf = 0.9f, goalConf   = beliefConf;
    //public float beliefConf = Util.PHI_min_1f, goalConf   = 0.9f;
    //    public float beliefConf = 0.75f, goalConf   = beliefConf;
    //      public float beliefConf = 0.5f, goalConf   = beliefConf;
    //public float beliefConf = 0.5f, goalConf   = 0.6f;
    //    public float beliefConf = 0.5f, goalConf   = 0.9f;
    //    public float beliefConf = 0.75f, goalConf   = 0.5f;
    //    public float beliefConf = 0.9f, goalConf   = 0.5f;
    //public float beliefConf = 0.25f, goalConf   = 0.25f;
    //    public float beliefConf = 0.75f, goalConf   = 0.75f;
    //public float beliefConf = 0.5f, goalConf   = 0.15f;
    /** TODO  */
    @JvmField
    var eternalization =  //1/16f;
        1 / 32f

    //0.1f/NAL.answer.REVISION_CAPACITY;
    //1 - Util.PHI_min_1f;
    //0;
    //1/1024f;
    //1/256f;
    //1/100f;
    //1/64f;
    //1/32f;
    //1/10f;
    //1/8f;
    //1/4f;
    //1/2f;
    @JvmField
    var volMax =  //16
    //20
    //24
        //28
        32 //48

    //64
    //96
    //128
    //256
    @JvmField
    var dtDither =  //4
    //5  //200hz (100hz nyquist)
        //8
        10 //100hz (50hz nyquist)

    //20   //50hz  (25hz nyquist)
    //40   //25hz  (12.5hz nyquist)
    @JvmField
    var freqRes = 0.01f

    //0.02f;
    //NAL.truth.TRUTH_EPSILON; //minimum
    var confRes = NAL.truth.TRUTH_EPSILON //minimum
    //freqRes;
    //0.01f;
    /** constructed by calling start()  */
    @Volatile
    var nar: NAR? = null
    private var loop: Loop? = null

    /** initial min confidence  */
    @JvmField
    var confMin = NAL.truth.CONF_MIN

    //NAL.truth.TRUTH_EPSILON / NAL.answer.REVISION_CAPACITY; //coarse
    //0.0000001f;
    //0.0001;
    var nalProcedural = true
    var nalStructural = true
    var nalSets = false

    @JvmField
    @Deprecated("")
    var iterTune = false

    /** warning; can easily confuse other priority controls  */
    var simplicityControl = true

    /** extra simplicity controls.  "complex" simplicity  */
    var simplicityExtendedControl = true
    var certaintyControl = false
    var opPri = false
    var puncPri = true
    var puncPriAmp = true
    var focusClear = true
    var focusSharp = true

    @Deprecated("")
    var implBeliefify = false

    @Deprecated("")
    var implGoalify = implBeliefify
    private val curiosityControl = true
    private val metaVolReduce = true

    constructor(vararg parts: NARPart?) : this(Stream.of<NARPart?>(*parts))
    constructor(parts: Collection<NARPart?>) : this() {
        add(parts)
    }

    constructor(parts: Stream<out NARPart?>) : this() {
        add(parts)
    }

    @Deprecated("")
    constructor(fps: Float, ready: Consumer<NAR?>) : this(ready) {
        fps(fps)
    }

    constructor(ready: Consumer<NAR?>) : this() {
        ready(ready)
    }

    @Synchronized
    fun ready(r: Consumer<NAR?>): Player {
        if (nar != null) r.accept(nar) else ready = ready.andThen(r)
        return this
    }

    fun add(vararg p: NARPart?): Player {
        return add(Stream.of(*p))
    }

    fun add(p: Stream<out NARPart?>): Player {
        return ready { nn: NAR? -> p.forEach { p: NARPart? -> nn!!.add(p) } }
    }

    fun add(p: Collection<NARPart?>): Player {
        return add(p.stream().parallel())
    }

    //    public void control(ControlModel m) {
    //
    //        this.control = m;
    //
    ////        if (!meta) {
    ////            n.emotion.want(MetaGoal.Question, -0.005f);
    ////            n.emotion.want(MetaGoal.Believe, 0.01f);
    ////            n.emotion.want(MetaGoal.Goal, 0.05f);
    ////        }
    //    }
    //    public void controlCreditAnalysis() {
    //        control(new CreditAnalysisModel());
    //    }
    //
    //    public void control(CreditControlModel.Governor g) {
    //        control(new CreditControlModel(g));
    //    }
    //
    //    public void controlMLP() {
    //        control(new MLPGovernor());
    //    }
    //
    //    public void controlLerp() {
    //        control(new LERPGovernor());
    //    }
    @Synchronized
    fun start(): Player {
        init()
        loop = nar!!.startFPS(fps)
        return this
    }

    @Deprecated("")
    @Synchronized
    fun runCycles(cycles: Int): Player {
        init()
        nar!!.run(cycles)
        return this
    }

    @Synchronized
    fun init() {
        ensureStopped()
        if (nar == null) __init()
    }

    private fun ensureStopped() {
        if (loop != null) throw RuntimeException("already running")
    }

    @Synchronized
    fun stop(): Player {
        //nar.synch();
//        nar.runLater(()->{
        nar!!.parts(Game::class.java).forEach { g: Game -> nar!!.remove(g) } //HACK
        //            nar.stop();
//        });
        return this
    }

    private fun __init() {
        nar = nar()
        val n = nar!!
        n.dtDither.set(dtDither)
        n.volMax.set(volMax)
        n.freqResolution.set(freqRes)
        n.confResolution.set(confRes)
        n.beliefConfDefault.set(beliefConf)
        n.goalConfDefault.set(goalConf)
        initEternalization(n)
        n.confMin.set(confMin)
        ready.accept(n)
        n.runLater {
            n.parts(Game::class.java).forEach { g: Game ->
                if (metaVolReduce) {
                    if (g is MetaGame) g.focus().volMax = Math.max(volMax / 2,
                        14) /* TODO */
                }
                ((g.focus() as BagFocus).bag as ArrayBag<*, *>).sharp = focusSamplingExponent
            }
        }
        if (meta) meta()
        if (commentary) {
            /*n.runLater(() -> */
            n.add(Commentary(n))
        }

//        if (impiler) {
//            playing().forEach(g -> {
//                    Impiler.impile(g);
//            });
//        }
        if (gameReflex) {
            /*n.runLater(() -> */
            games().forEach { g: Game? ->
                val bb = Reflex(
                    { inputs: Int, actions: Int ->
                        ValuePredictAgent.DQN(inputs,
                            actions)
                    },  //ValuePredictAgent::DQN_NTM,
                    //ValuePredictAgent::DQN_LSTM,
                    //ValuePredictAgent::DQN1,
                    g,
                    1f, 2f, 4f
                )
            }
        }
        if (uiStats) {
            n.runLater {
                val stats = BitmapLabel(0x00D8.toChar().toString())
                SpaceGraph.window(stats, 400, 600)
                val s = StringBuilder(16 * 1024)
                Loop.of {

                    //stats.viewMin(stats.view().w, stats.view().h);
                    s.setLength(0)
                    n.stats(false, true, s)
                    stats.text(s.toString()) //TODO avoid toString() and read from CharSequence directly
                }.fps(0.25f)
            }
        }
        if (uiTop) {
            //n.synch();
            n.runLater { SpaceGraph.window(NARui.top(n), 1024, 800) }
        }

//        if (uiGames) {
//            nar.runLater(() -> {
//                SpaceGraph.window(new Gridding(nar.parts(Game.class).map(NARui::gameUI).collect(toList())), 1024, 768);
//            });
//        }
    }

    private fun initEternalization(n: NAR?) {
        val ete =  //new Eternalization.TemporalsAndVariables()
            Flat()
        n!!.eternalization = ete.set(eternalization)
    }

    fun games(): Stream<Game> {
        return nar!!.parts(Game::class.java).filter { g: Game? -> g !is MetaGame }
    }

    fun meta() {
        val durs = 2f
        val metaFocusCapacity = 512
        val meta: SelfMetaGame = object : SelfMetaGame(nar, durs) {
            //            public Reflex rlBoostUI(Reflex r) {
            //                nar.runLater(()-> window(NARui.reflexUI(r), 800, 600));
            //                return r;
            //            }
            override fun initMeta() {
                heapSense()
                emotionSense()
                if (cpuThrottle) {
                    autoThrottle()
                    //cpuThrottle(0.05f, 1);
                }
                if (eternalizationControl) eternalization(0.01f, 0.5f)
                if (beliefConfControl) conf(Op.punc(Op.BELIEF), nar.beliefConfDefault, 0.1f, 0.9f)
                if (goalConfControl) conf(Op.punc(Op.GOAL), nar.goalConfDefault, 0.1f, 0.9f)

                //confMin(8);

                //truthPrecision(true, false);

                //conf(nar.goalConfDefault, 0.25f, 0.75f);
                if (volMaxControl && volMax > 12) {
                    volMax(12, volMax)
                }
                if (durMeta) durMeta(3f, 2f)
                if (selfFreq) {
                    action(`$`.inh(id, "freq"), FloatProcedure { x: Float -> focus().freq.setLerp(x, 0.01f, 0.1f) }
                    )
                }
                //                if (nar.control.model instanceof CreditControlModel) {
//                    metaGoals((CreditControlModel) nar.control.model);
////            CreditControlModel.Governor governor = ((CreditControlModel) nar.control.model).governor;
//                    //if (governor instanceof MLPGovernor) governor((MLPGovernor)governor); //probably isnt always necessary
//                }
                //        if (nar.memory instanceof HijackMemory) {
                //            memoryControl((HijackMemory)nar.memory);
                //        }

                //overderive(DERIVE, derivePri, 1, 1.5f);
                //opPri(SELF, derivePri.opPri);
            }

            private fun autoThrottle() {
                //computes throttle from total demand freq of the subgames
                onFrame(Runnable {
                    val max = game.stream()
                        .flatMap { z: SubMetaGame -> Stream.of(z, z.game) } //all subgames and their meta's
                        .mapToDouble({ g: Game -> g.focus().freq().toDouble() }) //.average()
                        .max()
                        .asDouble
                    //TODO + meta freq?

                    //System.out.println(demand);
                    val minThrottle = 0.01
                    nar.loop.throttle.set(Math.max(minThrottle, max))
                })
            }

            override fun initMetaEnd() {
                if (selfMetaReflex) {
                    val r = rlBoost() //rlBoostUI(rlBoost());
                    //                    if (subMetaReflexMerged) {
//                        for (SubMetaGame sm : game)
//                            r.add(sm, r);
//                    }
                }
            }

            private fun eternalization(min: Float, max: Float) {
                floatAction(`$`.inh(id, "eternalize"),
                    FloatProcedure { e: Float ->
                        (nar.eternalization as Flat).ete.setLerp(e,
                            min,
                            max)
                    } as FloatProcedure?).resolution(0.02f)
            }

            override fun initMeta(metaGame: SubMetaGame) {
                val G = metaGame.game
                if (actionRewardQuestion) actionRewardQuestion(metaGame, G)
                if (curiosityControl) metaGame.curiosity(G,
                    NAL.CURIOSITY_DURS * 4,
                    NAL.CURIOSITY_DURS / 4,
                    NAL.CURIOSITY_DURS,
                    NAL.CURIOSITY_DURS / 16
                )


                //senseGameRewards();

//                onFrame(()->{
////                    float subMetaDurs = 1;
////                    focus().dur(subMetaDurs * G.dur());
//
//                    metaGame.focus().dur(G.nar.dur());
//                });
                val f = G.focus() as BagFocus
                val B: DefaultBudget = f.budget as DefaultBudget
                if (puncPri) metaGame.puncPriSimple(
                    f.id, 0.25f, 1f, puncPriAmp,
                    B.linkPuncPri //B.puncDerived
                    //B.puncNALPremise
                    //B.puncTaskifyPremise/*, B.puncDerived*/ /*, B.puncNALPremise,  */
                )
                if (opPri) metaGame.opPri(f.id, B.linkOpPri,
                    0.25f, 1f)

//                if (subMetaPuncPri) {
//                    if (!(f.budget instanceof OpPuncActivator)) f.budget = new OpPuncActivator();
//                    g.puncPri(G.id, ((OpPuncActivator)f.budget).punc, 0.1f, 1f);
//                    //g.puncPriSimple(G.id, ((OpPuncActivator)f.activator).punc, 0.1f, 1f);
//                }
//                if (subMetaOpPri) {
//                    if (!(f.budget instanceof OpPuncActivator)) f.budget = new OpPuncActivator();
//                    g.opPri(G.id, ((OpPuncActivator)f.budget).opProb, 0.001f, 1);
//                }

                //g.focusSharp(f, 0.1f, 2f);
                if (simplicityControl) {
                    metaGame.simplicity(B,  /*1*/0.5f, 7f)
                    if (simplicityExtendedControl) metaGame.simplicityExtended(B, 0.5f, 1.5f)
                }
                if (certaintyControl) metaGame.certainty(B, 0f, 2f)
                //                if (ampControl)
//                    g.amp(B, 0.1f, 0.4f);
                if (focusClear) {
                    metaGame.focusClear(f)
                    metaGame.focusSustainBasic(f, 0f, 0.98f)
                } else {
                    metaGame.focusSustainBasic(f, -0.95f, 0.95f)
                }

                //g.focusAmp(f, 0.01f, 0.25f);
                //((BasicActivator)f.activator).amp.set(0.25f);
                //g.focusAmp(f, 0.01f, 0.25f);


                //g.focusShare(f, 0.001f); //LEAK
                metaGame.focusSimpleSensor(f)
                metaGame.focusPuncSensor(f)
                if (focusSharp) metaGame.focusSharpness(1f, 3f)

                //memoryControlSimple(w, -0.9f, 0.9f);

                //memoryControlPrecise(w, false, true, false);
                //memoryControlPrecise(w, true, false, true);


                //memoryPreAmp(w, true, false);

                //curiosityShift();
//                if (curiosityControl)
//                    g.curiosityRate();
                //curiosityStrength();


//                {
//
//                    OpPri op = new OpPri();
//                    PuncPri punc = new PuncPri();
//                    w.taskLinkPri = t -> op.floatValueOf(t) * punc.floatValueOf(t) * t.pri();
//                    Term root = w.id; /* $.p(w.id, pri)*/
//                    opPri(root, op);
//                    puncPri(root, punc);
//
//                }


//				if (w.inBuffer instanceof PriBuffer.BagTaskBuffer)
//					floatAction($.inh(gid, input), ((PriBuffer.BagTaskBuffer) (w.inBuffer)).valve);
                if (timeFocus) {
                    metaGame.timeFocus(f.id, f.time as BasicTimeFocus, { G.dur() },  //nar.dur(),
                        timeOctaves, shiftEpochs
                    )
                }
                val amp = 1f
                //0.5f;
                G.actions.pri.amp(amp)
                G.sensors.pri.amp(amp)
                G.rewards.pri.amp(amp)
                if (gamePri) {
                    if (autoReward) {
                        val min =  //0.01f;
                            //0.001f;
                            Math.max(0.01f, 0.1f / metaGame.game.rewards.size())
                        //0.5f/metaGame.game.rewards.size();
                        for (r in metaGame.game.rewards) {
                            val initialStrength = r.strength()
                            if (initialStrength > min * 2) {
                                metaGame.action(`$`.inh(r.id, conf), { s: Float ->
                                    /* controls reward goal conf */r.strength(s)

                                    /* controls reward sensor priority */if (r is ScalarReward) r.amp(
                                    s) else {
                                    /*throw new TODO();*/
                                }
                                }, min, initialStrength /* use initial strength as max */, Float.NaN)
                            }
                        }
                    } else {
                        /* global reward priority control */
                        metaGame.priRewards()
                    }
                    metaGame.priActions()
                    metaGame.priSensors()
                    //                G.sensors.pri.amp(0.25f);
                    metaGame.priVectorSensors(false, true, true)
                }
                //                if (rewardConf) {
//                    final float confBase = g.nar.confDefault(GOAL);
//                    g.rewardConf(confBase / 4, confBase);
//                }
                val gameReward: Reward =
                    metaGame.gameReward() //TODO make this a reward proxy, to avoid redundant concept
                val dexReward: Reward = metaGame.dexReward() //.strength(0.5f);
                if (subMetaReflex) {
                    val rr = metaGame.rlBoost()
                    //                    nar.runLater(()-> rlBoostUI(rr));
                }
                if (pausing) metaGame.pausing()
            }

            private fun actionRewardQuestion(g: SubMetaGame, G: Game) {
                G.onFrame(Runnable {
                    val rng = G.nar().random()
                    val _s: FocusLoop<*> = G.sensors.sensors[rng]!!
                    var s: Term
                    s = when (_s) {
                        is VectorSensor -> (Lst<Any?>(_s.components()).get(rng) as Termed).term()
                        else -> _s.term()
                    }
                    val _r: Reward = G.rewards.rewards[rng]!!
                    val r: Term = _r.term() //TODO goal term
                    if (rng.nextBoolean()) s = s.neg()
                    val q = Op.CONJ.the(Op.XTERNAL, s, r)
                    val startend = G.focus().`when`()
                    val start = startend[0]
                    val end = startend[1]
                    val punc =  //QUESTION;
                        Op.QUEST
                    val p: Task = NALTask.task(q, punc, null, start, end, g.nar.evidence())
                    p.pri(0.25f)
                    //                        System.out.println(p);
                    G.focus().accept(p)
                })
            }
        }

        //self.logActions();
        if (encourager) {
            val enc = FloatRange(0.5f, 0f, 1f)
            val e: LambdaScalarReward = meta.addEncouragement(enc)
            nar!!.runLater { //HACK
                SpaceGraph.window(Gridding(NARui.beliefChart(e.sensor, nar),
                    PushButton(":)", { enc.set(1f) }),
                    PushButton(":(", { enc.set(0f) })), 800, 800)
            }
        }
        (meta.focus() as BagFocus).capacity.set(metaFocusCapacity)
        nar!!.apply {
            runLater { //HACK
                meta.game.forEach(Consumer<SelfMetaGame.SubMetaGame> { g: SelfMetaGame.SubMetaGame ->
                    (g.focus() as BagFocus).capacity.set(
                        metaFocusCapacity)
                })
            }
            meta.onFrame(Runnable {
                val metaFreq = 0.05f / meta.game.size
                meta.focus().freq(metaFreq)
            })
            add(meta)
        }.synch()
//        nar.runLater(() -> {
//            List<RLBooster> b = nar.parts(RLBooster.class).toList();
//            if (!b.isEmpty()) {
//                SpaceGraph.window(
//                    new Gridding(b.stream().map(b1 -> NARui.rlbooster(b1)))
//                    , 800, 800);
//            }
//        });
    }

    var clock: Time = MS()

    /** TODO automatically compute this based on the maximum framerate of any game, up to a system hard limit (ex: 100 fps)  */
    fun fps(fps: Float): Player {
        this.fps = fps
        clock = MS().durFPS(
            fps //fps/2 //nyquist
                .toDouble())
        return this
    }

    private fun nar(): NAR {
        val exe: Exec = if (threads == 1) UniExec() else object : WorkerExec(threads, exeAffinity) {
            override fun reaction(n: NAR): ReactionModel {
                return deriver().compile(n)
                //nn.runLater(()-> window(new ObjectSurface(m, 2, NAR_UI(nn)), 600, 600) );
            }
        }
        val n = NARS()
            .exe(exe)
            .time(clock)
            .memory(memory())
            .get()
        if (threads == 1) {
            //Deriver d = new BagDeriver(deriver(), n);
            val d: Deriver = MixDeriver(deriver(), n)
            //HACK TODO
            n.onDur { N: NAR -> for (f in N.focus) d.next(f.id) }
        }
        if (iterTune) {
            val subCycles = 1.0 //TODO adjust dynamically to min # of focuses per threads
            BagDeriver.tune({ n.loop.periodNS() / subCycles }, { _iter: Float ->
                val iter = Math.round(_iter)

                //HACK
                nar!!.parts<BagDeriver>(BagDeriver::class.java)
                    .forEach({ deriver: BagDeriver -> deriver.iter = iter }
                    )
            })
        }
        n.main().pri(0f, 0f)
        n.focus.remove(n.main()) //HACK disables default context
        return n
    }

    /** TODO parameters  */
    private fun memory(): Memory {
        return memoryHijackAtomCompoundSplit(1, 128)

        //return memoryHijackTier();

        //return memoryCaffeine();

        //return memoryCaffeineSoft();


//        int largeThresh = Math.max(1,Util.sqrtInt(volMax));
        //				ramGB >= 0.75 ?
        //new HijackMemory((int) Math.round(ramGB * 64 * 1024), 3)

//					CaffeineMemory.soft()
    }

    private fun memoryCaffeine(): CaffeineMemory {
        val concepts = Math.round(ramGB * 10 * 1024).toInt()
        //return new CaffeineMemory(concepts);
        val averageVolume = volMax / 2
        return CaffeineMemory(CaffeineMemory.VolumeWeigher as Weigher<Term, Concept>,
            (concepts * averageVolume).toLong())
    }

    /** dangerous, can lose hardwired concepts briefly during temporary eviction  */
    private fun memoryCaffeineSoft(): CaffeineMemory {
        return CaffeineMemory.soft()
    }

    /** TODO support changing volume  */
    private fun memoryHijackTier(): TierMemory {
        val reprobes = 7
        val chunkSize = 3
        val chunks = Math.ceil(((volMax - 1).toFloat() / chunkSize).toDouble()).toInt()


        return TierMemory({ k: Term ->
                val kv = k.volume()
                if (kv == 1) 0 else (kv - 1) / chunkSize
            }, *Array<Memory>(chunks + 1) { i ->
                when (i) {
                    0 -> HijackMemory(Math.round(ramGB * 2 * 1024).toInt(), Math.max(3, reprobes / 2))
                    else -> {
                        //TODO optional complexity curve
                        HijackMemory(Math.round((if (conceptsMax <= 0) ramGB * 32 * 1024 else conceptsMax) / chunks.toDouble())
                            .toInt(), reprobes)
                    }
                }
            }
        )
    }

    private fun memoryHijackAtomCompoundSplit(atomCap: Int, compoundCap: Int): TierMemory {
        return TierMemory(
            { k: Term? -> if (k is Atomic) 0 else 1 },
            HijackMemory(Math.round(ramGB * atomCap * 1024).toInt(), 5),
            HijackMemory(Math.round(if (conceptsMax <= 0) ramGB * compoundCap * 1024 else conceptsMax).toInt(), 13)
        )
    }

    private fun memoryHybridTier(thresh: Int): TierMemory {
        return TierMemory(
            { k: Term -> if (k.volume() < thresh) 0 else 1 },
            HijackMemory(Math.round(if (conceptsMax <= 0) ramGB * 32 * 1024 else conceptsMax).toInt(), 4),
            CaffeineMemory.soft()
        )
    }

    //	public void addClock(NAR n) {
    //
    //		Atom FRAME = Atomic.atom("frame");
    //
    //		n.parts(Game.class).forEach(g -> g.onFrame(() -> {
    //			long now = n.time();
    //			int X = g.iterationCount();
    //			int radix = 16;
    //			Term x = $.pRecurse(false, $.radixArray(X, radix, Integer.MAX_VALUE));
    //
    //			Term f = $.funcImg(FRAME, g.id, x);
    //			Task t = new SerialTask(f, BELIEF, $.t(1f, n.confDefault(BELIEF)),
    //				now, Math.round(now + g.durLoop()),
    //				new long[]{n.time.nextStamp()});
    //			t.pri(n.priDefault(BELIEF) * g.what().pri());
    //
    //			g.what().accept(t);
    //		}));
    //	}
    //	public void addFuelInjection(NAR n) {
    //		n.parts(What.class).filter(w -> w.inBuffer instanceof PriBuffer.BagTaskBuffer).map(w -> (PriBuffer.BagTaskBuffer) w.inBuffer).forEach(b -> {
    //			MiniPID pid = new MiniPID(0.007f, 0.005, 0.0025, 0);
    //			pid.outLimit(0, 1);
    //			pid.setOutMomentum(0.1);
    //			float ideal = 0.5f;
    //			n.onDur(() -> b.valve.set(pid.out(ideal - b.load(), 0)));
    //		});
    //
    //	}
    fun deriver(): Reactions {
        val d = Derivers()
        if (nal) {
            //d = Derivers.nal(nalMin, nalMax);
            if (nalStructural) d.structural()
            if (nalProcedural) d.procedural()
            if (nalSets) d.sets()
            d.core(true, false)
        }
        val a = ActionTiming()
        if (stmLinker) {
            d.add(STMLinker(1,
                true,
                true,
                false,
                false) /*.taskVol(3, 9)*/ //                .isNotAny(PremiseTask, IMPL).isNotAny(PremiseBelief, IMPL)
            )
            //            d.add(new STMLinker(1, false, false, true, true));
        }
        if (conjClusterBelief) d.add(CompoundClustering(Op.BELIEF, Op.BELIEF, 8, 128))
        if (conjClusterGoal) d.add(CompoundClustering(Op.GOAL, Op.GOAL, 8, 128))
        if (taskResolveAction) d.add(TaskResolve(a, TaskResolver.AnyTaskResolver))
        if (beliefResolveAction) d.add(BeliefResolve(true, true, true, true, a,
            TaskResolver.AnyTaskResolver)) //extra belief resolver for present focus
        if (motivation) d.files("motivation.nal")
        if (xor) d.files("xor.nal")
        if (goalSpread) {
            val punc = PuncBag(0f, 0f, 1 / 8f, 1 / 16f)
            val l = LinkFlow( //LinkFlows.fromEqualsFrom,
                LinkFlows.fromEqualsFromOrTo,
                { x: TaskLink ->
                    val f: FloatArray = x.priGet(punc)
                    f
                },  //PriMerge.max
                PriMerge.plus //WARNING: unstable
            )
            l.spread.set(2)
            d.add(l)

            //d.add(new LinkFlow(0f, 0, 0.1f, 0f, LinkFlows.edgeToEdge).neq(TheTask,TheBelief));

            //TODO impl->impl link creation from results of adjacents

            //d.add(new xxTermLinking.PremiseTermLinking(new CachedAdjacenctTerms(EqualTangent.the, false)));
        }
        if (ifThenElse) d.files("if.nal")
        if (goalInduction) d.files("induction.goal.nal")
        if (inperience) {
            val tl = 0
            //1;
            d.addAll( //new Inperience.BeliefInperience(BELIEF, tl).timelessOnly().hasNot(PremiseTask, IMPL,CONJ),
                BeliefInperience(Op.GOAL, tl).timelessOnly().hasNot(NativeReaction.PremiseTask, Op.IMPL, Op.CONJ),
                QuestionInperience(Op.QUESTION).timelessOnly().hasNot(NativeReaction.PremiseTask, Op.IMPL, Op.CONJ),
                QuestionInperience(Op.QUEST).timelessOnly().hasNot(NativeReaction.PremiseTask, Op.IMPL, Op.CONJ)
            )
        }
        if (globalAdjacency) d.add(TermLinking.PremiseTermLinking(CachedAdjacenctTerms(EqualTangent.the, true)))
        if (explode) d.addAll(ExplodeAction())

//        if (eternalizeImpl)
//            d.addAll(new EternalizeAction()
//                    .isAny(TheTask, Op.or(IMPL))
//            );
        if (deltaIntro) {
            //Δ
            d.files("delta.nal")
            //d.files("delta.more.nal");
            if (deltaGoal) d.files("delta.goal.nal")
        }
        if (implBeliefify) {
            d.addAll(
                ImplTaskifyAction(true, true),
                ImplTaskifyAction(false, true))
        }
        if (implGoalify) {
            d.addAll(
                ImplTaskifyAction(true, false),
                ImplTaskifyAction(false, false))
        }
        if (arith) d.add(Arithmeticize.ArithmeticIntroduction())
        if (factorize) d.add(Factorize.FactorIntroduction())


//        if (answerLogging) {
//            //.log(true).apply(false)
//
//            //d.add(new AnswerQuestionsFromBeliefTable(a, true, true)); //extra belief resolver for present focus
//            d.add(new AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(new PresentFocus())
//                    //.log(true).apply(true)
//            );
//
//        }
        if (answerQuestionsFromTaskLinks) {
            d.add(AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(PresentTiming()).taskVol(2, 16))
            //d.add(new AnswerQuestionsFromBeliefTable(a, true, false)) //extra belief resolver for present focus
            //d.add(new TermLinking.PremiseTermLinking(new FirstOrderIndexer())) //<- slow
        }
        if (abbreviation) {
            d.addAll(
                Abbreviate.AbbreviateRecursive(9, 16),
                Abbreviate.UnabbreviateRecursive())
            //TODO re-abbreviate: abbreviate unabbreviated instances terms recursively which also appear in the term abbreviated
//                //.add(new Abbreviation.AbbreviateRoot(4, 9))
//                //.add(new Abbreviation.UnabbreviateRoot())
        }
        return d
    }

    fun <C> the(c: Class<C>?): Stream<C> = nar!!.parts(c)

    fun delete() {
        nar?.runLater {
            stop()
            nar?.delete()
            nar = null //changes lateinit nars to require !! everywhere
        }
    }

    fun rewardMean(): Double {
        return nar!!.parts(Game::class.java).filter { g: Game? -> g !is MetaGame }
            .mapToDouble({ obj: Game -> obj.rewardMean() }).average().asDouble
    }

    companion object {
        val ramGB = Runtime.getRuntime().maxMemory() / (1024 * 1024 * 1024.0)
    }
}