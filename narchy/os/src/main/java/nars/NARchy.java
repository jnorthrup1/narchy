package nars;

import nars.derive.reaction.ReactionModel;
import nars.exe.impl.WorkerExec;
import nars.func.language.NARHear;
import nars.memory.CaffeineMemory;
import nars.time.clock.RealTime;

public class NARchy extends NARS {

    //static final Logger logger = LoggerFactory.getLogger(NARchy.class);

    public static NAR core() {
        return core(Runtime.getRuntime().availableProcessors());
    }

    public static NAR core(int threads) {

        NAR nar = new NARS()
                .memory(CaffeineMemory.soft())
                //.index(new HijackConceptIndex(32*1024, 4))
                .exe(new WorkerExec(threads, false) {
                    @Override protected ReactionModel reaction(NAR n) {
                        return Derivers.nal(1, 8, "motivation.nal").core(true, true).compile(n);
                    }
                })
                .time(new RealTime.MS().durFPS(10f))
                .get();

        nar.dtDither.set(20);

		//new Arithmeticize.ArithmeticIntroduction(nar, );

        return nar;
    }

    public static NAR ui() {
        /** TODO differentiate this from UI, for use in embeddeds/servers without GUI */
        NAR nar = core();
        nar.exe.throttle(0.1f);
        
        nar.runLater(()->{

            //User u = User.the();

            NARHear.readURL(nar);

//            {
//                NARSpeak s = new NARSpeak(nar);
//                s.spoken.on(  s::speak);
//
//            }

//            InterNAR i = new InterNAR(nar);
//            i.fps(2);


        });

        return nar;
    }


}