package nars.gui;

import jcog.data.list.Lst;
import jcog.deep.Autoencoder;
import jcog.nn.MLP;
import jcog.nn.layer.AbstractLayer;
import jcog.nn.layer.AutoEncoderLayer;
import jcog.nn.layer.DenseLayer;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.EmptySurface;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.widget.meter.BitmapMatrixView;

import java.util.List;

public class MLPView extends Gridding implements Runnable {
    private final MLP m;
    private List<Surface> layers;

    public MLPView(MLP m) {
        aspect(VERTICAL);
        this.m = m;
    }

    @Override
    public void run() {
        if (m == null) return;
        var layers = m.layers;
        if (this.layers == null && layers!=null) {
            this.layers = new Lst(layers.length);
            for (int i = 0; i < layers.length; i++) {
                final AbstractLayer li = layers[i];
                if (li instanceof DenseLayer)
                    this.layers.add((new MLPLayerView((DenseLayer) li, false /*i == layers.length-1*/)));
                else if (li instanceof AutoEncoderLayer)
                    this.layers.add((new LSTMView.AEView((Autoencoder) ((AutoEncoderLayer)li).ae)));
            }
            set(this.layers);
        }
        if (this.layers!=null) {
            for (Surface l : this.layers)
                if (l instanceof Runnable)
                    ((Runnable)l).run();
        }
    }

    static class MLPLayerView extends Gridding implements Runnable {

        MLPLayerView(DenseLayer l, boolean showOutput) {
            super(
                    new BitmapMatrixView(l.in),
                    new BitmapMatrixView(l.W),
                    //new BitmapMatrixView(l.delta, Draw::colorBipolar),
//                    new BitmapMatrixView(l.dW, Draw::colorBipolar),
                    showOutput ? new BitmapMatrixView(l.out) : new EmptySurface() //HACK
            );
        }

        public void run() {
            forEach(x -> {
                if (x instanceof BitmapMatrixView)
                    ((BitmapMatrixView)x).updateIfShowing();
            });
        }

    }
}