package nars.gui;

import jcog.data.graph.Node;
import nars.NAR;
import nars.concept.Concept;
import nars.derive.op.DeriverFork;
import nars.derive.reaction.ReactionModel;
import nars.focus.PriNode;
import nars.game.Game;
import nars.game.sensor.VectorSensor;
import nars.gui.concept.ConceptSurface;
import nars.gui.sensor.VectorSensorChart;
import nars.term.control.AND;
import nars.term.control.FORK;
import nars.term.control.PREDICATE;
import org.eclipse.collections.api.tuple.Pair;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.graph.EdgeVis;
import spacegraph.space2d.container.graph.NodeVis;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.meta.ObjectGraphs;
import spacegraph.space2d.widget.text.VectorLabel;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class AttentionUI {


//    static class NodeUI extends Gridding {
//        public final PriNode node;
//
//        NodeUI(PriNode node) {
//            this.node = node;
//            add(new VectorLabel(node.toString()));
//            add();
//        }
//    }

//    public static GraphEdit serviceGraph(NAR n, NAgent a) {
//        GraphEdit<Surface> g = GraphEdit.window(800, 500);
//        g.add(new ExpandingChip(a.toString(), (x)->{
//            new ObjectSurface(a).forEach
//        }));
//        return g;
//    }
//
//    public static GraphEdit2D graphGraph(NAR n) {
//        GraphEdit2D g = new GraphEdit2D();
//        g.resize(800, 800);
//        //g.windoSizeMinRel(0.02f, 0.02f);
//
//        Exe.runLater(() -> {
////		g.add(NARui.game(a)).posRel(0.5f, 0.5f, 0.4f, 0.3f);
//            //g.add(NARui.top(n)).posRel(0.5f, 0.5f, 0.2f, 0.1f);
//            g.add(FocusUI.focusUI(n)).resize(400.25f, 400.25f);
//        });
//        return g;
//    }

    public static Surface objectGraphs(Object x, NAR n) {
        return objectGraphs(List.of(x), n);
    }

    public static Surface objectGraphs(PriNode x, NAR n) {
        return objectGraphs(() -> x.dfs(n.pri.graph).iterator(), n);
    }

    public static Map<Class, BiFunction<?, Object, Surface>> NAR_UI(NAR n) {
        return Map.of(
                NAR.class, (NAR nn, Object rel) -> new VectorLabel(nn.self().toString()),
                VectorSensor.class, (VectorSensor v, Object rel) -> new VectorSensorChart(v, n),
                //GameLoop.class, (GameLoop g, Object relation) -> g.components()
                DurSurface.class, (x, y) -> null,
                Concept.class, (Concept c, Object rel) -> new ConceptSurface(c, n),
                PriNode.class, (PriNode v, Object rel) -> new ConceptSurface(v.id.term(), n),
                ReactionModel.class, (ReactionModel m, Object rel) -> new Gridding(
                        Stream.of(m.how).map(h ->
                                //new ObjectSurface(h, 2)
                                new PushButton(h.term().toString())
                        )//.collect(toList())
                )
//                How.class, (How h, Object rel) -> {
//                    return new PushButton(h.term().toString());
//                }
        );
    }

    /** TODO visualize the effective multiplicity of VectorSensor's */
    @Deprecated public static Surface objectGraphs(Iterable o, NAR n) {

        ObjectGraphs g = new ObjectGraphs(o,
                NAR_UI(n)
                //TODO nars specific renderers
                , (xx, graph) -> {
            Object x = xx.id;


            final float minSize = 0.01f;

            if (x instanceof PriNode) {
                xx.pri = minSize + ((PriNode) x).pri();
            } else
                xx.pri = minSize;

//				if (x instanceof Prioritized) {
//					float s = ((Prioritized) x).priElseZero();
//					xx.pri = s;
//					//((Widget)xx.the()).color.set(Math.min(s, 1), Math.min(s, 1), 0, 1);
//				} else {
//					//..infer priority of unprioitized items, ex: by graph metrics in relation to prioritized
//				}


//				if (xx.id instanceof PriNode) {
//					EdgeVis<Object> eID = graph.edge(xx, ((PriNode)(xx.id)).id);
//					if (eID!=null)
//						eID.weight(1f).color(0.2f, 0.2f, 0.5f);
//				}

            Node<PriNode, Object> nn = n.pri.node(x);
            if (nn != null) {
                xx.pri = nn.id().pri();
                for (Node<PriNode, Object> c : nn.nodes(false, true)) {
                    EdgeVis<Object> e = graph.edge(xx, c.id());
                    if (e != null)
                        e.weight(0.25f).color(0.5f, 0.5f, 0.5f);
                }
            }
            if (x instanceof Game) {
                EdgeVis<Object> e = graph.edge(x, ((Game) x).focus().pri);
                if (e != null)
                    e.weight(0.25f).color(0.5f, 0.5f, 0.5f);
            }

            if (x instanceof Pair) {
                //HACK dereference
                x = ((Pair) x).getTwo();
            }
//            Object XX = X;
            if (x instanceof AND) {
                NodeVis[] prev = {xx};
                for (PREDICATE s : ((AND<?>) x).conditions()) {
                    NodeVis S = graph.nodeOrAdd(s);
                    graph.edge(prev[0], S).weight(1f).color(0.5f, 0.5f, 0.5f);
                    prev[0] = S; //link to prev
                }
            } else if (x instanceof FORK) {
                ((FORK) x).forEach(s -> {
                    NodeVis S = graph.nodeOrAdd(s);
                    graph.edge(xx, S).weight(1f).color(0.5f, 0.5f, 0.5f);
                });
            } else if (x instanceof DeriverFork) {
                for (PREDICATE s : ((DeriverFork) x).branches()) {
                    NodeVis S = graph.nodeOrAdd(s);
                    graph.edge(xx, S).weight(1f).color(0.5f, 0.5f, 0.5f);
                }
            }
        });

//        g.update();
//        return g;

        return DurSurface.get(g, g::update, n).every(4);
    }


}