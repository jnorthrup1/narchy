# NARchy
NARchy is a derivative of OpenNARS (Non-Axiomatic Reasoning System, a general-purpose experience-driven logic processor).
* Beliefs
* Goals
* Questions (and Quests)

## real-time performance and precision
for use in robotics and other applications interfacing with reality

## functors and evaluation
term reductions, prolog-like inline functor evaluation and variable binding

## sealed focus 
except when concepts are otherwise shared, focus bags do not leak into each other.  this supports memory management and staged pipelining of processes like a laboratory of reaction chambers..  termlink bags, and other plugins are generally focus-local by default and thus also maintain the 'closure'.

## parallel
* Multithreaded focus sampling.
* Bag implementations: ArrayBag and HijackBag
* TemporalBeliefTable
    * merge/compresson heuristics
* SeriesBeliefTable

## balanced truth extrema
freq(0) = "never" = not "always"
freq(1) = "always" = not "never"
User ultimately defines the meaning of "always" or "never".

## memory efficient
*Strategies for deduplication of terms, tasks, concepts, memoized functions, etc..
* conj InhBundling and factoring
* dynamic truth models - results optionally cacheable

## temporal sequence term encoding
temporally precise sequence reasoning, with "intermpolation".

## reinforcement learning API
sensors + actions + rewards
* for fluent, declarative implementations to control reality, or simulations (Games) of it
* mix in[stinct] or kickstart with "classical" RL ex: DQN
* adjustable training strength allows a unique reverse-mode operation for using RL like a sponge to absorb reasoner activity

## prediction and forecasting API
time-series forecasting with Predictor's and differentiable DeltaPredictors
from/to any sets of concepts from/to any time(s).

## metagame homonculus
for maximizing system meta-goals, ex: dexterity (conf in action), while minimizing system costs (ex: cpu usage)

## plugin API
see "Part", "Parts" classes

## reflection API
proxy instrumentation and scaffolding for reasoning about and controlling other JVM programs

## test API
for ensuring expected behavior and measuring performance

## configurability
opportunities for endless experimentation.  contribute your own implementations

## memory transfer
* serialization to/from disk or network, with filtering

## InterNARchy
* Decentralized mesh network