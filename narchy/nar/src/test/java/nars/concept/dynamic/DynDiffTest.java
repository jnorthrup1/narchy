package nars.concept.dynamic;

import nars.Narsese;
import nars.Term;
import nars.table.BeliefTables;
import nars.table.dynamic.DynTruthBeliefTable;
import nars.task.NALTask;
import org.junit.jupiter.api.Test;

import static nars.$.$;
import static nars.$.$$;
import static org.junit.jupiter.api.Assertions.*;

class DynDiffTest extends AbstractDynTaskTest {

//    @Test
//    void testRawDifference() throws Narsese.NarseseException {
//        
//        n.believe("x", 0.75f, 0.50f);
//        n.believe("y", 0.25f, 0.50f);
//        n.run(1);
//        Term xMinY = $("(x ~ y)");
//        Term yMinX = $("(y ~ x)");
//        assertDynamicTable(xMinY);
//        assertDynamicTable(yMinX);
//        assertEquals(
//                "%.56;.25%", n.beliefTruth(xMinY, n.time()).toString()
//        );
//        assertEquals(
//                "%.06;.25%", n.beliefTruth(yMinX, n.time()).toString()
//        );
//
//    }

    @Test void DiffUnion() throws Narsese.NarseseException {
        
        n.believe("c:x", 0.75f, 0.50f);
        n.believe("c:y", 0.25f, 0.50f);
        n.run(1);
        //Term xMinY = $("c:(x - y)"), yMinX = $("c:(y - x)");
        Term xMinY = $("c:(x & --y)"), yMinX = $("c:(y & --x)");

        assertNotNull(((BeliefTables) n.conceptualize(xMinY).beliefs()).tableFirst(DynTruthBeliefTable.class));
        assertNotNull(((BeliefTables) n.conceptualize(yMinX).beliefs()).tableFirst(DynTruthBeliefTable.class));
        assertEquals(
                "%.06;.25%", n.beliefTruth(yMinX, n.time()).toString()
        );
        assertEquals(
                "%.56;.25%", n.beliefTruth(xMinY, n.time()).toString()
        );
    }
    @Test void DiffIntersection() throws Narsese.NarseseException {
        
        n.believe("c:x", 0.75f, 0.50f);
        n.believe("c:y", 0.25f, 0.50f);
        n.run(1);
        Term xMinY = $("c:(x - y)"), yMinX = $("c:(y - x)");
        assertNotNull(((BeliefTables) n.conceptualize(xMinY).beliefs()).tableFirst(DynTruthBeliefTable.class));
        assertNotNull(((BeliefTables) n.conceptualize(yMinX).beliefs()).tableFirst(DynTruthBeliefTable.class));
        assertEquals(
                "%.56;.25%", n.beliefTruth(xMinY, n.time()).toString()
        );
        assertEquals(
                "%.06;.25%", n.beliefTruth(yMinX, n.time()).toString()
        );
    }

    @Test
    void testEviDilution() {
        
        n.believe("c:x", 0.75f, 0.50f, 0, 0);
        n.believe("c:y", 0.25f, 0.50f, 1, 1);
        n.believe("c:z", 0.25f, 0.50f, 2, 2);

        Term aa = $$("c:(x & --y)");
        NALTask a = n.answerBelief(aa, 0, 1);
        assertNotNull(a);
        assertEquals(aa, a.term());
        assertEquals(0, a.start());
        assertEquals(1, a.end());
        assertEquals(2, a.range());
        assertTrue(a.conf() < /*Util.sqr(0.5f)* */0.9f);

        Term bb = $$("c:(x & --z)");
        NALTask b = n.answerBelief(bb, 0, 2);
        assertNotNull(b);
        assertEquals(bb, b.term());
        assertEquals(3, b.range());

        assertEquals(b.freq(), a.freq());
        assertTrue((float) b.conf() < (float) a.conf());
        //assertTrue(b.pri() < a.pri());

//        assertEquals("", a.toStringWithoutBudget());
//        assertEquals("", b.toStringWithoutBudget());


    }
}