package nars.nal.nal7;

import nars.NAR;
import nars.NARS;
import nars.test.NALTest;
import org.junit.jupiter.api.BeforeEach;

public class AbstractNAL7Test extends NALTest {

    @Deprecated public static final float CONF_TOLERANCE_FOR_PROJECTIONS = 2; //200%
    protected static final int cycles = 40;

    @Override
    protected NAR nar() {
        return NARS.tmp(6, 8);
    }

    @BeforeEach
    void setTolerance() {
        test.volMax(11);
        test.confMin(0.3f);

        test.freqTolerance(0.1f);
        test.freqRes(0.1f);
        test.confTolerance(CONF_TOLERANCE_FOR_PROJECTIONS);
        test.nar.confResolution.set(0.05f);
    }

}