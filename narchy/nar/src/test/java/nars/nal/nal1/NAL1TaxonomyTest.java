package nars.nal.nal1;

import nars.NAR;
import nars.NARS;
import nars.Narsese;
import nars.task.NALTask;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/** tests inheritance-based taxonomical inference */
public class NAL1TaxonomyTest {

    @Disabled
    @Test
    void testCommonAncestor() throws Narsese.NarseseException {
//        NAL.DEBUG = true;
//        NAL.causeCapacity.set(4);
        NAR n = NARS.tmp(1);
        n.believe("(a-->b)")
         .believe("(b-->C)")
         .believe("(y-->C)")
         .believe("(z-->y)")
        ;
        //n.log();
        n.main().onTask(t -> n.proofPrint((NALTask)t));
        n.run(1000);

    }
}