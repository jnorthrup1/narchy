package nars.func;

import jcog.random.RandomBits;
import jcog.random.XoRoShiRo128PlusRandom;
import nars.$;
import nars.Term;
import nars.action.transform.Arithmeticize;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;
import static nars.$.$$;
import static org.junit.jupiter.api.Assertions.*;

class ArithmeticizeTest {

    @Test
    void scalar1() {
        assertArith("(2,3)",
                "(#_2,add(#_2,1))", "(add(#_3,-1),#_3)",
                //"(2,add(1,2))", "(add(-1,3),3)",
                //"((#1,add(#1,1))&&equal(#1,2))",
                 "((cmp(#1,#2)=-1)&&(#1,#2))"
        );
    }

    @Test
    void scalar_cmp() {
        assertArith("x(2,3)", "((cmp(#1,#2)=-1)&&x(#1,#2))",
                "x(#_2,add(#_2,1))", "x(add(#_3,-1),#_3)");
    }

    @Test
    void scalar3() {
        assertArith("(x(2,3) ==> y)",
                "(x(add(#1,-1),#1)==>y)", "(x(#1,add(#1,1))==>y)",
                "((x(#1,#2)==>y)&&(cmp(#1,#2)=-1))");
    }

    @Test
    void scalar_negate() {
        assertArith("(2 ==> -2)",
                "(#1==>add(#1,-4))",
                //"(add(#_-2,4)==>#_-2)",
                "(#1==>mul(#1,-1))", //"(mul(#_-2,-1)==>#_-2)",
                "((cmp(#1,#2)=-1)&&(#2==>#1))"
        );
    }
    @Test
    void vector_2d_1() {
        assertArith("((2,3) ==> (3,4))",
                //"((2,3)==>add((2,3),#1))&&equal(#1,(1,1)))"
                //"[(((2,3)==>add((2,3),#1))&&equal((1,1),#1)), ((add((3,4),#1)==>(3,4))&&equal((-1,-1),#1))]"
                "((#_2,#_3)==>add((1,1),(#_2,#_3)))", "(add((-1,-1),(#_3,#_4))==>(#_3,#_4))"
        );
    }
    @Test
    void vector_scale_1() {
        assertArith("(2,2,2)",
                //"((2,3)==>add((2,3),#1))&&equal(#1,(1,1)))"
                //"[(((2,3)==>add((2,3),#1))&&equal((1,1),#1)), ((add((3,4),#1)==>(3,4))&&equal((-1,-1),#1))]"
                "mul((2,2,2),2)"
        );
    }

    @Test
    void vector_scale_2() {
        assertArith("((1,2) ==> (2,4))",
                //"((2,3)==>add((2,3),#1))&&equal(#1,(1,1)))"
                //"[(((2,3)==>add((2,3),#1))&&equal((1,1),#1)), ((add((3,4),#1)==>(3,4))&&equal((-1,-1),#1))]"
                "((#_1,#_2)==>mul((#_1,#_2),#_2))", "(add((-1,-2),(#_2,#_4))==>(#_2,#_4))"
        );
    }


    static void assertArith(String q, String... p) {
        Random rng = new XoRoShiRo128PlusRandom(1);
        TreeSet<Term> expect = Stream.of(p).map($::$$$).collect(toCollection(TreeSet::new));
        Set<Term> actual = new TreeSet();
        Term Q = $$(q);
        int bound = p.length * 8;
        RandomBits r = new RandomBits(rng);
        for (int i = 0; i < bound; i++) {
            Term aa = Arithmeticize.apply(Q, r);
            assertNotNull(aa);
            actual.add(aa.normalize());
        }

        assertEquals(expect, actual);
    }
    public static void assertArithmetic(String q, String y) {
        Random rng = new XoRoShiRo128PlusRandom(1);
        Set<String> solutions = new TreeSet();
        Term Q = $$(q);
        for (int i = 0; i < 10; i++) {
            Term s = Arithmeticize.apply(Q, new RandomBits(rng));
            if (s != null) {
                solutions.add(s.toString());
            } else {
                assertNull(y);
                return;
            }
        }
        assertEquals(y, solutions.toString());
    }
}