package nars.exe;

import jcog.exe.InstrumentedLoop;
import jcog.exe.realtime.AbstractTimer;
import jcog.signal.FloatRange;
import nars.NAR;

/**
 * self managed set of processes which run a NAR
 * as a loop at a certain frequency.
 */
public final class NARLoop extends InstrumentedLoop {

    public final transient NAR nar;

    public final FloatRange throttle = new FloatRange(1.0f, 0.0f, 1.0f);
    private boolean async = false;

    /**
     * starts paused; thread is not automatically created
     */
    public NARLoop(NAR n) {
        super();
        nar = n;
    }

    public final float throttle() {
        return isRunning() ? throttle.floatValue() : 0;
    }

	@FunctionalInterface
    public interface Pausing {
        void pause(boolean pause);
    }

    @Override
    protected final void _execute(AbstractTimer ignored) {
        nar.exe.execute(this);
    }

    @Override
    protected void starting() {
        super.starting();
        nar.parts(Pausing.class).forEach(g->g.pause(false));
    }

    @Override
    protected void stopping() {
        nar.parts(Pausing.class).forEach(g->g.pause(true));
        super.stopping();
    }
    @Override
    public void ready() {
        phase.set(async ? PENDING : SCHEDULED);
    }

    final Runnable ready = this::ready;

    @Override
    public final boolean next() {
        NAR n = nar;
//        if (n.exe.isOff())
//            return false;

        n.time.next();
        if (async)
            n.eventCycle.emitAsync(n, n.exe, ready);
        else
            n.eventCycle.emit(n);

        return true;
    }

    @Override
    protected boolean async() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
        ready();
    }

}