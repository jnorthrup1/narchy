package nars.exe;

import jcog.data.iterator.ArrayIterator;
import jcog.exe.Loop;
import nars.NAR;
import nars.NARPart;
import nars.time.ScheduledTask;
import org.jctools.queues.MpscArrayQueue;

import java.io.IOException;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * manages low level task scheduling and execution
 */
public abstract class Exec extends NARPart implements Executor, Consumer {



    private static final int TIME_QUEUE_CAPACITY = 2 * 1024;
    final MpscArrayQueue<ScheduledTask> preSchedule = new MpscArrayQueue<>(TIME_QUEUE_CAPACITY);
    final PriorityQueue<ScheduledTask> scheduled = new PriorityQueue<>(TIME_QUEUE_CAPACITY /* estimate capacity */);

    /**
     * maximum possible concurrency; should remain constant
     */
    protected final int concurrencyMax;


    protected Exec(int concurrencyMax) {
        this.concurrencyMax = concurrencyMax; //TODO this will be a value like Runtime.getRuntime().availableProcessors() when concurrency can be adjusted dynamically

    }

    private void executeNow(Consumer<NAR> t) {
        t.accept(nar);
    }

    private static void executeNow(Runnable t) {
        t.run();
    }

    /**
     * execute now.
     * inline, synchronous
     */
    @Override public final void accept(Object runnable) {
//        try {
            if (runnable instanceof Consumer)
                executeNow((Consumer<NAR>)runnable);
            else
                executeNow((Runnable) runnable);
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        }
    }

    public final void next() {
        /*
         * drain scheduled tasks ready to be executed
         */
        schedulePre(true);

        scheduleNow();

    }

    protected abstract void executeSoon(ScheduledTask scheduledTask);

    public void print(Appendable out) {
        try {
            out.append(this.toString());
            out.append('\n');
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void synch() {

    }


    public void clear() {
        synchronized (scheduled) {
            //schedule(this::executeNow);
            preSchedule.clear();
            scheduled.clear();
        }
    }



    public final Stream<ScheduledTask> events() {
        ScheduledTask[] s;
        synchronized (scheduled) {
            s = scheduled.toArray(ScheduledTask[]::new);
        }
        return //Stream.concat(
                //toSchedule.stream(),
                ArrayIterator.stream(s) //a copy
        //)
        ;
    }


    public final void runAt(ScheduledTask event) {
        if (!preSchedule.offer(event)) {
            //check if it is already time to execute it
            if (event.next <= nar.time())
                executeSoon(event); //phew
            else
                throw new RuntimeException("jammed: " + event);
        }
    }


    private void scheduleNow() {
        long now = nar.time(); //update
        ScheduledTask t;
        while (((t = scheduled.peek()) != null) && (t.next <= now)) {
            ScheduledTask s = scheduled.poll(); //assert(s==t);
            executeSoon(s);
        }
    }

    /** @param fifo: execution of ready tasks
     *      true = fair time ordering, false = execute greedily (first come first serve) */
    private void schedulePre(boolean fifo) {
        long _now = fifo ? Long.MIN_VALUE : nar.time();
        preSchedule.drain(x -> {
            if (!fifo && x.next <= _now)  {
                //non-fifo greedy invocation
                executeSoon(x);
                //elides scheduled.offer
            } else {
                if (!scheduled.offer(x))
                    throw new RuntimeException("scheduled priority queue overflow");
            }
        });
    }


    public void throttle(float t) {
        nar.loop.throttle.set(t);
        Loop.logger.info("{} throttle={}", nar.self(), t);
    }


}