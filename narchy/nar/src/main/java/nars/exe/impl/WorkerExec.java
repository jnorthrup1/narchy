package nars.exe.impl;

import jcog.Util;
import jcog.data.list.FastCoWList;
import jcog.data.list.MetalConcurrentQueue;
import jcog.random.RandomBits;
import jcog.random.XoRoShiRo128PlusRandom;
import nars.NAR;
import nars.derive.Deriver;
import nars.derive.impl.MixDeriver;
import nars.derive.reaction.ReactionModel;
import nars.focus.Focus;
import nars.focus.util.FocusBag;
import nars.time.part.DurLoop;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.lang.System.nanoTime;
import static java.lang.invoke.MethodHandles.insertArguments;
import static java.lang.invoke.MethodHandles.lookup;
import static java.lang.invoke.MethodType.methodType;

public abstract class WorkerExec extends ThreadedExec {

//    static final int initial_throttle = 0;
    private static final int YAWN_MS = 5;

    public final FastCoWList<WorkPlayLoop> loops = new FastCoWList<>(WorkPlayLoop.class);
    private static final Object MY_DERIVER = new Object();
    private ReactionModel reaction;

    private transient long periodNS = 0;

    /**
     * ideal cycle end time
     */
    private DurLoop scheduling;

    public WorkerExec(int threads) {
        this(threads, false);
    }

    public WorkerExec(int threads, boolean affinity) {
        super(threads, affinity);
        sampleQueue = new MetalConcurrentQueue<>(concurrencyMax * 3);
    }

    @Override
    public void starting(NAR n) {
//        n.loop.throttle.set(initial_throttle);

        this.reaction = reaction(n);

        super.starting(n);

        scheduling = n.onDur(this::schedule);
    }

    @Override
    protected void stopping(NAR nar) {
        exe.exceptionRespawn = false;
        loops.forEach(WorkPlayLoop::close);
        super.stopping(nar);
    }

    protected abstract ReactionModel reaction(NAR n);

    @Override
    protected Supplier<Worker> worker() {
        return () -> new WorkPlayLoop(this::deriver);
    }

    private void schedule() {

        periodNS = nar.loop.periodNS();

        focusBufferClear();

        WorkPlayLoop[] loop = loops.array();
        int n = loop.length;

        float throttle = nar.loop.throttle();
        float runningIdeal = n * throttle;
        int running = (int) runningIdeal;

        //int partialSleep = (throttleSleepRemainder > 0) ? nar.random().nextInt(running) : -1;
        for (int i = 0; i < n; i++) {
            float idle;
            if (i >= running)
                idle = 1; //fully asleep
            else if (i == running-1)
                idle = Math.max(0, runningIdeal - running); //fraction
            else
                idle = 0; //fully awake

            loop[i].idle = idle;
        }
    }

    protected Deriver deriver(Focus f) {
        Deriver d =
                new MixDeriver(reaction, nar);
                //new SimpleDeriver(reaction, nar);
                //new BagDeriver(reaction, nar);
                //new OptiTreeDeriver(reaction, nar);

        return d;
    }

    public final class WorkPlayLoop implements ThreadedExec.Worker {



        private final RandomBits RNG;

        //        private final MethodHandle focusSampler;
//        private final RandomBits random;
        public volatile float idle = 0;

        //FloatAveragedWindow loopTime = new FloatAveragedWindow(32, 0.5f, false).mode(FloatAveragedWindow.Mode.Mean);
        public boolean alive = true;
        private final Function<Focus, Deriver> deriverBuilder;

        WorkPlayLoop(Function<Focus, Deriver> deriverBuilder) {
            this.deriverBuilder = deriverBuilder;
            var rng = new XoRoShiRo128PlusRandom(31L * System.identityHashCode(this) + nanoTime());
            RNG = new RandomBits(rng);

//            this.focusSampler = insertArguments(focusBagSample, 0,
//                    nar.focus, rng).asType(methodType(PLink.class));

            WorkerExec.this.loops.add(this);
        }



        @Override
        public void run() {

            //Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            while (alive) {
                int w = work();

                if (w==0 && RNG.nextBooleanFast8(idle)) {
                    sleep(0);
                } else {
                    play();
//                    if (!play() && w == 0)
//                        yawn();
                }
            }

            stop();

        }


//        private void nextProfiled() {
//            try (var __ = nextProfiled.get()) {
//                next();
//            }
//
//            if (nextProfiled.count() >= profilePrintPeriod)
//                logger.info("{} cycle time {}", WorkerExec.this, nextProfiled.toString(true, false, true, true));
//        }

        private void sleep(float throttle) {
            if (throttle < 1) {
                long sleepNS = Math.round(periodNS * (1.0 - throttle));
                Util.sleepNS(sleepNS);
            }
        }

        private void stop() {
            Thread.currentThread().setUncaughtExceptionHandler((t, e)-> { /* ignore */ });
            loops.remove(this);
        }



        private boolean play() {
//            try {
                Focus w;
                if ((w = focus()) == null)
                    return false;

                w.threadLocal(MY_DERIVER, deriverBuilder).next(w);//invokeExact();
                return true;

//            } catch (Throwable t) {
//                if (!(t instanceof InterruptedException))
//                    logger.error(this.toString(), t);
//                throw new RuntimeException(t);
//            }
        }

//        private void yawn() {
//            //Thread.yield(); //chill out, nothing is happening now
//            Util.sleepMS(YAWN_MS);
//        }

        @Nullable private Focus focus()  {
            return WorkerExec.this.focus(RNG.rng);
        }


        @Override
        public void close() {
            alive = false;
        }

    }

    /** TODO refactor this into a ProxyBag or ProxySampler for a bag which can be optionally used */
    @Deprecated final MetalConcurrentQueue<Focus> sampleQueue;
    @Deprecated final AtomicBoolean sampleQueueBusy = new AtomicBoolean(false);

    private Focus focus(Random rng) {
        return focusBuffered(rng);
        //return focusDirect(rng);
    }

    private Focus focusBuffered(Random rng) {
        Focus f = sampleQueue.poll();
        if (f == null) {
            if (sampleQueueBusy.weakCompareAndSetAcquire(false, true)) {
                try {
                    f = focusBufferFill(rng) ? sampleQueue.poll() : null;
                } finally {
                    sampleQueueBusy.setRelease(false);
                }
            }
        }

        if (f == null)
            f = focusDirect(rng); //last resort, if other thread is busy filling

        return f;
    }

    private void focusBufferClear() {
        if (sampleQueueBusy.compareAndSet(false, true)) {
            try {
                sampleQueue.clear();
            } finally {
                sampleQueueBusy.set(false);
            }
        }
    }
    private boolean focusBufferFill(Random rng) {
        if (nar.focus.isEmpty()) return false;
        nar.focus.sample(rng, sampleQueue.length(), (ff) -> {
            sampleQueue.push(ff.id);
        });
        return true;
    }

    private Focus focusDirect(Random rng) {
        var p = nar.focus.sample(rng);
//            var p = (PLink<Focus>) focusSampler.invokeExact();
        return p != null ? p.id : null;
    }

    private static final MethodType voidFocus = methodType(void.class, Focus.class);
    private static final MethodHandle deriverNext;
    private static final MethodHandle focusBagSample;

    static {
        try {
            deriverNext = MethodHandles.privateLookupIn(Deriver.class, lookup())
                    .findSpecial(Deriver.class, "next", voidFocus, Deriver.class);

            focusBagSample = MethodHandles.privateLookupIn(FocusBag.class, lookup())
                    .findSpecial(FocusBag.class, "sample",
                            methodType(Object.class, Random.class), FocusBag.class);

        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static MethodHandle deriverInvoker(Focus f, Deriver d) {
        return insertArguments(deriverNext, 0, d, f);
    }

}