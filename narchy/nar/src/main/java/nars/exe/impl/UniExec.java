package nars.exe.impl;

import nars.exe.Exec;
import nars.time.ScheduledTask;

import static nars.Op.TIMELESS;

/**
 * single thread executor used for testing
 */
public class UniExec extends Exec {

    public UniExec() {
        this(1);
    }

    protected UniExec(int concurrencyMax) {
        super(concurrencyMax);
    }

    @Override
    protected final void executeSoon(ScheduledTask t) {
        t.next = TIMELESS;
        t.accept(nar);
    }

    @Override
    public void execute(Runnable r) {
        r.run();
    }
}
