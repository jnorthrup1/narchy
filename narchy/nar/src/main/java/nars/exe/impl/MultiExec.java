package nars.exe.impl;

import nars.NAR;
import nars.exe.Exec;
import nars.time.clock.RealTime;

public abstract class MultiExec extends Exec {


    MultiExec(int concurrencyMax  /* TODO adjustable dynamically */) {
        super(concurrencyMax);
    }

    @Override
    protected void starting(NAR n) {

        if (!(n.time instanceof RealTime))
            throw new UnsupportedOperationException("non-realtime clock not supported");

        super.starting(n);
    }


    /**
     * execute later
     */
    protected abstract void execute(Object r);

    @Override
    public final void execute(Runnable async) {
        execute((Object) async);
    }


//    private static class QueueLatencyMeasurement implements Consumer<NAR> {
//
//        private final long start;
//
//        QueueLatencyMeasurement(long start) {
//            this.start = start;
//        }
//
//        /**
//         * measure queue latency "ping"
//         */
//        static void queueLatency(long start, long end, NAR n) {
//            long latencyNS = end - start;
//            double frames = latencyNS / ((double) (n.loop.periodNS()));
//            //if (frames > 0.5) {
//                Exec.logger.info("queue latency {} ({} frames)", Str.timeStr(latencyNS), Str.n4(frames));
//            //}
//        }
//
//        @Override
//        public void accept(NAR n) {
//            long end = nanoTime();
//            queueLatency(start, end, n);
//        }
//    }

}