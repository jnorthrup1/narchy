package nars.task.util;

import jcog.decide.Roulette;
import jcog.random.RandomBits;
import jcog.signal.FloatRange;
import nars.Task;
import nars.task.NALTask;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;

import static nars.Op.*;

/** bag of punctuations (fixed set of them) */
public class PuncBag implements FloatFunction<NALTask> /* TODO FixedBag<..> */ {

    public final FloatRange belief, question, goal, quest;

    public PuncBag() {
        this(0, 8);
    }

    public PuncBag(float min, float max) {
        belief = new FloatRange(1, min, max);
        question = new FloatRange(1, min, max);
        goal = new FloatRange(1, min, max);
        quest = new FloatRange(1, min, max);
    }

    public PuncBag(float b, float q, float g, float Q) {
        this(0, 1);
        set(b, q, g, Q);
    }

    public PuncBag(float v) {
        this(v,v,v,v);
    }


    public final float apply(Task t) {
        return apply(t.punc());
    }

    public float apply(byte punc) {
        return (switch (punc) {
            case BELIEF -> belief;
            case GOAL -> goal;
            case QUESTION -> question;
            case QUEST -> quest;
            default -> throw new UnsupportedOperationException();
        }).floatValue();
    }

    @Override
    public float floatValueOf(NALTask t) {
        return apply(t.punc());
    }

    /** NALTask.i(x) */
    private float _get(int index) {
        return (switch (index) {
            case 0 -> belief;
            case 1 -> question;
            case 2 -> goal;
            case 3 -> quest;
            default -> throw new UnsupportedOperationException();
        }).floatValue();
    }

    public byte sample(RandomBits r) {
        int i = Roulette.selectRoulette(4, this::_get, r.rng);
        return i == -1 ? 0 : NALTask.p(i);
    }

    /** beliefs, goals, questions, quests */
    public void mul(float[] p) {
        p[0] *= apply(BELIEF);
        p[1] *= apply(GOAL);
        p[2] *= apply(QUESTION);
        p[3] *= apply(QUEST);
    }

    public final PuncBag set(float b, float q, float g, float Q) {
        belief.set(b);
        question.set(q);
        goal.set(g);
        quest.set(Q);
        return this;
    }
}