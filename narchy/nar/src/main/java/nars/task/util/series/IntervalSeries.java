package nars.task.util.series;

import jcog.math.LongInterval;
import jcog.pri.Deleteable;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static jcog.math.LongInterval.TIMELESS;

public abstract class IntervalSeries<X extends LongInterval> {

//    @Deprecated public static final IntervalSeries Empty =
//        new EmptyTaskSeries();

    private final int cap;

    IntervalSeries(int cap) {
        this.cap = cap;
    }

    public int capacity() {
        return cap;
    }

    public abstract void push(X t);

    /** remove the oldest task, and delete it */
    public abstract @Nullable X poll();


    public final void compress() {
        int toRemove = (size()+1) - cap;
        while (toRemove-- > 0) {
            X x = poll();
            if (x instanceof Deleteable)
                ((Deleteable)x).delete();
        }
    }

    public final X add(X s) {
        compress();
        push(s);
        return s;
    }

    public abstract boolean remove(X x);

    public abstract int size();


    /**
     * returns false if the predicate ever returns false; otherwise returns true even if empty.  this allows it to be chained recursively to other such iterators
     *
     * intersectRequired: require returned items to temporally intersect (not contained by) the provided range
     */
    public abstract boolean whileEach(long minT, long maxT, boolean intersectRequired, Predicate<? super X> x);

    public abstract void clear();

    public abstract Stream<X> stream();

    public abstract void forEach(Consumer<? super X> each);


    /** returns Tense.TIMELESS (Long.MAX_VALUE) if seemingly empty, which may occurr spuriously */
    public long start() {
        X f = first();
        return f == null ? TIMELESS : f.start();
    }

    /** returns Tense.TIMELESS (Long.MAX_VALUE) if seemingly empty, which may occurr spuriously */
    public long end() {
        X l = last();
        return l == null ? TIMELESS : l.end();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * returns false if there is some data which occurrs inside the given interval
     */
    public boolean isEmpty(long start, long end) {
        return whileEach(start, end, true, x -> false /* at least one found */ );
    }

    public abstract X first();

    public abstract X last();


//    private static class EmptyTaskSeries extends IntervalSeries<NALTask> {
//
//        public EmptyTaskSeries() {
//            super(0);
//        }
//
//        @Override
//        public void push(NALTask t) {
//
//        }
//
//        @Nullable
//        @Override
//        public NALTask poll() {
//            return null;
//        }
//
//        @Override
//        public boolean remove(NALTask nalTask) {
//            return false;
//        }
//
//        @Override
//        public int size() {
//            return 0;
//        }
//
//        @Override
//        public boolean whileEach(long minT, long maxT, boolean intersectRequired, Predicate<? super NALTask> x) {
//            return false;
//        }
//
//        @Override
//        public void clear() {
//
//        }
//
//        @Override
//        public Stream<NALTask> stream() {
//            return Stream.empty();
//        }
//
//        @Override
//        public void forEach(Consumer<? super NALTask> action) {
//
//        }
//
//        @Override
//        public long start() {
//            return TIMELESS;
//        }
//
//        @Override
//        public long end() {
//            return TIMELESS;
//        }
//
//        @Override
//        public NALTask first() {
//            return null;
//        }
//
//        @Override
//        public NALTask last() {
//            return null;
//        }
//    }
}