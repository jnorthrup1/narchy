package nars.task.util;

import jcog.random.RandomBits;
import jcog.sort.FloatRank;
import jcog.sort.RankedN;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.task.proxy.SpecialNegTask;
import nars.task.proxy.SpecialTermTask;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termed;
import nars.term.util.DTDiffer;
import nars.time.Moment;
import nars.time.Tense;
import nars.truth.Truth;
import nars.truth.evi.EviInterval;
import nars.truth.proj.IntegralTruthProjection;
import nars.truth.proj.MutableTruthProjection;
import nars.truth.proj.SingletonTruthProjection;
import nars.truth.proj.TruthProjection;
import nars.util.Timed;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Predicate;

/**
 * heuristic task ranking for matching of evidence-aware truth values may be computed in various ways.
 * designed to be reusable
 */
public final class Answer implements Timed, Predicate<NALTask>, AutoCloseable {

    public final NAR nar;
    private final boolean beliefOrQuestion;
    private final EviInterval when;

    private @Nullable Term term;

    public final RankedN<NALTask> tasks;

    public Predicate<NALTask> filter;



    /**
     * time to live, # of tries remain
     */
    public int ttl;


    /** min mean evi for generated tasks */
    public double eviMin = NAL.truth.EVI_MIN;
    private RandomBits rng;

    /** dummy ranker for use before adding 2nd task. */
    private static final FloatRank NullRanker = (x,min) -> 0;

    /**
     * TODO filter needs to be more clear if it refers to the finished task (if dynamic) or a component in creating one
     */
    private Answer(boolean beliefOrQuestion, long s, long e, float dur, int capacity, NAR nar) {
        this.when = new EviInterval(s, e, dur);
        this.beliefOrQuestion = beliefOrQuestion;
        this.tasks = new RankedN<>(new NALTask[capacity], NullRanker);
        this.nar = nar;
    }

//    @Deprecated public static float durAuto(long s, long e) {
//        return 1;
//        //return Tense.occToDT(1 + (e-s)/2 /* half range */);
//        //return s == ETERNAL ? 1 : 1 + e - s;
//        //return 0;
//    }

    private FloatRank<NALTask> rank() {
        return strength(beliefOrQuestion, NAL.answer.RANGE_SPECIFIC > 0, term);
                //.filter(filter);
    }

    public Answer clear(int ttl) {
        tasks.clear();
        return ttl(ttl);
    }

    public Answer ttl(int ttl) {
        Answer.this.ttl = ttl;
        return Answer.this;
    }


    /**
     * for belief or goals (not questions / quests
     */
    public static Answer answer(@Nullable Term template, long start, long end, boolean notQuestion, int capacity, @Nullable Predicate<NALTask> filter, float dur, NAR nar) {
        return new Answer(notQuestion, start, end, dur, capacity, nar)
                .term(template)
                .filter(filter)
                .clear((int) Math.ceil((notQuestion ?
                        NAL.answer.ANSWER_TRIES_PER_CAPACITY : 1) * capacity));
    }


    private FloatRank<NALTask> strength(boolean beliefOrQuestion, boolean rangeSpecific, @Nullable Term template) {

        FloatRank<NALTask> f = beliefOrQuestion ?
            beliefStrength() :
            priRelevant();

        if (rangeSpecific) {
            long eRange = when.rangeIfNotEternalElse(-1);
            if (eRange >= 1)
                f = f.mulUnitPow(
                    new RangeSpecificRank(eRange),
                NAL.answer.RANGE_SPECIFIC);
        }

        return template==null || !template.TEMPORALABLE() ?
            f
            :
            f.mulUnitPow(intermpolateStrength((Compound)template), NAL.answer.DT_SPECIFIC);
    }

    private FloatRank<NALTask> beliefStrength() {
//        float minDur = nar.dtDither();
//        return at.dur < minDur ? at.clone(minDur) : at;
//        if (at.dur < 1)
//            Util.nop();
        return when;
    }

    @Nullable private FloatRank<Termed> intermpolateStrength(Compound template) {
        DTDiffer d = DTDiffer.the(template);
        return d == DTDiffer.DTDifferZero ? null : d;
    }

    private FloatRank<NALTask> priRelevant() {
        long start = when.start();
        if (start == EviInterval.ETERNAL)
            return (t, m) -> t.pri();
        else {
            long end = when.end();
            return (t, m) -> {
                float pri = t.pri(); // * t.originality();
                return (/*pri == pri && */pri > m) ?
                        (float) (pri / (1.0 + t.minTimeTo(start, end)))
                        :
                        Float.NaN;
            };
        }
    }


    public Answer term(@Nullable Term template) {

        if (template!= Answer.this.term) {
            if (template != null) {
                //template = template.concept();

                if (!template.TASKABLE())
                    throw new TaskException("not Answerable", template);
                Answer.this.term = template;
//                this.termOp = term.opID();
            } else {
                Answer.this.term = null;
//                this.termOp = -1;
            }
        }

        return Answer.this;
    }



    public Answer random(RandomBits r) {
        Answer.this.rng = r;
        return Answer.this;
    }

    @Override
    public RandomBits rng() {
        if (rng == null)
            Answer.this.rng = new RandomBits(nar.random());
        return rng;
    }

    @Override
    public void close() {
        tasks.delete();
        filter = null;
        rng = null;
    }

    public final Answer eviMin(double eviMin) {
        Answer.this.eviMin = eviMin;
        return Answer.this;
    }



//    public @Nullable NALTask task(boolean reviseOrSample, boolean occSpecific) {
//        return task(dur(), reviseOrSample, occSpecific);
//    }

    /**
     * matches, and projects to the specified time-range if necessary
     * note: if forceProject, the result may be null if projection doesnt succeed.
     * only useful for precise value summarization for a specific time.
     * <p>
     * <p>
     * clears the cache and tasks before returning
     */
    public @Nullable NALTask task(float truthDur, int revisionCap, boolean occSpecific) {
        int s = tasks.size();
        if (s == 0)
            return null;

        NALTask t = _task(truthDur, revisionCap, occSpecific);
        if (t == null || !eviSufficient(t))
            return null;

//        //dont bother sub-projecting eternal here.
//        if (forceProject) {
//            t = taskProject(t, ditherTime);
//            if (t == null) return null;
//        }

        return postFilter(t) ? postProcess(t) : null;
    }

    @Deprecated @Nullable public final NALTask taskProject(float dur, int revisionCap) {
        NALTask x = task(dur, revisionCap, true);
        return x == null ? null : _taskProject(x, dur, false);
    }

    @Deprecated @Nullable private NALTask _taskProject(NALTask x, float dur, boolean ditherTime) {
        if (x.ETERNAL()) return x; //unchanged

        long qs = when.start(); if (qs == EviInterval.ETERNAL) return x;

        long qe = when.end();
        if (ditherTime) {
            int dtDither = nar.dtDither();
            if (dtDither > 1) {
                qs = Tense.dither(qs, dtDither);
                qe = Tense.dither(qe, dtDither);
            }
        }
        return NALTask.projectAbsolute(x, qs, qe, dur,0, eviMin);
    }

    /**
     *
     * @param revisionCap if 0, revision is not attempted.
     */
    @Nullable private NALTask _task(float truthDur, int revisionCap, boolean occSpecific) {
        NALTask root = tasks.first();
        int n = tasks.size();
        if (n == 1)
            return root;

        //revision the only way to possibly overcome evi thresh
        boolean revise = !root.QUESTION_OR_QUEST() && (revisionCap > 0);// || !eviSufficient(root));

        if (revise) {
            TruthProjection p = occSpecific ?
                truthSpecificTime(truthDur) :
                truthAnyTime(truthDur);

            //TODO p.sizeMax()

            if (p!=null) {
                try {
                    if (p instanceof MutableTruthProjection m)
                        m.sizeMax(revisionCap);

                    NALTask y = p.task();

                    if (y != null) {
                        if (occSpecific || acceptProjection(p, y)) {
                            return y;
                        } else {
                            //too weak, sample (below)
                            //jcog.Util.nop();
                        }
                    }
                } finally {
                    p.clear();
                }
            }
        }

        return taskSample();
    }

    /** TODO refine */
    protected boolean acceptProjection(TruthProjection p, NALTask y) {
        return y.evi() >= p.minComponentEvi();
    }

//    @Nullable public NALTask taskReviseStrong() {
//        TruthProjection any = truthAny();
//        //if (p != null)p.minComponents(2);
//        NALTask anyTask = any.task();
//        if (anyTask!=null && (start() == TIMELESS || (start() == any.start() && end() == any.end())))
//            return anyTask;
//
//        //try the specific time interval, which may contain more evidence density
//        NALTask askedTask = truthAnswer().task();
//        if (askedTask == null) return anyTask;
//        if (anyTask == null) return askedTask;
//
//        //decide the stronger
//        return at.floatValueOf(askedTask) >= at.floatValueOf(anyTask) ?
//                askedTask :
//                anyTask;
//    }

    @Nullable private NALTask taskRevise(float truthDur, boolean occSpecific) {
        TruthProjection p = occSpecific ?
                truthSpecificTime(truthDur) :
                truthAnyTime(truthDur);

        return p != null ? p.task() : null;
    }

    @Nullable
    public NALTask taskSample() {
        return tasks.getRoulette(random());
    }

    /** implementations can override to apply a post filter */
    protected boolean postFilter(NALTask t) {
        return true;
    }

    private boolean eviSufficient(@Nullable NALTask t) {
        return !t.BELIEF_OR_GOAL() || t.evi() > eviMin;
    }

    private NALTask postProcess(NALTask t) {
        return t;

//        Term x = t.term();
//        Term y = x;
////        if (x instanceof LightCompound) {
////            //HACK undoes MutableTruthProjection MySpecialTermTask unbundling
////            y = x.op().the(x.subtermsDirect());
////        }
//        return (x!=y) ?
////        Term a = this.term;
////        return (a !=null && x.INH() && !a.equals(x) && Image.imageNormalize(a).equals(x)) ?
//                SpecialTermTask.proxy(t, y).copyMeta(t) :
//                t;
    }

    public final @Nullable Truth truth() {
        return truth(dur());
    }

    public final @Nullable Truth truth(float dur) {
        if (isEmpty()) return null;
        TruthProjection tp = truthSpecificTime(dur);
        return tp != null ? tp.truth() : null;
    }

    /** projects for the Answer's target time interval */
    @Nullable public final TruthProjection truthSpecificTime(float dur) {
        return truthProjection(when.start(), when.end(), dur);
    }

    /** projects for the time of the matched tasks,
     *  whenever it might be*/
    private TruthProjection truthAnyTime(float truthDur) {
        return truthProjection(EviInterval.TIMELESS, EviInterval.TIMELESS, truthDur);
    }

    @Nullable private TruthProjection truthProjection(long s, long e, float truthDur) {
        RankedN<NALTask> t = Answer.this.tasks;
        int n = t.size();
        return switch (n) {

            case 0 -> null;

            case 1 -> SingletonTruthProjection.the(t.first(), s, e,
                    nar.dtDither(), truthDur)
                        .eviMin(eviMin);

            default -> new IntegralTruthProjection(s, e)
                    .dur(truthDur)
                    .dtDither(nar.dtDither())
                    .freqRes(nar.freqResolution.floatValue())
                    .term(term)
                    .eviMin(eviMin)
                    .with(Arrays.copyOf(t.items, n), n);
        };
    }

    private void invalidateRanker() {
        tasks.rank = NullRanker;
    }

    /** make sure to call updateRanker() first */
    public Answer match(TaskTable t) {
        //assert(!t.isEmpty());
        assert(isEmpty());
        assert(ttl > 0);

        t.match(Answer.this);

        return Answer.this;
    }

    public final boolean isEmpty() {
        return tasks.isEmpty();
    }

    /**
     * acceptContinue():
     * consume a limited 'tries' iteration. also applies the filter.
     * a false return value should signal a stop to any iteration supplying results
     */
    @Deprecated @Override public final boolean test(NALTask t) {
        if (ttl < 0)
            return false;

        accept(t);

        return ttl > 0;
    }

    /** can be used to bypass ttl check */
    public boolean accept(NALTask t) {

        ttl--;
        Predicate<NALTask> f = Answer.this.filter;
        if (f == null || f.test(t)) {

            //if (tasks.rank == null) tasks.rank(rank());

            //initialize ranker only before adding the 2nd item.
            if (tasks.rank == NullRanker && tasks.size() >= 1)
                tasks.rank(rank());

            return tasks.add(t);
        }

        return false;
    }

    @Override public final float dur() {
        return when.dur;
    }

    /** TODO RandomBits */
    public final Random random() {
        RandomBits r = Answer.this.rng;
        return r == null ?
                nar.random()
                : r.rng;
    }

    @Override
    public final long time() {
        return nar.time();
    }

    public final Answer time(long start, long end) {
        if (when.whenChanged(start, end))
            invalidateRanker(); //HACK because beliefStrength() may clone

        return Answer.this;
    }

    public final Moment time(long start, long end, float dur) {
        boolean timeChanged = when.whenChanged(start, end);
        boolean durChanged  = when.durIfChanged(dur);
        if (timeChanged || durChanged)
            invalidateRanker(); //HACK because beliefStrength() may clone
        return when;
    }


    public final @Nullable Term term() {
        return term;
    }

    public final @Nullable NALTask sample() {
        return isEmpty() ? null : tasks.getRoulette(random()::nextFloat);
    }

    public NALTask taskify(Term x, Predicate<NALTask> filter, double componentEviMin, boolean occSpecific, float dur, int revisionCap) {

        if (Answer.this.tasks.retainIf(filter)) {
            if (Answer.this.isEmpty())
                return null;
        }

        Answer.this.eviMin(componentEviMin);

        NALTask y = occSpecific ?
                Answer.this.taskProject(dur, revisionCap) :
                Answer.this.task(dur, revisionCap, false);

        Answer.this.close();

        if (y == null)
            return null;

        assert(y.evi() >= componentEviMin);

        return x instanceof Neg ? new SpecialNegTask(y) : y;
    }

    /** rewrites matching task content to new term */
    public void replace(Term x, Term y) {
        NALTask[] items = tasks.items;
        int n = items.length;
        for (int i = 0; i < n; i++) {
            NALTask z = items[i];
            if (z == null) break; //done
            items[i] = SpecialTermTask.proxyUnsafe(z, y).copyMeta(z);
        }
    }

    /**
     * TODO dtSpecificity for ==> and &&
     */
    private static final class RangeSpecificRank implements FloatRank<NALTask> {

        private final long range;

        public RangeSpecificRank(long range) {
            this.range = range;
        }

        @Override
        public float rank(NALTask x, float min) {
            //weight by difference in target ranges, to filter different scales
            long s = x.start();
            if (s == EviInterval.ETERNAL) return 1; //entirely non-specific  TODO discount to prefer temporal?

            long xRange = 1 + (x.end() - s);
            long eRange = 1 + range;

            /* rangeRatio always >=1 */
            //assert(eRange>0 && xRange>0);
            double rangeRatio =
                    eRange > xRange ? ((double) eRange) / xRange : ((double) xRange) / eRange;
            //(float)Math.max(((double)xRange)/eRange, ((double)eRange)/xRange);
            //float rangeDiff =
            //Math.abs(xRange - eRange);
            //float y = (float) (1 / (1 + ((double) rangeDiff) / eRange));
            float y = (float) (1 / (/*1+*/rangeRatio));
            return y;
        }
    }


    public final Answer filter(@Nullable Predicate<NALTask> filter) {
        Answer.this.filter = filter;
        return Answer.this;
    }

    public final Answer dur(float dur) {
        float d = when.dur;
        if (d!=dur) {
            when.dur(dur);
            invalidateRanker(); //HACK because beliefStrength() may clone
        }
        return Answer.this;
    }

    public final long start() {
        return when.s;
    }

    public final long end() {
        return when.e;
    }

//    /** special pathway for accepting intermediate dynamic results which may optionally be cached (remembered) */
//    public final void acceptDynamic(NALTask x) {
//        if (NAL.answer.DYN_BELIEF_TABLE_CACHE && x.uncreated()) {
//            NALTask y = cacheDynamic(x);
//            if (y!=null) x = y;
//        }
//        accept(x);
//    }
//
//    @Nullable private NALTask cacheDynamic(NALTask x) {
//        Consumer<NALTask> a = acceptDynamic;
//        if (a == null)
//            return acceptDynamicDefault(x);
//        else {
//            a.accept(x);
//            return x;
//        }
//    }
//
//    @Nullable private NALTask acceptDynamicDefault(NALTask x) {
//        return DynRemember.cacheConcept(x, nar);
//    }
//    private static class DynRemember extends Remember {
//        @Nullable static NALTask cacheConcept(NALTask x, NAR n) {
//            TaskConcept c =
//                    n.concept(x);
//                    //n.conceptualize(x);
//
//            if (c == null) return null;
//
//            try (Remember r = new DynRemember(x, c, n)) {
//
//                c.table(x.punc()).remember(r);
//
//                return r.stored;
//            }
//        }
//
//        private final TaskConcept c;
//        private final NAR nar;
//
//        DynRemember(NALTask x, TaskConcept c, NAR nar) {
//            super(null);
//            focus(f).remember(x);
//            this.c = c;
//            this.nar = nar;
//        }
//
//        @Override
//        public NAR nar() {
//            return nar;
//        }
//
//        @Override
//        protected TaskConcept concept() {
//            return c;
//        }
//    }

//    /**
//     * shrink answer occurrence to the maximum range of answered tasks
//     * to prevent unnecessary dilution
//     */
//    public void occShrink() {
//        long s = Long.MAX_VALUE, e = Long.MIN_VALUE;
//        for (NALTask x : tasks) {
//            long xs = x.start(); if (xs == ETERNAL) continue;
//            long xe = x.end();
//            s = Math.min(s, xs); e = Math.max(e, xe);
//        }
//        if (s == Long.MAX_VALUE) return;
//
////        assert(e >= s);
//        long qs = start();
////        if (qs == ETERNAL || qs < s)
////            at.s = s;
//        long r = e - s;
//        long qe = end();
//        long qr = qe - qs;
//        if (qs == ETERNAL || qr > r)
//            at.e = at.s + r;
//
//    }
}