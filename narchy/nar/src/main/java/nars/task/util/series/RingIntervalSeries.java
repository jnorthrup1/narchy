package nars.task.util.series;

import jcog.Fuzzy;
import jcog.data.list.MetalConcurrentQueue;
import jcog.data.list.MetalRing;
import jcog.math.LongInterval;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static jcog.math.LongInterval.TIMELESS;
import static jcog.math.LongInterval.intersectsRaw;
import static nars.Op.ETERNAL;

public class RingIntervalSeries<X extends LongInterval> extends IntervalSeries<X> {

    public final MetalRing<X> q;

    public RingIntervalSeries(MetalRing<X> q) {
        super(q.length());
        this.q = q;
    }

    public RingIntervalSeries(int capacity) {
        this(new MetalConcurrentQueue<>(capacity));
    }

    @Override
    public final boolean remove(X x) {
        return q.remove(x);
    }

    public final X peek(int x) {
        return q.getHead(x);
    }

    @Override
    public final boolean isEmpty() {
        return q.isEmpty();
    }

    @Override
    public final void push(X x) {
        q.offer(x);
    }

    @Override
    public final @Nullable X poll() {
        return q.poll();
    }


    /**
     * binary search
     */
    public int indexNear(int head, long when) {
        int n = size();
        return switch (n) {
            case 0 -> -1;
            case 1 -> 0;
            case 2 -> indexNear2(head, when);
            default -> indexNearN(head, when, n);
        };
    }

    private int indexNearN(int head, long when, int n) {
        int cap = q.length();
        int low = 0, high = n - 1, mid = -1;
        while (low <= high) {
            if (low==high) return low;

            mid = (low + high) / 2;
            X midVal = q.getUnsafe(head, mid, cap);
            if (midVal == null) {
                //low = mid + 1; ///oops ?
                return -1;
            }

            long a = midVal.start(), b = midVal.end();
            if (when >= a && when <= b)
                break; //intersection found

            if (when > b) {
                //assert(when>a);
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return mid;
    }

    private int indexNear2(int head, long when) {
        X a = q.get(head, 0);
        if (a == null) return -1;

        X b = q.get(head, 1);
        if (b == null) return 0;


        long at = a.timeMeanDuringOrTo(when), bt = b.timeMeanDuringOrTo(when);
        if (at < bt) return 0;
        else if (at > bt) return 1;
        else return
                    //ThreadLocalRandom.current().nextBoolean() ? 0 : 1;
                    (int) ((head ^ when) % 2);
    }

    /**
     * TODO obey exactRange flag
     */
    @Override
    public boolean whileEach(long minT, long maxT, boolean intersectRequired, Predicate<? super X> whle) {
        return whileEach(minT, maxT, Fuzzy.mean(minT, maxT), intersectRequired, whle);
    }

    public boolean whileEach(long s, long e, long queryStart, @Deprecated boolean intersectRequired, Predicate<? super X> whle) {
        assert (s != ETERNAL && s != TIMELESS);

        long seriesStart = start();
        if (seriesStart == TIMELESS) return true; //nothing
        long seriesEnd = end();
        if (seriesEnd == TIMELESS) seriesEnd = seriesStart; //HACK

        if (intersectRequired && !intersectsRaw(seriesStart, seriesEnd, s, e))
            return true; //nothing

        int len = q.length();
        int head = q.head();
        int size = Math.min(len, this.size());

        boolean linear = s == queryStart;
        if (linear) {
            return whileEachLinear(s, e, intersectRequired, whle, len, head, size);
        } else {
            return whileEachRadial(s, e, queryStart, intersectRequired, whle, len, head, size);
        }
    }

    private boolean whileEachLinear(long s, long e, boolean intersectRequired, Predicate<? super X> whle, int len, int head, int size) {
        int p = indexNear(head, s);
        for (int i = 0; i < size; i++) {
            var x = q.getHead(p);
            if (x == null || (intersectRequired && !x.intersectsRaw(s, e)))
                break; //exit
            if (!whle.test(x))
                return false;
            p++;
        }

        return true;
    }

    private boolean whileEachRadial(long s, long e, long queryStart, boolean intersectRequired, Predicate<? super X> whle, int len, int head, int size) {
        //the +1 here is dangerous because it might return repeat value(s)
        int radHalf = size / 2 /* + 1*/;

        int center = indexNear(head, queryStart);

//        int sizeRemain = size;
        int r = 0;
        long u0 = Long.MAX_VALUE, v0 = Long.MIN_VALUE; //limits for detecting wrap-around
        boolean increase = true, decrease = size > 1;
        do {
            X v = null;
            if (increase) {
                v = q.getUnsafe(head, center + r, len); //ok for positive only
                if (v == null)
                    increase = false;
                else if (intersectRequired && !v.intersectsRaw(s, e)) {
                    increase = false;
                    v = null;
                }
            }

            r++;

            X u = null;
            if (decrease) {
                u = q.get(head, center - r, len);
                if (u == null)
                    decrease = false;
                else if (intersectRequired && !u.intersectsRaw(s, e)) {
                    decrease = false;
                    u = null;
                }
            }

            X U, V;
            if (u != null && v != null && v.diffRaw(s, e) < u.diffRaw(s, e)) {
                //swap to the closest one to try first because it may be the last
                U = v;
                V = u;
            } else {
                U = u;
                V = v;
            }

            if (U != null && !whle.test(U)) return false;
            if (V != null && !whle.test(V)) return false;

        } while ((increase || decrease) && (r <= radHalf));

        return true;
    }


    @Override
    public int size() {
        return q.size();
    }

    @Override
    public X first() {
        return q.first();
    }

    @Override
    public final X last() {
        return q.last();
    }

    @Override
    public void clear() {
        q.clear();
    }

    @Override
    public Stream<X> stream() {
        return q.stream();
    }

    @Override
    public void forEach(Consumer<? super X> each) {
        q.forEach(each);
    }

    @Nullable public final X findNearest(long t) {
        int h = q.head();
        int i = indexNear(h, t);
        return i < 0 ? null : q.get(h, i);
    }

}