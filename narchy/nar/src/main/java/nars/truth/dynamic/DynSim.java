package nars.truth.dynamic;

import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import nars.truth.func.TruthFunctions2;

import static nars.Op.SIM;

public class DynSim extends DynTruth {

    public static final DynSim the = new DynSim();

    private DynSim() {
        super();
    }

    @Override
    public Truth truth(DynTaskify d) {
        return TruthFunctions2
            //.intersect(
            .similarity(
                d.get(0).truth(), d.get(1).truth()
            , NAL.truth.CONF_MIN);
    }

    @Override
    public boolean decompose(Compound superterm, long start, long end, ObjectLongLongPredicate<Term> each) {
        Subterms ab = superterm.subtermsDirect();
        Term a = ab.sub(0), b = ab.sub(1);
        //return each.accept(INH.the(a,b), start, end) && each.accept(INH.the(b, a), start, end);
        return each.accept(a, start, end) && each.accept(b, start, end);
    }

    @Override
    public Term recompose(Compound superterm, DynTaskify d) {
        return SIM.the(d.get(0).term(), d.get(1).term());
    }

    @Override
    public int componentsEstimate() {
        return 2;
    }
}