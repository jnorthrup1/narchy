package nars.truth.dynamic;

import jcog.Util;
import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.table.util.DynamicTables;
import nars.task.util.TaskRegion;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.util.TermException;
import nars.term.util.conj.ConjBuilder;
import nars.term.util.conj.ConjList;
import nars.time.Tense;
import nars.truth.DynTaskify;
import org.jetbrains.annotations.Nullable;

import static nars.Op.*;
import static nars.term.atom.Bool.Null;
import static nars.term.atom.Bool.True;

public enum DynStatement {
	;

	public static final DynTruth DynImpl = new DynImpl(true);

	public static final DynTruth DynInhSubjSect = new DynStatementSect(true, false);

	public static final DynTruth DynInhPredSect = new DynStatementSect(false, false);


	/**
	 * statement common component
	 */
	static Term stmtCommon(boolean subjOrPred, Subterms superterm) {
		return superterm.sub(subjOrPred ? 1 : 0);
	}

	public static @Nullable DynTruth[] dynamicInhSect(Subterms ii) {
		if (NAL.truth.DYNAMIC_INH_SECT_TRUTH) {

			Term s = ii.sub(0), p = ii.sub(1);
			boolean sc = DynConj.condDecomposeable(s, false),
					pc = DynConj.condDecomposeable(p, false);
			if ((sc || pc) && DynamicTables.varSeparate(s, p)) {
				if (sc && pc)
					return new DynTruth[]{DynInhSubjSect, DynInhPredSect};
				else if (sc)
					return new DynTruth[]{DynInhSubjSect};
				else if (pc)
					return new DynTruth[]{DynInhPredSect};
			}
		}
		return null;
	}


	/**
	 * functionality shared between INH and IMPL
	 */
	static class DynStatementSect extends DynSect {

		final boolean subjOrPred;

		/**
		 * true = union, false = intersection
		 */
		final boolean unionOrIntersection;

		DynStatementSect(boolean subjOrPred, boolean union) {
			this.unionOrIntersection = union;
			this.subjOrPred = subjOrPred;
		}

		static Term recomposeStatement(Compound superterm, DynTaskify d, boolean subjOrPred, boolean union) {
			Subterms ss = superterm.subterms();

			int commonIndex = subjOrPred ? 1 : 0;
			Term common = ss.sub(commonIndex);
			int n = d.size();
			if (common.TEMPORALABLE()) {
				//test all components for consistency, and use the value especially if the superterm has XTERNAL in it
				//TODO intermpolate if not completely consistent
				Term cc = null;
				for (int i = 0; i < n; i++) {
					Term xi = d.get(i).term().unneg().sub(commonIndex);
					if (i == 0) cc = xi;
					else {
						if (!xi.equals(cc))
							return Null;
					}
				}
				common = cc;
			}

			Term superSect = ss.sub(subjOrPred ? 0 : 1);

			boolean negOuter = superSect instanceof Neg;
			if (negOuter)
				superSect = superSect.unneg();

			Term sect;
			int outerDT;
			Op op = superterm.op();
			if (op == IMPL) {
				//TODO DynamicConjTruth.ConjIntersection.reconstruct()

				long cs;
				Term constantCondition = null;
				//IMPL: compute innerDT for the conjunction
				ConjBuilder c =
						//new ConjTree();
						new ConjList();

				for (int i = 0; i < n; i++) {
					Term xx = d.term(i);
					if (xx.unneg().IMPL()) {

						boolean neg = xx instanceof Neg;
						if (neg) xx = xx.unneg();

						int tdt = xx.dt();

						if (tdt == XTERNAL)
							throw new TermException("xternal in dynamic impl reconstruction", xx);

						if (tdt == DTERNAL)
							tdt = 0;
						long tWhen = Tense.dither((subjOrPred ? (-tdt) : (+tdt)), d.ditherDT);

						if (!c.add(tWhen, xx.sub(subjOrPred ? 0 : 1).negIf(union ^ neg)))
							return Null;
					} else {
						//conjoin any constant conditions (which may precipitate from reductions)


						constantCondition = constantCondition != null ? CONJ.the(constantCondition, xx) : xx;

						if (constantCondition == True)
							constantCondition = null;
						else if (!xx.CONDABLE() || constantCondition == Null)
							return Null;
					}
				}

				sect = c.term();
				cs = c.shift();
				c.clear();

				if (constantCondition != null)
					sect = CONJ.the(constantCondition, sect);

				if (sect == Null)
					return Null; //allow non-Null Bool's?

				//temporal information not available or was destroyed
				outerDT = cs == ETERNAL ? DTERNAL :
						Tense.occToDT(subjOrPred ? -cs - sect.seqDur() : cs);

			} else {


				sect = superSect.op().the(Util.arrayOf(i ->
					subSubjPredWithNegRewrap(subjOrPred, d.get(i)), 0, n, Term[]::new
				));
				outerDT = DTERNAL;
			}


			if (negOuter)
				sect = sect.neg();

			outerDT = Tense.dither(outerDT, d.ditherDT);

			return subjOrPred ? op.the(sect, outerDT, common) : op.the(common, outerDT, sect);
		}

		private static Term subSubjPredWithNegRewrap(boolean subjOrPred, TaskRegion tr) {
			Term x = tr.task().term();
			boolean neg = x instanceof Neg;
			if (neg) x = x.unneg();
			return x.sub(subjOrPred ? 0 : 1 /* reverse */).negIf(neg);
		}


		/**
		 * statement component
		 */
		private static Term stmtDecomposeStructural(Op superOp, boolean subjOrPred, Term subterm, Term common) {
			boolean outerNegate;
			if (outerNegate = (subterm instanceof Neg))
				subterm = subterm.unneg();

			Term s, p;
			if (subjOrPred) {
				s = subterm;
				p = common;
			} else {
				s = common;
				p = subterm;
			}

			return superOp.the(s, p).negIf(outerNegate);

		}

        @Override
		protected boolean negResult() {
			return unionOrIntersection;
		}

		@Override
		public Term recompose(Compound superterm, DynTaskify components) {
			return recomposeStatement(superterm, components, subjOrPred, false);
		}

		@Override
		public boolean decompose(Compound superterm, long start, long end, ObjectLongLongPredicate<Term> each) {


			Term decomposed = stmtCommon(!subjOrPred, superterm);
			if (!decomposed.unneg().CONJ()) {
				//superterm = (Compound) Image.imageNormalize(superterm);
				decomposed = stmtCommon(!subjOrPred, superterm);
			}

			//if (unionOrIntersection) {
			if (decomposed instanceof Neg) {
				decomposed = decomposed.unneg();
			}

			Term common = stmtCommon(subjOrPred, superterm);

			Op op = superterm.op();

			for (Term y : decomposed.subterms()) {
				if (!each.accept(stmtDecomposeStructural(op, subjOrPred, y, common), start, end))
					return false;
			}

			return true;
		}

	}

}