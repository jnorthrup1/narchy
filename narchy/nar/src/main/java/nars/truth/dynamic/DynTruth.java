package nars.truth.dynamic;

import jcog.util.ObjectLongLongPredicate;
import nars.Term;
import nars.task.NALTask;
import nars.term.Compound;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static nars.Op.ETERNAL;
import static nars.Op.XTERNAL;


/** dynamic task calculation model */
public abstract class DynTruth {

	public abstract Truth truth(DynTaskify d /* eviMin, */);

	/** decompose term into evaluable components */
	public abstract boolean decompose(Compound superterm, long start, long end, ObjectLongLongPredicate<Term> each);

	/**
	 * construct dynamic task's term, from components
	 */
	@Nullable public abstract Term /*Compound*/ recompose(Compound superterm, DynTaskify d);


	/**
	 * estimates number of components, for allocation purposes
	 */
	public abstract int componentsEstimate();



	/**
	 * determines result occurrence time range, if not completely eternal.
	 * default implementation
	 *
	 * if temporal()==true then this is usually overridden with custom occurrence impl
	 *
	 */
	public long[] occ(DynTaskify d) {
		return d.size() == 1 ? d.get(0).startEndArray() : occUnion(d); //fast case
	}

//	private long[] occIntersectionMid(DynTaskify d) {
//		long m = d.occMid();
//		if (m == ETERNAL) return new long[] { ETERNAL, ETERNAL };
//		else {
//			long r = d.rangeIntersection();
//			return new long[]{m - r / 2, m + r / 2};
//		}
//	}

	private static long[] occUnion(DynTaskify d) {
		long s = d.earliestStart();
		if (s == ETERNAL)
			return new long[] { ETERNAL, ETERNAL };

		return new long[]{
			s,
			d.latestEnd()// + d.rangeIntersection()
		};
	}

	/** computes a lower bound for a component's acceptable avg evidence.
	 *  this implementation assumes the component confidences are multiplied
	 *  with each other such that any component not reaching the combined
	 *  allowed minimum evidence would be cause for short-circuiting the attempt.
	 * */
	public double componentEviMin(double eviMin) {
		return eviMin;
	}

	@Nullable public Predicate<NALTask> preFilter(int component, DynTaskify d) {
		return null;
	}

	/** whether component occurrence times should be dithered internally
	 * (ex: to ensure dt in temporal terms are dithered) */
	public boolean ditherComponentOcc() {
		return false;
	}

	public boolean loose(Compound template) {
		return template.dt()==XTERNAL;
	}
}