package nars.truth.dynamic;

import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Neg;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import nars.truth.func.NALTruth;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

import static nars.Op.IMPL;
import static nars.time.Tense.dither;
import static nars.time.Tense.occToDT;

public class DynImpl extends DynTruth {

    //public static final boolean FORWARD_ONLY = !NAL.belief.TEMPORAL_INDUCTION_BIDI;

    final boolean loose;

    public DynImpl(boolean loose) {
        this.loose = loose;
    }


    @Override
    public boolean loose(Compound template) {
        return loose;
    }

    @Override
    public boolean ditherComponentOcc() {
        return true;
    }

    @Override
    @Nullable
    public Predicate<NALTask> preFilter(int component, DynTaskify d) {
        if (!NAL.truth.DYNAMIC_IMPL_filter || component!=0) return null;

        float thresh =
                d.nar().freqResolution.asFloat();
                //NAL.truth.TRUTH_EPSILON / 2;

        return d.template.sub(0) instanceof Neg ?
                x -> x.freq() < 1 - thresh
                :
                x -> x.freq() > thresh;
    }


    @Override
    public final long[] occ(DynTaskify d) {
        long s = d.get(0).start();
        long p = d.get(1).start();
        s = s == Op.ETERNAL ? p : s;

        long r = d.rangeIntersection();
        assert(r>=0);
        return new long[] { s, s + r};
    }



    /**
     *     B, A, --is(A,"==>") |-          polarizeTask((polarizeBelief(A) ==> B)), (Belief:InductionDD, Time:BeliefRelative)
     *     B, A, --is(B,"==>") |-          polarizeBelief((polarizeTask(B) ==> A)), (Belief:AbductionDD, Time:TaskRelative)
     */
    @Override  public Truth truth(DynTaskify d) {
        //assert(d.pn.test(1));

        return NALTruth.Abduction.truth(
            d.taskTruth(0),
            d.taskTruth(1)
        );
    }

    @Override
    public boolean decompose(Compound superterm, long start, long end, ObjectLongLongPredicate<Term> each) {
        assert(superterm.IMPL());
        Term subj = superterm.sub(0), pred = superterm.sub(1);

           ///TODO ensure non-collapsing dt if collapse imminent


        long as, ae, bs, be;
        if (start == Op.ETERNAL) {
            if (subj.equalsRoot(pred))
                return false;
            as = ae = bs = be = Op.ETERNAL;

        } else {
            as = start;
            ae = end;
            bs = start;
            be = end;

            int sdt = superterm.dt();
            if (sdt!= Op.XTERNAL && sdt!= Op.DTERNAL) {
                bs+= sdt; be+= sdt;
            }

            int sr = subj.seqDur();
            if (sr!=0) {
                bs += sr; be += sr;
            }

            if (as == bs && ae == be && (ae!=as)) {
            //if ((as==bs || ae==be) && (subj.equalsRoot(pred))) {
                //split the time range so there is a delta
                long s = Math.min(as, bs); long e = Math.min(ae, be);
                long range = e - s;
                if (range < 1) return false;

                as = s; ae = s + range/2;
                bs = ae + 1; be = e;

                boolean forward = ThreadLocalRandom.current().nextBoolean();
                if (!forward) {
                    //swap
                    long ss = as, ee = ae; as = bs; ae = be; bs = ss; be = ee;
                }
            }
        }

        return each.accept(subj, as, ae) && each.accept(pred, bs, be);
    }

    @Override
    public Term recompose(Compound superterm, DynTaskify d) {
        NALTask subj = d.get(0), pred = d.get(1);
//        if (FORWARD_ONLY) {
//            if (pred.start() < subj.start())
//                return null;
//        }

        return IMPL.the(
            subj.term(),
            implDT(subj, pred, d.ditherDT),
            pred.term()
        );
    }

    /** computes appropriate dt interval between two tasks for an implication linking them */
    static int implDT(NALTask a, NALTask b, int ditherDT) {
        int dt;
        Term st = a.term(), pt = b.term();
        long SS = a.start(), PS = b.start();
        if (SS == Op.ETERNAL || PS == Op.ETERNAL) {
            dt = Op.DTERNAL;
        } else {
            int rawDT = occToDT(PS - SS - st.seqDur());
            dt = dither(rawDT, ditherDT);
            if (dt==0 && st.unneg().equals(pt.unneg())) {
                //HACK use rawDT so as not to collapse due to dithering
                dt = rawDT;
            }
        }
        return dt;
    }

    @Override
    public int componentsEstimate() {
        return 2;
    }
}