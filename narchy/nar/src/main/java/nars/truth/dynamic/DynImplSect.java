package nars.truth.dynamic;

import nars.term.Compound;

abstract class DynImplSect extends DynStatement.DynStatementSect {

    protected DynImplSect(boolean subjOrPred, boolean union) {
        super(subjOrPred, union);
    }

    @Override
    public final boolean loose(Compound template) {
        //return super.loose(template) || super.loose((Compound) template.sub(subjOrPred ? 0 : 1));
        return true;
    }

    @Override
    public boolean ditherComponentOcc() {
        return true;
    }

}