package nars.truth.dynamic;

import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.Term;
import nars.task.NALTask;
import nars.term.Compound;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import nars.truth.proj.IntegralTruthProjection;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static nars.Op.DELTA;
import static nars.truth.func.TruthFunctions.tt;

/** TODO 2-ary delta between two concepts at different times
 *      canonically sort the pair's order since the reverse
 *      will be redundant (1-d).
 */
public class DynDelta extends DynTruth {

    public static final DynTruth the = new DynDelta();

    private DynDelta() {

    }

    private static final Predicate<NALTask> notEternal = z -> !z.ETERNAL();

    @Override
    public @Nullable Predicate<NALTask> preFilter(int component, DynTaskify d) {
        return notEternal;
    }

    @Override
    public Truth truth(DynTaskify d) {
        NALTask S = d.get(0), E = d.get(1);
        if (!S.term().equals(E.term()))
            return null; //inequal content TODO avoid entirely

        double C = IntegralTruthProjection.reviseConf(d,
                d.start(), d.end());

        return C <= NAL.truth.CONF_MIN ? null : truth(S, E, C);
    }

    private static Truth truth(NALTask S, NALTask E, double conf) {
        double freqDiff = ((double) E.freq()) - S.freq();
        double freq = (1 + freqDiff) / 2;

        return tt(freq, conf);
    }

    @Override
    public boolean decompose(Compound superterm, long s, long e, ObjectLongLongPredicate<Term> each) {

        assert(superterm.DELTA());

        if (s == e) return false; //point or ETERNAL
        long r = e - s;
        long dh = r/2;
        long startA = s, endA = s + dh;
        long startB = e - dh, endB = e;

        if (endA == startB) {
            //prevent temporal overlap
            endA--;
            if (startA > endA) startA--; //HACK for point
        }
        assert(endA < startB);

        Term x = superterm.sub(0);
        return each.accept(x, startA, endA) && each.accept(x, startB, endB);
    }

    @Override
    public Term recompose(Compound superterm, DynTaskify d) {
        return DELTA.the(d.get(0).term());
    }

    @Override
    public int componentsEstimate() {
        return 2;
    }
}