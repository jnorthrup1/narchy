package nars.truth;

import jcog.math.LongInterval;
import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.task.NALTask;
import nars.task.util.TaskList;
import nars.task.util.TruthComputer;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.util.TermTransformException;
import nars.time.Tense;
import nars.truth.dynamic.DynTruth;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

public abstract class DynTaskify extends TaskList implements AutoCloseable, ObjectLongLongPredicate<Term>, TruthComputer {
    public final DynTruth model;

    /**
     * TODO change this to an int field that caches nar.dtDither()
     */
    public final int ditherDT;
    public final boolean beliefOrGoal;
    /**
     * TODO move to AnswerTaskify
     */
    @Deprecated
    public transient Compound template;

    private static final boolean ditherTruth = false;

    static final boolean ditherOcc = NAL.truth.DYNTASKIFY_OCC_DITHER;

    private transient Predicate<Term> templateMatch;

    protected DynTaskify(DynTruth model, boolean beliefOrGoal, int ditherDT, int initialCap) {
        super(initialCap);
        this.model = model;
        this.ditherDT = ditherDT;
        this.beliefOrGoal = beliefOrGoal;
    }

    @Override
    public void close() {
        delete();
    }

    public final NALTask taskClose() {
        NALTask z = task();
        close();
        return z;
    }

    @Nullable
    @Override
    public NALTask task() {

        assert (size > 0);

        Term x = model.recompose(template, this);

        if (x == null || !x.unneg().TASKABLE() || !matchesTemplate(x)) {
            if (NAL.test.DEBUG_EXTRA)
                throw new TermTransformException(this + " " + model + " fault", template, x);

            return null;
        }

        return task(x, this, this, true);
    }

    private boolean matchesTemplate(Term x) {
        return templateMatch == null || templateMatch.test(x);
    }

    @Override
    public Truth computeTruth() {
        Truth truth = model.truth(this);
        return (truth == null || truth.evi() < eviMin()) ?
                null :
                (ditherTruth ? dither(truth) : truth);
    }

    private Truth dither(Truth x) {
        //TODO if MutableTruth, modify in-place
        return x.dither(nar());
    }

    protected abstract double eviMin();

    @Override
    public long[] startEndArray() {
        long[] se = model.occ(this);
        return ditherOcc ?
                (se == null ? null : Tense.dither(se, ditherDT))
                :
                se;
    }

    public abstract NAR nar();

    /**
     * earliest start time, excluding ETERNALs.  returns ETERNAL if all ETERNAL
     */
    public long earliestStart() {
        long w = minValue(t -> {
            long ts = t.start();
            return ts == LongInterval.ETERNAL ? LongInterval.TIMELESS : ts;
        });
        return w == TIMELESS ? ETERNAL : w;
    }


    @Nullable
    public final NALTask task(Compound template, long start, long end) {
        template(template);
        return model.decompose(template, start, end, this) ?
                taskClose() : null;
    }

    private void template(Compound t) {
        if (this.template != t) {
            assert (!(t instanceof Neg));
            this.template = t;
            this.templateMatch = t != null ? templateFilter(t) : null;
        }
    }

    private Predicate<Term> templateFilter(Compound t) {
        return NAL.truth.TEMPLATE_MATCH_STRICT ?
                t.equalsRoot()
                :
                _templateFilterWeak(t);
    }

    /**
     * tests match in 2 categories: eventcontainer, equal op
     */
    private Predicate<Term> _templateFilterWeak(Compound t) {
        //return t.EVENTS() ? Term::EVENTS : t.equalsOp();
        return t.equalsOp();
    }

//    protected abstract void truthProjected(int i, Truth t);

    public int volMax() {
        return Integer.MAX_VALUE;
    }

    public abstract Truth taskTruth(int component);

//    @Deprecated private boolean project(int component, long s, long e, @Deprecated float dur) {
//
//        final NALTask x = items[component];
//        Truth t = //NAL.answer.DYN_PROJECTION_ABSOLUTE_OR_RELATIVE ?
//                x.truthAbsolute(s, e, dur, 0, EVI_MIN);
////                :
////                x.truthRelative(s, e, dur, now, 0, EVI_MIN);
//
//        if (t!=null) {
//            truthProjected(component, t);
//            return true;
//        } else
//            return false;
//    }
//
//
//    /** average mid-occurrence */
//    public long occMid() {
//        double s = 0;
//        int count = 0;
//        for (NALTask x : this) {
//            final long xs = x.mid();
//            if (xs!=ETERNAL) {
//                s += xs;
//                count++;
//            }
//        }
//        if (count == 0) return ETERNAL;
//        return Math.round(s/count); //TODO more accurate avg long calculation
//    }

}