package nars.time;

import jcog.pri.PLink;
import jcog.pri.bag.impl.PLinkArrayBag;
import jcog.pri.op.PriMerge;

import java.util.function.Consumer;

public class EventBag implements Consumer<TimeGraph.Event> {
    private final PLinkArrayBag<TimeGraph.Event> events;

    public EventBag(int cap) {
        this.events = new PLinkArrayBag<>(PriMerge.plus, cap);
    }


    public void load(TimeGraph g) {
        events.forEachKey(g::know);
    }

    public void saveSolutions(TimeGraph g) {
        g.solutions.forEach(this);
    }
    public void saveEvents(TimeGraph g) {
        g.events().forEach(this);
    }

//    public void save(TimeGraph g) {
//        Stream.concat(g.solutions.stream(), g.events())
////                .filter(z -> z instanceof TimeGraph.Absolute /*|| z.id.TEMPORAL()*/)
//                .forEach(this);
//    }

    @Override
    public void accept(TimeGraph.Event event) {
        events.put(new PLink<>(event, event.id.volume()/1024.f /* HACK */ ));
    }

    public final boolean isEmpty() {
        return events.isEmpty();
    }
}