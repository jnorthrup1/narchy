package nars.derive.reaction;

import nars.$;
import nars.Op;
import nars.Term;
import nars.action.Action;
import nars.action.pattern.SubPremise;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.premise.Premise;
import nars.term.Variable;

/** stateless by default */
public abstract class NativeReaction/*Builder*/ extends MutableReaction {

	public static final Variable PremiseTask = $.varPattern(1);
	public static final Variable PremiseBelief = $.varPattern(2);
	private final Term id;

	protected NativeReaction() {
		taskPattern = PremiseTask;
		beliefPattern = PremiseBelief;
		this.id = $.identity(this);
	}

	@Override
	public final Term term() {
		return id;
	}

	/** TODO use PremiseCould impl */
	@Deprecated protected abstract void run(Deriver d, @Deprecated Cause<Reaction> why);

	/** single premise, matching anything.  also applies to TaskLinks, selecting only TaskLink.self()==true */
	public final MutableReaction taskEqualsBelief() {
		return taskEqualsBelief(PremiseTask);
	}

	public final MutableReaction single() {
		return single(true, true, true, true);
	}
	public final MutableReaction single(boolean b, boolean g, boolean q, boolean Q) {
		return single(PremiseTask, b, g, q, Q);
	}

	public final MutableReaction taskIsNotAny(Op... o) {
		return isNotAny(PremiseTask, o);
	}

//	private static final Term ID = $.the(NativeReaction.class.getSimpleName());
//	private static final class NativeProcedures extends CompoundPremise<Term, MyNativeAction.MyNativeProcedure> {
//
//		NativeProcedures(Premise p) {
//			super(ID, p);
//		}
//
//		@Override
//		protected void act(Collection<MyNativeAction.MyNativeProcedure> e, Deriver d) {
//			for (MyNativeAction.MyNativeProcedure l : e)
//				l.act(d);
//		}
//
//	}


	public final class MyNativeAction extends Action {

		private MyNativeAction(Cause<Reaction> why) {
			super(why);
		}

		@Override
        public boolean test(Deriver d) {
//			Premise P = d.premise;
//			P.next(ID, new MyNativeProcedure(), (t, PP)->new NativeProcedures(PP), d);

			d.add(new MyNativeProcedure(d.premise));
			return true;
		}

		private final class MyNativeProcedure extends SubPremise {

			MyNativeProcedure(Premise p) {
				super(MyNativeAction.this.ref, p,
                        /* null ? */ p.why());
			}


			@Override
			public int cause() {
				return MyNativeAction.this.why.id;
			}


			@Override protected void act(Deriver d) {
				NativeReaction.this.run(d, MyNativeAction.this.why);
			}

			@Override public Reaction reaction() {
				return MyNativeAction.this.why.name;
			}

		}
	}


	private final Class thisClass = NativeReaction.this.getClass();

//	private class MyReaction extends Reaction {
//
//		MyReaction(Set<PREDICATE<Deriver>> c) {
//			super(c, NativeReaction.this, NativeReaction.this.tag);
//		}

	@Override
	public Action action(Cause<Reaction> why) {
		return new MyNativeAction(why);
	}

	@Override public Class<? extends MutableReaction> type() {
			return thisClass;
		}
//	}
}