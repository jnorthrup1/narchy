package nars.derive.reaction;

import com.google.common.base.Splitter;
import jcog.data.list.Lst;
import jcog.data.set.ArrayHashSet;
import jcog.memoize.CaffeineMemoize;
import nars.NAR;
import nars.truth.func.NALTruth;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;


/**
 *
 * reaction metaprogram
 *
 * a set of related rules, forming a module that can be combined with other rules and modules
 * to form customized derivers, compiled together.
 *
 * intermediate representation of a set of compileable Premise Rules
 * TODO remove this class, just use Set<PremiseDeriverProto>'s
 */
public class Reactions {

    private final ArrayHashSet<Reaction> reactions;


    public Reactions(ArrayHashSet<Reaction> r) {
        this.reactions = r;
    }

    public Reactions() {
        this(new ArrayHashSet<>(1024));
    }

    private static final Function<String, Collection<Reaction>> ruleFileCache = CaffeineMemoize.build((String n) -> {

        String bb;
        try (InputStream nn = NAR.class.getClassLoader().getResourceAsStream(n)) {
            //bb = IOUtils.readToString(nn);
            bb = new String(nn.readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return PatternReaction.parse(load(bb), NALTruth.the)
                .collect(toCollection(Lst::new));

    }, 128, false);


    public Reactions rules(String... rules) {
        add(PatternReaction.parse(rules));
        return this;
    }

    public static Collection<Reaction> file(String n) {
        return ruleFileCache.apply(n);
    }

    private static Stream<String> load(CharSequence data) {
        return Splitter.on('\n').splitToStream(data)
            .map(String::trim)
            .filter(s -> !s.isEmpty())
            .filter(s -> !s.startsWith("//"));
    }

    public int size() {
        return reactions.size();
    }

    public final ReactionModel compile(NAR n) {
        return ReactionCompiler.compile(reactions, n);
    }

    public final Reactions add(String... metalNALRules) {
        return add(PatternReaction.parse(metalNALRules));
    }

    public final Reactions add(MutableReaction r) {
        r.compile();
        if (!this.reactions.add(r))
            throw new UnsupportedOperationException("reaction already added: " + r);

        return this;
    }

    public final Reactions add(Stream<Reaction> r) {
        r.collect(toCollection(()->this.reactions));
        return this;
    }



    public final Reactions addAll(MutableReaction... h) {
        for (MutableReaction hh : h)
            add(hh);
        return this;
    }

    public Reactions files(String... ff) {
        return add(Stream.of(ff).flatMap(f -> file(f).stream()));
    }
}