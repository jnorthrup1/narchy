package nars.derive.reaction;

import jcog.Log;
import jcog.Util;
import jcog.WTF;
import jcog.data.list.Lst;
import jcog.util.ArrayUtil;
import nars.$;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.op.CeqConstraint;
import nars.derive.op.ConstraintAsPremisePredicate;
import nars.derive.op.HasBelief;
import nars.derive.util.PremiseTermAccessor;
import nars.derive.util.PuncMap;
import nars.link.TaskLink;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.subterm.Subterms;
import nars.subterm.util.SubtermCondition;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.control.NegPredicate;
import nars.term.control.PREDICATE;
import nars.term.control.TermMatching;
import nars.term.util.Image;
import nars.term.util.TermException;
import nars.term.util.TermPaths;
import nars.unify.UnifyConstraint;
import nars.unify.constraint.*;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static nars.Op.*;
import static nars.premise.Premise.Task;
import static nars.subterm.util.SubtermCondition.Cond;
import static nars.subterm.util.SubtermCondition.Recursive;
import static nars.term.atom.Bool.Null;
import static nars.term.util.transform.Retemporalize.patternify;
import static nars.unify.constraint.RelationConstraint.NegRelationConstraint;
import static nars.unify.constraint.RelationConstraint.NotEqual;

/**
 * mutable class collecting components for reaction construction.
 */
public abstract class MutableReaction extends Reaction {

    public static final PREDICATE<Deriver> TaskBeliefEqualRoot = new PREDICATE<>($.func("equalRoot", Task, Premise.Belief)) {

        @Override
        public boolean test(Deriver d) {
            return d.premise.from().equalsRoot(d.premise.to());
        }

        @Override
        public float cost() {
            return 0.05f;
        }
    };
    public static final PREDICATE<Deriver> conjParallelTaskBelief = new PREDICATE<>($.p("conjParallelTaskBelief")) {

        private static boolean para(Term t) {
            return t.CONJ() && !t.SEQ();
        }

        @Override
        public boolean test(Deriver d) {
            return para(d.premise.from()) && para(d.premise.to());
        }

        @Override
        public float cost() {
            return 0.05f;
        }
    };
    public static final PREDICATE<Deriver> taskPos = new PREDICATE<Deriver>($.p(Task, "positive")) {
        @Override
        public boolean test(Deriver d) {
            return taskPolarity(d);
        }
    };
    public static final PREDICATE<Deriver> taskEternal = new PREDICATE<>(Atomic.the("taskEternal")) {
        @Override
        public boolean test(Deriver d) {
            NALTask x = d.premise.task();
            return x != null && x.ETERNAL();
        }

        @Override
        public float cost() {
            return 0.02f;
        }
    };
    public static final PREDICATE<Deriver> taskInput = new PREDICATE<>(Atomic.the("taskInput")) {
        @Override
        public boolean test(Deriver d) {
            return d.premise.task().isInput();
        }

        @Override
        public float cost() {
            return 0.02f;
        }
    };
    public static final PremiseTermAccessor TaskTermAccessor = new PremiseTermAccessor(0, Premise.TaskInline) {
        @Override
        public Term apply(Deriver d) {
            return d.premise.from();
        }
    };
    public static final PremiseTermAccessor BeliefTermAccessor = new PremiseTermAccessor(1, Premise.BeliefInline) {
        @Override
        public Term apply(Deriver d) {
            return d.premise.to();
        }
    };
    public static final PREDICATE<Deriver> NoOverlap = new PREDICATE<Deriver>((Compound) $.p("overlap")) {

        @Override
        public float cost() {
            return 0.01f;
        }

        @Override
        public boolean test(Deriver d) {
            return ((NALPremise) d.premise).overlapDouble();
        }
    }.neg();
    public static final PREDICATE<Deriver> premiseTaskLink = new PREDICATE<>($.inh("tasklink", "premise")) {
        @Override
        public boolean test(Deriver d) {
            return d.premise instanceof TaskLink;
        }

        @Override
        public float cost() {
            return 0.001f;
        }
    };
    //private transient volatile Reaction rule;
    private static final int separateBitsMax = 8;
    private static final Logger logger = Log.log(MutableReaction.class);
    private static final Map<PREDICATE, PREDICATE> preds = new ConcurrentHashMap<>(1024);
    /**
     * cause tag for grouping rules under the same cause. if null, generates a unique cause
     */
    @Nullable
    public String tag;
    /**
     * optional for debugging and other purposes
     */
    public String source = getClass().getSimpleName() + "@" + System.identityHashCode(this);
    protected Term taskPattern = Null;
    Term beliefPattern = Null;
    /**
     * final constructed form, for runtime usage
     */
    transient UnifyConstraint<Deriver.PremiseUnify>[] constraints;
    /**
     * arity: 0 = unknown, 1 = single, 2 = double
     */
    transient int arity = 0;
    /**
     * constraints as input.  the active constraints in 'this.constraints' may be different and fewer once compiled.
     */
    private MutableSet<UnifyConstraint<Deriver.PremiseUnify>> _constraints = new UnifiedSet<>(4);
    private MutableSet<PREDICATE<Deriver>> _conditions = new UnifiedSet<>();

    private static PremiseTermAccessor TaskOrBelief(boolean taskOrBelief) {
        return taskOrBelief ? TaskTermAccessor : BeliefTermAccessor;
    }

    @Nullable
    private static PREDICATE<Deriver> predicateFromConstraint(UnifyConstraint cc, Term taskPattern, Term beliefPattern) {

        Variable x = cc.x;

        if (cc instanceof NegRelationConstraint) {
            PREDICATE<Deriver> p = predicateFromConstraint(((NegRelationConstraint) cc).r, taskPattern, beliefPattern);
            return p != null ? ((PREDICATE) p).neg() : null;
        } else if (cc instanceof RelationConstraint) {

            Variable y = ((RelationConstraint) cc).y;
            byte[] xInT = TermPaths.pathExact(taskPattern, x);
            byte[] xInB = TermPaths.pathExact(beliefPattern, x);
            if (xInT != null || xInB != null) {
                byte[] yInT = TermPaths.pathExact(taskPattern, y);
                byte[] yInB = TermPaths.pathExact(beliefPattern, y);
                if ((yInT != null || yInB != null)) {
                    if (xInT != null && xInB != null) {
                        if (xInB.length < xInT.length) xInT = null;
                        else xInB = null; //erase longer path
                    }
                    if (yInT != null && yInB != null) {
                        if (yInB.length < yInT.length) yInT = null;
                        else yInB = null; //erase longer path
                    }
                    return ConstraintAsPremisePredicate.the(cc, xInT, xInB, yInT, yInB);
                }
            }


        } else if (cc instanceof UnaryConstraint) {
            byte[] xInT = TermPaths.pathExact(taskPattern, x);
            byte[] xInB = TermPaths.pathExact(beliefPattern, x);
            if (xInT != null || xInB != null) {
                if (xInT != null && xInB != null) {
                    if (xInB.length < xInT.length) xInT = null;
                    else xInB = null; //erase longer path
                }
                return ConstraintAsPremisePredicate.the(cc, xInT, xInB, null, null);
            }

        }

        return null;
    }

    private static boolean isMirror(PREDICATE<Deriver> x, PREDICATE<Deriver> y) {
        if (x instanceof NegPredicate ^ y instanceof NegPredicate)
            return false;
        if (!(x.unneg() instanceof ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate))
            return false;
        if (!(y.unneg() instanceof ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate))
            return false;
        return ((ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate) x.unneg()).isMirror((ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate) y.unneg());
    }

    private static boolean separateBits(int struct) {
        int bc = Integer.bitCount(struct);
        return bc >= 2 && bc <= separateBitsMax;
    }

    private static <X> PREDICATE<X> intern(PREDICATE<X> y) {
        PREDICATE<X> z = preds.putIfAbsent(y, y);
        return z != null ? z : y;
    }

    private static boolean taskPolarity(Deriver d) {
        return d.premise.task().POSITIVE();
    }

    protected final MutableReaction taskEqualsBelief(Term taskPattern) {
        taskPattern(taskPattern);
        beliefPattern(taskPattern);
        return this;
    }

    /**
     * single premise
     */
    public final MutableReaction single(Term taskPattern, boolean b, boolean g, boolean q, boolean Q) {
        taskEqualsBelief(taskPattern);
        taskPunc(b, g, q, Q, false);
        hasBeliefTask(false);
        return this;
    }

    /**
     * match a command, ex: tasklink
     */
    public void taskCommand() {
        taskPunc(false, false, false, false, true);
    }

//    public void neqPN(Variable x, Variable y) {
//        eqPN(x, y, false);
//    }

    public void tasklink() {
        taskCommand();
        condition(premiseTaskLink);
    }

    public void noOverlap() {
        condition(NoOverlap);
    }

    public MutableReaction taskPattern(Term x) {

        this.taskPattern = patternify(x);

        if (taskPattern instanceof Neg || (!taskPattern.VAR_PATTERN() && !taskPattern.TASKABLE()))
            throw new TermException("invalid task pattern", taskPattern);

        return this;
    }

    public MutableReaction beliefPattern(Term x) {

        this.beliefPattern = Util.maybeEqual(patternify(x), taskPattern);

        if (beliefPattern instanceof Neg)
            throw new TermException("belief pattern can never be NEG", beliefPattern);

        return this;
    }

    public MutableReaction taskPunc(boolean belief, boolean goal, boolean question, boolean quest) {
        return taskPunc(belief, goal, question, quest, false);
    }

    public final MutableReaction taskPunc(byte... puncs) {
        if (puncs == null || puncs.length == 0) {
            //return; //no filtering
            throw new WTF();
        }

        return taskPunc(
                ArrayUtil.indexOf(puncs, BELIEF) != -1,
                ArrayUtil.indexOf(puncs, GOAL) != -1,
                ArrayUtil.indexOf(puncs, QUESTION) != -1,
                ArrayUtil.indexOf(puncs, QUEST) != -1
        );
    }

    public final MutableReaction taskPunc(boolean belief, boolean goal, boolean question, boolean quest, boolean command) {
        return taskPunc(new PuncMap(
                belief ? BELIEF : 0, goal ? GOAL : 0,
                question ? QUESTION : 0, quest ? QUEST : 0,
                command ? COMMAND : 0));
    }

    public final MutableReaction taskPunc(PuncMap m) {
        org.eclipse.collections.api.block.predicate.Predicate isPuncMap = x -> x instanceof PuncMap;

        _conditions.removeIf(isPuncMap); //remove any existing
        //TODO warn

//        if (pre.anySatisfy(isPuncMap))
//            throw new UnsupportedOperationException("puncmap already set: " + this + " " + pre);

        if (!m.all())
            condition(m); //add filter to allow only the mapped types
        return this;
    }

    /**
     * conjunction condition equality
     */
    public void ceq(Term xs, Term ys, boolean trueOrFalse) {
        ceqdeq(xs, true, ys, true, trueOrFalse);
    }

    /**
     * disjunction condition equality
     */
    public void deq(Term xs, Term ys, boolean trueOrFalse) {
        ceqdeq(xs, false, ys, false, trueOrFalse);
    }

    /**
     * conj/disj condition equality
     */
    public void ceqdeq(Term xs, boolean xc, Term ys, boolean yc, boolean trueOrFalse) {
        constrain((UnifyConstraint<Deriver.PremiseUnify>)
                new CeqConstraint(xs, xc, ys, yc).negIf(!trueOrFalse));
        if (trueOrFalse) {
            constrain(new RelationConstraint.StructureCoContained(
                    (Variable) xs.unneg(), (Variable) ys.unneg(),
                    //(Variable) xs.unnegIf(!xc || !yc), (Variable) ys.unnegIf(!xc || !yc),
                    CONJ.bit //incase INH in INH bundled, the && wont be present in INH only
                    //0
                    //!xc || !yc ? NEG.bit : 0
                    //CONJ.bit | NEG.bit
            ));
        }
    }

    public void eventConstraint(Term c, Term x, boolean exactOrUnifiable, boolean depolarized) {
        if (!(c instanceof Variable C))
            return;

        eventContainer(C);

        boolean xNeg = x instanceof Neg;
        assert (!depolarized || !xNeg);

        Term xu = x.unneg();
        if (!(xu instanceof Variable X))
            return;

        condable(X);


        if (exactOrUnifiable) {
            //EXACT
            constrain(new SubConstraint(C, X, Cond,
                    depolarized ? 0 :
                            (xNeg ? -1 : +1)));
            bigger(C, X);
        } else {
            //UNIFIABLE

            //assert (!depolarized);
            neq/*neqRoot*/(C, X);

            //TODO test more carefully, may be excluding positive cases
            biggerIfConstant(C, X);
            isNotAny(X, Variables);
        }
    }

    public final void eventContainer(Variable x) {
        iff(x, TermMatch.EVENT_CONTAINER);
    }

    public final void condOf(SubtermCondition cond, Variable x, Term y, boolean bipolar, boolean trueOrFalse) {
        condOf(cond, x, (Variable) (y.unneg()), y instanceof Neg, bipolar, trueOrFalse);
    }

    /**
     * bigger is necessary if CONJ_WITHOUT_ALL sub-events that must be matched individually
     */
    private void condOf(SubtermCondition cond, Variable x, Variable y, boolean yNeg, boolean bipolar, boolean trueOrFalse) {
        constrain(new SubConstraint<>(x, y, cond,
                bipolar ? 0 : (yNeg ? -1 : +1)), !trueOrFalse);

        if (trueOrFalse) {
            eventContainer(x);
            condable(y);


            /* TODO determine if true is 100% ok */
            boolean bigger =
                    //false;
                    true;
            if (bigger) {
                bigger(x, false, y, yNeg);
            } /*else
                neqPN(x, y);*/
        }
    }

    void neqRCom(Variable x, Variable y) {
        neq(x, y);
        constrain(new RelationConstraint.NotRecursiveSubtermOf(x, y));
    }

    public void containsRecursively(Variable x, Term y, boolean trueOrFalse) {
        Variable yV = (Variable) y.unneg();
        bigger(x, false, yV, y instanceof Neg); //TODO +1 margin if neg
        constrain((UnifyConstraint<Deriver.PremiseUnify>)
                new SubConstraint<>(x, yV, Recursive, y instanceof Neg ? -1 : +1).negIf(!trueOrFalse));
    }

    protected void hasBeliefTask(boolean trueOrFalse) {
        int nextArity = trueOrFalse ? 2 : 1;
        if (arity != nextArity) {
            if (arity != 0)
                throw new WTF("premise arity already set: " + arity + " is not " + nextArity);
            this.arity = nextArity;
        }
    }

    private void guard(boolean taskOrBelief) {
        guard(taskOrBelief, null, null);
    }

    private void guard(boolean taskOrBelief, @Nullable Term /* Compound */ x, @Nullable ByteArrayList p) {

        int depth;
        byte[] pp;

        Term root = taskOrBelief ? taskPattern : beliefPattern;

        if (p == null) {
            if (root.VAR_PATTERN())
                return; //top-level VAR_PATTERN, nothing to test
            else {
                //start descent
                pp = ArrayUtil.EMPTY_BYTE_ARRAY;
                assert (x == null);
                x = root;
                depth = 0;
            }
        } else {
            //continue descent
            pp = p.toArray();
            depth = pp.length;
        }


        PremiseTermAccessor r = taskOrBelief ? TaskTermAccessor : BeliefTermAccessor;
        Function<Deriver, Term> accessor = depth == 0 ? r : r.path(pp);

        guardStruct(x, depth, accessor);
        guardVolMin(x, depth, accessor);

        Subterms xx = x.subtermsDirect();
        if ((xx.structure() & (~VAR_PATTERN.bit)) == 0)
            return; //just pattern vars beneath
        int n = xx.subs();

        if (x.COMMUTATIVE()) {
            guardCommutiveSubterms(xx, depth, accessor);
        } else if (n > 0) {
            if (p == null) p = new ByteArrayList(4);

            for (byte i = 0; i < n; i++) {
                if (i == 0)
                    p.add((byte) 0);
                else
                    p.set(depth, i);
                Term y = xx.sub(i);
                if (!y.VAR_PATTERN())
                    guard(taskOrBelief, y, p);
            }
            p.removeAtIndex(p.size() - 1);//popByte();
        }
    }

    private void guardCommutiveSubterms(Subterms x, int depth, Function<Deriver, Term> accessor) {
        //TODO fully recursive
        //commutive term
        //proceed differently because of indeterminacy //TODO any other clues to find?
        //any properties shared by ALL elements of the commutive term can be tested as a precondition

        int structSurface = x.structureSurface(); //x.sub(0).opBit(); boolean sameOp = opBit == x.sub(1).opID();
        if ((structSurface & Variables) == 0) { //if all are non-variables

            int structCommon = x.structureIntersection(); //x.sub(0).opBit(); boolean sameOp = opBit == x.sub(1).opID();
            int volMin = x.volMin();

            SortedSet<TermMatch> matchers = new TreeSet<>(TermMatch.paramComparator);

            if (structSurface != 0)
                matchers.add(TermMatch.Is.is(structSurface));
            if (((structCommon & Variables) == 0) && structCommon != structSurface)
                matchers.add(new TermMatch.HasAllStruct(structCommon));
            if (volMin > 1)
                matchers.add(new TermMatch.VolMin(volMin));

            //TODO A eqNeg B?

            if (!matchers.isEmpty()) {
                condition(new TermMatching<>(
                        new TermMatch.TermMatcherSubterms(TermMatch.AND(matchers)),
                        accessor, depth
                ));
            }
        }

    }

    private void guardVolMin(Term t, int depth, Function<Deriver, Term> accessor) {
        //volume constraint
        int v = t.volume();
        if (v > 1)
            condition(new TermMatching<>(new TermMatch.VolMin(v), accessor, depth));
    }

    private void guardStruct(Term t, int depth, Function<Deriver, Term> accessor) {
        //structure constraint

        Op o = t.op();
        assert (o != VAR_PATTERN);
        int xs = t.structureSubs() & (~VAR_PATTERN.bit);

        condition(new TermMatching<>(TermMatch.Is.is(o), accessor, depth));

        if (xs != 0 && Integer.bitCount(xs) > 2)
            condition(new TermMatching<>(new TermMatch.HasAllStruct(xs), accessor, depth/*+1*/));
    }

//    public final ReactionBuilder complexityMin(Term x, int min) {
//        volMin(x, min);
//        return iff(x, new TermMatcher.ComplexityMin(min));
//    }

    public void eqPN(Variable x, Variable y, boolean trueOrFalse) {
        if (trueOrFalse) {
            constrain(new EqualPosOrNeg(x, y));
        } else {
            neq(x, y);
            neqNeg(x, y);
        }
    }

    public MutableReaction neq(Variable x, Term y) {

        if (y instanceof Neg && y.unneg() instanceof Variable) {
            neqNeg(x, (Variable) (y.unneg()));
        } else if (y instanceof Variable) {
            constrain(new NotEqual(x, (Variable) y));
        } else {
            iff(x, new TermMatch.Equals(y), false);
        }

        return this;
    }

    private void neqNeg(Variable x, Variable y) {
        constrainNeg(new EqualNeg(x, y));
    }

    public void neqRoot(Variable x, Variable y) {
        constrain(new RelationConstraint.NotEqualRoot(x, y));
    }

    public void bigger(Variable x, Variable y) {
        bigger(x, false, y, false);
    }

    public void bigger(Variable x, boolean xNeg, Variable y, boolean yNeg) {
        if (xNeg && yNeg)
            xNeg = yNeg = false; //either or neither, not both

        if (xNeg == yNeg)
            neq(x, y);
        constrain(new VolumeCompare(x, y, false, +1 /* X > Y */, xNeg, yNeg));
    }

    public MutableReaction biggerIfConstant(Variable x, Variable y) {
        //TODO dangerous, check before using
        return constrain(new VolumeCompare(x, y, true, +1, false, false));
    }
    public final MutableReaction condition(PREDICATE<Deriver> x) {
        if (NAL.DEBUG && _conditions.contains(x.neg()))
            throw new WTF();//contradiction
        if (!_conditions.add(x)) {
            if (NAL.DEBUG)
                logger.warn("adding duplicate condition {}\n{}\n{}", x, tag, source);
        }
        return this;
    }

    public final MutableReaction constrain(UnifyConstraint<Deriver.PremiseUnify> x) {
        //TODO test for presence of contradiction: --x
        if (!_constraints.add(x)) {
            if (NAL.DEBUG)
                logger.warn("adding duplicate constraint {}\n{}\n{}", x, tag, source);
        }
        return this;
    }

    @Deprecated
    public final MutableReaction constrain(UnifyConstraint<Deriver.PremiseUnify> e, boolean neg) {
        return constrain((UnifyConstraint<Deriver.PremiseUnify>) e.negIf(neg));
    }

    @Deprecated
    public final MutableReaction constrainNeg(UnifyConstraint<Deriver.PremiseUnify> e) {
        return constrain((UnifyConstraint<Deriver.PremiseUnify>) e.neg());
    }

    protected MutableReaction iff(Term x,
                                  BiConsumer<byte[], byte[]> preDerivationExactFilter,
                                  @Nullable Supplier<UnifyConstraint> ifNotPre
    ) {

        byte[] pt = TermPaths.pathExact(taskPattern, x);
        byte[] pb = TermPaths.pathExact(beliefPattern, x);// : null;
        if (pt == null && pb == null) {
            if (ifNotPre != null)
                constrain(ifNotPre.get());
        } else {
            if ((pt != null) && (pb != null)) {
                //only need to test one. use shortest path
                if (pb.length < pt.length)
                    pt = null;
                else
                    pb = null;
            }

            preDerivationExactFilter.accept(pt, pb);
        }
        return this;
    }

    public final MutableReaction iff(Term x, TermMatch m) {
        return iff(x, m, true);
    }

    public final MutableReaction iffNot(Term x, TermMatch m) {
        return iff(x, m, false);
    }

    public final MutableReaction volMin(Term x, int min) {
        return iff(x, new TermMatch.VolMin(min));
    }

    public final MutableReaction volMax(Term x, int max) {
        if (max < Integer.MAX_VALUE)
            iff(x, new TermMatch.VolMax(max));
        return this;
    }

    protected final MutableReaction volMaxMargin(Term x, int margin) {
        condition(new VolMaxMargin(x, margin));
        return this;
    }

    public final MutableReaction taskVol(int min, int max) {
        return vol(taskPattern, min, max);
    }

    public final MutableReaction vol(Term x, int min, int max) {
        volMin(x, min);
        volMax(x, max);
        return this;
    }

    public final MutableReaction ifNot(Term x, TermMatch m) {
        return iff(x, m, false);
    }

    public final MutableReaction iff(Term x, TermMatch m, boolean trueOrFalse) {
        return iff(x, m, trueOrFalse, true);
    }

    /**
     * if x is SETe, then applies to all its subterms
     */
    public final void iffThese(Term x, TermMatch m, boolean trueOrFalse) {
        if (x.SETe()) {
            for (Term xx : (Subterms) x)
                iff(xx, m, trueOrFalse);
        } else
            iff(x, m, trueOrFalse);
    }

    public MutableReaction iff(Term x, TermMatch m, boolean trueOrFalse, boolean constraintIfNotPre) {
        return iff(x, new ReactionBuilderPathMatch(m, trueOrFalse),
                (constraintIfNotPre ? () -> m.constraint((Variable) x, trueOrFalse) : null));
    }

    /**
     * cost-sorted array of constraint enable procedures, bundled by common term via CompoundConstraint
     */
    private UnifyConstraint<Deriver.PremiseUnify>[] reduce(SortedSet<UnifyConstraint<Deriver.PremiseUnify>> x) {

        int n = x.size();
        if (n == 0)
            return UnifyConstraint.EmptyUnifyConstraints;

        Stream<UnifyConstraint<Deriver.PremiseUnify>> cc = x.stream();

        //TODO move this to a final step, after having folded common predicates
        if (NAL.derive.REMAING_AMONG_REDUCER && n > 1) {
            //eliminate local subsumptions
            cc = cc.filter(new UnifyConstraintReducer(x));
        }

        Lst<PREDICATE<Deriver>> p = new Lst<>();

        UnifyConstraint<Deriver.PremiseUnify>[] uc = UnifyConstraint.the(cc.filter(c -> {
            PREDICATE<Deriver> d = predicateFromConstraint(c, taskPattern, beliefPattern);
            if (d != null) {
                if ((d.unneg() instanceof ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate) && c instanceof RelationConstraint)
                    p.add(d); //save for filtering in next stage
                else
                    condition(d);
                return false;
            }
            return true;
        }));


        if (!p.isEmpty()) {
            p.sortThis(); //canonical ordering
            int sn = p.size();
            main:
            for (int i = 0; i < sn - 1; i++) {
                var I = p.get(i);
                if (I == null) continue;

                for (int j = i + 1; j < sn; j++) {
                    var J = p.get(j);
                    if (J == null) continue;
                    if (I instanceof NegPredicate != J instanceof NegPredicate)
                        continue;
                    if (I instanceof NegPredicate) {
                        I = (PREDICATE<Deriver>) I.unneg();
                        J = (PREDICATE<Deriver>) J.unneg();
                    }

                    if (((ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate) I).symmetric() && isMirror(I, J)) {
                        p.setNull(j);
                        break;
                    }
                }

            }

            p.removeNulls();
            p.forEach(this::condition);
        }

        return uc;
    }


    public final MutableReaction isUnneg(Term x, int structAny, boolean trueOrFalse) {
        return iff(x, new TermMatch.IsUnneg(structAny), trueOrFalse);
    }

    public final MutableReaction is(Term x, Op o) {
        return isAny(x, o.bit);
    }

    public final MutableReaction hasNot(Term x, Op... o) {
        return hasAny(x, Op.or(o), false);
    }

    public final MutableReaction isNot(Term x, Op o) {
        return isNotAny(x, o.bit);
    }

    public final MutableReaction isAny(Term x, Op... ops) {
        return isAny(x, or(ops));
    }

    public final MutableReaction isAny(Term x, int struct) {
        return isAny(x, struct, false);
    }

    public final MutableReaction isNotAny(Term x, Op... o) {
        return isNotAny(x, or(o));
    }

    public MutableReaction isNotAny(Term x, int struct) {

        if (separateBits(struct)) {
            //NOT ANY = must test all, so can be decomposed into components
            int v = values().length;
            for (int i = 0; i < v; i++) {
                int mask = 1 << i;
                if ((struct & mask) != 0) isAny(x, mask, true);
            }
        } else
            isAny(x, struct, true);

        return this;
    }

    public MutableReaction isAny(Term x, int struct, @Deprecated boolean negated) {
        iff(x, TermMatch.Is.is(struct), !negated);
        return this;
    }

    public void condable(Variable X) {
        isAny(X, Condables);
    }

    public MutableReaction hasAny(Term x, Op o) {
        return hasAny(x, o, true);
    }

    public MutableReaction hasAny(Term x, int structure) {
        return hasAny(x, structure, true);
    }

    public MutableReaction hasAny(Term x, Op o, boolean trueOrFalse) {
        return hasAny(x, o.bit, trueOrFalse);
    }

    public MutableReaction hasAny(Term x, int structure, boolean trueOrFalse) {
        return hasAnyOrAll(x, structure, trueOrFalse, true);
    }

    public MutableReaction hasAll(Term x, int structure, boolean trueOrFalse) {
        return hasAnyOrAll(x, structure, trueOrFalse, false);
    }

    private MutableReaction hasAnyOrAll(Term x, int struct, boolean trueOrFalse, boolean anyOrAll) {
        if (!anyOrAll) {
            if (separateBits(struct)) {
                //NOT ANY = must test all, so can be decomposed into components
                int v = values().length;
                for (int i = 0; i < v; i++) {
                    int mask = 1 << i;
                    if ((struct & mask) != 0) _hasAnyAll(x, mask, trueOrFalse, false);
                }
                return this;
            }
        }

        return _hasAnyAll(x, struct, trueOrFalse, anyOrAll);
    }

    MutableReaction _hasAnyAll(Term x, int struct, boolean trueOrFalse, boolean anyOrAll) {
        return iff(x, anyOrAll ? new StructureMatcher.HasAny(struct) : new StructureMatcher.HasAll(struct), trueOrFalse);
    }

    public void taskPolarity(boolean b) {
        condition((PREDICATE<Deriver>) taskPos.negIf(!b));
    }

    public void taskEternal(boolean b) {
        condition((PREDICATE<Deriver>) taskEternal.negIf(!b));
    }

    protected void taskInput(boolean b) {
        condition((PREDICATE<Deriver>) taskInput.negIf(!b));
    }

    @Override
    protected java.util.Set<PREDICATE<Deriver>> compileConditions() {
        commit();
        final java.util.Set<PREDICATE<Deriver>> y = _conditions.stream().map(MutableReaction::intern).collect(toSet());
        _conditions = null;
        return y;
    }

    /**
     * expand mirror's
     */
    private SortedSet<UnifyConstraint<Deriver.PremiseUnify>> mirror(MutableSet<UnifyConstraint<Deriver.PremiseUnify>> x) {
        SortedSet<UnifyConstraint<Deriver.PremiseUnify>> y = null;
        for (UnifyConstraint<Deriver.PremiseUnify> c : x) {
            if (c instanceof RelationConstraint) {
                if (y == null) y = new TreeSet<>();
                y.add(((RelationConstraint<Deriver.PremiseUnify>) c).mirror());
            }
        }
        if (y == null)
            return x instanceof SortedSet ? ((SortedSet) x) : new TreeSet<>(x);
        else {
            y.addAll(x);
            return y;
        }
    }

    protected void commit() {

        Term t = this.taskPattern, b = this.beliefPattern;
        if (t instanceof Bool || b instanceof Bool)
            throw new TermException("Bool task or belief pattern");


        guard(true);

//        if (t.equalsRoot(b)) {
//            pre.add(TaskBeliefEqualRoot);
//        } else {
        guard(false);
//        }

        /* constraints must be computed BEFORE preconditions as some constraints may be transformed into preconditions */
        //int cBefore = constraintSet.size();
        constraints = reduce(mirror(_constraints));
        _constraints = null; //prevent further additions

        //float remainPct = constraints.length/((float)cBefore); if (remainPct > 0) System.out.println(remainPct);

        switch (arity) {
            case 1 -> condition(HasBelief.SINGLE);
            case 2 -> condition(HasBelief.DOUBLE);
            default -> { /* either. */ }
        }
    }

    public final String tag() {
        return tag;
    }

    public final MutableReaction tag(@Nullable String tag) {
        this.tag = tag;
        return this;
    }

    @Override
    public String toString() {
        return source;
    }

    public void compile() {
        conditions();
    }

    @Deprecated
    public static final class VolMaxMargin extends PREDICATE<Deriver> {
        private final int margin;

        @SuppressWarnings({"unused", "FieldCanBeLocal"}) //TODO
        private final Term pattern;

        VolMaxMargin(Term pattern, int margin) {
            super($.func("volMaxMargin", $.the(margin)));
            assert (margin >= 1);
            this.pattern = pattern;
            this.margin = margin;
        }

        @Override
        public float cost() {
            return 0.03f;
        }

        @Override
        public boolean test(Deriver d) {
            return (d.premise.from().volume() <= d.volMax - margin);
        }

    }

    static class UnifyConstraintReducer implements Predicate<UnifyConstraint<Deriver.PremiseUnify>> {

        final UnifyConstraint[] copy;
        int remain;

        UnifyConstraintReducer(Set<UnifyConstraint<Deriver.PremiseUnify>> x) {
            copy = x.toArray(UnifyConstraint.EmptyUnifyConstraints);
            remain = copy.length;
        }

        @Override
        public boolean test(UnifyConstraint<Deriver.PremiseUnify> c) {
            if (remain < 2 || c.remainAmong(copy))
                return true;
            else {
                copy[ArrayUtil.indexOfInstance(copy, c)] = null;
                remain--;
                return false;
            }
        }
    }

    class ReactionBuilderPathMatch implements BiConsumer<byte[], byte[]> {
        private final TermMatch m;
        private final boolean trueOrFalse;

        public ReactionBuilderPathMatch(TermMatch m, boolean trueOrFalse) {
            this.m = m;
            this.trueOrFalse = trueOrFalse;
        }

        @Override
        public void accept(byte[] pathInTask, byte[] pathInBelief) {


            if (pathInTask != null)
                iff(true, pathInTask);
            if (pathInBelief != null)
                iff(false, pathInBelief);

        }

        private void iff(boolean taskOrBelief, byte[] path) {
            PREDICATE<Deriver> m = new TermMatching<>(this.m, TaskOrBelief(taskOrBelief).path(path), path.length);
            condition(trueOrFalse ? m : m.neg());
        }
    }

    protected static final PREDICATE<Deriver> NonImages = new PREDICATE<Deriver>(Atomic.atom("NonImages")) {
        @Override
        public boolean test(Deriver deriver) {
            Premise p = deriver.premise;
            return Image.imageNormalized(p.from()) && Image.imageNormalized(p.to());
        }
    };

}