package nars.derive.reaction;

import jcog.data.list.Lst;
import nars.$;
import nars.Term;
import nars.action.Action;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.term.Termed;
import nars.term.control.PREDICATE;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

import static nars.Op.IMPL;
import static nars.Op.SETe;

/**
 * an intermediate representation of a premise rule
 * with fully expanded opcodes
 *
 * instantiated for each NAR, because it binds the conclusion steps to it
 *
 * anything non-NAR specific (static) is done in PremiseDeriverSource as a
 * ready-made template to make constructing this as fast as possible
 * in potentially multiple NAR instances later
 *
 * TODO implementations:
 *      --UI event/submission/warnings
 *      --experience replay
 */
public abstract class Reaction /*extends ProxyCompound*/ implements Termed {


    Set<PREDICATE<Deriver>> condition;

    @Nullable
    public String tag;

    public Object src;

    protected Reaction() {

    }

    public Reaction(Set<PREDICATE<Deriver>> condition, Object src, String tag) {
        //super((Compound)id(condition, src));

        this.src = src;
        this.tag = tag;
        this.condition = condition;
    }

    protected static Term id(Set<PREDICATE<Deriver>> c, Object src) {
        Term x = SETe.the(c);
        Term y = src instanceof Term ? ((Term) src) : $.identity(src);
        return IMPL.the(x, y);
    }


    public abstract Action action(Cause<Reaction> why);
    public abstract Class<? extends MutableReaction> type();

    protected final Set<PREDICATE<Deriver>> conditions() {
        Set<PREDICATE<Deriver>> c = this.condition;
        if (c == null)
            c = this.condition = compileConditions();
        return c;
    }

    abstract protected Set<PREDICATE<Deriver>> compileConditions();

    public List<PREDICATE<Deriver>> conditionsSortedByCost() {
        Lst<PREDICATE<Deriver>> l = new Lst(conditions());
        l.sort(PREDICATE.CostIncreasing);
        return l;
    }

}