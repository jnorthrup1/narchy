package nars.derive.reaction;

import jcog.Str;
import jcog.TODO;
import jcog.Util;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.action.Action;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.op.DeriverFork;
import nars.derive.util.Actions;
import nars.term.control.AND;
import nars.term.control.FORK;
import nars.term.control.PREDICATE;
import nars.term.control.SWITCH;

import java.io.PrintStream;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.util.function.Consumer;

import static jcog.io.Serials.jsonNode;
import static nars.NAL.derive.ACTION_METHODHANDLE;

/**
 * compiled derivation rules. can be shared by multiple Derivers
 * what -> can
 * TODO subclass to Weighted deriver runner; and make a non-weighted subclass
 */
public class ReactionModel {


    public final PREDICATE<Deriver> what;

    /**
     * the causes that this is responsible for, ie. those that may be caused by this
     */
    /*@Stable*/ public final Cause[] cause;

    /*@Stable*/ public final Action[] how;

    private final Consumer<Deriver> run;
    private final MethodHandle[] howMethods;

    protected ReactionModel(PREDICATE<Deriver> what, Action[] how) {
        this.what = what;
        this.how = how; assert (how.length > 0);
        this.cause = Util.map(x -> x.why, new Cause[how.length], how);
        this.howMethods = ACTION_METHODHANDLE ? methodHandles(how) : null;

        this.run =
            NAL.derive.RUNNER_METHODHANDLE ?
                runnerMH() :
                runnerInvoke();
                //TODO runnerBytecode()
    }


    public ConditionAnalysis analyze() {
        return new ConditionAnalysis(what);
    }


    public ReactionModel print() {
        return print(System.out);
    }

    public ReactionModel print(PrintStream p) {
        print(p, 0);
        return this;
    }

    public void print(PrintStream p, int indent) {
        print(what, p, indent);
    }

    protected static void print(Object x, PrintStream out, int indent) {

        Str.indent(indent);

        if (x instanceof ReactionModel r) {

            r.print(out, indent);

        } else if (x instanceof Actions b) {

            out.println(b.getClass().getSimpleName().toLowerCase() + " {");
            for (Action h : b.actions)
                print(h, out, indent+2);

            Str.indent(indent);out.println("}");


        } else if (x instanceof Action a) {

            //out.println(a.why.id + " ==> {");
            Object aa;
            // + ((PremisePatternAction.TruthifyDeriveAction) a).unify;
            //TODO
            //                out.println(((DirectPremiseUnify)x).taskPat + ", " + ((DirectPremiseUnify)x).beliefPat + " ==> {");
            //                print(((DirectPremiseUnify)x).taskify, out, indent + 2);
            //                Texts.indent(indent);
            //                out.println("}");
//            if (a instanceof PatternReactionBuilder.PatternReaction.DeriveTaskAction) {
//                PatternReactionBuilder.PatternReaction.DeriveTaskAction td = (PatternReactionBuilder.PatternReaction.DeriveTaskAction) a;
//                td.source()
//                aa = td.taskPattern + ", " + td.beliefPattern + " |- " + td.taskify + " " + td.truthify; //Arrays.toString(td.constraints) + " ...";
//            } else
                aa = a.toString();

            print(aa, out, indent /*+ 2*/);

            //Texts.indent(indent);out.println("}");

        } else if (x instanceof AND) {
            out.println("and {");
            ((AND) x).subStream().forEach(b->
                    print(b, out, indent + 2)
            );
            Str.indent(indent);
            out.println("}");
        } /*else if (p instanceof Try) {
            out.println("eval {");
            Try ac = (Try) p;
            int i = 0;
            for (PrediTerm b : ac.branches) {
                TermTrie.indent(indent + 2);
                out.println(i + ":");
                print(b, out, indent + 4);
                i++;
            }
            TermTrie.indent(indent);
            out.println("}");
        } */ else if (x instanceof FORK) {

            out.println("fork {");
            for (PREDICATE b : ((FORK) x).branch)
                print(b, out, indent + 2);
            Str.indent(indent);
            out.println("}");

        } else if (x instanceof SWITCH) {
            throw new TODO();
        } else {
            out.print( /*Util.className(p) + ": " +*/ x);
            out.println();
        }


    }

    /** by MethodHandles */
    private Consumer<Deriver> runnerMH() {
        return what.run();
    }

    private Consumer<Deriver> runnerInvoke() {
        return what::test;
    }

    /** TODO wrap runnerMH/runnerInvoke in this */
    public PremiseRunner run() {
        return howMethods != null ?
            new PremiseRunnerMethodHandles(howMethods)
            :
            new PremiseRunnerInvoke(how);
//        } else
//            return new CachedPremiseRunner(d, cache);
    }


    public static class ConditionAnalysis implements Consumer<PREDICATE<Deriver>> {
        public int forks;
        public long forkBranchSum;
        public int and;
        public long andConditionSum;

        public ConditionAnalysis(PREDICATE<Deriver> c) {
            accept(c);
        }

        @Override public String toString() {
            return jsonNode(this).toString();
        }

        @Override
        public void accept(PREDICATE<Deriver> x) {
            if (x instanceof AND) {
                and++;
                int c = 0;
                for (PREDICATE xx : ((AND<?>)x).conditions()) {
                    accept(xx);
                    c++;
                }
                andConditionSum += c;
            } else if (x instanceof DeriverFork) {
                forks++;
                int b = 0;
                for (PREDICATE xx : ((DeriverFork)x).branches()) {
                    accept(xx);
                    b++;
                }
                forkBranchSum += b;
            } else if (x instanceof FORK) {
                forks++;
                int b = 0;
                for (PREDICATE xx : ((FORK)x).branch) {
                    accept(xx);
                    b++;
                }
                forkBranchSum += b;
            } else {
                //throw new TODO();
            }
        }
    }

    private static final MethodType T = MethodType.methodType(void.class, Deriver.class);

    private static MethodHandle[] methodHandles(Action[] how) {
        return Util.arrayOf(i -> how[i].method().asType(T), new MethodHandle[how.length]);
    }

    public abstract static class PremiseRunner {

        public void run(Deriver d) {
            d.predicateMemoizations.clear();
            d.howsCount = 0;
            d.model.run.accept(d);
            runAll(d.hows, d.howsCount, d);
        }

        private void runAll(short[] s, int c, Deriver d) {
            if (c == 0) return;

            if (c > 1) ArrayUtil.shuffle(s, c, d.rng);

            int o = d.howOffset;
            for (int i = 0; i < c; i++)
                run(s[i] - o, d);
        }

        protected abstract void run(int i, Deriver d);

    }

    private static final class PremiseRunnerInvoke extends PremiseRunner {
        private final Action[] how;

        private PremiseRunnerInvoke(Action[] how) {
            this.how = how;
        }

        @Override protected void run(int x, Deriver d) {
            how[x].test(d);
        }

    }
    private static final class PremiseRunnerMethodHandles extends PremiseRunner {

        private final MethodHandle[] howMethods;

        PremiseRunnerMethodHandles(MethodHandle[] h) {
            howMethods = h;
        }

        @Override protected void run(int x, Deriver d) {
            try {
                howMethods[x].invokeExact(d);
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

    }

}


//    private Consumer<Deriver> runnerCompile() {
//        final String className = ReactionModel.class.getSimpleName() + hashCode();
//
//        StringBuilder source = new StringBuilder(1024);
//        source.append(
//            "import " + Consumer.class.getName() + ";" +
//            "import " + Deriver.class.getName() + ";" +
//            "public class " + className + " implements Consumer<Deriver> {" +
//                "@Override public void accept(Object d) {" +
//                    "System.out.println(\"x\");" +
//                "}" +
//            "}"
//        );
//
//
//        try {
//            final ISimpleCompiler cc = CompilerFactoryFactory.getDefaultCompilerFactory().newSimpleCompiler();
//            cc.cook(source.toString());
//
//            ClassLoader cll = cc.getClassLoader();
//
//            Class<?> cl = cll.loadClass(className);
//            return (Consumer<Deriver>) cl.getConstructor().newInstance();
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

//    private Consumer<Deriver> runnerCompile() {
//        // Start building the class.
//        // Add the main method.
//        // Compose the equivalent of this java code:
//        //     System.out.println("Hello, world!");
//        // We don't need to preverify simple code that doesn't have
//        // special control flow. It works fine without a stack map
//        // table attribute.
//        // Retrieve the final class.
//        final String className = ReactionModel.class.getSimpleName() + BinTxt.uuid128();
//
//        ProgramClass c = new ClassBuilder(
//                VersionConstants.CLASS_VERSION_14,
//                AccessConstants.PUBLIC,
//                className,
//                ClassConstants.NAME_JAVA_LANG_OBJECT)
//                .addInterface(Consumer.class.getName())
//                .addMethod(AccessConstants.PUBLIC,  "<init>", "()V")
//                .addMethod(
//                        AccessConstants.PUBLIC |
//                                AccessConstants.STATIC,
//                        "main",
//                        "([Ljava/lang/String;)V",
//                        50,
//
//                        // Compose the equivalent of this java code:
//                        //     System.out.println("Hello, world!");
//                        code -> code
//                                .getstatic("java/lang/System", "out", "Ljava/io/PrintStream;")
//                                .ldc("x")
//                                .invokevirtual("java/io/PrintStream", "println", "(Ljava/lang/String;)V")
//                                .return_())
//
//                // We don't need to preverify simple code that doesn't have
//                // special control flow. It works fine without a stack map
//                // table attribute.
//
//                // Retrieve the final class.
//                .getProgramClass();
//
//        DynBytes o = new DynBytes(16 * 1024);
//        new ProgramClassWriter(o).visitProgramClass(c);
//
//
//        Map<String, byte[]> m = new UnifriedMap();
//        m.put(className, o.arrayCompactDirect());
//
//        try {
//            final Class<?> cl = new ByteArrayClassLoader(getClass().getClassLoader(), m).loadClass(className);
//            return (Consumer<Deriver>) cl.getConstructor().newInstance();
//        } catch (Throwable e) {
//            throw new RuntimeException(e);
//        }
//    }