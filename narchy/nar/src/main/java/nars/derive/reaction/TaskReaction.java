package nars.derive.reaction;

import jcog.Log;
import jcog.exe.flow.Feedback;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.pri.DefaultBudget;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.SerialTask;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

/**
 * responds with zero or more resolved premises in response to input task
 *
 * ex: replies answers to question tasks
 * TODO  'AnswerQuestionsFromProlog' and 'AnswerQuestionsFromExhaustiveMemorySearch'
 */
public abstract class TaskReaction extends NativeReaction {

	@Deprecated protected TaskReaction() {
		this(true,true,true,true);
	}

	protected TaskReaction(boolean b, boolean g, boolean q, boolean Q) {
		single(b, g, q, Q);
	}

//	/** how to route the generated task; either as a continuation premise, or as a task to be stored. */
//	protected boolean store = true;

	private boolean log = false;

	private static final Logger logger = Log.log(TaskReaction.class);

	/** post-process priority of generated task, ex: complexity discount */
	public static void priPost(NALTask x, Deriver d) {
		double factor = ((DefaultBudget) (d.focus.budget)).simple(x.term(), d);
		x.priMul((float) factor);
	}

	public final TaskReaction log(boolean b) {
		this.log = b;
		return this;
	}

	protected final void react(@Nullable NALTask y, Deriver d, Cause<Reaction> why) {
		if (y != null) {
			reacting(y, d);

			//assume that y's why has been set already, just need to append this transform why
			Premise p = d.premise;
			if (d.add(y)) {
				if (log) logger.info("{} => {} ({})", p, y, why.id);
				return;
			}
		}

		Feedback.is("derive.NALTask.invalid");
	}

	/** subclasses can impl post-processing effects to generated tasks */
	protected void reacting(NALTask x, Deriver d) {
		if (!(x instanceof SerialTask) && discountComplexity()) {
			if (x.uncreated())
				priPost(x, d);
		}
	}

	protected boolean discountComplexity() {
		return false;
	}

}