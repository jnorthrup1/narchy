package nars.derive.util.memoize;

import jcog.data.bit.MetalBitSet;
import jcog.data.list.Lst;
import jcog.data.map.UnifriedMap;
import nars.Term;
import nars.derive.Deriver;
import nars.term.ProxyCompound;
import nars.term.control.AND;
import nars.term.control.FORK;
import nars.term.control.NegPredicate;
import nars.term.control.PREDICATE;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectIntHashMap;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public enum MemoizedPredicate {
    ;

    public static PREDICATE<Deriver> compileMemoize(PREDICATE<Deriver> t) {
        ObjectIntHashMap<PREDICATE<Deriver>> condCount = new ObjectIntHashMap<>(1024);
        t.recurseTermsOrdered(x -> true, _x -> {
            if (_x instanceof PREDICATE && !(_x instanceof FORK) && !(_x instanceof AND)) {
                PREDICATE<Deriver> x = (PREDICATE<Deriver>) _x;

                //NegPredicate's  contents will be unwrapped in the recurse. dont double count them
                if (!(x instanceof NegPredicate)) {
                    if (Float.isFinite(x.cost()))
                        condCount.addToValue(x, 1);
                }
            }
            return true;
        }, null);
        condCount.values().removeIf(x -> x < 2);

        int n = condCount.size();
        if (n == 0) return t;

        ObjectIntHashMap<PREDICATE<Deriver>> condid = new ObjectIntHashMap<>(n);
        int ci = 0;
        for (PREDICATE<Deriver> k : condCount.keySet())
            condid.put(k, ci++);

        //TODO co-occurrence graph to find combineable conditions

        Map<Term, PREDICATE<Deriver>> cached = new UnifriedMap<>(condCount.size());
        condid.forEachKeyValue((c,id) -> cached.put(c, new MemoizedPredicate1(c, id)));

        return merge(t.transformPredicate(_x -> {
            boolean neg = (_x instanceof NegPredicate);
            Term x = neg ? ((NegPredicate) _x).p : _x;
            PREDICATE<Deriver> y = cached.get(((ProxyCompound) x).ref);
            return y != null ? ((PREDICATE<Deriver>) y.negIf(neg)) : _x;
        }));
        //TODO merge memoization reads within an AND for potential fast fail before doing any tests
    }

    private static PREDICATE<Deriver> merge(PREDICATE<Deriver> t) {
        return t.transformPredicate(x -> {
            if (!(x instanceof AND<Deriver> a)) return x;

            int as = a.subs();
            MetalBitSet b = null;
            {
                int c = 0;
                for (PREDICATE<Deriver> aa : a.conditions()) {
                    if (aa instanceof MemoizedPredicate1) {
                        if (b == null) b = MetalBitSet.bits(as);
                        b.set(c);
                    }
                    c++;
                }
                if (b == null || b.cardinality() <= 1) return x;
            }

            List<PREDICATE<Deriver>> j = new Lst<>(as - b.cardinality());
            Lst<MemoizedPredicate1> m = new Lst<>(b.cardinality());
            int c = 0;
            for (PREDICATE<Deriver> aa : a.conditions()) {
                if (b.test(c++))
                    m.add((MemoizedPredicate1) aa);
                else
                    j.add(aa);
            }
            m.sort(increasingCost);
            j.add(0, new MemoizedPredicateN(m)); //add at beginning so that order is maintained
            return AND.the(j);
        });
    }

    private static final Comparator<MemoizedPredicate1> increasingCost = (ma,mb)-> PREDICATE.CostIncreasing.compare(ma.test, mb.test);
}