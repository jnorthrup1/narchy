package nars.derive.util.memoize;

import nars.$;
import nars.Term;
import nars.derive.Deriver;
import nars.term.atom.Atomic;
import nars.term.control.PREDICATE;

import java.util.List;

final class MemoizedPredicateN extends PREDICATE<Deriver> {

    /**
     * discount because of possible early failure
     */
    private static final float COST_FACTOR = 1.0f;

    private final int[] slots;
    private final float cost;
    private final PREDICATE<Deriver>[] cond;

    private static final Atomic MEMOIZED_N = Atomic.the(MemoizedPredicateN.class.getSimpleName());

    MemoizedPredicateN(List<MemoizedPredicate1> m) {
        super($.p(MEMOIZED_N, $.p(m.stream().map(x -> x.ref).toArray(Term[]::new))));

        int s = 0;
        int n = m.size();
        assert (n <= 32); //bitvector
        slots = new int[n];
        cond = new PREDICATE[n];
        float c = Float.POSITIVE_INFINITY;
        for (MemoizedPredicate1 ms : m) {
            slots[s] = ms.slot;
            cond[s] = ms.test;
            float cs = ms.cost();
            c = Math.min(c, cs);
            //c = Math.max(c, cs);
            s++;
        }
        //cost = cSum * COST_FACTOR;
        cost = c * COST_FACTOR;
    }

    @Override
    public float cost() {
        return cost;
    }

    @Override
    public boolean test(Deriver d) {
        return d.predicateMemoizations.memoizeVector(slots, d, cond);
    }
}