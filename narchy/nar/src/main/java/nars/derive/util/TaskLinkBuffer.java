package nars.derive.util;

import jcog.data.list.Lst;
import nars.derive.Deriver;
import nars.link.MutableTaskLink;

/** TODO */
public class TaskLinkBuffer {
    private final Lst<MutableTaskLink> taskLinkBuffer = new Lst<>();

    private void commitTaskLinks(Deriver d) {
        int bs = taskLinkBuffer.size();
        if (bs > 0) {
            if (bs > 1)
                taskLinkBuffer.sortThisByDouble(z -> -z.pri());
            taskLinkBuffer.clear(d::_link);
        }
    }

}