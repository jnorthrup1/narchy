package nars.derive.util;

import nars.NAR;
import nars.Term;
import nars.derive.Deriver;
import nars.task.NALTask;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import nars.truth.dynamic.DynTruth;

import static nars.NAL.truth.DYN_DT_DITHER;


public final class DeriverTaskify extends DynTaskify {

    private final Deriver d;

    public DeriverTaskify(DynTruth model, Deriver d, boolean beliefOrGoal, int capacity) {
        super(model, beliefOrGoal,
                DYN_DT_DITHER ? d.ditherDT() : 0,
                capacity);
        this.d = d;
    }

    public DeriverTaskify(DynTruth model, Deriver d, NALTask... components) {
        this(model, d, components[0].BELIEF() /* else: assume GOAL */, 0);
        this.setArray(components);
    }

//    public final DynTaskify ditherTruth() {
//        ditherTruth = true;
//        return this;
//    }

    @Override
    public final NAR nar() {
        return d.nar;
    }

    @Override
    public final double eviMin() {
        return d.eviMin;
    }

    @Override public final int volMax() { return d.volMax; }

    @Override
    public final Truth taskTruth(int component) {
        return items[component].truth();
    }

    @Override
    public boolean accept(Term object, long start, long end) {
        throw new UnsupportedOperationException();
    }

}