package nars.derive.util;

import nars.Task;
import nars.premise.NALPremise;
import nars.task.NALTask;
import nars.task.ProxyTask;
import org.jetbrains.annotations.Nullable;

public final class DerivedTask extends ProxyTask {

    private final Task parentTask;
    private final @Nullable Task parentBelief;

//    public DerivedTask(Term tc, byte punct, @Nullable Truth truth, long start, long end, long[] stamp, Deriver d) {
//        super(tc, punct, truth, start, end, stamp);
    public DerivedTask(NALTask y, NALPremise p) {
        super(y);

        this.parentTask = p.task().the();

        NALTask b = p.belief();
        this.parentBelief = b!=null ? b.the() : null;

        copyMeta(y);
    }

    @Override
    public NALTask the() {
        return this;
    }

    private Task parentTask() {
        return parentTask;
    }

    public final @Nullable Task parentBelief() {
        return parentBelief;
    }

    @Override
    public void proof(int indent, StringBuilder sb) {
        super.proof(indent, sb);

        //if (task instanceof DerivedTask) {
            Task pt = parentTask();
            if (pt != null)
                pt.proof(indent + 1, sb);
            Task pb = parentBelief();
            if (pb != null)
                pb.proof(indent + 1, sb);
        //}
    }
}