package nars.derive.impl;

import jcog.Is;
import jcog.Str;
import jcog.data.list.Lst;
import jcog.data.set.ArrayHashSet;
import jcog.pid.MiniPID;
import jcog.signal.anomaly.ewma.Ewma;
import jcog.signal.meter.Use;
import nars.NAR;
import nars.derive.Deriver;
import nars.derive.reaction.ReactionModel;
import nars.derive.reaction.Reactions;
import nars.derive.util.PremiseBag;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.link.TaskLink;
import nars.premise.Premise;
import org.HdrHistogram.Histogram;
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.DoubleSupplier;

public class BagDeriver extends Deriver {

    /** sharing the bag among threads can save memory but can become a point of contention if the bag is not wait-free */
    private static final boolean LOCAL_SHARED_BY_ALL_FOCUS_THREADS = false;

    private PremiseBag active;

    final ArrayHashSet<Premise> pending = new ArrayHashSet<>(new Lst<>(0, Premise.EmptyPremiseArray));



    public int iter = 128;

    /**
     * measures the tasklink-to-premise generation (fan-out) ratio.
     * ie. how many premises are formed by each tasklink seed.
     * used to calculate tasklink sampling to meet seedRatio
     *
     * resets to 1 when Focus changes.
     */
    private final Ewma explosivity = new Ewma();

    private transient int premiseCount;

    @Is("Air–fuel_ratio") public static float seedRatio =
        //1/3f;
        //1/4f;
        1/5f;
        //1/6f;
        //1/8f;
        //1/10f;
        //1/15f;
        //1/20f;
        //1/25f;
        //1/30f;
        //1/40f;


    /** bag sampling method.
     *  TODO contiguous may need to repeat if focus bag size < iters */
    static final boolean sampleIndividuallyOrContiguously = false;



    private static final Use profileCycle = new Use(BagDeriver.class + ".next", true);

    /** TODO auto-tune according to histogram measurement % error */
    private static final int profileCommitPeriod = 256;

    private static final AtomicBoolean profileMaintenance = new AtomicBoolean();
    static float profileRate = 0;
    static Consumer<Histogram> profiled;


    float capacityFactor = 2;

    float iterBatchGranularity =
            1;

    /** rate for periodic active bag clearing */
    private float activeClear = 1/100f;

    //0.333f;
//            0.9f;

    public BagDeriver(Reactions model, NAR n) {
        super(model, n);
    }

    public BagDeriver(ReactionModel model, NAR nar) {
        super(model, nar);
    }

    @Override
    protected void stopping(NAR nar) {
        active = null;
        super.stopping(nar);
    }

    public static synchronized void tune(DoubleSupplier periodNS, FloatProcedure motor) {
        if (profiled != null) {
            //throw new RuntimeException("tune already called");
            return;
        }

        BagDeriver.profiled = new BagDeriverProfiler(periodNS, motor);
    }

    @Override
    protected void accept(Premise p) {
        if ((p = active.put(p))!=null) {
        }
        premiseCount++;
    }

    @Override
    protected void next() {
        if (profile()) {
            _nextProfiled();
        } else {
            _next();
        }
    }

    @Override
    protected void start(Focus f) {
        super.start(f);
        explosivity.reset(1);
    }

    private void _nextProfiled() {
        try (var __ = profileCycle.get()) {
            _next();
        }

        if (profileMaintenance.weakCompareAndSetAcquire(false, true)) {
            try {
                if (profileCycle.count() >= profileCommitPeriod) {
                    profiled.accept(profileCycle.copy(true));
                }
            } finally {
                profileMaintenance.setRelease(false);
            }
        }
    }

    private boolean profile() {
        if (profiled == null) return false;

        float p = profileRate;
        return p > 0 &&
                rng.nextBooleanFast24(p);
//                rng.nextBoolean(p);
    }

    private void _next() {
        if (active == null)
             active = local();

        nextActive();

        int batchSize = Math.max(1, (int) Math.ceil(iter * iterBatchGranularity));

        int i = iter;
        explosivity.period(Math.max(1, i/2));
        do {
            i -= Math.max(1, ready(Math.min(batchSize, i)));
            runPending();
        } while (i > 0);
    }

    private void nextActive() {
        if (rng.nextBooleanFast8(activeClear))
            active.clear();

        /* TODO factor #threads (if shared), seed rate, etc */
        int localCap = (int) Math.ceil(((BagFocus) focus).bag.capacity() * capacityFactor);
        active.commit(localCap, now(), (int)
            focus.durBase
            //focus.dur()
        );
    }

    private PremiseBag local() {
        return LOCAL_SHARED_BY_ALL_FOCUS_THREADS ?
                focus.focusLocal(BagDeriver.class, (c) -> _local()) :
                _local();
    }

    private PremiseBag _local() {
        return new PremiseBag();
    }

    private float seedRate() {
        return Math.min(1,
            (float) (seedRatio / explosivity.mean())
        );
    }

    protected int ready(int iterTotal) {
        int seed = Math.max(1, Math.round(iterTotal * seedRate()));
        int seeded = seed(seed);

        int iter = Math.max(1, iterTotal - seeded);

        int queued = 0;
        Iterator<Premise> ii = active.sampleUnique(rng.rng);
        while (queued < iter && ii.hasNext()) {
            if (pending.add(ii.next()))
                queued++;
        }
        int remain = iter - queued;
        if (remain > 0)
            seeded += seed(remain);

        return queued+seeded;
    }

    private void runPending() {
        Lst<Premise> pp = pending.list;
        if (pp.isEmpty())
            return;

        boolean preSort = false; //TODO optional

        Premise[] ppp = pp.array();
        int pps = pp.size();

        if (preSort && pps > 2)
            Arrays.sort(ppp, 0, pps, PENDING_SORT);

        for (int i = 0; i < pps; i++) {
            Premise P = ppp[i]; ppp[i] = null;
            run(P);
        }
        pending.clear();
    }

    private int seed(int seed) {
        return sampleIndividuallyOrContiguously ?
                seedReSample(focus, seed) :
                seedBatchSample(focus, seed);
    }


    private int seedReSample(Focus f, int iter) {
        int i;
        for (i = 0; i < iter; i++) {
            Premise x = f.sample(rng.rng);
            if (x == null) break;
            pending.add(x);
        }
        return i;
    }

    private int seedBatchSample(Focus f, int iter) {
        Iterator<Premise> p = f.sampleUnique(rng.rng);
        int i = 0;
        while (iter-- > 0 && p.hasNext()) {
            if (pending.add(p.next()))
                i++;
        }
        return i;
    }

    protected void run(Premise P) {

        if (P instanceof TaskLink) premiseCount = 0;

        super.run(P);

        if (P instanceof TaskLink)
            explosivity.accept(premiseCount);
//        else
//            P.priMul(execution_decay);
    }

    protected void trace(Premise p) {
        System.out.println(now() + "\t" + p);
    }

//    private void decayBalanceAttempt(Premise p) {
////        if (p instanceof TaskLink) {
////            return;
////        }
//
//        PremiseBag ps = p.sub;
//        boolean priChange = false;
//
////        if (ps == null || ps.isEmpty()) {
//            if (!(p instanceof TaskLink)) {
//                float pp = p.priElseZero();
//                float pd = pp * decay_run;
//                float delta = pp - pd;
//                if (delta > 0) {
//                    p.priMul(decay_run);
//                    priChange = true;
//                    final Premise pa = p.parent;
//                    if (pa!=null) {
//                        int pn = pa.subsCount();
//                        if (pn > 0)
//                            pa.priAdd(delta / pn);
//                    }
//                }
//            }
////        }
//
////        if (ps!=null) {
//////            if (!(p instanceof TaskLink)) {
////            p.pri((float) ps.priMean());
////            priChange = true;
//////            }
////        }
//
//        if (priChange) {
//            if (p.parent != null) {
//                final PremiseBag pps = p.parent.sub;
//                if (pps != null)
//                    pps.commit(null);
//            }
//        }
//    }
//
//    private void decayNone(Premise p) {
//
//    }
//
//    /**
//     * i do not yet understand why this works good.
//     * somehow it radically manipulates tasklink values better than ordinary bag i/o alone
//     */
//    private void decay(Premise p) {
//
////        boolean priChange = false;
//
//        if (decay_run < 1) {
//            Set<Premise> ps = p.subs();
//            if (ps == null || ps.isEmpty()) {
//                if (!(p.id instanceof TaskLink)) {
//                    p.priMul(decay_run);
////                    priChange = true;
//                }
//            }
//        }
//
////        if (ps!=null) {
////            if (!(p instanceof TaskLink)) {
////                p.pri((float) ps.priMean());
////                priChange = true;
////            }
//////            if (p instanceof TaskLink) {
//////                ((MutableTaskLink)p).priScale((float)ps.priMean());
//////                priChange = true;
//////            }
////        }
//
////        if (priChange) {
////            if (p.parent != null) {
////                final Collection<Premise> pps = p.parent.sub;
////                if (pps != null)
////                    pps.commit(null);
////            }
////        }
//    }



//    protected int _next(Premise x, int ttl) {
//        if (x == null /* why?*/)
//            return ttl-1; //depleted iteration, HACK
//
//        Collection<Premise> y = x.subs();
//        int ys = y != null ? y.size() : 0;
//        boolean empty = ys == 0;
//        boolean runThis = empty || rng.nextBooleanFast8(
//                //REFRESH HEURISTIC TODO parameterize
//                forceRefreshRate / (1 + ys)
////                (float) (1f/(1+Math.pow(ys,2)))
////                (float) (1f/(1+Math.pow(ys,0.5)))
//        );
//
//        if (runThis) {
//
//            local.put(x):
//            if (--ttl <= 0)
//                return ttl;
//
//            if (run.add(x)) {
//
////                //TEMPORARY
////                List<Premise> p = path(x);
////                if (p.size() > 5)
////                    System.out.println(p);
//
//                pending.add(x);
//            }
//        }
//
////        if (!empty) {
////            //TODO rng.nextInt(float ) like: 1.5
////
////            Random rng = this.rng.rng;
////            if (ttl == 1) {
////                ttl = _next(y.sample(rng), ttl); //one
////            } else {
////                Iterator<Premise> ii = y.sampleUnique(rng); //some
////                while (ii.hasNext()) {
////                    ttl = _next(ii.next(), ttl);
////                    if (ttl <= 0) break;
////                }
////            }
////        }
//
//        return ttl;
//    }

//    private List<Premise> path(Premise x) {
//        List<Premise> p = new Lst<>();
//        while ((x = x.parent)!=null) {
//            p.add(x);
//        }
//        return p;
//    }

    public void print(Premise root) {
        print(root, 0);
    }

    public void print(Premise x, int depth) {
        if (x==null) return;

        System.out.print("  ".repeat(depth));
        System.out.println(x.getClass().getSimpleName() + " " + x);
//        Set<Premise> xy = x.subs();
//        if (xy != null) {
//            for (Premise y : xy)
//                print(link(y), depth + 1);
//        }
        if (depth == 0)
            System.out.println();
    }

    private static class BagDeriverProfiler implements Consumer<Histogram> {

        private static final int ITERATIONS_MAX = 2000;
        private final DoubleSupplier periodNS;
        private final FloatProcedure motor;
        MiniPID pid;

        BagDeriverProfiler(DoubleSupplier periodNS, FloatProcedure motor) {
            this.periodNS = periodNS;
            this.motor = motor;
            pid = new MiniPID(1f, 1f, 1f);
            profileRate = 0.03f;

            pid.outRange(1, ITERATIONS_MAX);
            pid.setOutMomentum(0.707f /* http://aar.faculty.asu.edu/classes/eee598S98/docs/Cartoon_Tour_Of_Control_Theory_JOSHI.pdf */);
        }

        @Override
        public void accept(Histogram h) {
            double profilePeriodMean = h.getMean();

//            double target = f.floatValue();
//            double actual = profilePeriodMean*(1.0E-9);
            double target = periodNS.getAsDouble();
            double actual = profilePeriodMean;

            double o = pid.out(actual, target);
            motor.value((float) o);

            if (logger.isInfoEnabled())
                logger.info("{} ~= {} -> {} iter", Str.toString(h, false, true, true), Str.timeStr(target), (int) Math.ceil(o));
        }
    }

    private static final Comparator<Premise> PENDING_SORT = //Comparator
//            .comparingInt(Premise::hashFromTo);

            //smallest first
//            .<Premise>comparingInt(z -> z.from().volume())
//            .thenComparingInt(z -> z.to().volume())
//
//            .thenComparingInt(z -> z.from().hashCode())
//            .thenComparingInt(z -> z.to().hashCode());
        (a,b)->{
            if (a==b) return 0;

            //sort tasklinks first
            boolean aTL = a instanceof TaskLink;
            boolean bTL = b instanceof TaskLink;
            if (aTL && !bTL) return -1;
            if (bTL && !aTL) return +1;

            int f = a.from().compareTo(b.from()); if (f!=0) return f;
            int t = a.to().compareTo(  b.to()); return t;
        };
}