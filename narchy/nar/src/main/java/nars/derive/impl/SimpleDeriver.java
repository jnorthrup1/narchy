package nars.derive.impl;

import jcog.data.list.Lst;
import jcog.signal.IntRange;
import nars.NAR;
import nars.derive.Deriver;
import nars.derive.reaction.ReactionModel;
import nars.focus.Focus;
import nars.premise.Premise;

public class SimpleDeriver extends Deriver {

    public final IntRange iter = new IntRange(128, 1, 512);
    public final IntRange subIter = new IntRange(16, 1, 32);

    final Lst<Premise> queue = new Lst();

    public SimpleDeriver(ReactionModel model, NAR nar) {
        super(model, nar);
    }

    @Override
    protected void accept(Premise p) {
        queue.add(p);
    }

    @Override
    protected void next() {
        int iter = this.iter.intValue(), subIter = this.subIter.intValue();
        Focus f = this.focus;
        for (int i = 0; i < iter; i++) {
            Premise p = f.sample();
            if (p == null) break;

            run(p);

            int qs = queue.size();
            if (qs == 0) continue;

            if (qs > subIter) {
                queue.shuffleThis(rng);
            }

            for (int j = 0; j < subIter; j++) {
                Premise q = queue.getAndNullIfNotNull(j);
                if (q==null) break;
                run(q);
            }

            queue.clear();
        }
    }
}