package nars.derive.impl;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.pri.PlainLink;
import jcog.pri.PriReference;
import jcog.pri.Prioritizable;
import jcog.pri.bag.Bag;
import jcog.pri.bag.Sampler;
import jcog.pri.bag.impl.ArrayBag;
import jcog.pri.bag.impl.PLinkArrayBag;
import jcog.pri.op.PriMerge;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import nars.NAR;
import nars.derive.Deriver;
import nars.derive.reaction.ReactionModel;
import nars.derive.reaction.Reactions;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.premise.Premise;

import java.util.function.Consumer;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class MixDeriver extends Deriver {

    /** lower values potentially react faster to changes in focus's bag */
    static final int batches = 5;
    static final int iterPerBatch = 32;
    static final float queueCapIters = 1 * Util.PHIf;
    public static float mixDefault =
        //1/2f;
        //1/6f;
        //1/5f;
        //1/3f;
        1/4f;
        //0.2f;
        //0.12f;
        //0.1f;
        //0.25f;
        //0.35f;
        //1/11f;
        //1/15f;
        //1/8f;

    /** <= 1, decay rate */
    float activatedMultiplier =
        //1;
        0.9f;
        //0.8f;
        //Util.PHI_min_1f;
        //0.5f;
        //0.25f;
        //0.1f;

    /** the rate (in 1/capacity's) for periodic active bag clearing.
     *  non-zero because it probably helps to clear some partial memory leaks due to recursive proxy tasks */
    private static final float activeClear =
        1;
        //4;

    public final IntRange iter = new IntRange(batches * iterPerBatch, 1, 2048);
    public final FloatRange mix = new FloatRange(mixDefault, 0, 1);

//    public final FloatRange forgetRate = new FloatRange(0.9f, 0, 1);

    private final PLinkArrayBag<Premise> active = new PLinkArrayBag<>(
            PriMerge.plus
            //PriMerge.max
            ,
        new ArrayBag.PlainListArrayBagModel<>()
    ) {
        @Override
        protected float merge(PriReference<Premise> existing, PriReference<Premise> incoming, float incomingPri) {
            //update budget of the premises also
            PriMerge.
                mean
                //max
                .apply(existing.get(), incoming.get().priElseZero());

            return super.merge(existing, incoming, incomingPri);
        }
    };
//    active = new PriHijackBag<>(
//            PriMerge.max, 3) {
//
//        @Override
//        public Premise key(NLink<Premise> value) {
//            return value.id;
//        }
//    };

    private final Lst<Prioritizable> next = new Lst<>();
    private final Consumer<Prioritizable> nextRunner = this::run;

    public MixDeriver(Reactions model, NAR nar) {
        this(model.compile(nar), nar);
    }

    public MixDeriver(ReactionModel model, NAR nar) {
        super(model, nar);
    }

    @Override
    protected void start(Focus f) {
        super.start(f);
        active.clear();
    }

//    /** intercept added tasklinks into buffer */
//    @Override
//    protected void _link(MutableTaskLink l) {
//        taskLinkBuffer.add(l);
//    }

    @Override
    protected void accept(Premise p) {
        active.put(new PlainLink<>(p, p.pri()));
    }

    @Override
    protected void next() {

        Focus f = this.focus;
        Bag<Premise, Premise> focusBag = ((BagFocus) f).bag;
        if (focusBag.isEmpty())
            return;

        int iter = this.iter.intValue();
        active.capacity(Math.round(queueCapIters * iter));

        float mix = this.mix.floatValue();

        int batchSize = (int) max(1, Math.ceil(((float)iter) / batches));

        for (int i = iter; i >= 0; ) {
            int ran = nextIter(min(batchSize, i), focusBag);

            if (ran == 0) break;

            i -= ran;
        }

        tryClear();

    }

    private int capacity() {
        return ((BagFocus) focus).capacity.intValue();
    }

    private void tryClear() {
        if (rng.nextBooleanFast16(activeClear/capacity()))
            active.clear();
    }

    private Bag<?, ? extends Prioritizable> nextSrc(Bag<Premise, Premise> focusBag, float mix) {
        return active.isEmpty() || rng.nextBooleanFast8(mix) ?
                focusBag : active;
    }

    private int nextIter(int batchSize, Bag<Premise, Premise> focusBag) {
        int activeSize = active.size();
        if (activeSize > 0)
            active.commit();

        Lst<Prioritizable> next = this.next; //assert(next.isEmpty());

        int _focusBatch = rng.floor(/*1 +*/ mix.floatValue() * batchSize);
        int focusBatch =
            //_focusBatch;
            //Math.max((batchSize - activeSize), _focusBatch);
            Math.max((batchSize - activeSize)/2 /* half-fill */, _focusBatch);
            //Math.max(1, _focusBatch);
        load(focusBag, focusBatch, next);

        int activeBatch = batchSize - next.size();
        load(active, activeBatch, next);

        int n = next.clear(nextRunner);
//        if (n > 0)
//            commitTaskLinks();

        return n;
    }

    private void load(Sampler<? extends Prioritizable> src, int batchSize, Lst<Prioritizable> tgt) {
        if (batchSize <= 0) return;

        tgt.ensureCapacityForAdditional(batchSize, true);

        var s = src.sampleUnique(rng.rng);
        while (s.hasNext()) {
            tgt.addFast(s.next());

            if (--batchSize <= 0)
                break;
        }
    }


    private void run(Prioritizable x) {
        Premise y;
        if (x instanceof Premise) {
            //tasklinks
            y = ((Premise) x);
        } else {
            //internal
            y = (((PriReference<Premise>) x).get());

            //y.pri(x.priElseZero()); //HACK

            x.priMul(activatedMultiplier);
        }
        run(y);
    }
}