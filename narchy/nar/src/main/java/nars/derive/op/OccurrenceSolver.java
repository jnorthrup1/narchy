package nars.derive.op;

import jcog.data.map.UnifriedMap;
import jcog.math.LongInterval;
import nars.NAL;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.reaction.PatternReaction;
import nars.premise.NALPremise;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.atom.Atomic;
import nars.time.TimeGraph;
import nars.truth.MutableTruthInterval;
import org.eclipse.collections.api.tuple.Pair;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static java.lang.Math.abs;
import static nars.Op.*;
import static org.eclipse.collections.impl.tuple.Tuples.pair;

public enum OccurrenceSolver {
    Task() {
        @Override
        public boolean relative() {
            return false;
        }

    },

    Belief() {
        @Override
        public boolean relative() {
            return false;
        }

        @Override
        protected boolean taskOrBelief(Deriver d) {
            return false;
        }
    },

//    Now() {
//        @Override
//        public boolean relative() {
//            return false;
//        }
//
//        @Override
//        protected void concOcc(NALPremise i, MutableProjectedTruth out, Deriver d) {
//            out.occurr(d.focus.when());
//        }
//    },

//    Mid() {
//        @Override
//        public boolean relative() {
//            return false;
//        }
//
//        @Override
//        public void target(long ts, long te, long bs, long be, NALPremise i, MutableProjectedTruth out) {
//            long mid =
//                mean(mean(ts,bs), mean(te,be)); //TODO try other range mean calculations
//                //mean(mean(ts,te),mean(bs,be));
//
//            long range = Math.min(te-ts, be-bs);
//            long radius = range/2;
//            out.occurr(mid-radius, mid+radius );
//        }
//    },

    /** smudges derivation to the union of the premise tasks.  resulting truth is diluted in proportion to any non-intersecting range  */
    Union() {
        @Override
        public boolean relative() {
            return false;
        }

        @Override
        public void target(long ts, long te, long bs, long be, MutableTruthInterval o, byte punc, Deriver d) {
            long s, e;
            assert(bs!=TIMELESS);
            if (ts == ETERNAL) { s = bs; e = be; }
            else if (bs == ETERNAL) { s = ts; e = te; }
            else {  s = Math.min(ts, bs); e = Math.max(te, be); }
            o.occurr(s, e);
        }
    },
//    /**
//     * dynamically choose either Task or TaskRel
//     */
//    @Deprecated TaskBoth() {
//
//        @Override
//        protected void concOcc(NALPremise i, MutableProjectedTruth out, Deriver d) {
//            (d.rng.nextBoolean() ? Task : TaskRel).concOcc(i, out, d);
//        }
//
//        @Override
//        protected boolean relative() {
//            return true; //HACK to be safe
//        }
//    },
//    /**
//     * dynamically choose either Task and Belief
//     */
//    @Deprecated Either() {
//
//        @Override
//        protected void concOcc(NALPremise i, MutableProjectedTruth out, Deriver d) {
//            (d.rng.nextBoolean() ? Task : Belief).concOcc(i, out, d);
//        }
//
//        @Override
//        protected boolean relative() {
//            return false;
//        }
//    },
//
//    /**
//     * dynamically choose either TaskRel and BeliefRel
//     */
//    @Deprecated EitherRel() {
//
//        @Override
//        protected void concOcc(NALPremise i, MutableProjectedTruth out, Deriver d) {
//            (d.rng.nextBoolean() ? TaskRel : BeliefRel).concOcc(i, out, d);
//        }
//
//        @Override
//        protected boolean relative() {
//            return true;
//        }
//    },

    TaskRel() {
        @Override
        public boolean relative() {
            return true;
        }
    },

    BeliefRel() {
        @Override
        public boolean relative() {
            return true;
        }

        @Override
        protected boolean taskOrBelief(Deriver d) {
            return false;
        }
    },


    /** TODO not fully impl yet: works only with question derivations, see impl.strong.nal */
    Pre() {
        @Override
        @Deprecated public void target(long ts, long te, long bs, long be, MutableTruthInterval o, byte punc, Deriver d) {
            prePost(false, ts, te, bs, be, (NALPremise) d.premise, o);
        }

    },
    /** TODO not fully impl yet: works only with question derivations, see impl.strong.nal */
    Post() {
        @Deprecated @Override
        public void target(long ts, long te, long bs, long be, MutableTruthInterval o, byte punc, Deriver d) {
            prePost(true, ts, te, bs, be, (NALPremise) d.premise, o);
        }

    },

//    /** custom solver for goal-based conjunction decomposition */
//    CondBefore() {
//        @Override
//        protected boolean relative() {
//            return true;
//        }
//
//        @Override
//        public Pair<Term, MutableProjectedTruth> solve(NALPremise i, Term x, MutableProjectedTruth t, PatternReaction rule, Deriver d) {
//            return conjShift(i, x, true, t, rule, d);
//        }
////        @Override protected boolean taskOrBelief(Deriver d) {
////            return true; //TASK
////            //return later((NALPremise) p);
////        }
//    },
//
//    /** variation of CondBefore which negates the events of the final result, AFTER occurrence solving */
//    CondBeforeNegate() {
//        @Override
//        protected boolean relative() {
//            return CondBefore.relative();
//        }
//
//        @Override
//        public Pair<Term, MutableProjectedTruth> solve(NALPremise i, Term x, MutableProjectedTruth t, PatternReaction r, Deriver d) {
//            var p = CondBefore.solve(i, x, t, r, d);
//            return p != null ?
//                pair(Conj.negateEvents(p.getOne()), p.getTwo())
//                : null;
//        }
//
//        @Override
//        protected boolean taskOrBelief(Deriver d) {
//            return CondBefore.taskOrBelief(d);
//        }
//    },

    /** custom solver for goal-based conjunction decomposition */
    CondAfter() {
        @Override
        protected boolean relative() {
            return true;
        }

        @Override
        public Pair<Term, MutableTruthInterval> solve(NALPremise i, Term x, MutableTruthInterval t, PatternReaction rule, Deriver d) {
            return conjShift(i, x, false, t, rule, d);
        }

        @Override protected boolean taskOrBelief(Deriver d) {
            return false; //BELIEF
            //return nearestNow(d);
            //return later((NALPremise) d.premise);
//            ////return true; //TASK
        }
    },


    NearestNow() {
        @Override protected boolean taskOrBelief(Deriver d) {
            return nearestNow(d);
        }
    },

    Later() {
        @Override protected boolean taskOrBelief(Deriver d) {
            return later((NALPremise) d.premise);
        }
        @Override protected boolean relative() {
            return true;
        }
    }
    ;

//    /** BeliefRel if belief after goal, or TaskRel if belief before goal
//     *  TODO find a better name.  LateRel?
//     */
//    Would() {
//        @Override
//        protected boolean taskOrBelief(Deriver d) {
//            final Premise p = d.premise;
//            return p.task().start() >= p.belief().start();
//        }
//
//        @Override
//        protected boolean relative() {
//            return true;
//        }
//    };

    private static boolean nearestNow(Deriver d) {
        NALPremise P = (NALPremise) d.premise;

        long t = P.taskMid();
        if (t == ETERNAL) return false; //belief

        long b = P.beliefMid();
        if (b == ETERNAL) return true; //task

        long now = d.now();
        return abs(now - t) <= abs(now - b);
    }

    private static boolean later(NALPremise p) {
        //choose the later of the two tasks:
        NALPremise P = p;
        long ts = P.taskMid();
        if (ts == ETERNAL) return false; //belief
        //TODO if they are close enough or intersect, choose randomly
        return ts >= P.beliefMid();
    }

    /**
     * @param direction after: +1, before: -1
     * @param y result content term for task being derived
     */
    static Pair<Term, MutableTruthInterval> conjShift(NALPremise i, Term y, boolean beforeOrAfter, MutableTruthInterval t, PatternReaction rule, Deriver d) {
        long cs = t.start();
        if (cs!=ETERNAL) {
            assert(cs!=TIMELESS);
            Term taskTerm = i.task.term();
            Term beliefTerm = i.beliefTerm();
            Compound conj = (Compound) (beforeOrAfter ? beliefTerm : taskTerm);
            int conjRange = conj.seqDur();
            Term event = beforeOrAfter ? taskTerm : beliefTerm;
            int resultRange = y.seqDur();

//            //HACK determine if we are looking for pos or neg of the event
//            //ex: conjBefore(C,--X) eventNeg=true
//            //ex: conjBefore(C,  X) eventNeg=false
//            Term r = rule.pattern.unneg();
//            if (Functor.func(r).equals(NEGATE_EVENTS))
//                r = Functor.argSub(r, 0); //HACK unwrap
//            boolean eventNeg = Functor.argSub(r, 1) instanceof Neg;
//            Term contained = event.negIf(eventNeg);
            Term contained = event;
            int eventOffset = conj.when(contained, !beforeOrAfter);
            if (eventOffset == DTERNAL)
                eventOffset = 0;
            else if (eventOffset==XTERNAL)
                return null;//throw new TermException("event not found in " + conj + ": " + contained); //TEMPORARY shouldnt happen

            int shift = beforeOrAfter ?
                    (eventOffset - resultRange) :
                    (conjRange - eventOffset) - resultRange;
            if (shift < 0)
                return null; //oops TODO why

            shift *= (beforeOrAfter ? -1 : +1);
            if (shift!=0) {
                if (!t.reoccurr(cs + shift, t.end() + shift, d.now(), d.nar))
                    return null;
            }
        }
        return pair(y, t);
    }

    private static void prePost(boolean fwd, long ts, long te, long bs, long be, NALPremise i, MutableTruthInterval out) {

        Term tt = i.task().term();
        boolean tImpl = tt.IMPL();
        Term bb = i.beliefTerm();
        boolean bImpl = bb.IMPL();
        assert(tImpl ^ bImpl);

        boolean occTB;
        if (bs == TIMELESS) occTB = true;
        else if (ts == ETERNAL) occTB = false;
        else if (bs == ETERNAL) occTB = true;
        else occTB = bImpl;

        long s, e;
        if (occTB) { s = ts; e = te; } else { s = bs; e = be; }

        if (s!=ETERNAL) {
            Compound impl = (Compound) (tImpl ? tt : bb);
            int idt = impl.dt();
            if (idt!=XTERNAL) {
                if (idt==DTERNAL) idt = 0;
                int shift;
                if (fwd) {
                    shift = idt;
                } else {
                    shift = -idt - impl.seqDurSub(0, false);
                }
                s += shift;
                e += shift;
            }
        }
        out.occurr(s, e);
    }


    public static final Map<Term, OccurrenceSolver> solvers;
    static final float pointsAbsolute = 1;
    static final float pointsNoXternal = 1;

    static {
        UnifriedMap<Term, OccurrenceSolver> tm = new UnifriedMap<>();
        for (OccurrenceSolver m : values())
            tm.put(Atomic.the(m.name()), m);
        tm.trimToSize();
        solvers = tm;
    }

    public final Atomic term;

    OccurrenceSolver() {
        this.term = Atomic.atom(name());
    }


    static float score(TimeGraph.Event e, int volIdeal, int structIdeal, boolean ifAbsolute) {
        float good = 1;
        if (e instanceof TimeGraph.Absolute)
            good += ifAbsolute ? pointsAbsolute : pointsAbsolute / 2;

        Term ee = e.id;
        if (!ee.TEMPORAL_VAR())
            good += pointsNoXternal;


        float bad = abs(volIdeal - ee.volume()) + different(structIdeal, ee.structure());

        float range = e instanceof TimeGraph.Absolute ?
                (1 + e.range()) /* /dur ? */ : 1;

        return ((1 + good * 1) / (1 + bad * 0.5f)) * range;
    }


    /**
     * whether full occurrence solving is involved
     */
    protected boolean relative() {
        return false;
    }

    public final MutableTruthInterval apply(NALPremise i, byte punc, Deriver d) {
        var t = d._truth.clear();
        target(i.taskStart, i.taskEnd, i.beliefStart(), i.beliefEnd(), t, punc, d);
        return t;
    }

    /** default solver impl */
    @Nullable public Pair<Term, MutableTruthInterval> solve(NALPremise i, Term x, MutableTruthInterval t, PatternReaction rule, Deriver d) {

        //        return !x.TEMPORAL_VAR() && (!relative() || !i.temporal);

        boolean s = relative() ?
                !i.temporal && !Compound._temporal(x) :
                !x.TEMPORAL_VAR();
        if (s)
            return pair(x,t); //simple case; use pre-computed occurrence
        else {

            PremiseTimeGraph g = d.timeGraph;
            try {
                if (NAL.Temporal.EVENT_BAG /*&& !dynamic*/) i.load(g);

                g.init(x, d);

                Term T = i.from();
                Term B = i.to();

                boolean negUnwrap = false;
                if (negUnwrap(x, T, B)) {
                    negUnwrap = true;
                    x = x.unneg();
                }


                //TODO elide 2nd loop (autoNeg=true) if no negatives
                for (boolean autoNeg : new boolean[] { false, true }) {
                    if (autoNeg) g.clear(); //HACK 2nd loop

                    g.autoNeg = autoNeg;

                    if (relative()) {
                        Pair<Term, MutableTruthInterval> z = solveOcc(x, t, d, g, T, B);
                        if (z == null) continue;

                        return negUnwrap ? pair(z.getOne().neg(), z.getTwo()) : z;


                    } else {
                        Term y = solveDT(x, /*c*/null, d, g, T, B);
                        if (y == null) continue;
                        return pair(negUnwrap ? y.neg() : y, t);
                    }

                }

                return null;

            } finally {
                if (NAL.Temporal.EVENT_BAG /*&& !dynamic*/) i.save(g);

                g.clear(); //TODO defer this until after all unification matches, so it can be-reused with re-init
            }

        }

    }

    private static boolean negUnwrap(Term x, Term T, Term B) {
        //TODO other cases, counting pos/neg events etc
        return x instanceof Neg && !T.hasAny(NEG) && !B.hasAny(NEG);
    }

    @Nullable
    private Pair<Term, MutableTruthInterval> solveOcc(Term x, MutableTruthInterval t, Deriver d, PremiseTimeGraph g, Term task, Term belief) {

        g.knowPremise(x, t, d, task, belief, taskOrBelief(d));

        Pair<Term, long[]> y = g.time(x, true);
        if (y != null) {
            long[] oo = y.getTwo();
            if (oo != null && oo[0] != TIMELESS) {
                if (t.reoccurr(oo[0], oo[1], d.now(), d.nar))
                    return pair(y.getOne(), t);
            }
        }

        return null;
    }

    private Term solveDT(Term x, @Nullable MutableTruthInterval c, Deriver d, PremiseTimeGraph g, Term t, Term b) {

        g.knowPremise(x, c, d, t, b, this != Belief);

        Pair<Term, long[]> s = g.time(x, false);
        return s!=null ? s.getOne() : null;
    }

    /** default target impl. should call out.occurr or other 'out' time setter method */
    protected void target(long ts, long te, long bs, long be, MutableTruthInterval o, byte punc, Deriver d) {
        if (bs == ETERNAL || bs == TIMELESS) {
            o.occurr(ts, te);
        } else if (ts == ETERNAL || (ts == bs && te == be)) {
            o.occurr(bs, be);
        } else {
            o.occurr(_target(ts, te, bs, be, punc, d));
        }
    }

    private long[] _target(long ts, long te, long bs, long be, byte punc, Deriver d) {
        boolean taskOrBelief = taskOrBelief(d);
        long[] SE = taskOrBelief ? new long[] {ts, te} : new long[] {bs, be};

        if (NAL.premise.TRIM_PREMISE_OCC)
            LongInterval.trimToward(SE, taskOrBelief ? bs : ts, taskOrBelief ? be : te);

        return SE;
    }

    /** whether the task (true) or belief (false) is the dominant temporal target
     * @param d*/
    protected boolean taskOrBelief(Deriver d) {
        return true; //default
    }


}