package nars.derive.op;

import jcog.Util;
import jcog.data.iterator.ArrayIterator;
import jcog.random.RandomBits;
import nars.derive.Deriver;
import nars.term.control.FORK;
import nars.term.control.PREDICATE;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;

import static java.lang.invoke.MethodHandles.insertArguments;
import static java.lang.invoke.MethodType.methodType;

@Deprecated public abstract class DeriverFork extends PREDICATE<Deriver> {

    protected final PREDICATE<Deriver>[] branch;

    public DeriverFork(FORK<Deriver> f) {
        super(f.ref);
        this.branch = f.branch;
    }

    public Iterable<PREDICATE<Deriver>> branches() {
        return ArrayIterator.iterable(branch);
    }

    public static DeriverFork the(FORK<Deriver> f) {
//        if (f.branch.length == 2)
//            return new DeriverFork2(f);
//        else
            return new DeriverForkN(f);
    }



    public static final class DeriverForkN extends DeriverFork {

        static MethodHandle N, N2;

        static {
            MethodHandle N = null, N2 = null;
            try {
                N = MethodHandles.privateLookupIn(DeriverForkN.class, MethodHandles.lookup())
                        .findStatic(DeriverForkN.class, "test", methodType(boolean.class, RandomBits.class, MethodHandle[].class));
                N2 = MethodHandles.privateLookupIn(DeriverForkN.class, MethodHandles.lookup())
                        .findStatic(DeriverForkN.class, "test2", methodType(boolean.class, RandomBits.class, MethodHandle.class, MethodHandle.class));
                DeriverForkN.N = N; DeriverForkN.N2 = N2;
            } catch (NoSuchMethodException | IllegalAccessException e) {
                throw new RuntimeException();
            }
        }

        private DeriverForkN(FORK<Deriver> f) {
            super(f);
        }


        @Override
        public MethodHandle method(Deriver d) {
            try {
                int s = subs();
                if (s == 2) {
                    return insertArguments(N2, 0, d.rng, branch[0].method(d), branch[1].method(d));
                } else {
                    return insertArguments(N, 0, d.rng,
                            Util.arrayOf(i -> branch[i].method(d), new MethodHandle[s])
                    );
                }
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
        public static boolean test2(RandomBits r, MethodHandle a, MethodHandle b) {
            if (r.nextBoolean()) {
                MethodHandle ab = b;
                b = a;
                a = ab;
            }
            try {
                boolean Aignored = (boolean) a.invokeExact();
                boolean Bignored = (boolean) b.invokeExact();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
            return true;
        }
        public static boolean test(RandomBits r, MethodHandle[] m) {
            //semi-shuffled order
            int n = m.length;
            int j = r.nextInt(n * 2);
            boolean d;

            if (j >= n) {
                j -= n;
                d = false; //down
            } else
                d = true; //up

            for (int i = n-1; i >=0; i--) {
                try {
                    boolean ignored = (boolean) m[j].invokeExact();
                } catch (Throwable throwable) {
                    throw new RuntimeException(throwable);
                }
                if (d) {
                    if (++j == n) j = 0;
                } else {
                    if (--j < 0) j = n - 1;
                }
            }

            return true;
        }

        @Override
        public boolean test(Deriver x) {

            //semi-shuffled order
            PREDICATE<Deriver>[] branch = this.branch;
            int n = branch.length;
            int j = x.rng.nextInt(n * 2);
            boolean d;

            if (j >= n) {
                j -= n;
                d = false; //down
            } else
                d = true; //up

            for (int i = n-1; i >=0; i--) {
                branch[j].test(x);
                if (d) {
                    if (++j == n) j = 0;
                } else {
                    if (--j < 0) j = n - 1;
                }
            }

            return true;
        }

    }
    public static final class DeriverFork2 extends DeriverFork {

        private DeriverFork2(FORK<Deriver> f) {
            super(f);
        }

        @Override
        public boolean test(Deriver x) {

            PREDICATE<Deriver>[] branch = this.branch;

            boolean j = x.rng.nextBoolean();
            PREDICATE<Deriver> a = branch[0], b = branch[1];
            if (j) {
                a.test(x);
                b.test(x);
            } else {
                b.test(x);
                a.test(x);
            }
            return true;
        }
    }
}