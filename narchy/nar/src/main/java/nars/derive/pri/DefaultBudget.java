package nars.derive.pri;

import jcog.Fuzzy;
import jcog.Is;
import jcog.pri.op.PriMerge;
import jcog.signal.FloatRange;
import nars.NAL;
import nars.Term;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.focus.Focus;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.util.OpPri;
import nars.task.util.PuncBag;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import static jcog.Util.*;
import static nars.truth.func.TruthFunctions.e2c;

/**
 * SeH's default budget impl
 */
@SuppressWarnings("WeakerAccess")
public class DefaultBudget extends Budget {

    private static final boolean confidenceCalculatedByEvidenceRatio = true;

    private static final boolean linkSimplicityAbsoluteOrRelative = true;



    /**
     * Taskify derived  task priority factor, by conclusion punctuation
     */
    public final PuncBag puncDerived = new PuncBag(
           0.99f, 0.8f, 0.99f, 0.8f
    );

    /**
     * Taskify premise selection probability, by conclusion punctuation
     */
    public final PuncBag puncNALPremise = new PuncBag(1);
    //public final PuncBag puncTaskifyPremise = new PuncBag();

    //public final OpPri opDerived = new OpPri();
    public final OpPri linkOpPri = new OpPri();

    /**
     * how important is it to retain conf (evidence).
     * leniency towards uncertain derivations
     * pressure to conserve confidence.
     *
     * "keep an open mind, but not too open that brains fall out."
     *
     * TODO maybe make separate belief and goal params
     */
    public final FloatRange certain   = new FloatRange(0.5f, 0, 12);

    /** complexity activation cost - occam's razor - demand for simplicity */
    public final FloatRange simple    = new FloatRange(1, 0, 16);

    /** complexity linking cost: spiral in (ex: compound decompose) */
    public final FloatRange simpleIn  = new FloatRange(1, 0, 12);

    /** complexity linking cost: spiral out (ex: termlinking) */
    public final FloatRange simpleOut = new FloatRange(1, 0, 12);

    /**
     * frequency polarity importance
     */
    public final FloatRange polarized = new FloatRange(0.01f, 0, 1);

//    /**
//     * task link (source) gain.
//     * along with novelty, determines how quickly the priority dynamic range can become saturated.
//     * <p>
//     * inevitably proportional to the avg speed that concepts flow through the bag.
//     */
//    public final FloatRange amp = new FloatRange(1, 0, 2);

    public PriMerge derivePriMerge =
            PriMerge.mean //stable
//            PriMerge.or //eager, ambitious
//            PriMerge.max
//            PriMerge.and
            //PriMerge.min
            ;

    /**  > 1: explosive */
    double premiseAmp =
            //Util.PHI;
            1.3
            //1.2
            //1.1
            //1
            //0.99
            //0.1
    ;

    public final PuncBag linkPuncPri = new PuncBag(1);

    /**
     * exploration vs exploitation (OptiTreeDeriver mode only)
     */
    @Deprecated float exploration = 0.5f;


    private static double eParent(@Nullable Truth x, @Nullable Truth y) {
        if (x == null && y == null)
            return 0;
        if (x == null)
            return y.evi();
        else if (y == null)
            return x.evi();
        else {
            /*
            max //stricter
            plus //most strict
            min //relaxed
            */
            return
                Math.max(
                //Math.min(
                //Fuzzy.mean(
                    x.evi(), y.evi()
                );
        }
    }


    @Override
    public double priDerived(NALTask xy, NALTask x, NALTask y, Deriver d) {

        return
            priTaskPremise(x, y, derivePriMerge) *

            puncDerived.apply(xy) *
            //opDerived.apply(xy.term()) *

            //simple(xy.term(), d) *

            (xy.BELIEF_OR_GOAL() ?
                (
                confident(xy, x, y) *
                polarized(xy.freq())
                )
            :
                1
//                simple(xy.term(), d)
            )
        ;
    }

//    private double priTaskPremise(Premise p) {
//        return priTaskPremise(p.task(), p.belief());
//    }

    private double priTaskPremise(NALTask t, NALTask b, PriMerge merge) {

//        if (t == null)
//            return 0; //HACK

        double tPri = t.priElseZero();
        if (b != null && b != t) {
            double bPri = b.priElseZero();
//            return Math.max(tPri, bPri);
            //return Math.min(tPri, bPri);
            //return Fuzzy.mean(tPri, bPri);
            //return Fuzzy.or(tPri, bPri);
            //return tPri + bPri;
//            return Fuzzy.and(tPri, bPri);
            return merge.apply(tPri, bPri);
        } else {
            return tPri;
        }
    }

    double polarized(float freq) {
        return lerpSafe(polarized.floatValue(), 1.0f, Fuzzy.polarity(freq));
    }

    /**
     * conservation of confidence factor
     */
    private double confident(NALTask xy, NALTask x, NALTask y) {

        double eParent = eParent(x != null ? x.truth() : null, y != null ? y.truth() : null);
        if (eParent < Double.MIN_NORMAL) return 1;

        double eDerived = xy.evi();

        double r =
                confidenceCalculatedByEvidenceRatio ?
                        eDerived / eParent :
                        e2c(eDerived) / e2c(eParent);

        //return lerpSafe(confident.floatValue(), 1, unitizeSafe(r));
        return Math.pow(unitizeSafe(r), certain.floatValue());
    }

    @Override
    public double priPremise(Premise p, Deriver d) {


        double y =
            premiseAmp * p.parent.priElseZero();
            //premiseAmp;
            //premiseAmp * p.root().pri();

        if (NAL.causeCapacity.intValue() > 0)
            y *= lerp(exploration, cause(p, d), 1);

        if (p instanceof NALPremise pp) {
            y *= puncNALPremise.apply(pp.task.punc());

//            if (pp.parent.task()==null) {
//                //discount at a transition from unresolved TaskLink to resolved NALPremise
//                y *= priTaskPremise(pp.task, pp.belief(), derivePriMerge);
//            }

        } /*else if (p instanceof TaskifyPremise) {
            y *= puncTaskifyPremise.apply(((TaskifyPremise) p).puncOut);
//        } else if (p instanceof SubPremise) {
//            y *= premiseOtherAmp * d.premise.priElseZero(); //parent
        } */ else {

//            Class<? extends MutableReaction> r = p.reactionType();
//            if (TemporalInduction.class.isAssignableFrom(r)
//                || STMLinker.class.isAssignableFrom(r)
//                || ImageUnfold.class.isAssignableFrom(r)
//                || ImageAlign.class.isAssignableFrom(r)
//            ) {
//                //STMLinker too?
//                y *= simpleDoublePremise(p, d);
//            } /*else if (TermLinking.PremiseTermLinking.class.isAssignableFrom(r)) {
////                y *= simpleDoublePremise(p, d);
//            } else {
//                Util.nop();
//            }*/

        }

        return y;
    }

//    private double simpleDoublePremise(Premise p, Deriver d) {
//        final int v = p.from().volume() + p.to().volume();
//        return simple(v/2f, d);
//    }

//    static private double simplicity(boolean preferComplexOrSimple, boolean maxOrTo, Deriver d) {
//        return simplicity(preferComplexOrSimple, maxOrTo, 1, d);
//    }
//
//    static private double complexity(boolean maxOrTo, float sensitivity, Deriver d) {
//        return simplicity(true, maxOrTo, sensitivity, d);
//    }
//
//    /** prioritizes for either simplicity conservation or complexity exploitation */
//    static private double simplicity(boolean preferComplexOrSimple, boolean maxOrTo, float sensitivity, Deriver d) {
//        final Premise p = d.premise;
//        final int toVol = p.to().volume();
//        float complexity = (maxOrTo ?
//                Math.max(p.from().volume(), toVol)
//                :
//                toVol)
//                    / ((float) d.volMax + 1);
//
//        complexity = Util.unitizeSafe(complexity); //safety
//
//        return Math.pow(preferComplexOrSimple ? complexity : 1 - complexity, sensitivity);
//    }

    private double cause(Premise p, Deriver d) {
        Term why = p.why();
        int action = d.premise.cause();
        Cause a = action >= 0 ? d.nar.control.why.get(action) : null;

        double v;
        if (a != null) {
            v = a.pri;
//                y *= 1 + v * strength;
//                System.out.println(v + "\t" + y + "\t" + p );
        } else {
            v = why != null ? Cause.pri(why, d.nar) : 1;
        }
        return v;
    }

    /**
     * complexity discount: factor for penalizing incremental complexity increase
     */
    @Is({"AIXI", "Relative_change_and_difference"})
    public double simple(Term t, Deriver d) {
        return simple(t.volume(), d);
    }

    public double simple(float vol, Deriver d) {
        //final Premise p = d.premise;
        //final int toVol = p.to().volume();
        //int premiseComplexity = Math.max(p.from().volume(), toVol);
        //return simpleRelative(premiseComplexity, t.volume());
        return simpleAbsolute(vol, d.volMax);
    }

    private double simpleAbsolute(float vol, int volMax) {
        return simpleAbsolute(vol, volMax, simple.floatValue());
    }

    private double simpleAbsolute(float vol, int volMax, float power) {
        return Math.pow(1 - unitizeSafe(vol / (volMax + 1)), power);
        //return Math.pow(1 - (clampSafe(vol, 0, volMax) / (volMax + 1)), power);
        //return 1 - Math.pow(unitizeSafe(vol / ((float) volMax + 1)), (1/power));
    }


//    public final FloatRange feedback = new FloatRange(Util.PHI_min_1f, 0, 2);


//    public final IntRange recursionDepthMax = new IntRange(2, 0, 4);
//    public final FloatRange recursionFactor = new FloatRange(0.9f, 0, 1);



    @Override
    public float tasklinkPri(Term x, byte punc, Focus f) {
        double P =
            linkPuncPri.apply(punc)
            * linkOpPri.apply(x)
            * simpleAbsolute(x.volume(), f.volMax());

        return (float)P;
    }

    @Override
    public void link(MutableTaskLink link, TaskLink source, Object d, boolean inOrOut) {
        float power = (inOrOut ? simpleIn : simpleOut).floatValue();
        int v = link.volSum();
        double priFactor =
            linkSimplicityAbsoluteOrRelative ?
                simpleAbsolute(v, _volMax(d) * 2, power)
                :
                simpleRelative(v, source.volSum(), power);

        link.priSet(source, (float) priFactor);
    }

    @Deprecated private static int _volMax(Object c) {
        return c instanceof Deriver ? ((Deriver) c).volMax : ((Focus) c).volMax();
    }

//    @Override
//    public void accept(Focus f) {
//        if (ampAuto)
//            ampAuto((BagFocus) f);
//    }
//
//    /**
//     * normalize fill rate to focus bag's capacity
//     */
//    private void ampAuto(BagFocus f) {
//        //TODO refine
//        float fillRateExp = 8;
//        amp.set(Math.pow(f.capacity.intValue(), -1f / fillRateExp));
//    }

//    /**
//     * override to modify the tasklink before linking
//     */
//    @Override public double amp(Term t, byte punc) {
//        return amp.doubleValue()
//                //* simple(t, f)
//                ;
//    }

    /**
     * measures growth in target relative to source
     */
    private double simpleRelative(int target, int source, float power) {
        return simpleAbsolute(target, target+source, power);
//        return source/(source + (source+target)); //because both source and the source,target will exist after the creation
        //return Math.pow(source / (source + target), power);
        //return source/(source+(target * simple.floatValue()));
    }


}
//public class OpPuncActivator extends BasicActivator {
//
//
//
//    /**
//     * tasklink multiplier per punc
//     */
//    public final PuncPri punc = new PuncPri();
//
//
////public final IntRange adjacencyMax = new IntRange(8, 0, 64);
//
//    /**
//     * tasklink multiplier per op
//     */
//    public final OpPri opProb = new OpPri();
//
////    @Override
////    protected void link(MutableTaskLink y, Focus f) {
////        super.link(y, f);
////
//////        if (y.self()) {
//////            final int m = adjacencyMax.intValue();
////////            if (m > 0) adjacency.flow(y, f, m);
//////        }
////    }
//
//    @Override
//    protected double amp(Term t, byte punc, Focus f) {
//        return super.amp(t, punc, f) * this.punc.apply(punc) * opProb.apply(t);
//    }
//
//
//}