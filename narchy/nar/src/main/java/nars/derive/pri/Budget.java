package nars.derive.pri;

import jcog.Is;
import jcog.pri.op.PriMerge;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.reaction.TaskifyPremise;
import nars.focus.Focus;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;

import java.util.function.Consumer;

/**
 * attention management strategy
 */
@Is({"Occam's_razor","Occam's_superglue","Economy","Attention"})
public abstract class Budget implements Consumer<Focus> {

    public static final PriMerge premiseMerge =
        PriMerge.max;
		//PriMerge.plus;
		//PriMerge.and;
		//PriMerge.mean;
		//PriMerge.or;

    public static final PriMerge tasklinkMerge =
		PriMerge.max;
		//PriMerge.plus;

    /** priority of derived NALTask */
	public final double priDerived(NALTask t, Deriver d) {
		Premise p = d.premise;

		if (p instanceof TaskifyPremise) p = NALPremise.nalPremise(p);

		return priDerived(t, p.task(), p.belief(), d);
	}

	public abstract double priDerived(NALTask xy, NALTask x, NALTask y, Deriver d);

	/** priority of derived premise (feedback) */
	public abstract double priPremise(Premise p, Deriver d);

	/** dependent link
	 * @param link may be modified.  source is not (unless source==link)
	 * @param inOrOut whether growing inward (decomposing) or outward
	 */
	public abstract void link(MutableTaskLink link, TaskLink source, Object d, boolean inOrOut);

	@Override
	public void accept(Focus f) {

	}

    public abstract float tasklinkPri(Term x, byte punc, Focus f);

}