package nars.derive;

import jcog.Log;
import jcog.TODO;
import jcog.data.bit.MetalDibitSet;
import jcog.data.map.UnifriedMap;
import jcog.event.Off;
import jcog.exe.flow.Feedback;
import jcog.random.RandomBits;
import jcog.random.XoRoShiRo128PlusRandom;
import jcog.signal.FloatRange;
import jcog.signal.meter.FastCounter;
import nars.*;
import nars.control.Caused;
import nars.derive.op.PremiseTimeGraph;
import nars.derive.op.Termify;
import nars.derive.reaction.ReactionModel;
import nars.derive.reaction.Reactions;
import nars.derive.reaction.TaskifyPremise;
import nars.derive.util.DeriverFunctors;
import nars.derive.util.NALTaskEvaluation;
import nars.focus.Focus;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.Variable;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.term.util.conj.CondMatch;
import nars.term.util.transform.RecursiveTermTransform;
import nars.time.Every;
import nars.truth.MutableTruthInterval;
import nars.truth.Truth;
import nars.truth.func.TruthFunction;
import nars.unify.Unify;
import nars.unify.UnifyConstraint;
import nars.unify.UnifyTransform;
import org.eclipse.collections.api.map.MutableMap;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.Random;

import static nars.Op.VAR_PATTERN;
import static nars.term.atom.Bool.Null;


/**
 * evaluates a premise (task, belief, termlink, taskLink, ...) to derive 0 or more new tasks
 * instantiated threadlocal, and recycled mutably
 *
 * TODO extract common "Context" super-interface which Answer also implements
 * there are enough similarities that they can be used interchangeably in several
 * instances
 */
public abstract class Deriver extends NARPart implements Caused {

	protected static final Logger logger = Log.log(Deriver.class);

	public final ReactionModel model;

	/** offset for virtual addressing scheme for rules */
	@Deprecated public final int howOffset;

	public final transient short[] hows;
	public transient int howsCount = 0;

	/**
	 * main premise derivation unifier
	 */
	public final PremiseUnify unify;
	private final MyUnifyTransform unifyTransform;

	private final NALTaskEvaluation taskEval;

	@Deprecated public final Random random;
	public final RandomBits rng;




	/**
	 * populates retransform map
	 */
	public final NAR nar;
	public transient double eviMin;

	public Focus focus;
	public final MetalDibitSet predicateMemoizations = new MetalDibitSet(384 /* increase as necessary */);

	private final MutableMap<Atomic, Term> derivationFunctors;

	public final PremiseTimeGraph timeGraph;

	public final transient MutableTruthInterval _truth = new MutableTruthInterval();

	@Deprecated private transient Term premiseWhy;

	@Deprecated public transient Premise premise;

	public transient int volMax = NAL.term.COMPOUND_VOLUME_MAX;

	@Deprecated public final ReactionModel.PremiseRunner rules;

	@Nullable private transient Off off;
	private long now;

	protected Deriver(Reactions model, NAR n) {
		this(model.compile(n), n);
	}

	protected Deriver(ReactionModel model, NAR nar) {
		this.nar = nar;

		this.random = new XoRoShiRo128PlusRandom(0); //TODO periodically seed from ThreadLocalRandom or other entropy source
		this.rng = new RandomBits(random);
		reseedRNG();

		this.model = model;
		this.hows = new short[model.how.length];
		this.howOffset = model.how[0].why.id;

		this.rules = model.run();
		this.now = nar.time();

		this.derivationFunctors = DeriverFunctors.get(this);

		this.unify = new PremiseUnify();

		taskEval = new NALTaskEvaluation(this) {
			/** deterministic solutions only */
			@Override protected boolean termutable() {
				return false;
			}

			@Override
			public void accept(NALTask y) {
				if (!invalidDerived(y))
					_add(y.copyMeta(this.task));
			}
		};

 		this.timeGraph = new PremiseTimeGraph(this);
 		this.unifyTransform =  new MyUnifyTransform();

		nar.add(this);
		//start(nar);
	}

	public final boolean add(NALTask x) {

		if (invalidDerived(x)) return false;

		if (rng.nextBooleanFast8(NAL.derive.AUTO_EVAL_NALTASKS) && Functor.evalable(x.term())) {
			taskEval.apply(x, 1, NAL.derive.POST_PROCESS_EVAL_TRIES);
			if (taskEval.solutionsRemain == 0) return true;
		}

		_add(x);
		return true;
	}

	/**
	 * tasks input here will be re-prioritized, so they should be new and not existing
	 */
	private void _add(NALTask x) {
		if (NAL.causeCapacity.intValue() > 0)
			x.why(premise.why());

		if (x.uncreated() && x.priElseZero() == 0 /* HACK */)
			prioritize(x);

		focus.remember(x);

		perceive(x);
	}

	private void perceive(NALTask x) {
		if (NAL.DEBUG_DERIVED_NALTASKS)
			trace(x);

		FastCounter dt = nar.emotion.derivedTask;

		if (NAL.derive.CAUSE_PUNC) {
			//TODO detailed per-punc feedback
			Feedback.is(switch (x.punc()) {
				case '.' -> "derive.";
				case '!' -> "derive!";
				case '?' -> "derive?";
				case '@' -> "derive@";
				default -> throw new UnsupportedOperationException();
			}, dt);
		} else {
			dt.increment();
		}
	}


	/** TODO move this into TaskifyPremise */
	@Deprecated private void prioritize(NALTask x) {
//		if (x.BELIEF_OR_GOAL() && (x instanceof DerivedTask || NAL.derive.TRUTH_AUTO_DITHER))
//			x = SpecialTruthTask.proxy(x, x.truth().dither(nar), nar).copyMeta(x); //TODO occ dither?

		x.pri((float) focus.budget.priDerived(x, this));
	}

	private synchronized void trace(NALTask x) {
		System.out.println(x + "\n\t" + premise);
	}

	public final void add(Premise p) {
		if (invalid(p))
			rejected(p);
		else
			accepted(p);
	}

	private void accepted(Premise p) {

		p.parent = premise;

		if (p.isDeleted())
			priAuto(p);

		if (NAL.causeCapacity.intValue()==0)
			nar.emotion.derivedPremise.increment();
		else
			Feedback.is("derive.premise", nar.emotion.derivedPremise);

		accept(p);
	}

	private void priAuto(Premise p) {
		p.pri((float) focus.budget.priPremise(p, this));
	}

	private void rejected(Premise p) {
		Feedback.is("derive.premise.invalid", nar.emotion.derivedPremiseInvalid);
//			throw new WTF();
	}

	private boolean invalid(TaskLink p) {
		Term f = p.from();
		if (invalidVol(f))
			return true;
		Term t = p.to();
		return t != f && invalidVol(t);
	}

	/**
	 * creates a dependent tasklink
	 * @param inOrOut whether growing inward (decomposing) or outward
	 */
	public final void link(MutableTaskLink l, boolean inOrOut) {
		if (invalid(l)) {
			Feedback.is("derive.tasklink.invalid", nar.emotion.derivedPremiseInvalid);
			return;
		}

		focus.budget.link(l, TaskLink.parent(premise), this, inOrOut);

		_link(l);
	}

	public void _link(MutableTaskLink l) {
		focus.link(l);
	}

//	private void accept(NALTask x) {
//		focus.rememberAsync(x);
//	}

	protected abstract void accept(Premise p);
	
	public boolean invalidVol(Term t) {
		//assert(!(t instanceof Neg));

		return t.volume() > volMax;
	}

	public boolean invalid(NALTask x) {
		return invalidVol(x.term()) || (x.BELIEF_OR_GOAL() && x.evi() < eviMin);
	}

	private boolean invalidDerived(NALTask x) {
//		if (NAL.DEBUG) {
//			if (!x.isInput()) {
//				Term why = x.why();
//				if (why == null)
//					throw new TaskException("cause missing", x);
//			}
//		}

		if (invalid(x)) {
			Feedback.is("derive.NALTask.invalid");
			return true;
		}

		if (NALTask.same(premise.task(), premise.belief(), x, nar)) {
			Feedback.is("derive.NALTask.same");
			return true;
		}

		if (NAL.derive.FILTER_SUB_EVI_NALTASK && x.BELIEF_OR_GOAL() && x.evi() < eviMin) {
			if (NAL.truth.EVI_STRICT) {
				Feedback.is("derive.NALTask.eviUnderflow", nar.emotion.deriveFailTruthUnderflow);
				return true; //TODO DEBUG and avoid these
			} else {
				throw new TODO();
			}
		}

		return false;
	}

	private boolean invalid(Premise p) {

		Premise x = this.premise;
		if (x.equals(p))
			return true; //duplicate loop

		if (p instanceof TaskLink) {
			throw new UnsupportedOperationException();
		} else {

			if (p instanceof NALPremise) {

				if (NAL.derive.FILTER_SUB_EVI_TASKPREMISE)  {
					NALTask task = ((NALPremise) p).task;
					if (task.BELIEF_OR_GOAL() && task.evi() < eviMin) {
						if (NAL.truth.EVI_STRICT) {
							//TODO DEBUG and avoid these
							return true;
						} else
							throw new TODO();
					}
				}

				return ((NALPremise)p).invalid(this);
			}
		}

		return false; //TODO?
	}

	/** @param w if null, doesnt switch */
	public final synchronized void next(Focus w) {
		focus(w);
		next();
	}


	private void focus(Focus f) {
		Focus prev = this.focus;
		if (f==null) {
			f = prev;
		} else {
			if (prev!=f) {
				this.focus = f;
				start(f);
			}
		}

		NAR n = nar;

		f.tryCommit(now = n.time());

		unify.dur = Math.round(unifyDur(f, n));
		volMax = f.volMax();
		eviMin = n.confMin.evi();

	}



	/** called if focus changed */
	protected void start(Focus f) {

	}

//	/**
//	 * called when switching to a new focus.  subclasses may impl behavior such a clearing to avoid cross-contaminating contexts
//	 * with lingering data
//	 */
//	protected void focusStart(Focus f) {
//
//	}

	protected abstract void next();

	protected float unifyDur(Focus f, NAR n) {
		return
			f.durBase * n.unifyDurs.floatValue() //SOLID
			//f.dur() * n.unifyDurs.floatValue()  //FLUID (depends on Focus's mutable temporal focus)
			//f.dur() * n.unifyDurs.floatValue()
		;
	}

	private void reseedRNG() {
		//entropy
		rng.setSeed(nar.random().nextLong());
	}


	/** recycles the same instance; must be closed when finished. dont use recursively */
	public MyUnifyTransform unifyTransform(int ttl) {
		if (ttl >= 0) unifyTransform.reset(ttl);
		return unifyTransform;
	}

	public final Off every(Focus w, Every e, float amount) {
		focus(w);
//		focusStart(w);
		return on(nar.on(e, amount, nn -> next(null)));
	}

	public final Off everyCycle(Focus w) {
		return every(w, Every.Cycle, 1);
	}

	private synchronized Off on(Off o) {
		if (this.off != null) {
			this.off.close(); //clear previous on
		}
		this.off = o;
		return o;
	}

	public final float dur() {
		return focus.dur();
	}

	public final Term polarize(Term arg, boolean taskOrBelief) {
		Truth t = taskOrBelief ?
				premise.task().truth() :
				(premise.belief() != null ? premise.belief().truth() : null);

		return arg.negIf(t != null ? t.NEGATIVE() : randomBoolean());
	}

	@Deprecated @Override public Term why() {
		if (this.premiseWhy==null)
			this.premiseWhy = premise.why();
		return premiseWhy;
	}

	public final boolean setPremise(Premise nextPremise) {
		if (this.premise != nextPremise) {
			this.premise = nextPremise;
			this.premiseWhy = null;
			return true;
		}
		return false;
	}

	public final boolean randomBoolean() {
		return rng.nextBoolean();
	}

	public final boolean randomBoolean(float prob) {
		return rng.nextBooleanFast8(prob);
	}

	public final boolean randomBoolean(FloatRange f) {
		return randomBoolean(f.asFloat());
	}

	public final Term conjMatch(Term conj, Term event, boolean fast, boolean unify, boolean before, boolean matched, boolean after, boolean during) {

		try (MyUnifyTransform u = unifyTransform(NAL.derive.TTL_CONJ_MATCH)) {
			if (!unify) u.vars = 0;

			Term y = CondMatch.beforeOrAfter((Compound) conj, event, fast, before, matched, after, during, u);

			assert((before && matched && after) || !y.equals(conj)): "should have returned Null";

			if (y != Null) u.postUnified();

			return y;
		}

	}

	public final byte puncSample(Premise p) {
		return TaskLink.parent(p).punc(rng);
	}

	public final boolean projectBeliefOrGoal(TruthFunction f, MutableTruthInterval out) {

		float dur = focus.durBase;
		Premise p = this.premise;
		NAR nar = this.nar;
		double eviMin = this.eviMin;

		Truth T;
		if (f.taskTruthSignificant()) {
			NALTask TASK = p.task();
			T = //TASK.BELIEF_OR_GOAL() ?
				out.project(TASK, dur, eviMin, nar);
				//null;
			if (T != null) T = f.preTask(T);
			if (T == null) return false;
		} else
			T = null;


		Truth B;
		if (!f.single() && f.beliefTruthSignificant()) {
			NALTask BELIEF = p.belief();
			B = out.project(BELIEF, dur, eviMin, nar);
			if (B != null) B = f.preBelief(B);
			if (B == null) return false;
		} else
			B = null;

		return f.truth(out, T, B, eviMin);// && out.ditherTruth(nar, eviMin);
	}

	public final Term eval(Term x) {
		if (x.volume() <= volMax) {
			Term y = nar.eval(x);
			if (x == y || y.volume() <= volMax)
				return y;
		}

		return Null;
	}

	public void can(short id) {
		hows[howsCount++] = id;
	}

	/**
	 * current NAR time, set at beginning of derivation
	 */
	public final long now() {
		return now;
	}

	public final int ditherDT() {
		return nar.dtDither();
	}

	protected void run(Premise p) {
		setPremise(p);
		p.run(this);
		setPremise(null);
	}

	/** hack for parent premise override during cause assignment */
	public final void add(NALTask x, TaskifyPremise t) {
		//HACK since this is only necessary if cause tracing
		if (NAL.causeCapacity.intValue() == 0) {
			add(x);
		} else {
			Premise parent = premise; t.parent = parent; premise = t;
			add(x);
			t.parent = null; premise = parent;
		}
	}

	public int randomInt(int i) {
		return rng.nextInt(i);
	}


    /**
	 * main premise unification instance
	 */
	public class PremiseUnify extends Unify {
		transient int tasksRemain = -1;
		//public transient PatternReaction.DeriveTaskAction.DeriveTask deriving;
		public transient Iterable<? extends TaskifyPremise> each;

		/**
		 * TODO can ignore common variables
		 */
		public final UnifriedMap<Term, Term> retransform = new UnifriedMap<>();

		PremiseUnify() {
			super(VAR_PATTERN, rng, NAL.unify.UNIFICATION_STACK_CAPACITY);
		}


//		private void unify(Term T, Term B, Iterable<TaskifyAction> each) {
//			boolean s = taskEqualsBeliefRoot && T == B;
//			final Term Tp = taskPattern, Bp = beliefPattern;
//			boolean f = fwd >= 0;
//
//			clear(constraints);
//
//			if (_unify(f ? Tp : Bp, f ? T : B, s ? each : null)) {
//				if (!s && live())
//					_unify(f ? Bp : Tp, f ? B : T, each);
//			}
//		}


		public void unifyPremise(Iterable<TaskifyPremise> each, Termify r) {
			Term T = premise.from(), B = premise.to();
			boolean s = r.taskEqualsBeliefRoot && T == B;
			Term Tp = r.taskPattern, Bp = r.beliefPattern;
			boolean f = r.fwd >= 0;

			clear(r.constraints);

			if (unify(f ? Tp : Bp, f ? T : B, s ? each : null)) {
				if (!s && live())
					unify(f ? Bp : Tp, f ? B : T, each);
			}
		}


		private boolean unify(Term x, Term y, @Nullable Iterable<? extends TaskifyPremise> each) {
			boolean finish = each!=null;

			this.each = finish ? each : null;

			boolean unified = unify(x, y, finish);

			if (finish) this.each = null; //release

			return unified;
		}

		/**
		 * should be created whenever a different NAR owns this Derivation instance, if ever
		 */
		public final RecursiveTermTransform transformDerived = new RecursiveTermTransform() {

			@Override
			public Term applyAtomic(Atomic x) {
				if (x instanceof Variable vx)
					return resolveVar(vx);
				else
					return x instanceof Atom ? applyAtom(x) : x;
			}

			private Term applyAtom(Atomic x) {
				if (x == Premise.TaskInline)
					return premise.from();
				else if (x == Premise.BeliefInline)
					return premise.to();
				else
					return derivationFunctors.getIfAbsentValue(x, x);
			}

			@Override
			public boolean evalInline() {
				return true;
			}

		};

		@Override protected final boolean match() {

			Iterable<? extends TaskifyPremise> e = this.each;
			this.each = null;

			if (e == null) return false; /* done */

			for (var p : e) {
				if (p == null)
					break; //deleted while running?
				p.taskify(Deriver.this);
			}

			return true;
		}

		/**
		 * resolve a target (ex: task target or belief target) with the result of 2nd-layer substitutions
		 */
		public Term retransform(Term x) {
			return x.replace(retransform);
		}

		public void clear(UnifyConstraint<PremiseUnify>[] constraints) {
			this.retransform.clear();
			this.clear();
			this.setTTL(NAL.derive.TTL_UNISUBST);
			this.tasksRemain = NAL.derive.PREMISE_UNIFICATION_TASKIFY_LIMIT;
			this.constrain(constraints);
		}

		public Deriver deriver() {
			return Deriver.this;
		}
	}

	@Deprecated
	public final class MyUnifyTransform extends UnifyTransform implements AutoCloseable {

		private MyUnifyTransform() {
			super(rng);
		}

		public void postUnified() {
			unify.retransform.putAll(xy);
		}

		@Override
		public void close()  {
			clear();
		}

		public void reset(int ttl) {
			this.ttl = ttl;
			this.vars = Op.Variables;
			this.dur = unify.dur;
			this.novel = false;
			//u.volMax = Deriver.this.volMax;
		}
	}

}