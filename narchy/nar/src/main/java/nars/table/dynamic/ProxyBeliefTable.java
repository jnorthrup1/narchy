package nars.table.dynamic;

import nars.Term;
import nars.action.memory.Remember;
import nars.concept.TaskConcept;
import nars.table.BeliefTable;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.proxy.SpecialTermTask;
import nars.task.util.Answer;
import nars.term.util.TermTransformException;

/** transforms task insertions to another concept's belief table */
public abstract class ProxyBeliefTable extends DynBeliefTable {

	protected ProxyBeliefTable(Term c, boolean beliefOrGoal) {
		super(c, beliefOrGoal);
	}

	/** map an external term to an internal term */
	public abstract Term resolve(Term external);

	@Override
	public final void remember(Remember r) {

		NALTask external = r.input;
		if (external instanceof SerialTask)
			return; //TODO test
		
		Term externalTerm = external.term();
		Term internalTerm = resolve(externalTerm);

		TaskConcept internalConcept = r.nar().conceptualizeTask(internalTerm);
		if (internalConcept == null)
			return;

		NALTask internal = SpecialTermTask.proxy(external, internalTerm, true);
		if (internal == null)
			throw new TermTransformException("proxy task failure", externalTerm, internalTerm);

		r.concept = internalConcept;
		r.input = internal.copyMetaAndCreation(external);

		if (r.stored(internalConcept.table(external.punc()))) {
			//restore original input
			r.stored = external.copyMetaAndCreation(r.stored);
		} else {
			//assert(r.stored==null);
		}
		r.input = external;

	}


	/** forward to the host concept's appropriate table */
	@Override  public void match(Answer a) {

		Term _x = a.term();
		Term x;
		if (_x == null) x = term; //use default
		else x = _x;

		Term y = resolve(x);
		BeliefTable t = a.nar.table(y, beliefOrGoal);
		if (t == null)
			return;

		assert(t!=this);
		a.term(y); //save (push)
		t.match(a); //TODO rewrite the terms of accumulated tasks in Answer?
		a.term(_x); //restore (pop)
		a.replace(y, x);
	}


}