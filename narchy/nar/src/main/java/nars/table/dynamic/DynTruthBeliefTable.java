package nars.table.dynamic;

import jcog.WTF;
import nars.NAL;
import nars.Term;
import nars.task.NALTask;
import nars.task.util.Answer;
import nars.term.Compound;
import nars.truth.AnswerTaskify;
import nars.truth.dynamic.DynTruth;


/**
 * computes dynamic truth according to implicit truth functions
 * determined by recursive evaluation of the compound's sub-component's truths
 */
public final class DynTruthBeliefTable extends DynBeliefTable {

    private final DynTruth model;

    public DynTruthBeliefTable(Term c, DynTruth model, boolean beliefOrGoal) {
        super(c, beliefOrGoal);
        this.model = model;
    }

    @Override
    public final void match(Answer a) {
        NALTask y;
        try (AnswerTaskify t = new AnswerTaskify(model, beliefOrGoal, a)) {
            y = t.task(term(a));
        }
        if (y != null) {

            //simple compound filter
            if (y.term() instanceof Compound != this.term instanceof Compound) {
                if (NAL.DEBUG) throw new WTF();//???
                return;
            }

            a.accept(y);
        }
    }

    private Compound term(Answer a) {
        Term template = a.term();
        return (Compound) (template != null ? template : term);
    }




}