package nars.table.question;

import nars.action.memory.Remember;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.task.util.Answer;

import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * task table used for storing Questions and Quests.
 * simpler than Belief/Goal tables
 */
public interface QuestionTable extends TaskTable {

    /**
     * allows question to pass through it to the link activation phase, but
     * otherwise does not store it
     */
    QuestionTable Empty = new QuestionTable() {

        @Override
        public Stream<? extends NALTask> taskStream() {
            return Stream.empty();
        }

        @Override
        public void match(Answer a) {

        }

        @Override
        public void remember(/*@NotNull*/ Remember r) {

        }


        @Override
        public void clear() {

        }

        @Override
        public void forEachTask(Consumer<? super NALTask> x) {

        }

        @Override
        public boolean remove(NALTask x, boolean delete) {
            return false;
        }


        @Override
        public void taskCapacity(int newCapacity) {

        }


        @Override
        public int taskCount() {
            return 0;
        }

    };



}
