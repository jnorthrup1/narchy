package nars.table.temporal;

import jcog.Is;
import nars.table.BeliefTable;
import nars.task.NALTask;

import java.util.function.Predicate;


/**
 * https://en.wikipedia.org/wiki/Compressed_sensing
 */
@Is("Compressed_sensing")
public interface TemporalBeliefTable extends BeliefTable {


    void taskCapacity(int temporals);


    void whileEach(Predicate<? super NALTask> each);

    void removeIf(Predicate<NALTask> remove, long finalStart, long finalEnd);

    @FunctionalInterface interface TimeTruthProcedure {
        void accept(long when, float freq, float conf);
    }

    default void plot(TimeTruthProcedure each) {
        forEachTask(t -> {
            float freq = t.freq(), conf = (float) t.conf();
            long e = t.end();
            for (long tt = t.start(); tt <= e; tt++) {
                each.accept(tt, freq, conf);
                //System.out.println(tt + ", " + freq + "," + conf);
            }
        });
    }

}