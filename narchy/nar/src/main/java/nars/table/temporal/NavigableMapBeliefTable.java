package nars.table.temporal;

import jcog.Fuzzy;
import jcog.Util;
import jcog.cluster.KMeansPlusPlus;
import jcog.data.DistanceFunction;
import jcog.data.list.Lst;
import jcog.data.map.ConcurrentSkipListMap2;
import jcog.math.LongInterval;
import jcog.random.XoRoShiRo128PlusRandom;
import jcog.sort.FloatRank;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.action.memory.Remember;
import nars.focus.Focus;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.Answer;
import nars.task.util.TaskOccurrence;
import nars.task.util.TaskRegion;
import nars.term.Compound;
import nars.term.util.DTVector;
import nars.term.util.DTVector.ConceptShapeException;
import nars.truth.Truth;
import nars.truth.proj.IntegralTruthProjection;
import nars.truth.proj.MutableTruthProjection;
import nars.truth.util.Revision;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.VarHandle;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.lang.Long.MAX_VALUE;
import static java.lang.Long.MIN_VALUE;
import static java.lang.Math.abs;
import static jcog.math.LongInterval.MAX;
import static nars.Op.ETERNAL;
import static nars.Op.TIMELESS;


/**
 * TODO eval https://github.com/yahoo/Oak
 */
public class NavigableMapBeliefTable implements TemporalBeliefTable /*, TODO TimeRangeBag<NALTask,NALTask> */ {

    /**
     * how many tasks to compress per compression iteration.
     * a batch size.  larger means potentially higher throughput, at the cost of less accuracy.
     */
    static final int COMPRESSION_RATE =
            8;
            //3;
            //4;
            //1/3f;
            //0.1f;
            //0.05f;

    /**
     * lower is more precision, >=2
     */
    private static final int MERGE_NEIGHBORS_ACTUAL = 2;

    private static final int MERGE_NEIGHBORS_POTENTIAL = MERGE_NEIGHBORS_ACTUAL+1; //TODO increase iff temporalable

    private static final VarHandle COMPRESSING = Util.VAR(NavigableMapBeliefTable.class, "compressing", int.class);




    private final NavigableMap<TaskRegion, NALTask> map;
    @SuppressWarnings({"unused", "FieldMayBeFinal"})
    private int compressing;
    private volatile int capacity;

    private final static MergeAccept mergeAccept =
            MergeAccept.Always
            //MergeAccept.BetterThanWeakest
            ;

    public NavigableMapBeliefTable(NavigableMap<TaskRegion, NALTask> map) {
        this.map = map;
    }

    public NavigableMapBeliefTable() {
        this(
                //new ConcurrentSkipListMap<>(cmp)
                new ConcurrentSkipListMap2<>(TaskRegionComparator.the)
                //Maps.synchronizedNavigableMap(new TreeMap<>(cmp)) //<- doesnt work
                //Collections.synchronizedNavigableMap(new TreeMap<>(cmp)) //<- doesnt work
        );
    }

    private NALTask _remove(NALTask x) {
        return map.remove(x);
    }

    @Override
    public void remember(Remember r) {
        NALTask x = r.input;
        if (x instanceof SerialTask || x.ETERNAL()) return;

        remember(x, r);
    }

    private boolean tryMerge(NALTask x, @Nullable Remember r) {


        long is = x.start(), ie = x.end();

        /*expand search interval include adjacent-but-not-intersecting tasks */
        int margin = 1;

        float freqRes = -1, confRes = -1; //lazy-init
        Truth it = null; //lazy-init

        for (Iterator<NALTask> yy = intersecting(is - margin, ie + margin).iterator(); yy.hasNext(); ) {
            NALTask y = yy.next();
            long es = y.start(), ee = y.end();
            if (LongInterval.intersectsRaw(is, ie, es, ee)) { //temporal union
                if (Arrays.equals(y.stamp(), x.stamp())) {

                    if (freqRes < 0) {
                        //initialize on first use
                        if (r != null) {
                            NAR n = r.nar();
                            freqRes = n.freqResolution.floatValue();
                            confRes = n.confResolution.floatValue();
                        } else {
                            freqRes = confRes = NAL.truth.TRUTH_EPSILON;
                        }
                        it = x.truth();
                    }

                    Truth et = y.truth();
                    if (it.equalFreq(et, freqRes)) {

                        //				int xys = Stamp.equalsOrContains(existing.stamp(), incoming.stamp());
                        //if (xys != Integer.MIN_VALUE) {

                        Term exT = y.term();
                        if (exT.equals(x.term())) { //TODO only necessary if terms can differ (ie: temporally) in this table

                            double ic = it.conf(), ec = et.conf();
                            NALTask xy = null;

                            //confidence difference; dominator short-cuts
                            if (ic <= ec + confRes / 2 && LongInterval.containsRaw(es, ee, is, ie)) {
                                xy = y;
                            } else if (ec <= ic + confRes / 2 && LongInterval.containsRaw(is, ie, es, ee)) {
                                xy = x;
                            } else if (Util.equals(ic, ec, confRes / 2)) {

                                /*if ((xys == 0 || xys == +1) && LongInterval.containsRaw(es, ee, is, ie)) //existing equals or contains incoming
                                    return existing;
                                else if ((xys == 0 || xys == -1) && LongInterval.containsRaw(is, ie, es, ee)) //incoming contains existing
                                    return incoming;
                                else if (xys == 0 &&*/
                                xy = Revision.mergeIntersect(y, x,
                                        //revise(te, ee-es, ti, ie-is)
                                        //Truth.stronger(te, ti)
                                        exT, /*is, ie, es, ee,*/ freqRes, confRes);//revise(te, ee-es, ti, ie-is)

                            }
                            if (xy != null && absorbMerge(xy, x, y, yy, r))
                                return true; //done

                        }
                    }
                }
            }

        }
        return false;
    }

    /**
     *
     * @param xy
     * @param x  incoming task
     * @param y  (next) existing task
     * @param yy iterator that supplies y
     * @param r
     * @return
     */
    private boolean absorbMerge(NALTask xy, NALTask x, NALTask y, Iterator<NALTask> yy, @Nullable Remember r) {
//                if (!xy.term().concept().equals(xConcept == null ? (xConcept = x.term().concept()) : xConcept)) {
//                    //merge result has different concept
//                } else

        if (xy == x) {
            yy.remove();
            /* y absorbed; continue scanning */
        } else {
            if (xy == y) {
                /* y absorbed x */
            } else {
                /* xy absorbs x and y */
                yy.remove();
                boolean stored = remember(xy,  new Remember(r.focus) /* null HACK */);
                if (!stored)
                    return false;
            }
            if (r != null) r.store(xy);
            return true;
        }
        return false;
    }

    private boolean remember(NALTask _x, @Nullable Remember r) {

        if (tryMerge(_x, r)) return true; //merged

        NALTask x = _x.the().copyMeta(_x);

//        //TEMPORARY
//        if (map.size() > 0) {
//            if (!conceptMatches(x.term()))
//                throw new WTF();
//        }

        NALTask y = map.putIfAbsent(x, x);

        if (y != null) {
            //equal merged
        } else {
            ensureCapacity(r);
            y = x;
        }

        if (r != null && !y.isDeleted())
            r.store(y);

        return r == null || r.stored != null;
    }

    private void ensureCapacity(@Nullable Remember r) {

        if (!COMPRESSING.compareAndSet(this, 0, 1))
            return; //already compressing
        try {

            int cap = capacity();
            if (cap == 0) {
                clear();
                return;
            }

            int excess = taskCount() - cap;
            if (excess <= 0)
                return;


            compress(r, Math.max(1, cap - COMPRESSION_RATE));

        } finally {
            COMPRESSING.setVolatile(this, 0);
        }

    }

    private void compress(@Nullable Remember r, int targetSize) {
        new MergeClusters(targetSize).accept(r);
    }

    public final int capacity() {
        return capacity;
    }

    public long mid() {
        LongInterval a = map.firstKey();
        if (a == null) return TIMELESS;
        LongInterval b = map.lastKey();
        if (a == b) {
            return a.mid();
        } else {
            if (b == null) return TIMELESS;
            return Fuzzy.mean(Math.min(a.start(), b.start()), Math.max(a.end(), b.end()));
        }
    }


    /**
     * revise a subset of tasks
     * TODO add a window that this zips along in batches
     */
    @Override
    public int taskCount() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void taskCapacity(int cap) {
        capacity = cap; //if (capacity.getAndSet(cap) > cap)
        ensureCapacity(null);
    }

    @Override
    public boolean remove(NALTask x, boolean delete) {
        NALTask y = _remove(x);
        if (y != null) {
            if (delete)
                y.delete();
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Stream<? extends NALTask> taskStream() {
//        Collection<NALTask> s = map.values();
//        return s.isEmpty() ? Stream.empty() : s.stream();
        return map.values().stream();
    }

    @Override
    public void match(Answer a) {
        int size = taskCount();
        if (size > 0) {
            if (size == 1 || size <= a.ttl) {
                whileEach(a); //ALL
            } else {
                scanNear(scanStart(a), a); //SOME
            }
        }
    }

    private static long scanStart(Answer a) {
        long qs = a.start();
        if (qs == ETERNAL) return a.time();

        return NAL.Temporal.SCAN_START_RANDOM_OR_MID ?
            a.rng().nextLong(qs, a.end()) //uniform random
            //TODO guassian random
            :
            Fuzzy.mean(qs, a.end()); //midpoint
    }

    public final void scanNear(long x, Predicate<NALTask> a) {
        scanNear(x, true, false, a);
    }

    /** outwards-proceeding scan,
     *  alternating between upwards and downwards to next nearest task in either direction,
     *  centered at 'x' */
    public void scanNear(long x, boolean inclusiveBelow, boolean inclusiveAbove, Predicate<NALTask> a) {
        TaskRegion m = TaskOccurrence.the(x);

        Iterator<NALTask>
                bot = head(inclusiveBelow, m),
                top = tail(inclusiveAbove, m);

        NALTask b = null, t = null;
        NALTask next;
        do {
            if (b == null && bot != null) {
                b = bot.hasNext() ? bot.next() : null;
                if (b == null) bot = null;
            }
            if (t == null && top != null) {
                t = top.hasNext() ? top.next() : null;
                if (t == null) top = null;
            }

            boolean tOrB;
            if (b == null && t == null)
                break;
            else if (b != null && t != null) {
                tOrB = scanNearest(x, b, t, a);
            } else
                tOrB = t!=null;

            if (tOrB) {
                next = t;
                t = null;
            } else {
                next = b;
                b = null;
            }

        } while (a.test(next));

    }


    private Iterator<NALTask> head(boolean inclusiveBelow, TaskRegion m) {
        NavigableMap<TaskRegion, NALTask> s;
        if (map instanceof ConcurrentSkipListMap2 M) {
            s = M.headMapDescending(m, inclusiveBelow);
        } else {
            s = map.headMap(m, inclusiveBelow).descendingMap();
        }
        return iter(s);
    }

    private Iterator<NALTask> tail(boolean inclusiveAbove, TaskRegion m) {
        return iter(map.tailMap(m, inclusiveAbove));
    }

    private Iterator<NALTask> iter(NavigableMap<TaskRegion, NALTask> s) {
        return s.values().iterator();
    }

    private static boolean scanNearest(long x, NALTask b, NALTask t, Predicate<NALTask> a) {
        long db = b.diff(x), dt = t.diff(x);
        return dt == db ?
            (a instanceof Answer A ? A.rng().nextBoolean() : ThreadLocalRandom.current().nextBoolean()) :
            dt < db;
    }

    @Override
    public void forEachTask(Consumer<? super NALTask> x) {
        map.values().forEach(x);
    }

    @Override
    public void forEachTask(long minT, long maxT, Consumer<? super NALTask> x) {
        intersecting(minT, maxT).forEach(x);
    }

    @Override
    public void removeIf(Predicate<NALTask> remove, long minT, long maxT) {

        Iterator<NALTask> ii = intersecting(minT, maxT).iterator();
        while (ii.hasNext()) {
            if (remove.test(ii.next()))
                ii.remove();
        }

    }

    @Override
    public void whileEach(Predicate<? super NALTask> each) {
        for (NALTask x : map.values()) {
            if (!each.test(x))
                return;
        }
    }

    private NavigableMap<TaskRegion, NALTask> subMap(long minT, long maxT) {
        //assert(minT <= maxT);

        if (minT == MIN_VALUE && maxT == MAX_VALUE)
            return map; //everything

        TaskRegion a = TaskOccurrence.the(minT);
        TaskRegion b = (maxT == minT) ? a : TaskOccurrence.the(maxT);
        return map.subMap(
                a, true,
                b, true);
    }

    private Iterable<NALTask> intersecting(long minT, long maxT) {
        return subMap(minT, maxT).values();
    }

    /**
     * provides an estimate for the total range spanned by the table
     */
    public long tableDur(long when) {
        long[] r = range();
        if (r == null) return 1;
        else return Math.max(1, Math.max(abs(r[1] - when), abs(when - r[0])));
    }

    @Nullable
    public long[] range() {
        TaskRegion a = map.firstKey();
        if (a != null) {
            TaskRegion b = map.lastKey();
            if (b != null) {
                return new long[]{a.start(), b.end()};
            }
        }
        return null;
    }

    protected String summary() {
        return taskCount() + "/" + capacity() + " tasks, range=" + Arrays.toString(range());
    }

//    private static boolean acceptMerge(MutableTruthProjection a, NALTask merged) {
//
//        if (NAL.DEBUG) {
//            if (!merged.term().equalConcept(a.getFirst().term()))
//                return false; //concept change
//        }
//
//        double bValue = -value(victim);
//        double aValue = value(merged) /* * a.density()*/ - a.sumOfDouble(NavigableMapBeliefTable::value);
//        return aValue >= bValue;
//    }

    private boolean conceptMatches(Term x) {
        Term c = _concept();
        return c == null || c.equalConcept(x);
    }

    @Deprecated @Nullable private Term _concept() {
        try {
            TaskRegion k = map.firstKey();
            return k != null ? ((NALTask) k).term().concept() : null;
        } catch (NoSuchElementException e) {
            return null; //HACK
        }
    }

    private int revise(MutableTruthProjection t, @Nullable Lst<NALTask> victimList, Remember r) {

        t.eviMin(Math.max(t.eviMin(), mergeAccept.eviMin(t)) );

        NALTask merged = t.task();

        if (merged == null || !mergeAccept.test(t, merged))
            return 0; //result became too dilute

        int net = 0;
        if (conceptMatches(merged.term())) {
            //insert to same concept
            if (remember(merged, r==null ? null : new Remember(r.focus)))
                net--;
        } else {
            //insert to another concept

            Focus f = r!=null ? r.focus : null;
            if (f == null) {
                //r.nar().input(merged);
                //throw new WTF();
                return 0; //??
            } else {
                f.accept(merged);
            }

        }

        for (NALTask s : t.sources()) {
            if (victimList != null)
                victimList.removeFirstInstance(s);
            if (remove(s, true))
                net++;
        }

        return Math.max(0, net);
    }

    enum MergeAccept implements BiPredicate<MutableTruthProjection,NALTask> {
        Always() {
            @Override
            public boolean test(MutableTruthProjection t, NALTask merged) {
                return true;
            }
        },
        BetterThanWeakest() {
            @Override
            public boolean test(MutableTruthProjection t, NALTask merged) {
                return value(merged) > value(weakest(t));
            }
            private static double value(NALTask b) {
                return b.evi();
                //return b.evi() * b.range();
            }

            double eviMin(MutableTruthProjection t) {
                return weakest(t).evi();
            }
        };

        private static NALTask weakest(MutableTruthProjection t) {
            return t.weakestSource();
        }

        double eviMin(MutableTruthProjection t) {
            return Double.NEGATIVE_INFINITY;
        }

    }


//    @Nullable
//    protected static NALTask revise(TruthProjection t, @Nullable Remember r) {
////        if (r != null) {
////            //test revision options
////            final NALTask fir = t.get(0);
////            Term x = fir.term();
////            if (x.IMPL()) {
////                //TODO > 2
////                if (t.size() == 2) {
////                    final NALTask sec = t.get(1);
////                    if (fir.stampOverlapping().test(sec)) {
////                        final Term y = sec.term();
////                        final Term s = x.sub(0);
////                        final Term ys = y.sub(0);
////                        if (s.equals(ys)) {
////
////                            int dt0 = x.dt();
////                            if (dt0 == DTERNAL) dt0 = 0; //HACK
////                            int dt1 = y.dt();
////                            if (dt1 == DTERNAL) dt1 = 0; //HACK
////                            if (Math.abs(dt0 - dt1) >= r.nar().dur()) { //different DT //TODO how different
////                                final Term xp = x.sub(1), yp = y.sub(1);
////                                final boolean n0 = fir.NEGATIVE();
////                                final boolean n1 = sec.NEGATIVE();
////                                Term template = IMPL.the(s, XTERNAL, CONJ.the(xp.negIf(n0), XTERNAL, yp.negIf(n1)));
////                                if (template.volume() <= r.nar().volMax()) {
////                                    DynTaskify d = new ReviseTaskify(DynImplConj.DynImplConjPred, (Compound) template, t, r);
////                                    d.add(n0 ? new SpecialNegTask(fir) : fir);
////                                    d.add(n1 ? new SpecialNegTask(sec) : sec);
////                                    NALTask z = d.taskClose();
////                                    if (z != null)
////                                        return z;
////                                    else {
////                                        //else: fail, continue below
////                                    }
////                                }
////                            }
////                        }
////                    }
////                }
////            } else {
////                //TODO conj, etc..
////            }
////
////        }
//
//        //TODO implement intermpolation revision when revision fails (ex: stamps overlap)
//
//        return t.task();
//    }

    private abstract static class Victimizer implements FloatRank<NALTask> {
        protected long now;
        @Deprecated
        protected float dur;

        public final double far(LongInterval x) {
            return x.timeTo(now, false, MAX /*MEAN*/)/dur; //divide by dur is not necessary, but provides extra precision because of foating-point
        }
    }


    private static class Weak extends Victimizer {
        @Override
        public float rank(NALTask x, float min) {
            //return (float) -nalTask.evi();
            //return (float) ((1 - x.conf()) / x.range());
            return (float) (-x.evi() / x.range());
        }
    }


    private static class Far extends Victimizer {
    //        static final double rangeImportance = 1/4f;

        @Override
        public float rank(NALTask x, float min) {
            double d = far(x);
    //            final double r = x.range()/dur;

            //final double dist = 1 + Math.max(0, (d - rangeImportance * r));
            //final double dist = d / (1 + r * rangeImportance);
    //            return (float) (d / (1 + x.conf() * r/(r+d)));
            //return (float) (d / (1 + x.conf() * r));
            return (float) d;
            //return (float)(dist / (1 + x.evi()));
        }
    }

    private static class FarWeak extends Victimizer {
        @Override
        public float rank(NALTask x, float min) {
            //long dist = abs(now - x.mid());
            double dist = x.timeMeanTo(now);
            return (float) ((1+dist)/((1+x.evi()) * x.range()));
            //return (float) ((1+Math.pow(dist, 2))/((1+x.evi()) * x.range()));
        }
    }
//    /** TODO needs refined */
//    private static class FarWeak extends Victimizer {
//
//
//        @Override
//        public float rank(NALTask x, float min) {
//
//            long rHalf = x.range()/2; //TODO maybe /4, etc
//            double xEvi = x.eviInteg(
//                //now, now,
//                now - rHalf, now + rHalf,
//                    dur, 0);
//            double d = -xEvi;
//
//            //double d = (1+far(x)) * (1/(1 + (x.evi() * (x.range()/dur))));///*1 + 0.5f * */ (1 - x.conf()));
//
//            //double d = (1+far(x)) * (/*1 + 0.5f * */ (1 - x.conf()));
//            //double d = far(x) + 0.5f * (1 - x.conf());
//            //double d = far(x) / (1 + x.conf());
////            final double r = x.range()/dur;
//
//            //final double dist = 1 + Math.max(0, (d - rangeImportance * r));
//            //final double dist = d / (1 + r * rangeImportance);
////            return (float) (d / (1 + x.conf() * r/(r+d)));
//            //return (float) (d / (1 + x.conf() * r));
//            return (float) d;
//            //return (float)(dist / (1 + x.evi()));
//        }
//
//
//    }


    private static final class MyEviIntegralTruthProjection extends IntegralTruthProjection {

        MyEviIntegralTruthProjection() {
            super(MERGE_NEIGHBORS_POTENTIAL); //assert(timeAuto);
            sizeMin(2).sizeMax(MERGE_NEIGHBORS_ACTUAL);
        }

        @Override protected void sort() {
            //reverse; accommodate the weakest tasks of the pool
            sort((int a, int b) -> compare(b,a));
        }

        @Override
        protected boolean concentrate() {
            return NAL.Temporal.COMPRESS_MERGE_CONCENTRATE;
        }

    }

    abstract class TemporalCompression implements Consumer<Remember> {

        final int targetSize;

        protected long now;
        protected float dur;

        @Nullable
        protected Remember r;


        protected TemporalCompression(int targetSize) {
            this.targetSize = Math.max(1, targetSize);
            assert (targetSize >= 0);
        }

        protected abstract void iterate(MutableTruthProjection t);


        @Override
        public final void accept(Remember r) {

            this.r = r;
            now = r != null ? r.time() : mid();
            dur = tableDur(now);

            MutableTruthProjection t = new MyEviIntegralTruthProjection();
            if (r!=null)
                t.dtDither(r.nar().dtDither());

            iterate(t);

            t.delete();
        }

    }

    private class MergeClusters extends TemporalCompression implements DistanceFunction {

        /** compression eligibility */
        private static final float ELITISM =
            0; //all tasks
            //0.1f; //most tasks
            //0.25f;

        /** TODO determine according to size, and how many need removed, etc */
        @Deprecated static final float clusterRedundancy = 1;

        //private static final float R_BALANCE = 8;
        //private static final float T_BALANCE = 4;
        private final float S_FACTOR = 1;

        /** polarity balance: determines low-pass oscillation frequency that can be held before wanting to flatline it */
        private final float F_FACTOR = 0.75f;

        /** TODO balance with DT_SPECIFICITY in Answer strength */
        private final float DT_FACTOR = 1.25f;

        private transient DTVector dtv;

        final Victimizer w =
            new FarWeak();
            //new FarWeak();
            //new Far();

        MergeClusters(int targetSize) {
            super(targetSize);
        }

        private static final int dimsBase = 3;
        
        double[] apply(NALTask x, double[] y) {

            long xs = x.start(), xe = x.end();
            long now = this.now;

            double dur2 = dur * 2;
            /* shift start */ y[0] = (xs-now)/ dur2;
            /* shift end   */ y[1] = (xe-now)/ dur2;
            y[2] = x.freq();

            if (y.length > 3) {
                boolean ok = dtv.set((Compound) x.term(), y, 3);
                if (!ok) {
                    x.delete();
                    throw new ConceptShapeException();
                }
            }

            return y;
        }

        @Override
        public double distance(double[] a, double[] b) {

            ///* temporal separation */ double dse = LongInterval.separation(as,ae,bs,be);
            ///* total range delta */ double dse = abs(as-bs) + abs(ae-be);
            ///* mid difference */ double dm = abs((ae - as) - (be - bs));
            ///* range difference */

            //double dr = abs((a[1] - a[0]) - (b[1] - b[0]));

//            /* time diff */ double dt =
//                    abs(a[0] - b[0]) + abs(a[1] - b[1]);
            /* time sep */ double ds =
                    LongInterval.minTimeTo(a[0], a[1], b[0], b[1]);
                    //LongInterval.diffTotal(a[0], a[1], b[0], b[1]);

            //double dsNorm = ds > 0 ? ds / (1/dur + Math.min(a[1] - a[0], b[1] - b[0])) : 0;

            /* freq difference */double df = abs(a[2] - b[2]);

            double dfR = df > 0 ?
                    df * /*rangeSum */ ((a[1] - a[0]) + (b[1] - b[0]))
                    : 0;

            //double d = sqr(T_BALANCE * dt) + sqr(S_BALANCE * ds) + sqr(F_BALANCE * df);
            double d =
                //R_BALANCE * dr +
                //T_BALANCE * dt +
                S_FACTOR * ds +
                //S_FACTOR * dsNorm +
                //F_FACTOR * df;
                F_FACTOR * dfR
            ;

            int al = a.length;
            if (al > 3)
                d += DT_FACTOR * DTVector.pctDiff(a, b, 3, al);

            return d;
        }


        @Override
        protected void iterate(MutableTruthProjection t) {

            Lst<NALTask> victims = victims();

            int v = victims.size(); //assert(t>1);
            if (v < 2)
                return;

            int mergesRemain = taskCount() - targetSize;

            /* TODO tune */
            int centroids =
                (int) Math.ceil(((float)v)/MERGE_NEIGHBORS_POTENTIAL);
//                (int)Math.ceil(Math.max(
//                    mergesRemain * clusterRedundancy,
//                    ((float)v)/MERGE_NEIGHBORS_POTENTIAL)
//                );
            if (centroids < 2 || v <= centroids)
                return; //WTF??

            int dtDims = DTVector.count(victims.get(0).term());
            int d = dimsBase + dtDims;
            if (dtDims > 0)
                dtv = new DTVector();

            KMeansPlusPlus<NALTask> n = new KMeansPlusPlus<>(centroids, d, this, new XoRoShiRo128PlusRandom()) {
                @Override
                public double[] coord(NALTask x, double[] c) {
                    return apply(x, c);
                }
            };

            try {
                learn(victims, mergesRemain, n);

                mergesRemain = mergeClusters(t, victims, mergesRemain, n);

                if (mergesRemain > 0) //TODO use actual size because reinsertions may have caused their own merges making the forced eviction here unnecessary
                    evict(mergesRemain, victims, w);

            } catch (ConceptShapeException e) {
                //HACK
                if (NAL.DEBUG)
                    throw e;
                else {
                    //HACK nuke it from orbit to be sure
                    boolean removed = map.keySet().removeIf(
                            z -> ((NALTask)z).isDeleted());
                    //map.clear();
                }
            } finally {
                n.clusters.delete();
                n.clear();
                victims.delete();

                if (dtv!=null) {
                    dtv.close();
                    dtv = null;
                }
            }

        }

        private int mergeClusters(MutableTruthProjection t, Lst<NALTask> victims, int mergesRemain, KMeansPlusPlus<NALTask> n) {
            int batchSize = MERGE_NEIGHBORS_POTENTIAL;

            for (int c = 0; c < n.k && mergesRemain > 0; c++) {

                int cn = n.valueCount(c);
                if (cn < 2) continue;

                int batches = Math.max(1, (int) ((float) cn) / batchSize);
                Iterator<NALTask> cc = n.valueIterator(c);
                for (int b = 0; b < batches && mergesRemain > 0; b++) {
                    t.clear();

                    for (int bs = 0; bs < batchSize && cc.hasNext(); bs++)
                        t.add(cc.next());

                    if (t.size() > 1)
                        mergesRemain -= revise(t, victims, r);
                }
            }

            return mergesRemain;
        }

        private void learn(Lst<NALTask> victims, int mergesRemain, KMeansPlusPlus<NALTask> n) {

            int centroids = n.k;
            int iters = 1 + (int)Math.ceil(Math.sqrt(centroids) * NAL.answer.NAVIGABLE_MAP_CLUSTERING_PRECISION);

            n.clusterDirect(victims/*HACK*/.clone(), iters);

            n.sortClustersByVariance();
            //clusters.shuffleThis(rng);
            //n.clusters.sortThisByFloat(z -> (float) z.center[2]); //sort by increasing (center) conf
        }



        private Lst<NALTask> victims() {

            int n = taskCount();

            Lst<NALTask> victims = new Lst<>(n);
            map.values().removeIf(x -> {
                if (x.isDeleted()) return true;
                victims.add(x);
                return false;
            });

            //weakest first TODO refine
            w.now = this.now;
            w.dur = this.dur;// / n;
            //TODO use RankedN<NALTask> not Lst<NALTask>
            victims.sortThisByFloat(Z -> -w.floatValueOf(Z));

            if (ELITISM > 0)
                victims.removeAbove(Math.round(victims.size() * ELITISM) );

            return victims;
        }


    }

    /**
     * forcibly evict tasks until capacity reached
     */
    private void evict(int toRemove, Lst<NALTask> tasks, FloatRank<NALTask> w) {

        if (toRemove <= 0) return;

        for (int i = 0, n = tasks.size(); toRemove > 0 && i < n; i++) {
            NALTask k = tasks.getAndNull(i);
            if (k!=null) {
                if (remove(k, true))
                    toRemove--;
            }
        }

        if (toRemove > 0)
            evictForce(toRemove, w);
    }

    /** HACK */
    private void evictForce(int toRemove, FloatRank<NALTask> w) {

//        if (map.size() <= toRemove) {
//            //EMERGENCY GENOCIDE
//            map.clear();
//            return;
//        }

        while (toRemove > 0) {
            //TODO compare first and last entry and remove weaker, or weighted random by their strength

            NALTask first = (NALTask) map.firstKey();
            if (first == null)
                break; //map is empty

            NALTask last = (NALTask) map.lastKey();
            if (first==last) {
                map.pollFirstEntry();
                break;
            } else {
                //compare
                if (w.rank(first) > w.rank(last))
                    map.pollFirstEntry();
                else
                    map.pollLastEntry();
            }
            toRemove--;
        }
    }


}