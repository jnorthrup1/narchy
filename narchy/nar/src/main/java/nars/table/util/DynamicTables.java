package nars.table.util;

import jcog.Util;
import jcog.WTF;
import jcog.data.list.Lst;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.table.BeliefTable;
import nars.table.BeliefTables;
import nars.table.dynamic.AbbreviationBeliefTable;
import nars.table.dynamic.DynTruthBeliefTable;
import nars.table.dynamic.ImageBeliefTable;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Variable;
import nars.term.atom.BytesAtom;
import nars.term.util.Image;
import nars.truth.dynamic.*;
import org.eclipse.collections.api.block.function.primitive.ObjectBooleanToObjectFunction;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static nars.Op.AtomicConstant;
import static nars.Op.hasAny;

public enum DynamicTables {
	;

	/**
	 * returns the overlay tables builder for the term, or null if the target is not dynamically truthable
	 */
	public static @Nullable ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicModel(Term t) {

		if (t instanceof Compound && !dynamicPreFilter((Compound)t))
			return null; //TODO maybe this can answer query questions by index query

		return switch (t.op()) {
			case ATOM -> dynamicAtom(t);
			case INH -> dynamicInh((Compound) t);
			case SIM -> dynamicSim(t);
			case IMPL -> dynamicImpl((Compound) t);
			case CONJ -> dynmicConj((Compound) t);
			case DELTA -> dynamicDelta();
			case NEG -> throw new WTF("negation terms can not be conceptualized as something separate from that which they negate");
			default -> null;
		};
	}

	@Nullable
	private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicAtom(Term t) {
		if (t instanceof BytesAtom)
			return (x,bOrG) -> new BeliefTable[] { new AbbreviationBeliefTable((BytesAtom)x, bOrG) };
		else
			return null;
	}

	@Nullable
	private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicDelta() {
		return NAL.truth.DYNAMIC_DELTA ? (
				NAL.truth.DYNAMIC_DELTA_GOALS ?
						table(DynDelta.the)
						:
						tableBeliefs(DynDelta.the)) : null;
	}

	@Nullable private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynmicConj(Compound t) {
		return NAL.truth.DYNAMIC_CONJ_TRUTH && DynConj.condDecomposeable(t, true) ?
				table(DynConj.Conj) : null;
	}

	/**
	 * TODO maybe some Indep cases can work if they are isolated to a sub-condition
	 */
	private static boolean dynamicPreFilter(Compound x) {
		int s = x.structure();
		return !hasAny(s, Op.VAR_QUERY.bit
				/*| Op.VAR_INDEP.bit*/
//				| Op.VAR_DEP.bit
		) && hasAny(s, AtomicConstant);
	}

	/** TODO allow indep var if they are involved in (contained within) either but not both subj and pred */
	@Nullable private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicImpl(Compound t) {

		List<DynTruth> c = new Lst<>(3);

		Subterms tt = t.subterms();
		Term s = tt.sub(0), p = tt.sub(1);
		Term su = s.unneg();


		boolean varSeparate = varSeparate(su, p);

		if (tt.hasAny(Op.CONJ) && varSeparate) {
			
			if (NAL.truth.DYNAMIC_IMPL_SUBJ_CONJ_TRUTH) {
				if (DynConj.condDecomposeable(su, true))
					c.add(s instanceof Neg ? DynImplConj.DynImplDisjSubj : DynImplConj.DynImplConjSubj);
			}
			if (NAL.truth.DYNAMIC_IMPL_SUBJ_DISJ_MIX_TRUTH && s instanceof Neg) {
				if (DynConj.condDecomposeable(su, true, 2 /* because of TruthFunctions.mix non-associativity */))
					c.add(DynImplConj.DynImplDisjMixSubj);
			}


			if (NAL.truth.DYNAMIC_IMPL_PRED_CONJ_TRUTH) {
				if (DynConj.condDecomposeable(p, true))
					c.add(DynImplConj.DynImplConjPred);
			}
		}

		if (NAL.truth.DYNAMIC_IMPL && varSeparate)
			c.add(DynStatement.DynImpl);

		if (NAL.truth.DYNAMIC_IMPL_CONTRAPOSITION && DynImplContraposition.validSubject(s))
			c.add(DynImplContraposition.DynImplContraposition);


		return c.isEmpty() ? null : table(c.toArray(DynTruth[]::new));
	}

	/** true if there are no variables shared by the two terms */
	public static boolean varSeparate(Term x, Term y) {
		if (x instanceof Variable || y instanceof Variable) return false;
		if (!x.hasVars() || !y.hasVars()) return true;
		int commonBits = (x.structure() & Op.Variables) & (y.structure() & Op.Variables);
		if (commonBits == 0)
			return true;

		var xv = ((Compound)x).recurseSubtermsToSet(commonBits);
		Subterms yy = y.subtermsDirect();
		for (Term xxv : xv) {
			if (yy.containsRecursively(xxv))
				return false;
		}
		return true;
	}

	private static @Nullable ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicInh(Compound i) {

		Subterms ii = i.subterms();

		@Nullable DynTruth[] m = DynStatement.dynamicInhSect(ii);

		@Nullable ObjectBooleanToObjectFunction<Term, BeliefTable[]> s = m != null ? table(m) : null;

		int iis = ii.structure();
		if (Op.hasAll(iis,  Op.PROD.bit | Op.IMG.bit)
				//&& !hasAny(iis, Op.Temporals)
				//&& (!hasAny(iis, Op.Variables) || ii.vars() <= 1)
		) {

			Term n = Image._imageNormalize(i);
			if (!i.equals(n) && n.INH()) {
				return s == null ?
					(t, bOrG) -> new BeliefTable[]{new ImageBeliefTable(t, bOrG)} :
					(t, bOrG) -> ArrayUtil.add(s.valueOf(t, bOrG), new ImageBeliefTable(t, bOrG));
			}
		}

		return s;
	}

	@Nullable private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> dynamicSim(Term t) {
		Term s = t.sub(0), p = t.sub(1);
		if (!varSeparate(s,p))
			return null;
		else
			return table(DynSim.the);
	}

	public static BeliefTable tableDynamic(Term x, boolean beliefOrGoal) {

		ObjectBooleanToObjectFunction<Term, BeliefTable[]> dd = dynamicModel(x);
		if (dd != null) {
			BeliefTable[] d = dd.valueOf(x, beliefOrGoal);
			if (d!=null) {
				return switch (d.length) {
					case 0 -> null;
					case 1 -> d[0];
					default -> new BeliefTables(d);
				};
			}
		}

		return null;

	}

	private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> table(DynTruth... models) {
		return (t, beliefOrGoal) ->
			Util.map(m -> new DynTruthBeliefTable(t, m, beliefOrGoal),
				new BeliefTable[models.length], models);
	}
	private static ObjectBooleanToObjectFunction<Term, BeliefTable[]> tableBeliefs(DynTruth... models) {
		return (t, beliefOrGoal) ->
				beliefOrGoal ? Util.map(m -> new DynTruthBeliefTable(t, m, beliefOrGoal),
						new BeliefTable[models.length], models) :
						null;
	}
}