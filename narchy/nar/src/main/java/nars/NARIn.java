package nars;

/**
 * NAR Input methods
 */
@FunctionalInterface
public interface NARIn {

    void input(Task t);

    /** parses one and only task */
    default <T extends Task> T inputTask(T t) {
        input(t);
        return t;
    }


}
