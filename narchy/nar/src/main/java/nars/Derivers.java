package nars;

import nars.action.answer.AnswerQuestionsFromBeliefTable;
import nars.action.decompose.*;
import nars.action.link.STMLinker;
import nars.action.link.TermLinking;
import nars.action.link.index.CachedAdjacenctTerms;
import nars.action.link.index.EqualTangent;
import nars.action.resolve.BeliefResolve;
import nars.action.resolve.TaskResolve;
import nars.action.transform.ImageAlign;
import nars.action.transform.ImageUnfold;
import nars.action.transform.TemporalInduction;
import nars.action.transform.VariableIntroduction;
import nars.derive.reaction.Reactions;
import nars.focus.time.FocusTiming;
import nars.focus.time.TaskWhen;

import static nars.Op.*;
import static nars.action.resolve.TaskResolver.AnyTaskResolver;

/**
 * utility class and recipes for building Deriver's
 */
public class Derivers extends Reactions {

    /** standard ruleset */
    @Deprecated public static Derivers nal(int minLevel, int maxLevel, String... extraFiles) {

        Derivers r = new Derivers();

        for (int level = minLevel; level <= maxLevel; level++) {
            switch (level) {
                case 1 -> r.structural();
                case 2 -> r.sets();
                case 3,5,6 -> r.procedural();
            }
        }

        r.files(extraFiles);

        return r;
    }

    /** TODO ensure called only once */
    public Derivers structural() {
        files(
                "inh.nal",
                "sim.nal"
                //"comparison.nal",
                //"comparison.question.nal"
        );

        addAll(
            new ImageUnfold(),
            new ImageAlign.ImageAlignBidi()
//			new ImageAlign.ImageAlignUni_Root(),
//			new ImageAlign.ImageAlignUni_InCompound(true),
//			new ImageAlign.ImageAlignUni_InCompound(false),
        );

        return this;
    }

    public Derivers sets() {
        files("nal2.compose.nal",
                "nal2.decompose.nal",
                "nal2.guess.nal");
        return this;
    }

    public Derivers procedural() {

        files(
                "cond.decompose.nal",
                "cond.decompose.must.nal",
                "cond.decompose.should.nal",
                "cond.decompose.would.nal",

                "nal3.question.nal",

                "contraposition.nal",
                "conversion.nal",

                "impl.syl.nal",
                "impl.syl.combine.nal",

                "impl.strong.nal",
//                "impl.strongx.nal",

                "impl.compose.nal"

              , "impl.decompose.nal"
              , "impl.combine.nal"

              , "impl.decompose.specific.nal"

              , "impl.recompose.nal"

              , "analogy.anonymous.conj.nal"

              , "analogy.mutate.nal"

              //,"analogy.anonymous.impl.nal"
              //,"analogy.goal.nal"

                //,"impl.decompose.inner.nal"
                //,"impl.decompose.inner.question.nal",




                //,"quest_induction.nal"
                //"equal.nal"
                //"xor.nal"
                //"impl.disj.nal"
                //"nal6.layer2.nal"
                //"nal6.mutex.nal"
        );
        return this;
    }

    public final Derivers core() {
        return core(true, true);
    }

    /**
     * standard derivation behaviors
     */
    public Derivers core(boolean temporalInduction, boolean stmLinker) {
        TaskWhen when =
            new FocusTiming();
            //new JitterTiming(when),

        addAll(

                new TaskResolve(when,
                        AnyTaskResolver
                ),

                /* beliefs and goals */
                new BeliefResolve(
                        true, true, true, true,
                        when,
                        AnyTaskResolver
                ),

                //new Evaluate(),

                new Decompose1().taskIsNotAny(INH, SIM, IMPL, CONJ, DELTA),

                new DecomposeStatement(INH),
                new DecomposeStatement(SIM)/*.bidi()*/,

                new DecomposeCond().bidi(),

                //1-step IMPL decompose (immediate)
                //new DecomposeImpl(true).bidi(),

                //2-step IMPL decompose (progressive - less combinatorial explosion)
                new DecomposeImpl(false).bidi(),
                new DecomposeCondProgressive(true, IMPL),
                new DecomposeCondProgressive(false, IMPL),


                new TermLinking.PremiseTermLinking(
                        new CachedAdjacenctTerms(EqualTangent.the, false)),
                //new TermLinking.TaskLinkTermLinking(adj),


//			new AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(now)
//					.log(true)
//			,

                new AnswerQuestionsFromBeliefTable(when, true, true, AnyTaskResolver),

                new VariableIntroduction()./*anon().*/taskPunc(true, true,
                        NAL.derive.VARIABLE_INTRODUCE_QUESTIONS,
                        NAL.derive.VARIABLE_INTRODUCE_QUESTIONS)


//			new ImplTaskifyAction(true, true),
//			new ImplTaskifyAction(false, true),
//
//
//			new ImplTaskifyAction(true, false),
//			new ImplTaskifyAction(false, false)
        );

        if (temporalInduction)
            addAll(

                    new TemporalInduction.ImplTemporalInduction(0,
                            NAL.belief.TEMPORAL_INDUCTION_BIDI ? 0 : +1), //AUTO
//				new TemporalInduction.ImplTemporalInduction(+1, NAL.belief.TEMPORAL_INDUCTION_BIDI ? 0 : +1),
//				new TemporalInduction.ImplTemporalInduction(-1, NAL.belief.TEMPORAL_INDUCTION_BIDI ? 0 : +1),

                    new TemporalInduction.ConjInduction(0, 0) //AUTO
//				  new TemporalInduction.ConjInduction(+1, +1)
//				, new TemporalInduction.ConjInduction(-1, +1)
//				, new TemporalInduction.ConjInduction(+1, -1)
//				, new TemporalInduction.ConjInduction(-1, -1)

//				  new TemporalInduction.ConjInduction(true, 0, 0) //CONJ induction, auto-negate
//				, new TemporalInduction.ConjInduction(false,0, 0) ////DISJ induction, auto-negate

//					.iff(  TheTask, TermMatcher.ConjSequence.the, false)
//					.iff(TheBelief, TermMatcher.ConjSequence.the, false)
            );

        if (stmLinker)
            add(new STMLinker(true, true, true, true));

        return this;
    }
    //				case 4 -> r.files(
//					//"nal4.nal"
//					//"nal4.composition.nal" //dangerous until positioning restriction is added
//					//"nal4.hol.nal"
//					//"relation_extraction.nal"
//				);
}