package nars.memory;

import jcog.Log;
import jcog.Str;
import jcog.Util;
import jcog.pri.PLink;
import jcog.pri.Prioritizable;
import jcog.pri.bag.impl.hijack.PriHijackBag;
import jcog.pri.op.PriMerge;
import jcog.pri.op.PriMult;
import jcog.signal.FloatRange;
import jcog.signal.anomaly.ewma.Ewma;
import nars.NAR;
import nars.Term;
import nars.concept.Concept;
import nars.concept.PermanentConcept;
import nars.term.Termed;
import nars.time.part.DurLoop;
import org.HdrHistogram.Histogram;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.function.Consumer;
import java.util.stream.Stream;

import static nars.Op.XTERNAL;

/**
 * Created by me on 2/20/17.
 */
public class HijackMemory extends Memory {

    private static final Logger logger = Log.log(HijackMemory.class);
    /**
     * eliding is faster but records less accurate access statistics.
     * but if eliding, cache statistics will not reflect the full amount of access otherwise would be necessary
     */
    private static final boolean ElideGets = false;
    public final PriHijackBag<Term, PLink<Concept>> bag;
    public final FloatRange add = new FloatRange(0.25f /* main temperature */, 0, 1);
    public final FloatRange boost;
    public final FloatRange updateRate = new FloatRange(0.01f, 0, 1);
    public final FloatRange forget = new FloatRange(
            (float) Ewma.halfLife(64),
            0, 2);

    public static final boolean TRACE_AGE = false;
    private final Histogram age = TRACE_AGE ? new Histogram(2) : null;

    private transient float priAdd;
    private transient float priBoost;
    private transient DurLoop updater;
    private int updateIndex = 0;
    private long now;


    public HijackMemory(int capacity, int reprobes) {
        super();

        boost = new FloatRange(add.floatValue() / reprobes, 0, 1);

        this.bag = new PriHijackBag<>(PriMerge.plus, capacity, reprobes) {

            {
                resize(capacity());
            }

            @Override
            public Term key(PLink<Concept> value) {
                return value.id.term();
            }

            @Override
            protected boolean regrowForSize(int s, int sp) {
                return false;
            }

            @Override
            protected boolean reshrink(int length) {
                return false;
            }

            @Override
            public int spaceMin() {
                return capacity();
            }

            @Override
            protected boolean replace(PLink<Concept> incoming, float inPri, PLink<Concept> existing, float exPri) {
                return HijackMemory.this.replace(incoming, existing) &&
                        super.replace(incoming, inPri, existing, exPri);
            }

            @Override
            public void onRemove(PLink<Concept> value) {
                HijackMemory.this.onRemove(value.id);
            }
        };
        _update();
    }

    private boolean replace(PLink<Concept> incoming, PLink<Concept> existing) {
        boolean existingNotPermanent = !(existing.id instanceof PermanentConcept);

        return existingNotPermanent;

//        return existingNotPermanent
//                &&
//                incoming.id.term().volume() <= existing.id.term().volume()
//                ;
    }

    private void _update() {
        priAdd = add.floatValue();
        priBoost = boost.floatValue();
    }

    @Override
    public void start(NAR nar) {
        super.start(nar);
        updater = nar.onDur(this::update);
    }

    /**
     * measures accesses for eviction, so do not elide
     */
    @Override
    public final boolean elideConceptGets() {
        return ElideGets;
    }

    private void update() {

        Consumer<PLink<Concept>> r = TRACE_AGE ? this::recordAge : null;

        float u = this.updateRate.floatValue();

        float f = forget.floatValue() * (1/u);
        Consumer<PLink<Concept>> updater =
            f > 0 ? Util.compose(new PriMult<>(
                    1/(1+f)
                    //1-f
            ), r) : r;

        this.now = TRACE_AGE ? nar.time() : XTERNAL;

        int capacity = bag.capacity();
        int n = 1 + (int) (u * capacity);
        int start = updateIndex;

        bag.forEachIndex(start, n, updater);

        //next updateIndex:
        updateIndex = (start + n) % capacity;

        _update();

        if (TRACE_AGE && nar.random().nextFloat() < 0.005f)
            logger.info("Concept Age:\n{}", Str.histogramString(age, true));

        if (age!=null && age.getTotalCount() >= 2 * bag.size())
            age.reset();
    }

    private void recordAge(PLink<Concept> x) {
        long t = now - x.id.creation;
        this.age.recordValue(Math.max(0, t));
    }

    @Override
    public Concept get(Term key, boolean createIfMissing) {
        PLink<Concept> x = bag.get(key);
        if (x != null) {
            boost(x);
            return x.id;
        } else {
            return createIfMissing ? create(key) : null;
        }
    }

    private @Nullable Concept create(Term x) {
        Concept c = nar.conceptBuilder.apply(x);
        PLink<Concept> inserted = bag.put(insertion(x, c));
        if (inserted == null) {
            return c;
        } else {
            //boost(inserted);
            return inserted.id;
        }
    }

    private float priPut(Term x) {
        return pri(x, true);
    }

    private float pri(Termed x, boolean addOrBoost) {
        return (float)(
             1 //equal opportunity
             //Math.pow(0.5, x.term().volume()/32f)
             //Math.pow(x.term().volume(), -0.25f)
             //Math.pow(x.voluplexity(), -0.25f);
             * (addOrBoost ? priAdd : priBoost)
        );
    }

    private void boost(PLink<Concept> x) {
        ((Prioritizable) x).priAdd(pri(x.id, false));
        //bag.pressurize(boost);
    }


    @Override
    public void set(Term x, Concept c) {
        PLink<Concept> existing = bag.get(x);
        if (existing == null || (existing.id != c && !(existing.id instanceof PermanentConcept))) {
            PLink<Concept> inserted = bag.put(insertion(x, c));
            if (inserted == null && (c instanceof PermanentConcept))
                throw new RuntimeException("unresolvable hash collision between PermanentConcepts: " + null + ' ' + c);
        }
    }

    private PLink<Concept> insertion(Term x, Concept c) {
        return new PLink<>(c, priPut(x));
    }

    @Override
    public void clear() {
        bag.clear();
    }

    @Override
    public void forEach(Consumer<? super Concept> c) {
        for (PLink<Concept> k : bag)
            c.accept(k.id);
    }

    @Override
    public int size() {
        return bag.size(); /** approx since permanent not considered */
    }

    @Override
    public String summary() {
        return bag.size() + " concepts";
    }

    @Override
    public @Nullable Concept remove(Term entry) {
        PLink<Concept> e = bag.remove(entry);
        return e != null ? e.id : null;
    }

    @Override
    public Stream<Concept> stream() {
        return bag.stream().map(z -> z.id);
    }

}