package nars.control.channel;

import nars.Task;
import nars.Term;
import nars.control.Cause;
import nars.task.NALTask;

public final class TaskChannel extends CauseChannel<Task> {

    private final Term uniqueCause;

    public TaskChannel(Cause cause) {
        super(cause);
        this.uniqueCause = cause.ID;
    }

    @Override
    protected void preAccept(Task x) {
//            if (x instanceof Task) {
        ((NALTask) x).why(uniqueCause);
//            } else if (x instanceof Remember) {
//                preAccept(((Remember) x).input); //HACK
//            }
    }


}