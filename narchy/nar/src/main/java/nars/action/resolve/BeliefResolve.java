package nars.action.resolve;

import jcog.signal.FloatRange;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.control.Cause;
import nars.control.Why;
import nars.derive.Deriver;
import nars.derive.reaction.Reaction;
import nars.focus.time.TaskWhen;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.proxy.SpecialTermTask;
import nars.term.Neg;
import nars.term.atom.Bool;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static nars.Op.*;

/**
 * matches a belief task for a premise's beliefTerm, converting a single-premise to a double-premise
 */
public class BeliefResolve extends TaskResolver {

    /**
     * variable types unifiable in premise formation
     */
//    private final int unifyVars =
//            VAR_QUERY.bit
//            //VAR_QUERY.bit | VAR_DEP.bit
//            //Op.Variables //all
//            //0 //none, unify disabled
//            ;
    public final FloatRange unifyQuery = FloatRange.unit(1f/2);
    public final FloatRange unifyDep   = FloatRange.unit(1f/4);
    public final FloatRange unifyIndep = FloatRange.unit(1f/100);

    //public final FloatRange evalRate = new FloatRange(0.5f, 0, 1);

    public BeliefResolve(boolean belief, boolean goal, boolean question, boolean quest, TaskWhen timing, AbstractTaskResolver resolver) {
        super(timing, resolver);

        taskPunc(belief, goal, question, quest);
        hasBeliefTask(false);

        isAny(PremiseBelief, Taskables);
//        hasAny(PremiseBelief, Op.AtomicConstant); //dont bother with variable-only terms
    }


    private static Predicate<NALTask> notEqualTo(NALTask task) {
        return ((Predicate<NALTask>) task::equals).negate();
    }


    @Override
    protected void run(Deriver d, Cause<Reaction> why) {

        Premise p = d.premise;

        NALTask task = p.task();
        NALTask task0 = task;

        Term _t = task.term();
        Term _b = p.to();

        Term taskTerm = _t, beliefTerm = _b;

        boolean tEQb = taskTerm.equalsRoot(beliefTerm);
        if (tEQb && task.BELIEF()) //TODO make a PREDICATE for this case
            return; //HACK dont bother resolving (another) belief with same term as Task, if Task is BELIEF

        if (!tEQb) {
            int unifyVars = unifyVars(d);

            if (unifyVars != 0) {
                boolean taskMutable =
                    task.QUESTION_OR_QUEST()
                    &&
                    taskTerm.hasAny(unifyVars);

                boolean beliefMutable =
                    beliefTerm.hasAny(unifyVars);

                if (taskMutable || beliefMutable) {
                    if (taskTerm.unifiable(unifyVars, 0).test(beliefTerm)) {
                        //TODO if cant unify at top-level, maybe unify with a subterm, recursively
                        try (Deriver.MyUnifyTransform u = d.unifyTransform(NAL.derive.TTL_UNISUBST)) {
                            u.vars = unifyVars;
//                        u.varvar = VAR_GREEDY;

                            Term candidate = taskMutable ? taskTerm : beliefTerm;
                            Term candidateY = u.unifySubst(taskTerm, beliefTerm, candidate);
                            if (candidateY != null) {

                                Term taskTermUnified = taskMutable ? candidateY /* u.transform(taskTerm) */ : null;
                                if (taskTermUnified instanceof Neg) taskTermUnified = taskTermUnified.unneg(); //HACK

                                Term beliefTermUnified = beliefMutable ? (taskMutable ? u.transform(beliefTerm) : candidateY) : null;
                                if (beliefTermUnified instanceof Neg)
                                    beliefTermUnified = beliefTermUnified.unneg();//HACK

                                if (valid(taskTerm, taskTermUnified, d) && taskTermUnified.TASKABLE()) {
//                                if (task.BELIEF_OR_GOAL() && taskTermUnified.TEMPORAL_VAR()) {
//                                    //nope
//                                    Util.nop();
//                                } else
                                    taskTerm = taskTermUnified;
                                }

                                if (valid(beliefTerm, beliefTermUnified, d)) {
                                    if (!beliefTermUnified.equalsRoot(taskTerm))
                                        beliefTerm = beliefTermUnified;
                                }
                            }

                        }
                    }
                }
            }
        }


        NALTask belief = null;
        if (beliefTerm.CONCEPTUALIZABLE() /*&& (!taskBelief || !beliefTerm.equals(_t))*/ && !beliefTerm.hasAny(VAR_QUERY)) {
            long[] w = timing.whenRelative(task, d);
            var beliefCandidate = resolver.resolveTask(beliefTerm, BELIEF, w[0], w[1], d,
                    task.BELIEF() ? notEqualTo(task) : null
            );
            if (beliefCandidate != null && valid(null, beliefCandidate.term(), d) && valid(beliefCandidate, task)) {
                belief = beliefCandidate;
                beliefTerm = beliefCandidate.term();
            }
        }

        if (!_t.equals(taskTerm)) {
            NALTask questionNext = SpecialTermTask.proxy(task, taskTerm, true);
            if (questionNext == null)
                return;

            task = questionNext.copyMeta(task);
        }

        Premise y;
        if (belief == null) {
            if (beliefTerm.equals(_b) && task == task0)
                return;

            y = NALPremise.the(task, beliefTerm, false, Why.why(p, task, why)); //different belief term?
        } else {
            y = NALPremise.the(task, belief, true, Why.why(p, task, belief, why));//belief task resolved
        }

        d.add(y);
    }

    private int unifyVars(Deriver d) {
        return unifyVarBit(VAR_DEP, unifyDep, d)
        |
        unifyVarBit(VAR_QUERY, unifyQuery, d)
        |
        unifyVarBit(VAR_INDEP, unifyIndep, d);
    }

    private static int unifyVarBit(Op varDep, FloatRange unifyDep, Deriver d) {
        return d.randomBoolean(unifyDep.floatValue()) ? varDep.bit : 0;
    }

    private static boolean valid(@Nullable Term x, @Nullable Term y, Deriver d) {
        return y!=null && !(y instanceof Bool) &&
                (x==null || !y.equalsRoot(x)) &&
                y.volume() <= d.volMax;
    }

    /**
     * only allow unstamped tasks to apply with stamped beliefs.
     * otherwise stampless tasks could loop forever in single premise or in interaction with another stampless task
     */
    private static boolean valid(NALTask belief, NALTask task) {
        return !(
                task.BELIEF() && task.equals(belief)
                        ||
                        task.stampLength() == 0 && belief.stampLength() == 0
        );
    }

}