package nars.action.resolve;

import jcog.TODO;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.focus.time.TaskWhen;
import nars.table.TaskTable;
import nars.table.question.QuestionTable;
import nars.task.NALTask;
import nars.task.proxy.SpecialNegTask;
import nars.task.util.Answer;
import nars.term.Neg;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static jcog.Util.hashCombine;
import static nars.Op.BELIEF;
import static nars.Op.GOAL;

public abstract class TaskResolver extends NativeReaction {

    /** default instance; warning has mutable params that will be shared */
    public static final AbstractTaskResolver AnyTaskResolver = new DirectTaskResolver();

    public final AbstractTaskResolver resolver;
    public final TaskWhen timing;

    public TaskResolver(TaskWhen timing, AbstractTaskResolver resolver) {
        this.timing = timing;
        this.resolver = resolver;
    }

    @FunctionalInterface
    public interface AbstractTaskResolver {

        /** main implementation method
         *  NOTE some impl may non-uniformly assume that it will use resolveTerm(x) */
        @Nullable NALTask resolveTask(Term x, byte punc, long s, long e, Deriver d, @Nullable Predicate<NALTask> filter);

        @Nullable
        @Deprecated
        default /* final */ NALTask resolveTask(Term x, byte punc, long[] w, Deriver d) {
            return resolveTask(x, punc, w[0], w[1], d, null);
        }


        /** accepts and wraps negated terms for lookup */
        default NALTask resolveTaskPN(Term x, byte punc, long s, long e, Deriver d, @Nullable Predicate<NALTask> filter) {
            boolean neg = x instanceof Neg;
            if (neg) x = x.unneg();
            NALTask y = resolveTask(x, punc, s, e, d, filter);
            return y!=null && neg ? new SpecialNegTask(y) : y;
        }



    }

    //    public static final AbstractTaskResolver InquisitiveTaskResolver = new InquisitiveTaskResolver(Revise);
    public static class DirectTaskResolver implements AbstractTaskResolver {

        private static final boolean ditherDT = true;

//        private final FloatRange reviseOrSample = new FloatRange(
//            1 - (1f/NAL.STAMP_CAPACITY)
//            //0.95f
//            //0.5f
//            , 0, 1);

        protected boolean conceptualize(byte punc) {
            //return conceptualize;
            return (punc==BELIEF || punc==GOAL); //not questions
        }

        @Override
        @Nullable
        public NALTask resolveTask(Term x, byte punc, long s, long e, Deriver d, @Nullable Predicate<NALTask> filter) {
            if (x instanceof Neg)
                throw new UnsupportedOperationException("use resolveTaskPN");

            NAR n = d.nar;
//            if (x.volume() > d.volMax) {
//                n.emotion.deriveResolveVolLimit.increment();
//                return null;
//            }

            @Nullable TaskTable t = table(x, punc, n);
            if (t == null || t.isEmpty())
                return null;

            if (ditherDT) {
                int ditherDT = d.ditherDT();
                if (ditherDT > 1) {
                    s = Tense.dither(s, ditherDT);
                    e = Tense.dither(e, ditherDT);
                }
            }

            int answerCap = answerCapacity(d);

            float durMatch = durMatch(d, x);

            try (Answer a = Answer.answer(x, s, e, !(t instanceof QuestionTable), answerCap, filter, durMatch, n)) {

                a.random(d.rng).eviMin(d.eviMin).match(t);

                return a.isEmpty() ? null :
                        a.task(d.focus.durBase,
                            Math.min(answerCap, revisionCapacity(d)),
                            false);

            }
        }


        private int revisionCapacity(Deriver d) {
            return
                    NAL.answer.REVISION_CAPACITY;
        }

        protected int answerCapacity(Deriver d) {
            return
                NAL.answer.ANSWER_CAPACITY;
                //1 + d.randomInt(NAL.answer.ANSWER_CAPACITY-1); //TODO abstract distribution interface
                //d.randomBoolean() ? 1 : NAL.answer.ANSWER_CAPACITY;
        }

//        /** decides if the tasks are not too sparse across time,
//         *  which if they are will result in a dilute revision */
//        private boolean temporallyDense(Iterable<NALTask> tasks) {
//            //TODO
//            return true;
//        }

//        protected boolean reviseOrSample(Deriver d) {
//            return d.rng.nextBooleanFast8(reviseOrSample.floatValue());
//        }

//        @Nullable
//        protected Term template(Term x) {
//            return template || Compound.temporalSpecific(x) ? x : null;
//        }

//        protected float durProject(NAR n, Term x) {
//            return n.dur();
//        }

        protected float durMatch(Deriver d, Term x) {
            return d.dur();
        }

        protected @Nullable TaskTable table(Term x, byte punc, NAR n) {
            return n.table(x, punc, conceptualize(punc));
        }
    }


    /**
     * TODO
     */
    public static class CachedTaskResolver implements AbstractTaskResolver {

        final AbstractTaskResolver resolver;

        public CachedTaskResolver(AbstractTaskResolver resolver) {
            this.resolver = resolver;
        }

        //TODO for cache wrapper:
        //record TaskLookup(TaskTable t, Term x, Longerval l) { }

        @Override
        @Nullable
        public NALTask resolveTask(Term x, byte punc, long s, long e, Deriver d, @Nullable Predicate<NALTask> filter) {
            throw new TODO();
        }

        static class TaskLookup {
            public final TaskTable t;
            public final Term x;
            public final long start, end;
            private final int hash;

            public TaskLookup(TaskTable t, Term x, long start, long end) {
                this.t = t;
                this.x = x;
                this.start = start;
                this.end = end;
                this.hash = hashCombine(hashCombine(System.identityHashCode(t), x), start, end);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof TaskLookup that)) return false;
                return
                        hash == that.hash &&
                                start == that.start && end == that.end &&
                                t == that.t &&
                                x.equals(that.x);
            }

            @Override
            public int hashCode() {
                return hash;
            }
        }
    }


//    @Skill("Flashback_(psychology)")
//    public static class FlashbackTaskResolver extends DirectTaskResolver {
//
//        FlashbackTaskResolver(boolean conceptualize, boolean template) {
//            super(conceptualize, template);
//        }
//
//        @Override
//        public @Nullable NALTask resolveTask(Term x, byte punc, long s, long e, Deriver d, @Nullable Predicate<NALTask> filter) {
//            NALTask z = super.resolveTask(x, punc, s, e, d, filter);
//
//            if (z != null && !z.ETERNAL() && apply(z)) {
//                //FLASHBACK REPLAY shift
//                NALTask z2 = task(z, d);
//                NALTask.copyMeta(z2, z);
//                z = z2;
//            }
//
//            return z;
//        }
//
//        protected NALTask task(NALTask z, Deriver d) {
//            long now = d.now;
//            return SpecialOccurrenceTask.proxy(z, now, now + (z.end() - z.start()));
//        }
//
//        protected boolean apply(NALTask z) {
//            //return true;
//            return !(z instanceof SerialTask) && z.BELIEF();
//            //return z.term().TEMPORAL();
//        }
//    }

//    public static class EternalTaskResolver extends FlashbackTaskResolver {
//
//        EternalTaskResolver(boolean conceptualize, boolean template) {
//            super(conceptualize, template);
//        }
//
//        @Override
//        protected NALTask task(NALTask z, Deriver d) {
//            return SpecialOccurrenceTask.proxy(z, ETERNAL, ETERNAL);
//        }
//    }


//    public static class InquisitiveTaskResolver extends DirectTaskResolver {
//
//        InquisitiveTaskResolver() {
//            super(false, true);
//        }
//
//        @Override
//        protected @Nullable TaskTable table(Term x, byte punc, Deriver d) {
//            TaskTable t = super.table(x, punc, d);
//            if (t == null || t.isEmpty()) {
//                byte punc2 = switch (punc) {
//                    case BELIEF -> QUESTION;
//                    case GOAL -> QUEST;
//                    case QUESTION -> x.hasAny(VAR_QUERY) ? (byte) 0 : BELIEF;
//                    case QUEST -> x.hasAny(VAR_QUERY) ? (byte) 0 : GOAL;
//                    default -> (byte) 0;
//                };
//                if (punc2 != 0)
//                    return super.table(x, punc2, d);
//            }
//            return t;
//        }
//    }
}