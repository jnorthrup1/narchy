package nars.action.answer;

import nars.NAL;
import nars.Term;
import nars.action.resolve.TaskResolver;
import nars.derive.Deriver;
import nars.derive.op.Taskify;
import nars.derive.reaction.TaskReaction;
import nars.focus.time.TaskWhen;
import nars.task.NALTask;
import nars.task.proxy.SpecialTermTask;

import static nars.Op.BELIEF;
import static nars.Op.GOAL;

public abstract class AnswerQuestions extends TaskReaction {

	private final TaskResolver.AbstractTaskResolver resolver;
	final TaskWhen timing;

//	private boolean priBoostAnswer = false;

	protected AnswerQuestions(TaskWhen timing, boolean questions, boolean quests, TaskResolver.AbstractTaskResolver resolver) {
		super(false, false, questions, quests);
		this.timing = timing;
		this.resolver = resolver;
	}

	protected final NALTask answer(NALTask Q, Term q, boolean lookup, boolean acceptRefinedQuestion, long[] when, Deriver d) {

		if (!lookup && !acceptRefinedQuestion)
			return null; //HACK

		q = q.unneg(); //HACK

		NALTask x = lookup ? resolver.resolveTask(q, Q.QUESTION() ? BELIEF : GOAL, when, d) : null;

		if (x != null)
			return x;
		else
			return acceptRefinedQuestion && !q.unneg().equalsRoot(Q.term().unneg()) ?
					SpecialTermTask.proxy(Q, Taskify.questionSalvage(q), !NAL.DEBUG) : null;

	}

//	@Override
//	protected void reacting(NALTask answer, Deriver d) {
//		super.reacting(answer, d);
//		if (priBoostAnswer) {
//			NALTask question = d.premise.task(); assert (question.QUESTION_OR_QUEST());
//			answer.priMax(question.priElseZero()); //boost A's priority by Q's
//		}
//	}

	@Override
	protected boolean discountComplexity() {
		return true;
	}
}