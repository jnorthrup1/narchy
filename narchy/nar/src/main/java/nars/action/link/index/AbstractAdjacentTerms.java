package nars.action.link.index;

import com.google.common.collect.Iterators;
import jcog.Util;
import nars.Term;
import nars.derive.Deriver;
import nars.premise.Premise;

import java.util.Iterator;
import java.util.function.Function;

public abstract class AbstractAdjacentTerms extends AdjacentTerms {
    public Iterator<Term> adjacent(Term from, Term to, Deriver d) {
        Function<Premise, Term> test = test(from, to);


        Iterator<Premise> i = d.focus.sampleUnique(d.random);
        return Util.nonNull(Iterators.transform(i, test::apply));
    }

    protected abstract Function<Premise, Term> test(Term from, Term to);
}