package nars.action.link.index;

import nars.Term;
import nars.premise.Premise;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * samples the tasklink bag for a relevant reversal
 * memoryless, determined entirely by tasklink bag, O(n)
 *
 * this finds the first matching result via Bag.sampleUnique
 */
public class EqualTangent extends AbstractAdjacentTerms {

	public static final EqualTangent the = new EqualTangent();

	private EqualTangent() {

	}

	@Override protected Function<Premise, Term> test(Term from, Term to) {
		int fromHash = from.hashCodeShort(), toHash = to.hashCodeShort();
		Predicate<Term> fromEq = from.equals(), toEq = to.equals();
		return Y -> Y.equalReverse(fromEq, fromHash, toEq, toHash);
	}

}