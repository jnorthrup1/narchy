package nars.action.link;

import jcog.Fuzzy;
import jcog.data.list.MetalConcurrentQueue;
import jcog.data.list.MetalRing;
import nars.Task;
import nars.action.transform.TemporalComposer;
import nars.control.Cause;
import nars.control.Why;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.unify.constraint.TermMatch;
import org.jetbrains.annotations.Nullable;

import static nars.term.util.Image.imageNormalize;

public class STMLinker extends NativeReaction implements TemporalComposer {

	/** whether to override the resulting premise's priority with a local heuristic, rather than rely on the deriver's budget impl */
	private static final boolean BUDGET_OVERRIDE = false;



//	static final boolean premiseOrLink = true; //TODO subclass

//	/** for link: */
//	public final AtomicBoolean bidi = new AtomicBoolean(false);
	//TODO 'FloatRange balance' if bidi



	private final int capacity;

	private static final boolean allowSeq = true;

	public STMLinker(boolean b, boolean g, boolean q, boolean Q) {
		this(1, b, g, q, Q);
	}

	public STMLinker(int capacity, boolean b, boolean g, boolean q, boolean Q) {
		super();
		single();
		taskPunc(b, g, q, Q);

		if (!allowSeq)
			iff(PremiseTask, TermMatch.SEQUENCE, false);

		this.capacity = capacity;
	}

	@Override
	protected final void run(Deriver d, Cause<Reaction> why) {

        MetalRing<nars.task.NALTask> q = d.focus.focusLocal(this, s->new MetalConcurrentQueue<>(capacity));

		int capacity = q.length();
		boolean novel;
		NALTask x = d.premise.task();
		if (capacity == 1) {
			//optimized 1-ary case
			novel = link(x, q.peek(), d, why);
		} else {
			//TODO test
			novel = true;
            int h = q.head();
			for (int i = 0; novel && i < capacity; i++)
				novel &= link(x, q.get(h, i), d, why);
		}

		if (novel && keep(x)) {
			q.poll();
			boolean accepted = q.offer(x);
		}

	}

	private static boolean keep(Task x) {
		return true;
	}


	/** returns if novel */
	private static boolean link(NALTask next, @Nullable NALTask prev, Deriver d, Cause<Reaction> why) {
		if (prev == null)
			return true;

		if (prev.equals(next))
			return false;

		return //premiseOrLink ?
			taskPremise(prev, next, d, why);
//			taskLink(next, prev, d);
	}

	private static boolean taskPremise(NALTask a, NALTask b, Deriver d, Cause<Reaction> why) {

		if (!b.BELIEF() && a.BELIEF()) {
			//swap so that 'a' is the belief task to non-belief 'b'
			NALTask c = a;
			a = b;
			b = c;
		}

		//nars.term.Termed belief = b.isBelief() ? b : b.term();
		Premise y = NALPremise.the(
				a,
				b.BELIEF() ? b : imageNormalize(b.term()),
				true,
				Why.why(a, b, why));

		if (BUDGET_OVERRIDE)
			y.pri(pri(a,b));

		d.add(y);

		return true;
	}


//	private boolean taskLink(Task next, @Nullable Task prev, Deriver d) {
//		//TODO imageNormalize?
//		Term n = imageNormalize(next.term()).concept(), p = imageNormalize(prev.term()).concept();
//		if (n.equals(p))
//			return false;
//
//		boolean bidi = this.bidi.getOpaque();
//
//		float pri = (float) (Fuzzy.and(prev.priElseZero(), next.priElseZero()) * (bidi ? 0.5 : 1));
//
//		Focus f = d.focus();
//		taskLink(p, n, prev.punc(), pri, f);
//		if (bidi)
//			taskLink(n, p, next.punc(), pri, f);
//
//		return true;
//	}
//
//	private static boolean taskLink(Term a, Term b, byte punc, float pri, Focus f) {
//		if (pri > Prioritized.EPSILON) {
//			MutableTaskLink l = AtomicTaskLink.link(a, b);
//			l.priSet(punc, pri);
//			//		l.why = why;
//			f.link(l);
//			return true;
//		}
//		return false;
//	}


	/** stm budget function */
	protected static float pri(NALTask a, NALTask b) {
		//return Util.sqr(Util.mean(a.pri() , b.pri()));
		return Fuzzy.and(a.priElseZero() , b.priElseZero())/2;
	}

//	@Override public float prob(Deriver d) {
//		return (float) d.pri.probGrow(d.taskTerm.volume() >= d.beliefTerm.volume() ? d.taskTerm : d.beliefTerm, d);
//	}
	//original tasklink version:

}