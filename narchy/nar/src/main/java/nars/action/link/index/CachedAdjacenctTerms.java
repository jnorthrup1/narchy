package nars.action.link.index;

import com.google.common.base.Function;
import com.google.common.collect.Iterators;
import jcog.Util;
import jcog.io.BinTxt;
import jcog.pri.PriReference;
import jcog.pri.bag.Bag;
import nars.NAL;
import nars.Term;
import nars.concept.Concept;
import nars.concept.snapshot.Snapshot;
import nars.derive.Deriver;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.link.TermLinkBag;
import nars.premise.Premise;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import static jcog.Util.lerpSafe;
import static nars.NAL.premise.TERMLINK_CAP_MAX;
import static nars.NAL.premise.TERMLINK_CAP_MIN;

/**
 * caches ranked reverse atom termlinks in concept meta table
 * stateless
 */
public class CachedAdjacenctTerms extends AdjacentTerms {

	//1/20f;

	//0.5f;


	private final AdjacentTerms source;

	/** if not shared, then each What remains isolated in its own unique cache. */
	private final boolean global;

	public CachedAdjacenctTerms(AdjacentTerms source, boolean global) {
		this.source = source;
		this.global = global;
	}

	/** TODO refine */
	protected boolean cache(Term x, Deriver d) {
		return true;
	}

	/** expiration period, in cycles */
	private float expires(Term to, Focus f) {
		//return to.volume() * d.dur(); //next duration
		//return to.volume() * d.nar.dur(); //next duration
		return f.durBase;
	}

	/** cache capacity per concept,bag */
	public int capacity(Term x, Deriver d) {
		//TODO proportional to focus capacity
		return Math.round(lerpSafe(
				Util.unitizeSafe(((float)x.volume()) / d.volMax),
				TERMLINK_CAP_MAX,
				TERMLINK_CAP_MIN));
	}

	/** bag scan rate, in max # of items to test from Bag */
	public int tries(Term x, Bag<Premise,Premise> b) {
		return Integer.MAX_VALUE; //exhaustive
		//return b.size()/2; //semi-exhaustive
		//return Math.max(1, (int) Util.sqrt(b.capacity()));
	}

	private static final String idGlobal = "C";//CachedAdjacency.class.getSimpleName();

	private Iterator<Term> cached(Term from, Term to, Deriver d) {

		Focus f = d.focus;
		String id = global ? idGlobal : idLocal(f);

		TermLinkBag cached = Snapshot.get(id, to, (int)expires(to, f), updateCached(d), f);

		return cached == null || cached.isEmpty() ? null : sample(from, d.random, cached);
	}

	private BiFunction<Concept,TermLinkBag,TermLinkBag> updateCached(Deriver d) {

		return (Concept x, TermLinkBag tgt) -> {

			Term t = x.term();

			int capTgt = capacity(t, d);
			if (tgt == null)
				tgt = new TermLinkBag(capTgt);
			else {
				tgt.capacity(capTgt);
//				if (tgt.size() == capTgt)
//					System.out.println(tgt.size());
			}

			Bag<Premise, Premise> src = ((BagFocus) d.focus).bag;
			double priFactor =
					//1.0 / Math.sqrt(1 + capacity);
					NAL.derive.TERMLINK_FILL_RATE * (capTgt / (1f + src.capacity()));

			int tries = tries(t, src);
			tgt.commit(t, src.sampleUnique(d.random),
					priFactor,
					tries,
					NAL.derive.TERMLINK_FORGET_RATE);

			return tgt;
		};
	}

	private Iterator<Term> sample(Term from, Random rng, TermLinkBag match) {
		return Util.nonNull(Iterators.transform(
				match.sampleUnique(rng), new SampleUnwrap(from)));
	}

	private String idLocal(Focus w) {
		return idGlobal + BinTxt.toString(System.identityHashCode(w));
	}

	@Override
	public Iterator<Term> adjacent(Term from, Term to, Deriver d) {
		return cache(to, d) ?
			cached(from, to, d) :
			source.adjacent(from, to, d);
	}


	private static class SampleUnwrap implements Function<PriReference<Term>, Term> {

		final Predicate<Term> fromEq;

		SampleUnwrap(Term from) {
			fromEq = from.equals();
		}

		@Override
		public @Nullable Term apply(@Nullable PriReference<Term> X) {
			Term x = X.get();
			return fromEq.test(x) ? null : x;
		}
	}
}