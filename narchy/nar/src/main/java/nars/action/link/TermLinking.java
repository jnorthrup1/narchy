package nars.action.link;

import jcog.signal.FloatRange;
import nars.Op;
import nars.Term;
import nars.action.link.index.AdjacentTerms;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.link.MutableTaskLink;
import nars.premise.Premise;

import java.util.Iterator;

public abstract class TermLinking extends NativeReaction {

	/** adjacency among ONLY image-normalized targets */
	private static final boolean NON_IMAGES_ONLY = true;

	public final FloatRange balance = FloatRange.unit(
		0.5f
		//0.75f
	);

	private final AdjacentTerms adj;

	private static final int MAX = 1;

	protected TermLinking(AdjacentTerms adj) {

		this.adj = adj;

//		constrain(new VolumeCompare(TheTask, TheBelief, false, -1).neg()); //belief.volume <= task.volume

		tasklink();

		isAny(PremiseBelief, Op.Conceptualizables);
		hasAny(PremiseBelief, Op.AtomicConstant); //dont bother with variable-only terms

		hasAny(PremiseBelief, Op.Variables, false); //only constant terms, because unification is not tested

		if (NON_IMAGES_ONLY)
			condition(NonImages);
	}


	@Override
	protected final void run(Deriver d, Cause<Reaction> why) {

		Premise p = d.premise;

		Term x, y;
		boolean dir = d.randomBoolean(balance);

		if (dir) {
			//forward
			x = p.from(); y = p.to();
		} else {
			//reverse
			x = p.to(); y = p.from();
		}

		//Term x = x.concept();

		Iterator<Term> zz = adj.adjacent(x, y, d);
		if (zz == null) return;

		int max = MAX;
		while (zz.hasNext()) {
			Term z = zz.next();

			if (dir)
				link(x, z, d);
			else
				link(z, x, d);

			if (--max <= 0) break;
		}

	}

	protected abstract void link(Term from, Term to, Deriver d);

	public static class PremiseTermLinking extends TermLinking {

		public PremiseTermLinking(AdjacentTerms adj) {
			super(adj);
		}

		@Override
		protected void link(Term from, Term to, Deriver d) {
			d.link(MutableTaskLink.link(from, to),
					false);
		}

	}


}