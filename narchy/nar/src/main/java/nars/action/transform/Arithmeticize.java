package nars.action.transform;

import jcog.Is;
import jcog.Research;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.decide.Roulette;
import jcog.pri.AtomicPri;
import jcog.random.RandomBits;
import jcog.util.ArrayUtil;
import nars.$;
import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.func.ArithmeticCommutiveFunctor;
import nars.func.Cmp;
import nars.func.MathFunc;
import nars.term.Compound;
import nars.term.Variable;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import nars.term.util.Terms;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;

import static nars.Op.*;
import static nars.func.MathFunc.mul;

/**
 * introduces arithmetic relationships between differing numeric subterms
 * responsible for showing the reasoner mathematical relations between
 * numbers appearing in compound terms.
 * <p>
 * TODO
 * greater/less than comparisons
 * ranges
 * IntervalTree
 * https://github.com/konsoletyper/teavm/blob/master/core/src/main/java/org/teavm/common/RangeTree.java
 */
@Research
@Is("Arithmetic_coding") public enum Arithmeticize {
    ;

    static final boolean monotonicOnly = false;

    private static final Variable A = $.varDep("A_");
    private static final Variable B = $.varDep("B_");
    private static final Op Aop = A.op();

    private static final Variable VAR_DEP_1 = $.varDep(1);
    private static final Variable VAR_DEP_2 = $.varDep(2);

    public static Term apply(Term x, RandomBits rng) {
        //pre-evaluate using the arith operators; ignore other operators (unless they are already present, ex: member)

        IntHashSet ints = new IntHashSet(2);
        IntObjectHashMap<Set<Compound>> vectors = x.hasAny(PROD) ? new IntObjectHashMap<>(0) : null;

        x.ANDrecurse(t -> t.hasAny(INT), t -> {
            if (t instanceof Int)
                ints.add(Int.the(t));
            else if (vectors!=null && t.PROD()) {
                int arity;
                if ((arity = t.subs()) > 1 && t.structureSurface() == INT.bit)
                    vectors.getIfAbsentPut(arity, ()->new UnifiedSet<>(2)).add((Compound) t);
            }
        });

        int iu = ints.size();

        int vu = countVectors(vectors);

        return iu < 2 && vu < 2 ? null :
                apply(x, rng, transforms(rng, ints, vectors, vu));

    }

    private static int countVectors(IntObjectHashMap<java.util.Set<Compound>> uniqueVectors) {
        if (uniqueVectors==null) return 0;
        int vu = 0;
        Iterator<Set<Compound>> vuu = uniqueVectors.iterator();
        while (vuu.hasNext()) {
            int s = vuu.next().size();
            if (s < 2)
                vuu.remove();
            else
                vu += s;
        }
        return vu;
    }

    private static PrioritizedTransform[] transforms(RandomBits rng, IntHashSet uniqueInts, IntObjectHashMap<java.util.Set<Compound>> uniqueVectors, int vu) {
        PrioritizedTransform[] m = null;
        if (vu > 0)
            m = vectorMods(uniqueVectors, rng);
        if (m == null)
            m = scalarMods(uniqueInts);
        return m;
    }

    @Nullable private static Term apply(Term x, RandomBits rng, PrioritizedTransform[] m) {
        if (m == null || m.length == 0)
            return null;

        return m[m.length > 1 ? Roulette.selectRoulette(m.length, c -> m[c].pri(), rng::nextFloat) : 0].apply(x);
    }


    @Nullable
    private static PrioritizedTransform[] vectorMods(IntObjectHashMap<Set<Compound>> uniqueVectors, RandomBits rng) {
        Set<Compound> s;
        if (uniqueVectors.size() > 1) {
            //choose one
            Object[] a = uniqueVectors.toArray();
            int which =
                    rng.nextInt(a.length); //random
            //TODO , max unique, max arity, etc

            s = (Set<Compound>) a[which];
        } else {
            s = uniqueVectors.getFirst();
        }

        //choose 2 at random
        //TODO more advanced heuristics: incl. clustering etc
        Compound[] ss = s.toArray(EmptyCompoundArray);
        ArrayUtil.shuffle(ss, rng);
        Compound A = ss[0];
        Compound B = ss[1];

        return new PrioritizedTransform[]{new Translate(A, B)};
    }

    private static PrioritizedTransform[] scalarMods(IntHashSet iii) {

        int[] ii = iii.toSortedArray();

        int iin = ii.length;

        Lst<PrioritizedTransform> o = new Lst<>(2);

        for (int bIth = 0; bIth < iin - 1; bIth++) {
            int smaller = ii[bIth];
            for (int aIth = bIth + 1; aIth < iin; aIth++) {
                int bigger = ii[aIth];

                compareMods(smaller, bigger, o);

                scalarMods(smaller, bigger, o);

                if (!monotonicOnly)
                    scalarMods(bigger, smaller, o);
            }
        }

        return o.isEmpty() ? PrioritizedTransform.EmptyArray : o.toArray(PrioritizedTransform.EmptyArray);
    }

    private static void scalarMods(int x, int y, Lst<PrioritizedTransform> o) {
        Scale.tryScale(x, y, o);

        o.add(new Translate(x, y));
    }

    private static void compareMods(int x, int y, Lst<PrioritizedTransform> o) {
        if (x < y)
            o.add(new CompareOp(x, y));
    }


    public static class ArithmeticIntroduction extends CondIntroduction {

        static final int VOLUME_MARGIN = 3;

        {
            hasAny(PremiseTask, INT);
            volMin(PremiseTask, 3);
            volMaxMargin(PremiseTask, VOLUME_MARGIN);
            //store = false;

//            codec = ()->new TermCodec.AnonTermCodec(true /* int's */,false)/* {
//                @Override
//                protected boolean abbreviate(Compound x) {
//                    return !x.hasAny(INT);
//                }
//            }*/;
        }

        @Override
        protected boolean discountComplexity() {
            return true;
        }

        @Override
        protected Term apply(Term x, int volMax, Deriver d) {
            /* rate at which input is pre-evaluated.  TODO make FloatRange etc */
            Term y = Arithmeticize.apply(x, d.rng);

            //System.out.println(y + "\t<=\t" + x);
            return y;
        }

    }

    abstract static class PrioritizedTransform extends AtomicPri implements UnaryOperator<Term> {

        static final PrioritizedTransform[] EmptyArray = new PrioritizedTransform[0];

        PrioritizedTransform(float pri) {
            super(pri);
        }

    }

    private static class CompareOp extends PrioritizedTransform {
        static final Term cmpABNormalized = _cmp(VAR_DEP_1, VAR_DEP_2, -1);
        static final Term cmpABUnnormalized = _cmp(A, B, -1);
        private final int a;
        private final int b;


        CompareOp(int smaller, int bigger) {
            super(1);
            //assert(smaller < bigger);
            //if (smaller >= bigger) throw new WTF();
            this.a = smaller;
            this.b = bigger;
        }

        @Deprecated
        private static Term _cmp(Term a, Term b, int c) {
            return EQ.the(Int.the(c), $.func(Cmp.cmp, a, b));
        }

        @Override
        public Term apply(Term x) {
            //TODO anon
            Term cmp;
            Variable A, B;
            if (!x.hasAny(Aop)) {
                A = VAR_DEP_1;
                B = VAR_DEP_2; //optimistic prenormalization
                cmp = cmpABNormalized;
            } else {
                A = Arithmeticize.A;
                B = Arithmeticize.B;
                cmp = cmpABUnnormalized;
            }

            Term xx = x.replace(Map.of(Int.the(a), A, Int.the(b), B));

            return (xx instanceof Bool) ? null : CONJ.the(xx, cmp);
        }

    }

    /**
     * b > a
     */
    private static class Scale extends PrioritizedTransform {
        final int a, b;

        private Scale(int a, int b) {
            super(a == -b ? 1 : 0.25f);
            this.a = a;
            this.b = b;
        }

        static void tryScale(int a, int b, Lst<PrioritizedTransform> ops) {
            if (a != 0 && b != 0) {

                //noinspection IntegerDivisionInFloatingPointContext
                if ((-a == b) || (Math.abs(a) != 1 && Math.abs(b) != 1 && Util.equals(b / a, (float) b / a))) {
                    ops.add(new Scale(a, b));
                }
            }
        }


        @Override
        public Term apply(Term x) {
            Int A = Int.the(a);
            Term aV = erase(A);
            return x.replace(A, aV).replace(Int.the(b), _arith(mul, aV, Int.the(b / a)));
        }
    }

    private static class Translate extends PrioritizedTransform {
        final Term a, b;

        Translate(int a, int b) {
            this(Int.the(a), Int.the(b));
        }

        Translate(Term a, Term b) {
            super(1 /*0.5f * (a.subs() + 1)*/);
            this.a = a;
            this.b = b;
        }

        @Override
        public Term apply(Term x) {
            Term e = null;

            Term aV = erase(a);

            Term bMinA;
            if (a instanceof Int) {
                int A = Int.the(a);
                int bMinusA = Int.the(b) - A;
                if (A != 1 && A == bMinusA) {
                    e = _arith(mul, Int.TWO, aV);
                } else if (A == -bMinusA) {
                    //HACK
                    return null; //e = func(mul, Terms.sort(Int.the(-2), a));
                }
                bMinA = Int.the(bMinusA);

            } else {
                int n = a.subs();
                int[] delta = new int[n];
                for (int i = 0; i < n; i++)
                    delta[i] = Int.the(b.sub(i)) - Int.the(a.sub(i));

                bMinA = PROD.the($.intSubs(delta));
                if (a.equals(bMinA))
                    e = _arith(mul, Int.TWO, aV);
                //TODO if a.equals(bMinA.negateInts())
            }


            if (e == null)
                e = _arith(MathFunc.add, aV, bMinA);

//            UnifriedMap<Term,Term> R = new UnifriedMap(2);
//            R.put(b, e);
//            R.put(a, aV);
//            Term y = x.replace(R);
//            R.clear();

            //do in 2 steps to avoid erasing constants in 'e'
            Term y = x.replace(a, aV).replace(b,e);
            return y;
        }



    }

    private static Term _arith(ArithmeticCommutiveFunctor f, Term x, Term y) {
        return $.func(f, Terms.sort(x, y));
    }

    private static Term erase(Term a) {
        return $.varDep("_" + (a instanceof Int ? Int.the(a) : System.identityHashCode(a)));
    }

}