package nars.action.transform;

import jcog.TODO;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.action.TaskTransformAction;
import nars.action.resolve.TaskResolver;
import nars.derive.Deriver;
import nars.focus.Focus;
import nars.premise.NALPremise;
import nars.task.NALTask;
import nars.time.Tense;
import nars.truth.Stamp;
import nars.truth.Truth;
import nars.truth.evi.EviInterval;
import nars.truth.func.NALTruth;
import org.jetbrains.annotations.Nullable;

import static nars.NAL.truth.EVI_MIN;
import static nars.Op.*;

/**
 * implBeliefify and implGoalify
 */
@Deprecated public class ImplTaskifyAction extends TaskTransformAction {

    static {
        if (NAL.truth.REPROJECT_OCCURRENCE_TRUTH)
            throw new TODO();
    }

    /**
     * false is less spammy
     */
    final boolean projectToFocusOrImpl;

    final boolean fwd, beliefOrGoal;

    /** immediate does an inline derivation, generating tasks; non-immediate forms premises for eventually deriving the same result */
    final boolean immediate;

    public ImplTaskifyAction(boolean fwd, boolean beliefOrGoal) {
        this(fwd, beliefOrGoal, true, true);
    }

    public ImplTaskifyAction(boolean fwd, boolean beliefOrGoal, boolean projectToFocusOrImpl, boolean immediate) {
        this.fwd = fwd;
        this.beliefOrGoal = beliefOrGoal;
        this.projectToFocusOrImpl = projectToFocusOrImpl;
        this.immediate = immediate;


        is(PremiseTask, IMPL);
        hasAny(PremiseTask, VAR_INDEP, false);
        //TODO other conditions

//        if (singlePremiseMode) {
//            //more aggressive
         single(true,false,false,false);
//        } else {
//            //more careful
//            taskPunc(true, false,false,false);
//            hasBeliefTask(false);
//            Variable a = $.varPattern(1);
//            Variable b = $.varPattern(2);
//            Variable c = $.varPattern(3);
//            taskPattern(IMPL.the(a, XTERNAL, b));
//            beliefPattern(c);
//            constrain(new EqualPosOrNeg((Variable) (fwd ? taskPattern.sub(0) : taskPattern.sub(1)), c));
//        }
    }

    @Override
    protected @Nullable NALTask transform(NALTask x, Deriver d) {
        return validImpl(x.term()) ? ImplTaskify.the(x, d.focus)
                .premise(x, fwd, beliefOrGoal, immediate, d) : null;

    }

    @Deprecated static private boolean validImpl(Term z) {
        return //z.IMPL() && !z.hasAny(Op.VAR_INDEP) &&
               //!z.sub(0).unneg().equals(z.sub(1)) &&
               validImplSub(z.sub(0).unneg()) && validImplSub(z.sub(1));
    }

    static private boolean validImplSub(Term sub) {
        return !sub.VAR() && !sub.IMPL();
    }

    /**
     *
     * emulates impl.strong derivation with inline truth resolution of the opposite subj/pred condition
     *
     * TODO expand to pre/post belief
     */
    private static final class ImplTaskify extends EviInterval {

        private static final boolean TRUTH_DITHER = false;

        private float durBase;


        private float ete;

        /** dt between subject and predicate */
        private int o;

        public static ImplTaskify the(NALTask impl, Focus f) {

            long s, e;
            if (impl.ETERNAL()) {
                long[] w = f.when();
                s = w[0]; e = w[1];
//                if (!implEte) {
//                    long r = impl.range() - 1;
//                    if (e - s > r) {
//                        //mid align
////                        long mid = Fuzzy.mean(s, e);
////                        s = mid - r / 2;
////                        e = mid + r / 2;
//
//                        //left align
//                        e = s + r;
//                    }
//                }
            } else {
                //impl is temporal
                s = impl.start();
                e = impl.end();
            }

            return new ImplTaskify(s, e, f.dur());
        }

        ImplTaskify(long s, long e, float durFocus) {
            super(s, e, durFocus);
        }

        /** form double premise from single premise */
        private NALTask premise(NALTask implTask, boolean fwd, boolean beliefOrGoal, boolean immediate, Deriver d) {

            NAR n = d.nar;
            ete = n.eternalization.floatValueOf(implTask);
            durBase = n.dur();

            Term implTerm = implTask.term();
            {
                int implDT = implTerm.dt();
                if (implDT == DTERNAL) implDT = 0; //HACK
                o = implTerm.sub(0).seqDur() + implDT;
            }

            Term condTerm = implTerm.sub(fwd ? 0 : 1);
            NALTask condTask = resolve(condTerm, beliefOrGoal, fwd ? 0 : o, d);
            if (condTask == null) return null;

            if (!condTask.term().equals(condTerm))
                return null; //term not equal

            if (implTask.stampOverlapping(condTask)) return null;

            if (!condTask.ETERNAL()) {
                if (s == ETERNAL) {
                    s = condTask.start(); e = condTask.end();
                } else {
//                    //trim range to condition
//                    long[] se = new long[] { s, e };
//                    LongInterval.trimToward(se, condTask.start(), condTask.end());
//                    s = se[0];
//                    e = se[1];
////                    long cr = condTask.range() - 1;
////                    if (e - s > cr)
////                        e = s + cr;
                }
            }

            if (immediate) {
                return derive(condTask, implTask, fwd, beliefOrGoal, d);
            } else {
                d.add(NALPremise.the(condTask, implTask, false, null /* TODO */));
                return null;
            }

        }

        /**
         * TODO implement this as a streamlined NALPremise evaluation strategy that can be applied
         * to other derivation rules when specific parts of the derivation are already known or can be
         * assumed, or calculated directly
         */
        private NALTask derive(NALTask condTask, NALTask implTask, boolean fwd, boolean beliefOrGoal, Deriver d) {
            Truth implBelief = truth(implTask, 0);
            if (implBelief == null) return null;

            Truth condTruth = truth(condTask, fwd ? 0 : o);
            if (condTruth == null) return null;

            NAR n = d.nar;
            Truth t = NALTruth.implStrong(fwd, beliefOrGoal).truth(condTruth, implBelief, n.confMin.evi());
            if (t == null) return null;

            if (TRUTH_DITHER)
                t = t.dither(n);

            long concStart, concEnd;
            if (s == ETERNAL || (condTask.ETERNAL() && implTask.ETERNAL()))
                concStart = concEnd = ETERNAL;
            else {
                concStart = this.s; concEnd = this.e;
                if (fwd && o!=0) {
                    concStart += o; concEnd += o;
                }
            }

            return task(implTask.term().sub(fwd ? 1 : 0), t, Stamp.zip(condTask.stamp(), implTask.stamp(), condTask, implTask),
                    concStart, concEnd, beliefOrGoal, d.focus);
        }

        private final TaskResolver.AbstractTaskResolver resolver = new TaskResolver.DirectTaskResolver();

        @Nullable private NALTask resolve(Term t, boolean beliefOrGoal, int shift, Deriver d) {
            long s = this.s, e = this.e;
            if (s!=ETERNAL && shift!=0) {
                s += shift; e += shift;
            }
            return resolver.resolveTaskPN(t, beliefOrGoal ? BELIEF : GOAL, s, e, d, null);
        }
        private Truth truth(NALTask t, int shift) {
            long s = this.s, e = this.e;
            if (s!=ETERNAL) {
                s += shift; e += shift;
            }
            return t.truth(s, e, durBase, ete, EVI_MIN);
        }


        private NALTask task(Term x, Truth truth, long[] stamp, long concStart, long concEnd, boolean beliefOrGoal, Focus f) {
            if (concStart!=ETERNAL) {
                int dtDither = f.nar.dtDither();
                if (dtDither > 1) {
                    concStart = Tense.dither(concStart, dtDither);
                    concEnd = Tense.dither(concEnd, dtDither);
                }

            }

            return NALTask.task(x, beliefOrGoal ? BELIEF : GOAL, truth, concStart, concEnd, stamp);
        }




    }
}