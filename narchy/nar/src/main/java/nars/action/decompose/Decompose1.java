package nars.action.decompose;

import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.term.Compound;
import org.jetbrains.annotations.Nullable;

public class Decompose1 extends DecomposeTerm {

    public Decompose1() {
        super();
    }

    public Decompose1(Op... ops) {
        this();
        isAny(PremiseTask, ops);
    }

    @Override
    @Nullable
    public Term decompose(Compound src, Deriver d) {
        return decomposer.apply(src, d.rng);
    }

    static final DynamicDecomposer decomposer = new DecomposeN(1);
}