package nars.action.decompose;

import nars.Term;
import nars.derive.Deriver;
import nars.term.Compound;
import org.jetbrains.annotations.Nullable;

import static nars.Op.IMPL;

public class DecomposeImpl extends DecomposeStatement {

    private final boolean secondLayer;

    public DecomposeImpl(boolean secondLayer) {
        super(IMPL);
        this.secondLayer = secondLayer;
    }

    @Override
    public @Nullable Term decompose(Compound root, Deriver d) {
        Term a = super.decompose(root, d);

        if (secondLayer && a.CONDS() && d.randomBoolean())
            return DecomposeCond.decomposeCond((Compound) a, d.rng);

        return a;
    }
}