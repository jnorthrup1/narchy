package nars.action.decompose;

import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.premise.Premise;
import nars.term.Compound;
import nars.term.Variable;
import nars.unify.constraint.TermMatch;
import org.jetbrains.annotations.Nullable;

import static nars.derive.reaction.PatternReaction.HasEvents;

public class DecomposeCondProgressive extends DecomposeTerm {
    private final boolean fromOrTo;

    public DecomposeCondProgressive(boolean fromOrTo, Op other) {
        super(false);
        this.fromOrTo = fromOrTo;

        Variable src = fromOrTo ? PremiseTask : PremiseBelief;
        Variable oth = fromOrTo ? PremiseBelief : PremiseTask;
        iff(src, HasEvents);
        iff(src, new TermMatch.SubsMin((short) 1));
        isAny(oth, other);
    }

    @Override
    protected Term x(Premise premise) {
        return fromOrTo ? premise.from() : premise.to();
    }

    @Override
    protected void run(Term x, Term y, Deriver d) {
        Premise p = d.premise;
        Term f, t;
        if (fromOrTo) {
            f = y; t = p.to();
        } else {
            f = p.from(); t = y;
        }

//        if (f.equalConcept(t))
//            return;

        link(f, t, d);
    }

    @Override
    public @Nullable Term decompose(Compound src, Deriver d) {
        return DecomposeCond.decomposeCond(src,
                1, 0, //one layer at a time
                d.rng);
    }
}