package nars.action.decompose;

import jcog.random.RandomBits;
import nars.Term;
import nars.derive.Deriver;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.util.conj.ConjBundle;
import nars.term.util.conj.ConjList;
import org.jetbrains.annotations.Nullable;

import static nars.Op.DTERNAL;
import static nars.Op.XTERNAL;
import static nars.derive.reaction.PatternReaction.HasEvents;

public class DecomposeCond extends DecomposeTerm {

    static final float rawRate = 1 / 2f;
    static final float DTERNAL_decompose_rate = 1 / 3f;

    public DecomposeCond() {
        super();
        iff(PremiseTask, HasEvents);
    }

    @Override
    @Nullable
    public Term decompose(Compound conj, Deriver d) {
        return decomposeCond(conj, d.rng);
    }

    static Term decomposeCond(Compound cond, RandomBits r) {
        return decomposeCond(cond, rawRate, DTERNAL_decompose_rate, r);
    }

    /**
     * TODO (random) distributed factor mode,
     *      ex: ((seq1 &&+1 seq2) && par) => (seq1&&par), (seq2&&par)
     */
    @Nullable
    static Term decomposeCond(Compound cond, float rawRate, float dternalRate, RandomBits r) {

        boolean decomposeEte;
        Term y;
        if (cond.CONJ()) {
            if (r.nextBoolean(rawRate)) {
                //HACK for decomposing factored sequences at top-level
                return Decompose1.decomposer.subterm(cond, r);
            } else {

                int dt = cond.dt();
                boolean dtDternal = dt == DTERNAL;
//            if (dtDternal && cond.SEQ())
//                return distributeFactoredSeq(cond, r);

                boolean decomposeXte =
                        dt == XTERNAL || r.nextBoolean();
                decomposeEte = //(!decomposeXte &&
                        (dtDternal || r.nextBoolean(dternalRate));
                y = decomposeConj(cond, r, decomposeEte, decomposeXte,
                        r.nextBoolean()
                );
            }
        } else {
            decomposeEte = true;
            y = cond;
        }

        if (decomposeEte && ConjBundle.bundled(y))
            y = decomposeInh(r, y);

        return y;
    }

    @Nullable
    private static Term decomposeInh(RandomBits r, Term y) {
        TermList f = ConjBundle.events(y);
        try {
            Term y0 = f.get(r);
            if (y0 == null)
                return null; //HACK

            y = y0.unneg();
            return y;
        } finally {
            f.delete();
        }
    }

    private static Term decomposeConj(Compound cond, RandomBits r, boolean decomposeEte, boolean decomposeXte, boolean redistributed) {

        redistributed &= cond.dt() == DTERNAL;

        ConjList e = ConjList.events(cond, decomposeEte, decomposeXte);

        Term y;
        if (redistributed) {
            int n = e.size();
            if (n > 2) {
                e.sortThisByTime();
                for (int i = 0, j = -1; i < n; i++) {
                    if (e.eternal(i)) continue;
                    if (j == -1)
                        j = r.nextInt(n - i) + i; //initialize
                    if (j != i)
                        e.setNull(i);
                }
                e.removeNulls();
            }
            y = e.term();
        } else {
            y = e.get(r); //individual
        }
        e.delete();
        return y.unneg();
    }

//    private static Term distributeFactoredSeq(Compound cond, RandomBits r) {
//        int seqIndex = cond.indexOf(z -> z.dt() != XTERNAL);
//        Term seq = cond.sub(seqIndex);
//        Term y = decomposeCond((Compound) seq, r);
//        if (y == null) return null; //??
//
//        int n = cond.subs();
//        TmpTermList z = new TmpTermList(n);
//        z.addFast(y);
//        for (int i = 0; i < n; i++)
//            if (i!=seqIndex) z.addFast(cond.sub(i));
//
//        return CONJ.the((Subterms) z);
//    }

}