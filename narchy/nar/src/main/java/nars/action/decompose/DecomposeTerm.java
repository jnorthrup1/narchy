package nars.action.decompose;

import jcog.Is;
import nars.NAL;
import nars.Term;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.link.MutableTaskLink;
import nars.premise.Premise;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.util.TermTransformException;
import nars.unify.constraint.TermMatch;
import org.jetbrains.annotations.Nullable;

/** acts as Compound-term 'feature detector' by extracting a sub-component
 *   (or derivative thereof) in forming Premise's
 */
@Is({"Feature_detection_(nervous_system)", "Solvent"})
public abstract class DecomposeTerm extends NativeReaction {

	private int dir = +1;

	private static final boolean ALLOW_NON_CONCEPTUALIZABLE = false;

	/**
	 * determines forward growth target. null to disable
	 * override to provide a custom termlink supplier
	 * expected that this will not return a NEG so unneg if necessary
	 */
	public abstract @Nullable Term decompose(Compound src, Deriver d);


	protected DecomposeTerm() {
		this(true);
	}

	protected DecomposeTerm(boolean single) {
		super();

		tasklink();

		if (single) {
			taskEqualsBelief();
			iff(PremiseTask, new TermMatch.SubsMin((short) 1));
		} else {
			neq(PremiseTask, PremiseBelief);
		}
	}

	public final DecomposeTerm forward() {
		dir = +1;
		return this;
	}
	public final DecomposeTerm bidi() {
		dir = 0;
		return this;
	}
	public final DecomposeTerm reverse() {
		dir = -1;
		return this;
	}

	@Override
	protected void run(Deriver d, Cause<Reaction> why) {

		Term x = x(d.premise);

		Term y = decompose((Compound) x, d);
		if (y == null)
			return;

		assert(!(y instanceof Neg)): "decomposed is Neg";

		if (!ALLOW_NON_CONCEPTUALIZABLE && !y.CONCEPTUALIZABLE())
			return;

		if (y.equalConcept(x)) {
			if (NAL.test.DEBUG_EXTRA)
				throw new TermTransformException("decompose fail: " + this, x, y); //TODO
			return;
		}

		run(x, y, d);
	}

	protected void run(Term x, Term y, Deriver d) {
		boolean dir = switch (this.dir) {
			case -1 -> false;
			case +1 -> true;
			default -> d.randomBoolean();
		};
		Term X, Y;
		if (!dir && y.TASKABLE() /*&& y.equals(y.concept())*/) {
			X = y; Y = x;
		} else {
			X = x; Y = y;
		}
		link(X, Y, d);
	}

	protected Term x(Premise premise) {
		return premise.from();
	}

	protected static void link(Term x, Term y, Deriver d) {
		d.link(MutableTaskLink.link(x, y), true);
	}

}