package nars.action.pattern;

import com.google.common.base.Objects;
import jcog.Util;
import nars.$;
import nars.Term;
import nars.derive.Deriver;
import nars.premise.AbstractPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.term.Termed;
import org.jetbrains.annotations.Nullable;

public abstract class SubPremise<T extends Termed> extends AbstractPremise {

    protected final T id;

    protected SubPremise(T id) {
        this(id, null, null);
    }

    protected SubPremise(T id, @Nullable Premise superPremise, @Nullable Termed why) {
        super(superPremise!=null ? Util.hashCombine(superPremise, id) : id.hashCode(), why);
        this.id = id;
        this.parent = superPremise;
    }

    @Override
    public final void pre(Deriver d) {
        parent.pre(d);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (getClass() != o.getClass()) return false;

        SubPremise that = (SubPremise) o;
        return hash==that.hash && id.equals(that.id) && Objects.equal(parent, that.parent);
    }

    @Override
    @Deprecated public final Term term() {
        return $.pFast(id.term(), parent.term());
    }

    @Override
    public final String toString() {
        return "$" + super.toString() + " " + id + " " + parent;
    }


    @Override @Deprecated public final NALTask task() { return parent.task(); }

    @Override @Deprecated public final NALTask belief() {
        return parent.belief();
    }

    @Override @Deprecated public final Term from() {
        return parent.from();
    }

    @Override @Deprecated public final Term to() {
        return parent.to();
    }
}