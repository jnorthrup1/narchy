package nars.focus;

/**
 * temporal sampling envelope, relative to present
 */
public abstract class TimeFocus {

    /** returns a unique instance, not shared */
    public abstract long[] when(Focus f);

    abstract public float dur();
    abstract public void dur(float dur);

    static long[] when(long now, long shift, float dur) {
        return when(now + shift, Math.round(dur));
    }

    static long[] when(long mid, long dur) {
        return new long[]{mid - dur / 2, mid + dur / 2};
    }

}