package nars.focus.time;


import jcog.math.LongInterval;
import jcog.signal.FloatRange;
import nars.derive.Deriver;
import nars.task.NALTask;

public class ActionTiming extends PresentTiming {

    /** TODO mutable histogram model for temporal focus duration  */
    public final FloatRange focusDurs = new FloatRange(1, 0, 32);

    /** TODO mutable histogram model for temporal focus position  */
    public final FloatRange horizonDurs = new FloatRange(1, 0, 32);

    @Override
    public long[] whenRelative(NALTask task, Deriver d) {

        float dur = d.dur();

        long shift =  Math.round(
            dur * horizonDurs.doubleValue() * d.random.nextGaussian()
        );

        return LongInterval.range(d.now() + shift, dur * focusDurs.floatValue());
    }


}