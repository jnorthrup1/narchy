package nars.focus;

import jcog.Util;
import jcog.pri.Forgetting;
import jcog.pri.Prioritizable;
import jcog.pri.bag.Bag;
import jcog.pri.op.PriMult;
import jcog.signal.FloatRange;
import org.eclipse.collections.api.block.function.primitive.FloatToObjectFunction;

import java.util.function.Consumer;
import java.util.function.Function;

/** 'sustain' Bag forgetting */
public class BagSustain implements Function<Bag, Consumer<Prioritizable>>, FloatToObjectFunction<Consumer<Prioritizable>> {

    public final FloatRange sustain = new FloatRange(0f, -1, +1);

    @Override
    public Consumer<Prioritizable> apply(Bag b) {
        float s = sustain.floatValue();
        var base = Forgetting.forget(b, Math.min(1, 1 - s), s < 0 ? z -> new PriMult(1) : this);
        return s < 0 ? drain(s, base instanceof PriMult ? (PriMult) base : null) : base;
    }

    /**
     * forced draining; vacuum
     */
    @Deprecated
    private Consumer<Prioritizable> drain(float s, PriMult base) {
        if (base == null) base = new PriMult(1);
        int exponent = 4;
        base.mult = (float) Util.lerp(Math.pow(-s, exponent), base.mult, 0);
        return base;
    }

    @Override
    public Consumer<Prioritizable> valueOf(float v) {
        return new PriMult<>(v);
    }
}