package nars.concept.util;

import nars.Term;
import nars.concept.Concept;
import nars.concept.NodeConcept;
import nars.table.eternal.EternalTable;
import nars.table.question.HijackQuestionTable;
import nars.table.question.QuestionTable;
import nars.table.temporal.NavigableMapBeliefTable;
import nars.table.temporal.TemporalBeliefTable;

import java.util.function.Consumer;

public class DefaultConceptBuilder extends ConceptBuilder {

    private final Consumer<Concept> alloc;

    public DefaultConceptBuilder(Consumer<Concept> allocator) {
        this.alloc = allocator;
    }

    @Override protected Concept nodeConcept(Term t) {
        return new NodeConcept(t);
    }

    @Override public EternalTable eternalTable(Term t) {
        return new EternalTable(0);
    }

    @Override
    public TemporalBeliefTable temporalTable(Term t, boolean beliefOrGoal) {
        //return new RTreeBeliefTable();
        return new NavigableMapBeliefTable();
    }

    @Override public QuestionTable questionTable() {
        return new HijackQuestionTable(0);
    }

    @Override
    public void start(Concept c) {
        super.start(c);
        alloc.accept(c);
    }


}