package nars.concept.snapshot;

import nars.NAR;
import nars.Term;
import nars.concept.Concept;
import nars.focus.Focus;
import org.jetbrains.annotations.Nullable;

import java.lang.ref.Reference;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

/** a container for time-stamped expiring cache of data that can be stored in Concept meta maps */
public class Snapshot<X> {

	/** TODO use AtomicFieldUpdater */
	protected final AtomicBoolean busy = new AtomicBoolean(false);

	/** occurrence time after which the data should be invalidated or refreshed */
	protected volatile long expires = Long.MIN_VALUE;

	/** of type:
	 *     X, SoftReference<X>, WeakReference<X> */
	protected volatile Object value;


	public static @Nullable <X> X get(String id, Term src, int ttl, BiFunction<Concept, X, X> updater, Focus f) {
		return get(id, src, f.now(), ttl, updater, f.nar);
	}

	/** concept()'s the given term in the given NAR */
    public static @Nullable <X> X get(String id, Term src, long now, int ttl, BiFunction<Concept, X, X> updater, NAR nar) {
		Concept c = nar.concept(src);
		return c != null ? get(id, c, now, ttl, updater) : null;
	}

	private static @Nullable <X> X get(String id, Concept src, long now, int ttl, BiFunction<Concept, X, X> updater) {
		return src.<Snapshot<X>>meta(id, Snapshot::new)
			.get(now, ttl, existing -> updater.apply(src, existing));
	}


	public @Nullable X get() {
		Object v = value;
		return v instanceof Reference ? ((Reference<X>) v).get() : (X) v;
	}

	/** here the value may be returned as-is or wrapped in a soft or weak ref */
	protected Object got(X x) {
		return x;
	}

	/** ttl = cycles of cached value before next expiration ( >= 0 )
	 * 			or -1 to never expire */
	public X get(long now, int ttl, UnaryOperator<X> updater) {
		long expires = this.expires;
		X current = get();
		if (busy.compareAndSet(false, true)) {
			try {
				if (now >= expires || current == null) {
					current = updater.apply(current);
					this.expires = ttl >= 0 ? now + ttl : Long.MIN_VALUE /* forever */;
					this.value = got(current);
				}
			} finally {
				busy.set(false);
			}
		}
		return current;
	}
}