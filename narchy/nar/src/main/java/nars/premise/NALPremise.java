/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */
package nars.premise;

import jcog.Fuzzy;
import jcog.Util;
import nars.NAL;
import nars.Op;
import nars.Task;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.op.PremiseTimeGraph;
import nars.derive.reaction.Reaction;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.proxy.SpecialTermTask;
import nars.term.Neg;
import nars.term.Termed;
import nars.term.util.transform.VariableShift;
import nars.time.EventBag;
import nars.truth.MutableTruthInterval;
import nars.truth.Stamp;
import org.jetbrains.annotations.Nullable;

import static jcog.Util.maybeEqual;
import static nars.Op.ETERNAL;
import static nars.Op.TIMELESS;
import static nars.term.Compound._temporal;

/**
 * single or double premise ready for derivation
 * TODO split into Single and Double premise subclasses
 */
public abstract class NALPremise extends AbstractPremise {

    private volatile boolean initialized = false;

    public static NALPremise the(NALTask task, Termed _belief, boolean varShift, Termed why) {
//        //freeze serialtasks so they dont stretch while this is pending compute
//        if (task instanceof SerialTask ts)
//            task = ts.freeze();
//        if (_belief instanceof SerialTask bs)
//            _belief = bs.freeze();

        if (NAL.DEBUG)
            validate(task, _belief);


        Termed belief = belief(task, _belief, varShift);
        int hash = Util.hashCombine(task.hashCode(), belief.hashCode());

        return belief instanceof NALTask ?
                new DoubleTaskPremise(task, (NALTask) belief, hash, why) :
                new SingleTaskPremise(task, (Term) belief, hash, why);
    }

    public final NALTask task;
    public boolean temporal;
    long[] stampSingle;
    public long taskStart, taskEnd;

    private NALPremise(NALTask task, int hash, Termed why) {
        super(hash, why);
//        if (task.term() instanceof LightCompound)
//            Util.nop();
        this.task = task;
    }



    public static NALPremise nalPremise(Deriver d) {
        return nalPremise(d.premise);
    }

    public static NALPremise nalPremise(Premise p) {
        return p instanceof NALPremise ? (NALPremise) p : ((NALPremise)(p.parent));
    }

    /** TODO */
    @Override public Reaction reaction() {
        return null;
    }

    @Override
    public Term term() {
        throw new UnsupportedOperationException();
    }

//    public static SingleTaskPremise store(NALTask x) {
//        final TaskPremise t = TaskPremise.the(x, x.term(), false, null);
//        ((SingleTaskPremise) t).store = true;
//        return (SingleTaskPremise) t;
//    }

    private static Termed belief(NALTask task, Termed _belief, boolean varShift) {
        Termed belief;
        if (_belief instanceof NALTask)
            belief = _belief;
        else {
            Term beliefTerm = ((Term) _belief).unneg();
            belief = varShift ? VariableShift.varShift(beliefTerm, task.term(), false, false) : beliefTerm;
        }

        return maybeEqual(belief, belief instanceof Task ? task : task.term());
    }

    private static void validate(NALTask t, Termed b) {
        assert (!t.equals(b));
        assert (!t.COMMAND());
        assert (
                (b instanceof Term && (!(b instanceof Neg)))
                        ||
                        (b instanceof NALTask && !t.equals(b) && ((Task) b).BELIEF()));

        if (!(t instanceof SpecialTermTask)) //HACK for ImageAlign.java
            NALTask.validTaskTerm(t.term(), t.punc(), false);

        if (b instanceof Task)
            if (!(b instanceof SpecialTermTask)) //HACK for ImageAlign.java
                NALTask.validTaskTerm(b.term(), ((Task) b).punc(), false);
    }


    /**
     * lazy init procedure
     */
    @Override public final void pre(Deriver d) {
        if (initialized) return; //already initialized

        _pre();

        initialized = true;
    }

    protected void _pre() {
        NALTask T = task;

        stampSingle = T.stamp();

        Term taskTerm, beliefTerm;

        Termed B = beliefTermed();
        temporal =
                !T.ETERNAL() ||
                (!single() && T != B && !((NALTask) B).ETERNAL()) ||
                _temporal(taskTerm = from()) ||
                ((beliefTerm = B.term()) != taskTerm && _temporal(beliefTerm));

        refreshTime();
    }


    @Override
    public final Term from() {
        return task.term();
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) return true;
        if (getClass() != obj.getClass()) return false;
        NALPremise p = (NALPremise) obj;

        //TEMPORARY
//        if (hash == p.hash) {
//            if (beliefTermed().equals(p.beliefTermed()) && task.equals(p.task))
//                return true;
//            return false;
//        }
//        return false;

        return hash == p.hash && beliefTermed().equals(p.beliefTermed()) && task.equals(p.task);
    }

    public abstract Termed beliefTermed();

    public final Term beliefTerm() {
        return to();
    }

    @Override
    public final NALTask task() {
        return task;
    }

    public void refreshTime() {
        this.taskStart = task.start();
        this.taskEnd = task.end();
        //this.dur = _dur();

    }

//    protected float _dur() {
//        return 1 + taskEnd - taskStart;
//    }

    @Nullable public abstract NALTask belief();

    public abstract long beliefStart();

    public abstract long beliefEnd();

    public abstract boolean overlapDouble();

    public abstract long[] stamp(byte concPunc);

    public abstract boolean single();

    long taskBeliefRange() {
        return this.taskStart == ETERNAL ? ETERNAL : this.taskEnd - this.taskStart;
    }

    public abstract boolean invalid(Deriver deriver);

    public transient EventBag eventBag;

    public void load(PremiseTimeGraph g) {
        EventBag e = eventBag;
        if (e!=null)
            e.load(g);
    }

    public void save(PremiseTimeGraph g) {
        if (!g.solutions.isEmpty()) {
            EventBag e = eventBag;
            if (e == null)
                eventBag = e = new EventBag(16 /* TODO size better */);

            e.saveSolutions(g);

            if (e.isEmpty()) eventBag = null;//HACK
        }
    }

    public final long taskMid() {
        return Fuzzy.mean(taskStart, taskEnd);
    }
    public final long beliefMid() {
        return Fuzzy.mean(beliefStart(), beliefEnd());
    }

    public boolean same(byte punc, Term t, MutableTruthInterval oo, NAL n) {
        NALTask T = this.task;
        if (NALTask.same(T, t, punc, oo, true, n))
            return true;

        NALTask B = belief();
        return (B != null && B!= T && NALTask.same(B, t, punc, oo, true, n));
    }

    public void refreshTimeIfSerial() {
        if (task instanceof SerialTask || belief() instanceof SerialTask)
            refreshTime();
    }


    private static final class SingleTaskPremise extends NALPremise {
        public final Term belief;

//        boolean store = false;

        SingleTaskPremise(NALTask task, Term belief, int hash, Termed why) {
            super(task, hash, why);
            this.belief = belief;
        }


        @Override
        public boolean invalid(Deriver d) {
            return d.invalid(task) || d.invalidVol(belief);
        }

        @Override
        public Term to() {
            return belief;
        }

        @Override
        public String toString() {
            return "$" + pri() + " (" + task + (
                    belief.equals(task.term()) ? ")" : (" >> " + belief + ')')
            );
        }

        @Override
        public Termed beliefTermed() {
            return belief;
        }

        @Override
        @Nullable
        public NALTask belief() {
            return null;
        }

        @Override
        public long beliefStart() {
            return TIMELESS;
        }

        @Override
        public long beliefEnd() {
            return TIMELESS;
        }

        @Override
        public boolean single() {
            return true;
        }

        @Override
        public boolean overlapDouble() {
            return false;
        }


        @Override
        public long[] stamp(byte punc) {
            return stampSingle;
        }
    }

    public static final class DoubleTaskPremise extends NALPremise {
        public final NALTask belief;
        public boolean overlapDouble;
        long beliefStart, beliefEnd;
        private long[] stampDouble;


        private DoubleTaskPremise(NALTask task, NALTask belief, int hash, Termed why) {
            super(task, hash, why);
            this.belief = belief;
        }

//        @Override protected float _dur() {
//            final boolean tt = taskStart != ETERNAL, bt = beliefStart != ETERNAL;
//            if (tt && bt)
//                return durTaskBelief();
//            else if (!tt)
//                return durBelief();
//            else
//                return durTask();
//        }
//
//        private float durBelief() {
//            return 1 + beliefEnd - beliefStart;
//        }
//
//        private float durTaskBelief() {
//            union:
//            return 1 + Math.max(taskEnd, beliefEnd) - Math.min(taskStart, beliefStart);
//
//            //min:
//            //return Math.min(durTask(), durBelief());
//
//            //max:
//            //return Math.max(durTask(), durBelief());
//        }
//
//        private float durTask() {
//            return super._dur();
//        }


        @Override
        protected void _pre() {
            super._pre();
            overlapDouble = Stamp.overlap(task, belief);
        }

        @Override
        public boolean invalid(Deriver d) {
            return d.invalid(task) || d.invalid(belief);
        }

        @Override
        public String toString() {
            return "$" + pri() + " (" + task + " >> " + belief + ')';
        }

        @Override
        public Term to() {
            return belief.term();
        }

        @Override
        public Termed beliefTermed() {
            return belief;
        }

        @Override
        public long beliefStart() {
            return beliefStart;
        }

        @Override
        public long beliefEnd() {
            return beliefEnd;
        }

        @Override
        public boolean single() {
            return false;
        }

        @Override
        @Nullable
        public NALTask belief() {
            return belief;
        }

        @Override
        public void refreshTime() {
            //synchronized(this) {
                super.refreshTime();
                if (belief != task) {
                    beliefStart = belief.start();
                    beliefEnd = belief.end();
                } else {
                    beliefStart = taskStart;
                    beliefEnd = taskEnd;
                }
            //}
        }

        @Override
        public long taskBeliefRange() {

            long taskRange = super.taskBeliefRange();

            long beliefRange = this.beliefStart == ETERNAL ? ETERNAL : this.beliefEnd - this.beliefStart;

            if (taskRange == ETERNAL)
                return beliefRange;
            else if (beliefRange == ETERNAL)
                return taskRange;
            else
                return Math.max(taskRange, beliefRange);
        }

        @Override
        public boolean overlapDouble() {
            return overlapDouble;
        }

        @Override public long[] stamp(byte concPunc) {
            if (stampDouble == null) {
                long[] beliefStamp = belief.stamp();

                if (NAL.premise.QUESTION_TO_BELIEF_DONT_ZIP && Op.QUESTION_OR_QUEST(task.punc()) && !Op.QUESTION_OR_QUEST(concPunc))
                    stampDouble = beliefStamp; //special case: (question,belief)->belief
                else
                    stampDouble = Stamp.zip(stampSingle, beliefStamp, task, belief); //lazy
            }

            return stampDouble;
        }

    }



//    /** temporary field for caching a growing continuation branch */
//    @Deprecated private final Map<Object, CompoundPremise> next = new ConcurrentHashMap();
//
//    @Deprecated public <X, P extends Premise> void next(X k, P sub, BiFunction<X, Premise, CompoundPremise> branchBuilder, Deriver d) {
//        CompoundPremise b;
//
//        boolean shared =
//                //(this instanceof AtomicTaskLink);
//                false;
//        if (!shared) { //since it's shared
//            b = this.next != null ? this.next.get(k) : null;
//            if (b == null) {
//                b = next.computeIfAbsent(k,
//                        K -> branchBuilder.apply((X)K, this));
//            }
//        } else {
//            b = branchBuilder.apply(k, this);
//        }
//        b.sub(sub);
//        d.add(b); //TODO when isnt this necessary?
//    }
//
//    @Override
//    public void delete(Deriver d) {
//        next.clear();
//        super.delete(d);
//    }
}