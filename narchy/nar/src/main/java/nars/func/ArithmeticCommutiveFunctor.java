package nars.func;

import nars.$;
import nars.Term;
import nars.eval.Evaluation;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.atom.Atomic;
import nars.term.atom.Int;
import nars.term.functor.InverseFunctor;
import nars.term.util.transform.InlineFunctor;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.ListIterator;
import java.util.function.Consumer;

import static nars.Op.PROD;
import static nars.func.MathFunc.add;
import static nars.func.MathFunc.mul;
import static nars.term.atom.Bool.Null;

public abstract class ArithmeticCommutiveFunctor extends Functor implements InlineFunctor<Evaluation>, InverseFunctor {

    ArithmeticCommutiveFunctor(String name) {
        super(name);
    }

    @Override
    @Nullable public Term apply(Evaluation terms, Subterms x) {
        return theReducedCommutive(x);
    }

    @Nullable
    private Term theReducedCommutive(Subterms x) {
        int xs = x.subs();
        if (xs == 0) return Null;
        if (xs == 1) return x.sub(0);

        TermList xx = x.toList();
        if (!xx.AND(Term::INT))
            xx.sortThis(); //sort if doesnt contain only int's

        Subterms y = reduceCommutive(xx);
        if (y == null)
            return Null;

        int ys = y.subs();
        switch (ys) {
            case 0: return Null;
            case 1: return y.sub(0);
            default: {
                return ys == xs && x.equals(y) ? null : $.func(this, y); //unchanged
            }
        }
    }


    Subterms reduceCommutive(TermList xx) {

        int tt = xx.subs();
        if (tt <= 1)
            return xx;

        boolean ADD = this == add, MUL = this == mul;
        boolean addOrMult = ADD;  assert(ADD || MUL); //HACK


        //flattening //TODO recurse
        TermList vector = null;
        ListIterator<Term> l = xx.listIterator();
        Consumer<Term> accum = null;
        int scalar = addOrMult ? 0 : 1;
        boolean hasScalar = false;
        while (l.hasNext()) {
            Term x = l.next();
            Atomic f = Functor.func(x);
            if (f!=null && f.equals(this)) {
                l.remove();
                if (accum == null) accum = l::add;
                Functor.args(x).forEach(accum); //TODO dont add if == identity
            }
            if (x instanceof Int) {
                int X = Int.the(x);
                if (addOrMult) {
                    if (X!=0) {
                        scalar += X;
                        hasScalar = true;
                    }
                } else {
                    if (X!=1) {
                        scalar *= X;
                        hasScalar = true;
                    }
                }
                l.remove();
            } else if (x.PROD()) {

                if (vector!=null) {
                    if (vector.getFirst().subs()!=x.subs())
                        return null; //early shape mismatch detection
                }
                if (ADD && hasScalar)
                    return null; //early add shape mismatch detect

                //Subterms xp = x.subterms();
                //if (xp.structureSurface()==INT.bit) {
                (vector == null ? (vector = new TermList(1)) : vector).add(x);
                l.remove();
                //}
            }
//            /            boolean yvv = !yi && varOrVector(y);
        }
        if (vector!=null) {
            if (ADD) {
                if (hasScalar)
                    return null; //shape incompatible
                if (vector.size() == 1) {
                    xx.add(vector.getFirst()); //no change
                } else {
                    //addition: allow multiple but of the same shape

                    Compound v0 = (Compound) vector.get(0);
                    int v0s = v0.subs();
                    Term[] V = v0.subterms().arrayClone();
                    int vectorSize = vector.size();
                    for (int i = 1; i < vectorSize; i++) {
                        Term x = vector.get(i);
                        if (x.subs() != v0s)
                            return null; //shape mismatch - should have been detected earlier

                        MathFunc.addTo(x.subtermsDirect(), V);
                    }

                    //TODO separate into addition of vector if it consists of all add()'s of same arity
                    boolean separate = true;
                    int arity = -1;
                    for (Term v : V) {
                        if (!(v instanceof Compound)) {
                            separate = false; break;
                        }
                        Atomic vf = Functor.func(v);
                        if (vf==null || !vf.equals(add)) {
                            separate = false; break;
                        }
                        int a = Functor.args(v).subs();
                        if (arity == -1) arity = a;
                        else if (a!=arity) {
                            separate = false; break;
                        }
                    }
                    if (separate) {
                        //TODO test if exact same as input (no reduction)
                        Term[] r = new Term[arity];
                        int k = 0;
                        for (int i = 0; i < arity; i++) {
                            Term[] c = new Term[V.length];
                            for (int j = 0, vLength = V.length; j < vLength; j++)
                                c[j] = Functor.argSub(V[j], i);
                            r[k++] = PROD.the(c);
                        }
                        Arrays.sort(r);
                        return new TermList(r);
                    } else {
                        xx.add(PROD.the(V));
                    }
                }

            } else {
                //multiple: dot product only
                if (vector.size()!=1)
                    return null; //shape mismatch - should have been detected earlier

                Term v = vector.get(0);
                if (hasScalar) {
                    int S = scalar;
                    if (S != 1) {
                        Term[] k = v.subterms().arrayClone();
                        for (int i = 0; i < k.length; i++)
                            k[i] = mul(k[i], S);
                        v = PROD.the(k);
                    }

                    hasScalar = false; //absorbed
                } //else: re-use input term instance unchanged

                xx.add(v);
            }
        }
        if (hasScalar)
            xx.add(Int.the(scalar));

        if ((tt = xx.size()) <= 1)
            return xx;
        else {
            //else if not termlist: assume it is from a pre-commuted source HACK
            xx.sortThis(); //dont dedup

            //factor duplicates from add->mul or mul->pow
            int start = 0;
            Term last = xx.sub(0);
            for (int i = 1; i < tt; i++) {
                Term wi = xx.sub(i);
                if (!last.equals(wi)) {
                    last = wi;
                    start = i;
                } else {
                    if (i > start) {

                        Int factor = Int.the((i + 1) - start);

                        xx.setFast(start,
                                addOrMult ?
                                        mul(last /* addend */, factor) :
                                        $.func(MathFunc.pow, last /* multiplier, base */, factor));

                        for (int r = 0; r < (i - start); r++) {
                            xx.remove(start + 1);
                            tt--;
                            i--;
                        }
                    }

                }
            }
        }

        return xx;
    }

    @Override
    public final Term applyInline(Subterms args) {
        return apply(null, args);
    }

    Term theCommutive(Term... x) {
        TermList xx = new TermList(x);
        @Nullable Term y = theReducedCommutive(xx);
        return y == null ? $.func(this, x) : y;
    }


}