package nars.experiment;

import jcog.signal.wave2d.LensDistortion2D;
import jcog.signal.wave2d.ScaledBitmap2D;
import jcog.signal.wave2d.WrappedBitmap2D;
import nars.$;
import nars.Player;
import nars.Term;
import nars.experiment.pacman.PacmanGame;
import nars.experiment.pacman.maze.Maze;
import nars.game.Game;
import nars.game.GameTime;
import nars.game.sensor.AbstractSensor;
import nars.gui.sensor.VectorSensorChart;
import nars.sensor.BitmapSensor;
import nars.video.AutoClassifiedBitmap;
import nars.video.SwingBitmap2D;
import org.eclipse.collections.api.block.procedure.primitive.BooleanProcedure;

import static nars.$.$$;
import static spacegraph.SpaceGraph.window;


public class Pacman extends Game {

    private final PacmanGame g = new PacmanGame();

    static float fps =
            //20;
            25;
            //50;

    static final boolean autoencoder = true;
    static final boolean lensDistortion = false;
    static final boolean brightnessNormalize = true;

    private BitmapSensor see;

    public Pacman(String id) {
        super($$(id), GameTime.fps(fps));
    }

    @Override
    protected void init() {
        //int w = 32, h = 32;
        int w, h;
        if (autoencoder) {
            w = h = 256; //higher-res pre-code
        } else {
            w = h = 64;
        }

        ScaledBitmap2D gView = new ScaledBitmap2D(new SwingBitmap2D(g.view), w, h);

        WrappedBitmap2D cam0 = new WrappedBitmap2D(
                gView
        ) {
            @Override
            public void updateCenter() {
                setCenter(
                        (float) (w * (1 - g.player.x / g.maze.width) - 2),
                        (float) (h * (1 - g.player.y / g.maze.height) - 2)
                );
            }
        };
        var b = lensDistortion ? new LensDistortion2D(cam0) : cam0;
        //b = brightnessNormalize ? new BrightnessNormalize(b) : b;

        if (autoencoder) {

//            gView.mode(ColorMode.Hue);


            AutoClassifiedBitmap aa =
                    new AutoClassifiedBitmap(id, b,
                            16, 16, 5, this);
            aa.alpha.set(0.01f);
            aa.resolution(0.05f);


            nar.runLater(()-> window(aa.newChart(), 500, 500));
        } else {
            onFrame(b::updateBitmap);

            AbstractSensor c = senseCamera((x, y) ->
                            //$.inh($.p(id, "see"), $.p(x, y)),
                            $.inh(id, $.p("see", $.p(x, y))),
                    b/*, 0*/);
//                .model(
//                        //new QueueVectorSensorAttention(0.5f)
//                        new ChunkedVectorSensorAttention(1f, 2)
//                );
            this.see = (BitmapSensor)c;
            c.resolution(0.05f);
        }

//        for (MonoBufImgBitmap2D.ColorMode cm : new MonoBufImgBitmap2D.ColorMode[]{
//                MonoBufImgBitmap2D.ColorMode.R,
//                MonoBufImgBitmap2D.ColorMode.G,
//                MonoBufImgBitmap2D.ColorMode.B
//        }) {
//            Bitmap2DSensor c = senseCamera(
//                    (x,y)->$.func((Atomic)id, $.the(cm.name()), $.the(x), $.the(y)),
//                    camScale.filter(cm)
//            );
//
//            VectorSensorView v = new VectorSensorView(c, this);
////            onFrame(v::update);
//            gg.add(v/*.withControls()*/);
//            c.resolution(0.1f);
//        }
        /*, 0*/

        //        SpaceGraph.window(gg, 300, 300);

        boolean[] keys = g.keys;

        Term L = $.inh(id, $.p("go", $.p(-1, 0)));
        Term R = $.inh(id, $.p("go", $.p(+1, 0)));
        Term U = $.inh(id, $.p("go", $.p(0, +1)));
        Term D = $.inh(id, $.p("go", $.p(0, -1)));
        actionToggle(L, R, (BooleanProcedure)
            p -> toggleKey(keys, p, 0, 1, 2, 3, Maze.Direction.left),
            p -> toggleKey(keys, p, 1, 0, 2, 3, Maze.Direction.right)
        );
        actionToggle(U, D, (BooleanProcedure)
            p -> toggleKey(keys, p, 2, 3, 0, 1, Maze.Direction.up),
            p -> toggleKey(keys, p, 3, 2, 0, 1, Maze.Direction.down)
        );
//        actionTriState($.p(id,$.p($.the("x"), $.varQuery(1))), (dh) -> {
//            switch (dh) {
//                case +1:
//                    g.keys[1] = true;
//                    g.keys[0] = false;
//                    break;
//                case -1:
//                    g.keys[0] = true;
//                    g.keys[1] = false;
//                    break;
//                case 0:
//                    g.keys[0] = g.keys[1] = false;
//                    break;
//            }
//        });
//
//        actionTriState($.p(id,$.p($.the("y"), $.varQuery(1))), (dh) -> {
//            switch (dh) {
//                case +1:
//                    g.keys[2] = true;
//                    g.keys[3] = false;
//                    break;
//                case -1:
//                    g.keys[3] = true;
//                    g.keys[2] = false;
//                    break;
//                case 0:
//                    g.keys[2] = g.keys[3] = false;
//                    break;
//            }
//        });


        reward("alive", ()->{
            return g.dying ? 0 : 1;
        });//.usually(1);

        //TODO multiple reward signals: eat, alive, dist->ghost (cheat)
        reward("score", ()->{
            g.update();

            int nextScore = g.score;

            float r = (nextScore - lastScore);
//            if(r == 0)
//                return Float.NaN;


            lastScore = nextScore;
            if (r > 0) return +1;
            return 0;
//            else if (r < 0) return 0;
//            else
//                return 0.5f;
                //return Float.NaN;

        });//.usually(0);

    }

    private boolean toggleKey(boolean[] keys, boolean pressed, int a, int b, int c, int d, Maze.Direction dir) {
        if (pressed) {
            keys[a] = true;
            keys[b] = false;
            keys[c] = keys[d] = false;
            return !g.player.walled(dir);
        } else {
            keys[a] = false;
            return false;
        }
    }

    int lastScore;


    public static void main(String[] args) {

        Player p = new Player(
                new Pacman("pac")
                //new Pacman("(pac,p)"),new Pacman("(pac,q)")
            ).fps(fps*2).start();

        p.getNar().runLater(()->{
            p.the(Pacman.class).forEach(a -> {
                if (a.see!=null)
                    window(new VectorSensorChart(a.see, a).withControls()
                            , 400, 400);
            });

        });

    }

}