package nars.experiment;

import jcog.agent.Agent;
import jcog.ql.dqn.ValuePredictAgent;
import nars.NAR;
import nars.NARS;
import nars.game.Game;
import nars.game.action.util.Reflex;
import nars.gui.NARui;
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction;

import java.util.function.Consumer;

import static spacegraph.SpaceGraph.window;

public class RLPlayer {
    public static void main(String[] args) {

        Game g =
                new Tetris(); window(Tetris.tetrisView(((Tetris)g)), 400, 800);
                //new PoleCart($.the("p"), true);
//                new ArkaNAR($.$$("a")) {
//                    @Override
//                    protected void init() {
//                        super.init();
//                        nar.runLater(()-> window(view(), 400, 400));
//                    }
//                };
                //new Arm2D("a");
                //new FZero($.$$("f"));
                //new NObvious("o", 1);
                //new NObvious("o", 0);
                //new NObvious("o", 0.5f);
                //new SineRider("s");
                //new NARio();
                //new Gradius($$("g"));
                //new TrackXY($.the("t"), 9, 1);
                //new TrackXY($.the("t"), 4, 4);
                //new BitGame.BitGame1($.the("t"), 2, 2);
                //new Recog2D();
                //new Pacman("p");

        //run(g);
        run(g, 100);

    }

    @Deprecated public static void run(Game p) {
        run(p, 0);
    }

    public static void run(Game g, float fps) {
        run(g, fps, ValuePredictAgent::DQN);
    }

    public static void run(Game g, float fps, IntIntToObjectFunction<Agent> agent) {
        NAR n =
            fps > 0 ? NARS.realtime(fps).get() :
            NARS.shell();

        if (fps == 0)
            n.time.dur(3);

        Reflex r = new Reflex(
                agent
                //ValuePredictAgent::DQrecurrent
                //ValuePredictAgent::direct
                //ValuePredictAgent::DQNbig
                //ValuePredictAgent::DQN_NTM
                //ValuePredictAgent::DQNmini
                //ValuePredictAgent::CMAES
                //ValuePredictAgent::Random
                //ValuePredictAgent::DQN_LSTM
                , g,
                //1
                1,2
                //1,4
                //1,2,4
                //1,2,4,16
                //1,2,4,8,16,32,64
        ) {
            @Override
            public void startIn(Game g) {
                super.startIn(g);
                n.runLater(()->
                    window(NARui.reflexUI(this), 900, 900));
            }
        };

        n.add(g);
        g.nar = n; //HACK
        //new GameStats(p);


        g.afterFrame(new Consumer<Game>() {
            int c = 0;
            @Override
            public void accept(Game z) {
                if (++c % 100 == 0) {
                    System.out.println(z.time() + "\t" + g.rewards.rewardStat.getSum());
                }
            }
        });

        window(NARui.top(n), 800, 500);

        //n.startFPS(fps);
        n.start();
    }


}