package nars.experiment

import jcog.Config
import jcog.Util
import jcog.math.FloatDifference
import jcog.math.FloatSupplier
import jcog.signal.FloatRange
import jcog.signal.IntRange
import jcog.signal.MutableEnum
import jcog.signal.wave2d.AbstractBitmap2D
import jcog.signal.wave2d.Bitmap2D
import nars.Player
import nars.Term
import nars.`$`
import nars.experiment.Tetris
import nars.experiment.Tetris.TetrisModel.PossibleBlocks
import nars.func.java.Opjects
import nars.game.Game
import nars.game.action.AbstractGoalAction
import nars.game.action.BiPolarAction
import nars.gui.sensor.VectorSensorChart
import nars.sensor.BitmapSensor
import nars.term.atom.Atomic
import org.eclipse.collections.api.block.function.primitive.FloatToFloatFunction
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction
import org.eclipse.collections.api.block.predicate.primitive.BooleanPredicate
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure
import spacegraph.SpaceGraph
import spacegraph.space2d.container.Splitting
import spacegraph.space2d.container.grid.Gridding
import spacegraph.space2d.widget.meta.ObjectSurface
import spacegraph.space2d.widget.meter.BitmapMatrixView
import spacegraph.space2d.widget.meter.ImmediateMatrixView
import spacegraph.video.Draw
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.function.Function
import java.util.function.IntFunction
import java.util.function.LongSupplier
import java.util.stream.Collectors
import java.util.stream.IntStream
import java.util.stream.Stream

/**
 * Created by me on 7/28/16.
 */
class Tetris @JvmOverloads constructor(
    id: Term? = Atomic.atom("tetris"),
    private val width: Int = tetris_width,
    private val height: Int = tetris_height,
) : Game(id) {
    var rewardLowHeight = true
    var rewardHighDensity = true
    var rewardStability = true
    var rewardLowHeightChange = false
    private var opj: Opjects? = null

    //    public boolean rewardDensityChange = false;
    enum class PieceSupplier {
        None {
            override operator fun next(): TetrisPiece? {
                return null
            }
        },
        Simple {
            val CENTER_5_X_5 = arrayOf(
                TetrisPiece.EMPTY_ROW,
                TetrisPiece.EMPTY_ROW,
                TetrisPiece.CENTER,
                TetrisPiece.EMPTY_ROW,
                TetrisPiece.EMPTY_ROW)
            val b: TetrisPiece = object : TetrisPiece() {
                init {
                    setShape(0, *CENTER_5_X_5)
                    setShape(1, *CENTER_5_X_5)
                    setShape(2, *CENTER_5_X_5)
                    setShape(3, *CENTER_5_X_5)
                }
            }

            override operator fun next(): TetrisPiece? {
                return b
            }
        },
        Block {
            override operator fun next(): TetrisPiece? {
                return PossibleBlocks.Square.shape
            }
        },
        Tetris {
            val x = Stream.of(*PossibleBlocks.values()).map { posBlocks: PossibleBlocks -> posBlocks.shape }
                .collect(Collectors.toList())

            override operator fun next(): TetrisPiece? {
                return x[ThreadLocalRandom.current().nextInt(x.size)]
            }
        };

        abstract operator fun next(): TetrisPiece?
    }

    val mode = MutableEnum(PieceSupplier.Tetris)
    val debounceDurs = FloatRange(0f, 0f, 10f)
    var timePerFall: IntRange

    @JvmField
    var vision: BitmapSensor<Bitmap2D>?

    //    /** LR and rotate sensitivity (action threshold) */
    //    public final FloatRange sensitivity = new FloatRange(0.9f, 0, 1);
    private val state: TetrisModel
    private val tLEFT: Term =  //$.the("L");
        `$`.inh(id, "L")

    //$.inh(id, NAct.NEG);
    private val tRIGHT: Term =  //$.the("R");
        `$`.inh(id, "R")

    //$.inh(id, NAct.POS);
    private val tROT: Term =  //$.the("rotate");
        //$.inh("rotate", id);
        `$`.inh(id, "rotate")
    private val tFALL: Term =  //$.the("fall");
        //$.inh("fall", id);
        `$`.inh(id, "fall")
    private val grid: AbstractBitmap2D?

    //private final FloatToFloatFunction sensitivityThresh = (x)->NAct.sensitivityToThresh(sensitivity).asFloat();
    init {
        state = if (opjects) actionsReflect() else TetrisModel(width, height)
        timePerFall = state.timePerFall
        if (speed) {
            val speedAction: AbstractGoalAction =
                action(`$`.inh(id, "speed"), FloatProcedure { s: Float -> timePerFall.setLerp(s) })
        }
        val lowHeight: FloatSupplier = FloatSupplier {
            when {
                state.dead() -> 0f
                else -> {
                    val filled = state.rowsFilled
                    if (filled > 0) 1 - filled.toFloat() / height else 0f
                }
            }
        }
        if (rewardLowHeight) reward(`$`.inh(id, "low"), lowHeight)
        if (rewardStability) reward(`$`.inh(id, "ordered"), StabilitySupplier())
        if (rewardHighDensity) reward(`$`.inh(id, "dense"), DensitySupplier())
        if (rewardLowHeightChange) reward(`$`.inh(id, "lower"),
            FloatDifference(lowHeight,
                LongSupplier { time() }).compose(FloatToFloatFunction { d: Float -> if (d >= 0) Float.NaN else 0f })) //.strength(0.25f)

//        if (rewardDensityChange)
//            rewardNormalizedPolar($.inh(id, "denser"),
//                    new FloatDifference(new DensitySupplier(), this::time).compose(freqSigNumZeroNaN))
//                    //.strength(0.25f)
//        ;
        LRtoggle()
        rotatePushButton()
        if (drop) fallPushButton()
        grid = object : AbstractBitmap2D(state.width, state.height) {
            override fun value(x: Int, y: Int): Float {
                return if (state.seen[y * w + x] > 0) 1f else 0f
            }
        }
        vision = if (bitmapVision) sensors.addSensor(BitmapSensor<Bitmap2D>(
            grid,  //(x, y) -> $.inh($.p(x, y), id),
            IntIntToObjectFunction { x: Int, y: Int ->
                `$`.inh(id,
                    `$`.p(x, y))
            } //(x, y) -> $.p(id, $.the(x), $.the(y)) //FLAT PRODUCT
        )) else null
        onFrame(Runnable {
            state.nextMode = mode.get()
            state.next()
            if (grid != null) grid.updateBitmap()
        })
    }

    override fun init() {
        super.init()
        if (opj != null) nar.add(opj) //HACK
    }

    private fun actionsReflect(): TetrisModel {
        val oi = id //$.p("opjects", id);
        opj = Opjects(focus())
        opj!!.exeThresh.set(0.5f)
        opj!!.methodExclusions.add("toVector")
        return opj!!.a(oi, TetrisModel::class.java, tetris_width, tetris_height)
    }

    private fun LRtoggle() {
        val lr: BiPolarAction = actionToggle(tLEFT, tRIGHT,
            BooleanPredicate { b: Boolean -> b && state.act(TetrisModel.actions.LEFT) },
            BooleanPredicate { b: Boolean -> b && state.act(TetrisModel.actions.RIGHT) } //,sensitivityThresh, q()
        )
    }

    private fun rotatePushButton() {
        actionPushButton(tROT,
            debounce({ it && state.act(TetrisModel.actions.CW) },
                { debounceDurs.toFloat() }) //,sensitivityThresh
        )
    }

    private fun fallPushButton() {
        actionPushButton(tFALL,
            debounce({ b: Boolean -> b && state.act(TetrisModel.actions.FALL) },
                { debounceDurs.toFloat() }) //,sensitivityThresh
        )
    }

    fun actionsTriState() {
        actionTriState(`$`.inh("X", id)) { i: Int ->
            when (i) {
                -1 -> state.act(TetrisModel.actions.LEFT)
                +1 -> state.act(TetrisModel.actions.RIGHT)
                else -> true
            }
        }
        actionPushButton(tROT, Runnable { state.act(TetrisModel.actions.CW) })
    }

    fun density(): Float {
        val cellsFilled = Util.count({ s: Double -> s > 0 }, state.grid)
        val rowsFilled = state.rowsFilled
        return if (rowsFilled > 0) cellsFilled.toFloat() / (rowsFilled * state.width) else 0f
    }

    open class TetrisPiece {
        var thePiece = Array(4) { Array(5) { IntArray(5) } }
        var currentOrientation = 0
        fun setShape(Direction: Int, vararg rows: IntArray) {
            thePiece[Direction] = rows as Array<IntArray>
        }

        override fun toString(): String {
            val shapeBuffer = StringBuilder()
            val p = thePiece[currentOrientation]
            for (i in p.indices) {
                for (j in p[i].indices) shapeBuffer.append(' ').append(p[i][j])
                shapeBuffer.append('\n')
            }
            return shapeBuffer.toString()
        }

        companion object {
            var CENTER = intArrayOf(0, 0, 1, 0, 0)
            var EMPTY_ROW = intArrayOf(0, 0, 0, 0, 0)
            var PAIR1 = intArrayOf(0, 0, 1, 1, 0)
            var PAIR2 = intArrayOf(0, 1, 1, 0, 0)
            var MIDDLE = intArrayOf(0, 1, 1, 1, 0)
            var LINE1 = intArrayOf(0, 1, 1, 1, 1)
            var LEFT1 = intArrayOf(0, 1, 0, 0, 0)
            var RIGHT1 = intArrayOf(0, 0, 0, 1, 0)
        }
    }

    class TetrisModel(var width: Int, var height: Int) {
        val AFTERLIFE_TIME = IntRange(100, 0, 1000)
        val timePerFall = IntRange(
            Config.INT("TETRIS_FALL_TIME", 2),
            1,  //fast
            Config.INT("TETRIS_FALL_MAX", 6) //slow
        )
        var mode: PieceSupplier? = PieceSupplier.None
        var nextMode: PieceSupplier? = null

        /** grid with current block  */
        var seen: DoubleArray

        /** grid without the current block  */
        var grid: DoubleArray
        var running = true

        /** what is the current_score  */
        var score = 0f
        var time = 0
        var currentRotation = 0

        /** position of the falling block  */
        var currentX = 0
        var currentY = 0
        var blockID = -1

        /** > 0:  have we reached the end state yet */
        var dead = 0
        var rowsFilled = 0
        private var rowsFilledAtLastSpawn = 0
        var currentPiece: TetrisPiece? = null

        init {
            grid = DoubleArray(width * height)
            seen = DoubleArray(width * height)
            reset()
        }

        protected fun reset() {
            currentX = width / 2 - 1
            currentY = 0
            currentRotation = 0
            score = 0f
            Arrays.fill(grid, 0.0)
            Arrays.fill(seen, 0.0)
            running = true
            restart()
            //            spawnBlock();
        }

        /**
         * do nothing method for signaling to NAR restart occurred, but not to allow it to trigger an actual restart
         */
        fun restart() {}
        private fun toVector(monochrome: Boolean, target: DoubleArray) {
            Arrays.fill(target, -1.0)
            var x = 0
            for (i in grid) {
                when {
                    monochrome -> when {
                        i > 0 -> 1.0f
                        else -> -1.0f
                    }.toDouble()
                    i > 0 -> i
                    else -> -1.0f
                }.toDouble()
                    .also { target[x] = it }
                x++
            }
            writeCurrentBlock(0.5f, target)
        }

        private fun writeCurrentBlock(color: Float, f: DoubleArray) {
            (currentPiece)?.run {
                val thisPiece = thePiece[currentRotation]
                (color.takeUnless { it == -1f } ?: 1f).let { color -> //currentBlockId + 1;
                    for (y in thisPiece[0].indices)
                        for (x in thisPiece.indices)
                            if (thisPiece[x][y] != 0)
                                f[i(currentX + x, currentY + y)] = color.toDouble()
                }
            }
        }

        fun gameOver(): Int {
            val o = dead
            if (o > 0) dead--
            return o
        }

        /* This code applies the action, but doesn't do the default fall of 1 square */
        fun act(theAction: actions?): Boolean {
            synchronized(this) {
                var nextRotation = currentRotation
                var nextX = currentX
                var nextY = currentY
                when (theAction) {
                    actions.CW -> nextRotation = (currentRotation + 1) % 4
                    actions.CCW -> {
                        nextRotation = currentRotation - 1
                        if (nextRotation < 0) nextRotation = 3
                    }
                    actions.LEFT -> nextX = currentX - 1
                    actions.RIGHT -> nextX = currentX + 1
                    actions.FALL -> {
                        nextY = currentY
                        var isInBounds = true
                        var isColliding = false
                        while (isInBounds && !isColliding) {
                            nextY++
                            isInBounds = inBounds(nextX, nextY, nextRotation)
                            if (isInBounds) isColliding = colliding(nextX, nextY, nextRotation)
                        }
                        nextY--
                    }
                    else -> throw RuntimeException("unknown action")
                }
                return act(nextRotation, nextX, nextY)
            }
        }

        fun act(): Boolean {
            return act(currentRotation, currentX, currentY)
        }

        fun act(nextRotation: Int, nextX: Int, nextY: Int): Boolean {
            synchronized(this) {
                if (inBounds(nextX, nextY, nextRotation)) if (!colliding(nextX, nextY, nextRotation)) {
                    currentRotation = nextRotation
                    currentX = nextX
                    currentY = nextY
                    return true
                }
            }
            return false
        }

        /**
         * Calculate the learn array position from (x,y) components based on
         * worldWidth.
         * Package level access so we can use it in tests.
         *
         * @param x
         * @param y
         */
        private fun i(x: Int, y: Int): Int {
            return y * width + x
        }

        /**
         * Check if any filled part of the 5x5 block array is either out of bounds
         * or overlapping with something in wordState
         *
         * @param checkX           X location of the left side of the 5x5 block array
         * @param checkY           Y location of the top of the 5x5 block array
         * @param checkOrientation Orientation of the block to check
         * @return
         */
        private fun colliding(checkX: Int, checkY: Int, checkOrientation: Int): Boolean {
            if (currentPiece == null) return false
            var result = false
            val thePiece = currentPiece!!.thePiece[checkOrientation]
            val ll = thePiece.size
            //            try {
            var y = 0
            while (y < thePiece[0].size && !result) {
                val i = checkY + y
                val b1 = !(i < 0 || i >= height)
                var x = 0
                while (x < ll && !result) {
                    val i1 = checkX + x
                    result = (!b1 || i1 < 0 || i1 >= width || grid(i1, i) != 0.0) && thePiece[x][y] != 0
                    ++x
                }
                ++y
            }
            //            } catch (ArrayIndexOutOfBoundsException e) {
//                System.err.println("Error: ArrayIndexOutOfBoundsException in GameState::colliding called with params: " + checkX + " , " + checkY + ", " + checkOrientation);
//                System.err.println("Error: The Exception was: " + e);
//                Thread.dumpStack();
//                System.err.println("Returning true from colliding to help save from error");
//                System.err.println("Setting is_game_over to true to hopefully help us to recover from this problem");
//                is_game_over = 1;
//            }
            return result
        }

        private fun collidingCheckOnlySpotsInBounds(checkX: Int, checkY: Int, checkOrientation: Int): Boolean {
            var result = false
            val thePiece = currentPiece!!.thePiece[checkOrientation]
            val ll = thePiece.size
            //            try {
            var y = 0
            while (y < thePiece[0].size && !result) {
                val i1 = checkY + y
                if (i1 >= 0 && i1 < height) {
                    var x = 0
                    while (x < ll && !result) {
                        val i = checkX + x
                        result = thePiece[x][y] != 0 && i >= 0 && i < width && grid(i, i1) != 0.0
                        ++x
                    }
                }
                ++y
            }
            //            } catch (ArrayIndexOutOfBoundsException e) {
//                System.err.println("Error: ArrayIndexOutOfBoundsException in GameState::collidingCheckOnlySpotsInBounds called with params: " + checkX + " , " + checkY + ", " + checkOrientation);
//                System.err.println("Error: The Exception was: " + e);
//                Thread.dumpStack();
//                System.err.println("Returning true from colliding to help save from error");
//                System.err.println("Setting is_game_over to true to hopefully help us to recover from this problem");
//                is_game_over = 1;
//            }
            return result
        }

        /**
         * This function checks every filled part of the 5x5 block array and sees if
         * that piece is in bounds if the entire block is sitting at (checkX,checkY)
         * on the board.
         *
         * @param checkX           X location of the left side of the 5x5 block array
         * @param checkY           Y location of the top of the 5x5 block array
         * @param checkOrientation Orientation of the block to check
         * @return
         */
        private fun inBounds(checkX: Int, checkY: Int, checkOrientation: Int): Boolean {
            var result = false
            if (currentPiece == null) return true

//            try {
            val thePiece = currentPiece!!.thePiece[checkOrientation]
            var finished = false
            var y = 0
            while (!finished && y < thePiece[0].size) {
                val i1 = checkY + y
                val b = i1 >= 0 && i1 < height
                var x = 0
                while (!finished && x < thePiece.size) {
                    val i = checkX + x
                    finished = (!b || i < 0 || i >= width) && thePiece[x][y] != 0
                    ++x
                }
                ++y
            }
            result = !finished

//            } catch (ArrayIndexOutOfBoundsException e) {
//                System.err.println("Error: ArrayIndexOutOfBoundsException in GameState::inBounds called with params: " + checkX + " , " + checkY + ", " + checkOrientation);
//                System.err.println("Error: The Exception was: " + e);
//                Thread.dumpStack();
//                System.err.println("Returning false from inBounds to help save from error.  Not sure if that's wise.");
//                System.err.println("Setting is_game_over to true to hopefully help us to recover from this problem");
//                is_game_over = true;
//            }
            return result
        }

        fun nextInBounds(): Boolean {
            return inBounds(currentX, currentY + 1, currentRotation)
        }

        fun nextColliding(): Boolean {
            return colliding(currentX, currentY + 1, currentRotation)
        }

        /*Ok, at this point, they've just taken their action.  We now need to make them fall 1 spot, and check if the game is over, etc */
        private fun update() {
            act()
            time++
            if (!inBounds(currentX,
                    currentY,
                    currentRotation)
            ) System.err.println("In GameState.Java the Current Position of the board is Out Of Bounds... Consistency Check Failed")
            gomezAdamsMethod(!nextInBounds())
        }

        /**
         * casey jones you better watch your speed...
         *
         * @param onSomething is the engineer onSomething that has side effects?
         */
        fun gomezAdamsMethod(onSomething: Boolean) {

            //running NextInbounds without nextColliding appears mutually exclusive
            if (onSomething || nextColliding()) {
                running = false
                writeCurrentBlock(-1f, grid)
            } else {
                if (time % timePerFall.toInt() == 0) currentY += 1
            }
        }

        fun spawnBlock() {
            blockID++
            running = true
            rowsFilledAtLastSpawn = rowsFilled
            currentPiece = mode!!.next()
            currentRotation = 0
            currentX = width / 2 - 2
            currentY = -4
            var hitOnWayIn = false
            while (!inBounds(currentX, currentY, currentRotation)) {
                hitOnWayIn = collidingCheckOnlySpotsInBounds(currentX, currentY, currentRotation)
                currentY++
            }
            val is_game_over = hitOnWayIn || colliding(currentX, currentY, currentRotation)
            if (is_game_over) {
                dead = AFTERLIFE_TIME.toInt()
                reset()
                running = false
            }
        }

        fun checkScore() {
            var numRowsCleared = 0
            var rowsFilled = 0
            var y = height - 1
            while (y >= 0) {
                if (isRow(y, true)) {
                    removeRow(y)
                    numRowsCleared += 1
                    y += 1
                } else if (!isRow(y, false)) rowsFilled++
                --y
            }
            val prevRows = this.rowsFilled
            this.rowsFilled = rowsFilled
            if (numRowsCleared > 0) {
            } else {
            }
            val diff = prevRows - rowsFilled
            score = if (diff >= height - 1) Float.NaN else diff.toFloat()
        }

        fun height(): Float {
            return rowsFilled.toFloat() / height
        }

        /**
         * Check if a row has been completed at height y.
         * Short circuits, returns false whenever we hit an unfilled spot.
         *
         * @param y
         * @return
         */
        fun isRow(y: Int, filledOrClear: Boolean): Boolean {
            val bound = width
            for (x in 0 until bound) {
                val s = grid(x, y)
                if (if (filledOrClear) s == 0.0 else s != 0.0) return false
            }
            return true
        }

        fun grid(x: Int, y: Int): Double {
            return grid[i(x, y)]
        }

        /**
         * Dec 13/07.  Radkie + Tanner found 2 bugs here.
         * Bug 1: Top row never gets updated when removing lower rows. So, if there are
         * pieces in the top row, and we clear something, they will float there.
         *
         * @param y
         */
        fun removeRow(y: Int) {
            if (!isRow(y, true)) {
                System.err.println("In GameState.java remove_row you have tried to remove a row which is not complete. Failed to remove row")
                return
            }
            for (x in 0 until width) {
                val linearIndex = i(x, y)
                grid[linearIndex] = 0.0
            }
            for (ty in y downTo 1) for (x in 0 until width) {
                val linearIndexTarget = i(x, ty)
                val linearIndexSource = i(x, ty - 1)
                grid[linearIndexTarget] = grid[linearIndexSource]
            }
            for (x in 0 until width) {
                val linearIndex = i(x, 0)
                grid[linearIndex] = 0.0
            }
        }

        /**
         * Utility methd for debuggin
         */
        protected fun printState() {
            val index = 0
            for (i in 0 until height - 1) {
                for (j in 0 until width) print(grid[i * width + j])
                print("\n")
            }
            println("-------------")
        }

        operator fun next() {
            val o = gameOver()
            if (o > 0) return
            val running = running
            if (running) update()
            if (!running || nextMode !== mode) {
                mode = nextMode
                spawnBlock()
            }
            checkScore()
            if (!dead()) toVector(false, seen)

//            if (o > 0) {
//                if (--o == 0)
//                    reset();
//            }
        }

        private fun score(): Float {
            return score
        }

        fun dead(): Boolean {
            return dead > 0
        }

        enum class actions {
            /**
             * Action value for a move left
             */
            LEFT,

            /**
             * Action value for a move right
             */
            RIGHT,

            /**
             * Action value for a clockwise rotation
             */
            CW,

            /**
             * Action value for a counter clockwise rotation
             */
            CCW,

            /**
             * The no-action Action
             */
            NONE,

            /**
             * fall down
             */
            FALL
        }

        enum class PossibleBlocks(var shape: TetrisPiece) {
            Line(object : TetrisPiece() {
                init {
                    setShape(0, CENTER, CENTER, CENTER, CENTER, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, EMPTY_ROW, LINE1, EMPTY_ROW, EMPTY_ROW)
                    setShape(2, CENTER, CENTER, CENTER, CENTER, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, EMPTY_ROW, LINE1, EMPTY_ROW, EMPTY_ROW)
                }
            }),
            Square(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, PAIR1, PAIR1, EMPTY_ROW, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, PAIR1, PAIR1, EMPTY_ROW, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, PAIR1, PAIR1, EMPTY_ROW, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, PAIR1, PAIR1, EMPTY_ROW, EMPTY_ROW)
                }
            }),
            Tri(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, CENTER, MIDDLE, EMPTY_ROW, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, CENTER, PAIR1, CENTER, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, EMPTY_ROW, MIDDLE, CENTER, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, CENTER, PAIR2, CENTER, EMPTY_ROW)
                }
            }),
            SShape(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, LEFT1, PAIR2, CENTER, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, PAIR1, PAIR2, EMPTY_ROW, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, LEFT1, PAIR2, CENTER, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, PAIR1, PAIR2, EMPTY_ROW, EMPTY_ROW)
                }
            }),
            ZShape(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, CENTER, PAIR2, LEFT1, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, PAIR2, PAIR1, EMPTY_ROW, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, CENTER, PAIR2, LEFT1, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, PAIR2, PAIR1, EMPTY_ROW, EMPTY_ROW)
                }
            }),
            LShape(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, CENTER, CENTER, PAIR1, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, EMPTY_ROW, MIDDLE, LEFT1, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, PAIR2, CENTER, CENTER, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, RIGHT1, MIDDLE, EMPTY_ROW, EMPTY_ROW)
                }
            }),
            JShape(object : TetrisPiece() {
                init {
                    setShape(0, EMPTY_ROW, CENTER, CENTER, PAIR2, EMPTY_ROW)
                    setShape(1, EMPTY_ROW, LEFT1, MIDDLE, EMPTY_ROW, EMPTY_ROW)
                    setShape(2, EMPTY_ROW, PAIR1, CENTER, CENTER, EMPTY_ROW)
                    setShape(3, EMPTY_ROW, EMPTY_ROW, MIDDLE, RIGHT1, EMPTY_ROW)
                }
            });
        }
    }

    private inner class DensitySupplier : FloatSupplier {
        var lastBlock = -1
        var lastScore = 0f
        override fun asFloat(): Float {
            if (!state.dead()) {
                val nextBlock = state.blockID
                if (nextBlock != lastBlock) {
                    //only update score after piece lands
                    lastBlock = nextBlock
                    lastScore = density()
                }
                return lastScore
            }
            return 0f
        }
    }

    /** counts proportion of columns which do not have empty spaces beneath a filled space  */
    private inner class StabilitySupplier : FloatSupplier {
        override fun asFloat(): Float {
            if (!state.dead()) {
                var colsError = 0
                for (col in 0 until state.width) {
                    var fill = false
                    var emptyBelowFill = false
                    for (row in 0 until state.height) {
                        val g = state.grid(col, row)
                        val rowFill = g != 0.0
                        if (rowFill) fill = true else {
                            if (fill) emptyBelowFill = true
                        }
                    }
                    if (emptyBelowFill) colsError++
                }
                return 1 - colsError.toFloat() / state.width
            }
            return 0f
        }
    }

    object MiniTris {
        @JvmStatic
        fun main(args: Array<String>) {
            val t = Tetris(Atomic.atom("tetris"), 6, 12)
            val p = Player(t).fps(FPS)
            p.start()
            SpaceGraph.window(Gridding(p.the(Tetris::class.java)
                .map(Function<Tetris, Splitting<*, *>> { t: Tetris -> view(t) })), 800, 800)
        }
    }

    companion object {
        @JvmField
        val tetris_width = Config.INT("TETRIS_WIDTH", 8)

        @JvmField
        val tetris_height = Config.INT("TETRIS_HEIGHT", 16)
        const val GAMES =  //1;
        //2;
            //3;
            4

        //8;
        //16;
        var FPS = 60f
        var opjects = false
        var bitmapVision = true
        private const val drop = false
        private const val speed = true

        @JvmStatic
        fun main(args: Array<String>) {
            val p = Player(if (GAMES == 1) Stream.of(Tetris()) else IntStream.range(0, GAMES).mapToObj(
                IntFunction { i: Int ->
                    Tetris(
                        `$`.p("tetris", Character.toString('a'.code + i)),
                        tetris_width, tetris_height)
                }
            )
            ).fps(FPS)
            p.start()
            SpaceGraph.window(Gridding(p.the(Tetris::class.java)
                .map { t: Tetris -> view(t) }), 800, 800)
        }

        private fun view(t: Tetris): Splitting<*, *> {
            return Splitting(
                ObjectSurface(t),
                0.9f,
                if (t.vision != null) VectorSensorChart(t.vision, t).withControls() else tetrisView(t)
            ).resizeable()
        }

        @JvmStatic
        fun tetrisView(t: Tetris): ImmediateMatrixView {
            return ImmediateMatrixView(t.width, t.height)
                { x: Int, y: Int -> Draw.colorBipolar(t.grid!!.value(x, y)) }
        }
    }
}