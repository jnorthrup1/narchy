package nars.experiment.misc;

import jcog.Log;
import jcog.Str;
import jcog.Util;
import jcog.agent.Agent;
import jcog.decide.DecideSoftmax;
import jcog.io.Shell;
import jcog.signal.ArraySensor;
import nars.$;
import nars.experiment.RLPlayer;
import nars.game.Game;
import nars.sensor.BitmapSensor;
import org.json.JSONArray;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

/**
 * TODO not completely working
 * http://www.infowars.com/creeps-in-space/ */
public class AIGymGame extends Game {

    private static final Logger logger = Log.log(AIGymGame.class);

    private final String environmentID;
    private final MyShell shell;
    private boolean envReady;
    private final AtomicReference<Predicate<String>> nextLineConsumer = new AtomicReference(null);
    private int framesRemain, batchesRemain;

    private int inputs = -1, outputs = -1;
    private int nextAction = -1;
    private double[] low;
    private double[] high;
    private double[] input, output;
    private boolean finished;
    private double reward;
    private double[] lastInput;


    /**
     * terminates if batches < 0 or frames < 0
     */
    private AIGymGame(String environmentID, int batches, int frames) throws IOException {
        super(environmentID);

        this.environmentID = environmentID;

        shell = new MyShell("python", "-i" /* interactive */, "-q" /* quiet */, "-u" /* unbuffered */,
            "-X utf8"
        );
        envReady = false;
        batchesRemain = batches;
        framesRemain = frames;

        input("import gym, logging, json, yaml\n");
        input("import numpy as np\n");
        input("logging.getLogger().setLevel(logging.WARN)\n"); //INFO and LOWER are not yet supported
        input("def enc(x):\n\tprint('\\n')\n\treturn json.dumps(x)\n");
        input("def encS(x):\n\tx.pop() ; x[0] = x[0].flatten().tolist() ; x[2] = int(x[2])\n\treturn json.dumps(x)\n");
        input("env = gym.make('" + environmentID + "')\n");
//        (line) -> {
//            if (line.contains("Making new env: " + environmentID)) {
//
//
//
//
////                this.agentBuilder = a;
//
//                //video_callable=None
////                input("env.monitor.start('/tmp/" + this.environmentID + "',force=True)", x -> {
////
////                    if (x.contains("Starting") || x.contains("Clearing")) {
////                    //if (x.contains("Clearing")) {
////                        //monitor started
////
////                        reset();
////                    }
////
////                    return true;
////                });
////                reset();
//
//            }
//            return true;
//        });
        nextIO();

        //wait for initialization
        while (inputs < 0) {
            Util.sleepMS(10);
        }

    }

    private synchronized void nextIO() {
        finished = false;

        input("enc([env.observation_space.low.flatten().tolist(),env.observation_space.high.flatten().tolist(),str(env.action_space),env.reset().flatten().tolist()])", y -> {

            batchesRemain--;
            envReady = true;

            onFrame(y);

            return true;
        });
    }

    protected void input(String line) {
        //input(line, null);
        shell.println(line);
    }

    protected void input(String line, Predicate<String> result) {
        //if (nextLineConsumer.compareAndSet(null, result) || nextLineConsumer.compareAndSet(result, result)) {

        synchronized (nextLineConsumer) {

            nextLineConsumer.set(result);

            shell.println(line);

        }
        //}
        //throw new RuntimeException("repl interruption");
    }

//    protected void waitEnv() {
//        //TODO dont use polling method but a better async interrupt
//        if (envReady)
//            return;
//        logger.info("environment {} starting...", environmentID);
//        while (!envReady) {
//            Util.pause(20);
//        }
//        logger.info("environment {} ready", environmentID);
//    }

    @FunctionalInterface
    interface AgentBuilder {
        Agent newAgent(/*int inputs, float[] inputLow, float[] inputHigh, int outputs*/);
    }


    private boolean onFrame(String f) {


        f = f.trim();

        if (f.startsWith(">>> "))
            f = f.substring(4);

        if (f.startsWith("'")) {
            f = f.substring(1, f.length() - 1);

            if (onJSON(f)) {
                framesRemain--;
                nextFrame();
            } else {
                System.err.println("not json: " + f);
            }
            return true;
        } /*else {
            System.err.println("ignored: " + f);
        }*/
        return false;
    }

    private void nextFrame() {
        if ((--framesRemain < 0) || (batchesRemain < 0))
            return;

//        if (finished) {
//            nextIO();
//            return;
//        }

        nextAction = new DecideSoftmax(0.5f, ThreadLocalRandom.current()).applyAsInt(output);
        if (nextAction == -1) {
            nextAction = (int) Math.floor(Math.random() * outputs); //HACK
        }

        String actionString = actionModel.toEnvironment(nextAction);
        input("env.render() ; encS(list(env.step(" + actionString + ")))");

        System.out.println( Str.n4(reward) + " -| " + actionString
                // + " -| "  + Str.n4(input)
                );

    }

    private ActionModel actionModel;

    interface ActionModel {

        String toEnvironment(int i);

        int actions();
    }

    public static class DiscreteActionModel implements ActionModel {
        final int actions;

        DiscreteActionModel(String a) {
            this.actions = Integer.parseInt(a.substring(9, a.length() - 1));
        }

        @Override
        public String toEnvironment(int i) {
            return Integer.toString(i);
        }

        @Override
        public int actions() {
            return actions;
        }
    }

    public static class BoxActionModel implements ActionModel {

        final int actions;
        private final float deltaSpeed;
        private final float maxSpeed;
        private final float decay = 0.9f;
        float[] f;

        BoxActionModel(String a, float deltaSpeed, float maxSpeed) {
            this.deltaSpeed = deltaSpeed;
            this.maxSpeed = maxSpeed;
//            if (a.endsWith(",)")) {
//                int dims = Integer.parseInt(a.substring(4, a.length() - 2));
//                if (dims != 1)
//                    throw new UnsupportedOperationException(a);
            int dims = 1;
                actions = dims * 4; //TODO support proportional control
                f = new float[dims];
//            } else {
//                throw new UnsupportedOperationException(a);
//            }
        }

        @Override
        public String toEnvironment(int i) {
            int index = i % 4;
            switch (index) {
                case 0:
                    break;
                case 1:
                    f[0] = f[0] * decay;
                    break;
                case 2:
                    f[0] = Util.clamp(f[0] + deltaSpeed, -1, +1);
                    break;
                case 3:
                    f[0] = Util.clamp(f[0] - deltaSpeed, -1, +1);
                    break;
            }



            float[] g = f.clone();
            for (int x = 0; x < g.length; x++)
                g[x] = maxSpeed * g[x];

            return "np.array(" + Arrays.toString(g) + ")";
        }

        @Override
        public int actions() {
            return actions;
        }
    }

    private boolean onJSON(String f) {
        JSONArray j = (JSONArray) pyjson(f);
        if (j == null)
            return false;


        if (inputs < 0 && (f.contains("Discrete") || f.contains("Box"))) {
            //first cycle, determine model parameters
            //model = Tuples.twin()
            input = asArray(j, 3);
            inputs = input.length;

            low = asArray(j, 0);
            high = asArray(j, 1);
            //restore +-Infinity HACK
            for (int i = 0; i < inputs; i++) {
                if (low[i] == high[i]) {
                    low[i] = Double.NEGATIVE_INFINITY;
                    high[i] = Double.POSITIVE_INFINITY;
                }
            }

            String a = j.get(2).toString();
            if (a.startsWith("Discrete")) {
                actionModel = new DiscreteActionModel(a);
            } else if (a.startsWith("Box")) {
                actionModel = new BoxActionModel(a, 0.3f /*default */, 4f);
            } else {
                throw new UnsupportedOperationException("Unknown action_space type: " + a);
            }
            outputs = actionModel.actions();

//            agent = agentBuilder.newAgent(/*inputs, Util.toFloat(low), Util.toFloat(high), outputs*/);
//            agent.start(inputs, outputs);
            initGym();

        } else {

            //ob, reward, done, _

            //TODO dont re-allocate arrays
            input = asArray(j, 0);
            reward = asDouble(j, 1);
            finished = j.getInt(2) == 1;
        }

//        nextAction = agent.act((float) reward, normalizeInput(input, lastInput), null);
//        lastInput = input;

        return true;

    }

    private synchronized void initGym() {

        addSensor(new BitmapSensor(new ArraySensor(inputs, false) {
            @Override
            protected float value(int i) {
                return (float) input[i];
            }
        }, id));

        output = new double[outputs];
        for (int o = 0; o < outputs; o++) {
            int O = o;
            action($.inh(id, Integer.toString(o)), (a)->{
                output[O] = a;
               //TODO
            });
        }

    }

    protected void computeDelta(double[] a, double[] b) {
        if (b == null) return;

        int byteDiffs = 0, bitDiffs = 0;
        for (int i = 0; i < a.length; i++) {
            if (!Util.equals(a[i], b[i], 0.5f/256.0f)) {
                byteDiffs++;
                int x = (int)Math.round(a[i]);
                int y = (int)Math.round(b[i]);
                int z = x ^ y;
                bitDiffs += Integer.bitCount(z);
            }
        }
        System.out.println("byte diffs: " + byteDiffs + " / " + a.length + "\t\tbit diffs: " + bitDiffs + " / " + (a.length * 8));
    }

    private double[] normalizeInput(double[] x, double[] x0 /* prev */) {

        //computeDelta(x, x0);

        //normalize input to low/high range
        double[] low = this.low;
        double[] high = this.high;
        for (int i = 0; i < x.length; i++) {
            double l = low[i];
            double h = high[i];
            x[i] = Util.normalize(x[i], l, h);
        }
        return x;
    }

    private static Object pyjson(String j) {
        j = j.replace("Infinity", "1"); //HACK

        try {
            //doesnt handle Inf
            return new JSONArray(j);
            //return JSONObject.stringToValue(j);
            //return ((JsonArray) Json.parse(j));
        } catch (Exception e) {
            System.err.println("can not parse: " + j);
            e.printStackTrace();
        }
        return null;

    }

    private static double[] asArray(JSONArray j, int index) {
        return asArray(j.getJSONArray(index));
    }

    private static double[] asArray(JSONArray x) {
        double[] y = new double[x.length()];
        for (int i = 0; i < y.length; i++)
            y[i] = x.getDouble(i);
        return y;
    }

    private static double asDouble(JSONArray j, int index) {
        return j.getDouble(index);
    }


    public static void main(String[] args) throws IOException {
        Game g = new AIGymGame(
                //"BeamRider-ram-v0"

                //"CrazyClimber-ram-v0"
                //"CrazyClimber-v0"

                //"CartPole-v0" //WARNING: infinity
                //"MountainCar-v0"
                //"DoomDeathmatch-v0" //2D inputs
                //"LunarLander-v1"
                "Pendulum-v0"
                //"InvertedDoublePendulum-v1"

                //"Pong-v0"
                //"Pong-ram-v0"

                //"BipedalWalker-v1"
                //"Hopper-v1"
                //"MsPacman-ram-v0"
                //"SpaceInvaders-ram-v0" //<---
                //"Hex9x9-v0"
                , 1000, 100000);

        RLPlayer.run(g);

/*
    # The world's simplest agent!
    class RandomAgent(object):
        def __init__(self, action_space):
            self.action_space = action_space

        def act(self, observation, reward, done):
            return self.action_space.sample()


    # You can optionally set up the logger. Also fine to set the level
    # to logging.DEBUG or logging.WARN if you want to change the
    # amount of output.
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # also dump to a tempdir if you'd like: tempfile.mkdtemp().
    outdir = '/tmp/random-agent-results'
    env.monitor.start(outdir, force=True)

    episode_count = 100
    max_steps = 200
    reward = 0
    done = False

    for i in range(episode_count):
        ob = env.reset()

        for j in range(max_steps):
            action = agent.act(ob, reward, done)
            ob, reward, done, _ = env.step(action)
            if done:
                break

    # Dump result info to disk
    env.monitor.close()
*/
        //Util.pause(1000);


    }

    private class MyShell extends Shell {
        public MyShell(String... cmd) throws IOException {
            super(cmd);
        }

        @Override
        protected void readln(String line) {
            super.readln(line);


            if (nextLineConsumer != null) /* since it can be called from super class constructor */ {

                synchronized (nextLineConsumer) {
                    Predicate<String> c = nextLineConsumer.get();
                    if (c != null)
                        c.test(line);

                }


            }


        }
    }


//    public static void main(String[] args) throws Exception {
//        File pyFile = new File("/tmp/x.py");
//        String ret = (String) PyCaller.call(pyFile, "hi", (Object)new String[]{"all", "my", "friend", "!"});
//        System.out.println(ret);
//        //assertEquals("hello, all my friend ! ",ret);
//    }


//        Socket s = new Socket();
//        //s.setKeepAlive(true);
//        s.setReceiveBufferSize(1);
//        s.setSendBufferSize(1);
//        //s.setTcpNoDelay(true);
//
//        String host = "127.0.0.1";
//        int port = 7777;
//        PrintWriter s_out = null;
//
//            s.connect(new InetSocketAddress(host , port));
//
//            //writer for socket
//            s_out = new PrintWriter( s.getOutputStream());
//        BufferedReader s_in = new BufferedReader(new InputStreamReader(s.getInputStream()));
//
//
//        System.out.println(s_in.readLine());
//
//        //s_out.println( "admin\nadmin\n" );
//        //s_out.flush();
//
//        //Send message to server
//        String message = "print 1+2\nprint 3+1\n";
//        s_out.println( message );
//        s_out.flush();
//
//
//
//        //Get response from server
//        String response;
//
//        do {
//            response = s_in.readLine();
//            System.out.println( response );
//
//            Thread.sleep(100);
//        } while (true);
//
//
//
//    }
}