package nars.experiment;

import jcog.Util;
import jcog.data.bit.MetalBitSet;
import jcog.lab.Lab;
import nars.$;
import nars.NAL;
import nars.Player;
import nars.Term;
import nars.derive.pri.DefaultBudget;
import nars.experiment.trackxy.TrackXY;
import nars.game.Game;
import nars.task.NALTask;
import nars.term.Termed;
import spacegraph.space2d.TsneRenderer;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toSet;
import static jcog.Str.n2;
import static nars.$.$$;
import static spacegraph.SpaceGraph.window;

public enum Simplest { ;

    static boolean logAll = false;
    static int logVolMax =
            Integer.MAX_VALUE;
            //9;

    static boolean logGoals = false;

    static class A_SineRider {
        public static void main(String[] args) {
            run(new SineRider("s"));
        }
    }

    static class Apos {
        public static void main(String[] args) {
            run(new NObvious("p", 1));
        }
    }

    static class Aneg {
        public static void main(String[] args) {
            run(new NObvious("n", 0));
        }
    }

    static class Aposneg {
        public static void main(String[] args) {
            run(new NObvious("c", 0, 1));
        }
    }
    static class A3 {
        public static void main(String[] args) {
            run(new NObvious("c", 0, 0.5f, 1));
        }
    }

    static class B {
        public static void main(String[] args) {
            run(
                new TrackXY($.the("t"), 4, 4)
            );
        }
    }

    static class C {
        public static void main(String[] args) {
            BitGame.BitGame1 b = new BitGame.BitGame1($$("b"), 2, 1);
            Player p = run(b);
            b.view();
        }

    }


    private static Player run(Game... x) {
        Player p = games(30f, x);
        p.start();
        return p;
    }

    private static Player games(float fps, Game... x) {
        NAL.DEBUG = false;
        NAL.DEBUG_DERIVED_NALTASKS = NAL.DEBUG;
        if (NAL.DEBUG) {
            NAL.causeCapacity.set(8);
        }

        Collection<Game> g = List.of(x);



        Player p = new Player(
                g
//				new TrackXY_NAR($.the("b"), 3, 3, fps),
                //new TrackXY_NAR($.the("c"), 3, 3, fps)
//				new TrackXY_NAR($.the("t"),
//						//2, 1,
//						//3,1,
//						//3,3,
//						4,4,
//						//8,8,
//						fps)

        ).fps(fps);
        p.ready(n-> g.forEach(G -> {
            G.focus().dur(fps==0 ? 1 : 50);
//                G.onFrame(()->{
//                    System.out.println("durLoop=" + G.dur() + "\tdurFocus=" + G.durFocus());
//                });
        }));



        p.inperience = /*p.implBeliefify = p.implGoalify = */false;
        p.eternalization = 0;
        p.setMeta( false);
            //true;

        //p.nalMin = 6;
        //p.freqRes = 0.25f;
        p.volMax = 9;
        //g.forEach(G -> G.capacity(512));
        //p.conceptsMax = 16*1024;

        p.arith = false;
        p.deltaIntro = p.explode = false;

        p.threads = 4;

        if (logAll)
            g.forEach(G->G.focus().log(t -> ((NALTask)t).volume() <= logVolMax));


//        g.forEach(G -> {
////            G.actions.forEach(a -> {
//                G.rewards.forEach(r -> {
//                    G.onFrame(()->{
////                        final Term ar = CONJ.the(a.term(), r.term());
//                        ImplTree.ImplTreeEvaluator e = new ImplTree(r.term(), p.nar).truth(p.nar);
//                        e.want($.t(1, p.nar.goalConfDefault.conf()));
//                    });
//                });
////            });
//        });

        if (logGoals) {
            g.forEach(G -> {
                Set<Term> a = G.actions.concepts().map(Termed::term).collect(toSet());
                G.focus().onTask(t -> {
                    if (/*!(t instanceof Curiosity.CuriosityTask) &&*/ t.GOAL() && a.contains(t.term())) {
                        //System.out.println(t);
                        p.getNar().proofPrint((NALTask) t);

                    }
                });
            });
        }
        //TEMPORARY
//        g.forEach(G -> {
//            Set<Term> watch =
//                    Stream.of(
//                            "(a==>r)","(--a ==> r)","(--a && --r)","(a&&r)"
//                    ).map(z -> $$(z).concept()).collect(toSet());
//            G.focus().onTask(t -> {
//                if (t.BELIEF() && watch.contains(t.term().concept())) {
//                    //System.out.println(t);
//                    p.nar.proofPrint((NALTask) t);
//                }
//            });
//        });
        return p;
    }

    static class Experiments {
        static float fps = 0;
        static int runSeconds = 1;
        static int trials = 64;
//        static int repeats = 4;
        static int volMax = 9;
        static int threadsPerExperiment = 4;
        static float gameDur = 2;

        /** should be true if no static values are being changed */
        static final boolean parallelizable = true;

        public static void main(String[] args) {

            var e = new Lab<>(()->{
                var g =
                        //new PoleCart($$("x"))
                        new TrackXY($$("x"), 3, 1); g.ui = false;
                        //new TrackXY($$("x"), 3, 3); g.ui = false;
                        //new NObvious("p", 1);
                        //new Tetris();


                return simplePlayer(g);
            })
//            .var("mix", 0.01f, 0.75f, 0.25f, (p,v)->{
//                return MixDeriver.mixDefault = v;
//            })
            .var("simpleIn", 0.1f, 3, 0.25f, (p,v)->{
                p.ready((N)->{
                    p.games().forEach(g -> ((DefaultBudget)g.focus().budget).simpleIn.set(v));
                });
            })
            .var("simpleOut", 0.1f, 3, 0.25f, (p,v)->{
                p.ready((N)-> {
                    p.games().forEach(g -> ((DefaultBudget) g.focus().budget).simpleOut.set(v));
                });
            })
//            .var("beliefConf", 0.01f, 0.9f, 0.25f, (p,v)->{
//                return p.beliefConf = Util.round(v, NAL.truth.TRUTH_EPSILON);
//            })
//            .var("goalConf", 0.01f, 0.9f, 0.25f, (p,v)->{
//                return p.goalConf = Util.round(v, NAL.truth.TRUTH_EPSILON);
//            })
            .sense("concepts", (Player x)-> x.getNar().memory.size())
            .sense("volMean", (Player x)-> x.getNar().concepts().mapToInt(z -> z.term.volume()).average().getAsDouble())
            .sense("time", (Player x)-> x.getNar().time())

            //.optimize(repeats, parallelizable, Experiments::simplePlayerExperiment)
            .optimize(Experiments::simplePlayerExperiment)
            .run(trials)
            .report();

            window(TsneRenderer.view(e.data,
                            MetalBitSet.bits(3).set(0,1,2)),
                    800, 800);
        }



        private static Player simplePlayer(Game g) {
            Player p = games(fps,
                    g
            );
            p.uiTop = false;
            p.threads = threadsPerExperiment;
            p.volMax = volMax;
            p.iterTune = false;
            return p;
        }
        private static float simplePlayerExperiment(Supplier<Player> P) {
            var p = P.get();
            p.init();
            p.games().forEach(g -> g.time.dur(gameDur));

            p.start();
            Util.sleepMS(runSeconds * 1000);
            //p.runCycles(20000);
            //m.record(p);

            float r = (float) p.rewardMean();

            p.delete();

            return r;
        }


    }

    static class NObviousUnitTests {
        public static void main(String[] args) {
            int volMax = 6;
            int runSeconds = 4;
            float gameDur = 250;
            var g =
                    new NObvious("x", 1);
                    //new PoleCart($$("x"))
                    //new TrackXY($$("x"), 3, 3); g.ui = false;
                    //new Tetris();

            Player p = games(0, g);
            g.focus().log();
            p.uiTop = false;
            p.threads = 1;
            p.volMax = volMax;
            p.dtDither = 1;
            //p.confMin = 0.1f;
            p.start();
            g.time.dur(gameDur);
            Util.sleepMS(runSeconds * 1000);

            float rewardMean = (float) p.rewardMean();

            p.delete();

            System.out.println("@" + p.getNar().time() + " rewardMean=" + n2(rewardMean));

        }
    }
}