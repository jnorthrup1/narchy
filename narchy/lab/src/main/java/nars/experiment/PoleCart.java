package nars.experiment;

import com.google.common.collect.Iterables;
import com.google.common.collect.Streams;
import jcog.Is;
import jcog.Util;
import jcog.activation.SigLinearActivation;
import jcog.lstm.LSTM;
import jcog.math.FloatSupplier;
import jcog.nn.MLP;
import jcog.nn.optimizer.SGDOptimizer;
import jcog.normalize.FloatNormalized;
import jcog.pid.MiniPID;
import jcog.predict.LivePredictor;
import jcog.predict.Predictor;
import jcog.signal.FloatRange;
import nars.$;
import nars.NAR;
import nars.Player;
import nars.Term;
import nars.func.TruthPredict;
import nars.game.Game;
import nars.game.action.AbstractAction;
import nars.game.reward.MultiReward;
import nars.game.sensor.Sensor;
import nars.gui.DurSurface;
import nars.gui.LSTMView;
import nars.gui.MLPView;
import nars.gui.NARui;
import nars.term.Termed;
import nars.term.atom.Atomic;
import org.eclipse.collections.api.block.function.primitive.FloatToFloatFunction;
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction;
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static jcog.Util.fma;
import static nars.$.p;
import static org.hipparchus.util.MathUtils.normalizeAngle;
import static spacegraph.SpaceGraph.window;

/**
 * adapted from:
 * http:
 * https:
 * <p>
 * see also: https:
 * https://github.com/tensorflow/tfjs-examples/blob/master/cart-pole/cart_pole.js
 * https://github.com/gyscos/CMANes/blob/master/src/pole/Pole.java
 * https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py#L66
 */
@Is("Inverted_pendulum") public class PoleCart extends Game {
    static final boolean speedControl = false;
    static final boolean sensorPredict = false;
    static final boolean actionPredict = false;
    static final boolean inputVelocities = true;
    private static final float fps = 50;
    private static final int sensorDetail =
        //1;
        2;
        //3;
        //4;
        //5;
        //8;
        //16;

    private static final int rewardPower = 1;

    private static final float posMin = -2f;
    private static final float posMax = +2f;
    private static final double cartMass = 1;
    private static final double poleMass = 0.1;
    private static final double poleLength = 1;
    private static final double cartLength = poleLength / 2;
    private static final double poleMassLength = poleMass * cartLength;
    private static final double gravity = 9.8;
    private static final double totalMass = cartMass + poleMass;
    private static final double fourthirds = 4. / 3;
    /**
     * TODO correct for >1?
     */
    private static final int substeps = 2;
    public static int mode =
            0;
    public final FloatRange fricPole = new FloatRange(0.005f, 0, 0.1f);
    public final FloatRange fricCart = new FloatRange(0.01f, 0, 1);
    public final FloatRange forceAmp = new FloatRange(4, 0, 6);
    public final FloatRange tau = new FloatRange(
            0.04f, 0.01f, 0.08f
            //0.02f, 0.01f, 0.04f //<-- OpenAI Gym parameters
            //0.06f, 0.01f, 0.12f
            //0.019f, 0.001f, 0.05f
//            0.03f, 0.001f, 0.05f
//            0.02f, 0.001f, 0.05f
    );
    private final AtomicBoolean paintQueued = new AtomicBoolean(false);

    Sensor dx;
    Sensor x;
    Sensor angX, angY;
    Sensor angVel;
    private boolean rewardMulti = false;
    private float sensorResolution = 0.05f;
    private boolean manualOverride;


    private float posSpeedMax = (posMax - posMin) / 2;
    private double angSpeedMax = 8;

    private volatile double pos;
    private volatile double posDot;
    private volatile double ang = 0.2;
    private volatile double angDot;
    private volatile double forceNext;
    private volatile double forceL;
    private volatile double forceR;
    private JPanel panel;
    private Dimension offDimension;
    private Graphics offGraphics;

    PoleCart(Term id) {
        this(id, false);
    }

    PoleCart(Term id, boolean visible) {
        //super(id, fps(fps));
        super(id);

        		/*
		 returnObs.doubleArray[0] = theState.getX();
		 returnObs.doubleArray[1] = theState.getXDot();
		 returnObs.doubleArray[2] = theState.getTheta();
		 returnObs.doubleArray[3] = theState.getThetaDot();
		 */


        FloatSupplier R = mode == 0 ?
                this::reward0 :
                this::reward1;

        if (rewardMulti)
            reward(MultiReward.ewma($.p(id, "balance"), R, 0.01f).resolution(0.05f));
        else {
            reward("balanced", R).resolution(0.04f);
            //r.rewardFreqOrConf.set(false);
        }


        onFrame(() -> {
            double tau = this.tau.floatValue();
            for (int i = 0; i < substeps; i++)
                this.update(tau / substeps);
        });
        afterFrame(() -> {


            if (paintQueued.compareAndSet(false, true))
                panel.repaint();

        });


        x = senseNumberN($.inh($.p("x", id), $.varDep(1)), () -> (float) (pos - posMin) / (posMax - posMin), sensorDetail);
        if (inputVelocities) {
            dx = senseNumberN($.inh($.p("dx", id), $.varDep(1)),
                    new FloatNormalized(() -> (float) posDot).polar(), sensorDetail
            );
        }

        angX = senseNumberN($.inh($.p($.the("ang"), $.the("x"), id), $.varDep(1)),
                () -> (float) (0.5f + 0.5f * Math.cos(ang)), sensorDetail);
        angY = senseNumberN($.inh($.p($.the("ang"), $.the("y"), id), $.varDep(1)),
                () -> (float) (0.5f + 0.5f * Math.sin(ang)), sensorDetail);

//        ang = senseAngle($.inh($.p("ang", id), $.varDep(1)),
//                () -> (float) (angle+Math.PI/2), digitization);
//		,
//			() -> (float) (0.5f + 0.5f * Math.cos(angle)), digitization);

        FloatSupplier angVel = () -> (float) angDot;

        if (inputVelocities) {
            this.angVel = senseNumberN($.inh(p("dAng", id), $.varDep(1)),
                    new FloatNormalized(angVel).polar(), sensorDetail);
        }

        sensors.sensors.forEach(x -> x.resolution(sensorResolution));

        actionBipolar();
        //actionBipolarVelocity();
        //initUnipolar();

        if (speedControl) {
            var speedAction = action($.inh(id, "speed"),
                    (FloatProcedure) this.forceAmp::setLerp);
            //speedAction.poles = 1;
            //speedAction.resolution(0.1f);
        }


        panel = new JPanel(new BorderLayout()) {
            final Stroke stroke = new BasicStroke(4);
            private final double[] xs = {-2.5, 2.5, 2.5, 2.3, 2.3, -2.3, -2.3, -2.5};
            private final double[] ys = {-0.4, -0.4, 0., 0., -0.2, -0.2, 0, 0};
            private final transient int[] pixxs = new int[8];
            private final transient int[] pixys = new int[8];
            private Image offImage;

            {
                setIgnoreRepaint(true);
            }

            @Override
            public void paint(Graphics g) {
                update(g);
            }

            @Override
            public void update(Graphics g) {

                //paintQueued.set(false);

                Dimension d = panel.getSize();


                if (offGraphics == null
                        || d.width != offDimension.width
                        || d.height != offDimension.height) {
                    offDimension = d;
                    offImage = panel.createImage(d.width, d.height);
                    offGraphics = offImage.getGraphics();
                }


                float clearRate = 0.5f;
                offGraphics.setColor(new Color(0, 0, 0, clearRate));
                offGraphics.fillRect(0, 0, d.width, d.height);

                for (int i = 0; i < 8; i++) {
                    pixxs[i] = px(d, xs[i]);
                    pixys[i] = py(d, ys[i]);
                }
                Color trackColor = Color.GRAY;
                offGraphics.setColor(trackColor);
                offGraphics.fillPolygon(pixxs, pixys, 8);


                Color cartColor = Color.ORANGE;
                offGraphics.setColor(cartColor);
                offGraphics.fillRect(px(d, pos - 0.2), py(d, 0), pixDX(d, 0.4), pixDY(d, -0.2));

                ((Graphics2D) offGraphics).setStroke(stroke);


                offGraphics.drawLine(px(d, pos), py(d, 0),
                        px(d, pos + Math.sin(ang) * poleLength),
                        py(d, poleLength * Math.cos(ang)));


                if (forceNext != 0) {
                    int signAction = forceNext > 0 ? 1 : forceNext < 0 ? -1 : 0;
                    int tipx = px(d, pos + 0.2 *
                            //signAction
                            forceNext * 0.8f //TODO normalize to max force range
                    );
                    Color arrowColor = new Color(0.1f, 0.5f + Util.tanhFast(Math.abs((float) forceNext * 0.2f)) / 2f, 0.1f);
                    int tipy = py(d, -0.1);
                    offGraphics.setColor(arrowColor);
                    offGraphics.drawLine(px(d, pos), py(d, -0.1), tipx, tipy);
                    offGraphics.drawLine(tipx, tipy, tipx - 4 * signAction, tipy + 4);
                    offGraphics.drawLine(tipx, tipy, tipx - 4 * signAction, tipy - 4);
                }


                g.drawImage(offImage, 0, 0, panel);


                paintQueued.set(false);
            }

        };


        if (visible) {
            JFrame f = new JFrame();
            f.setContentPane(panel);
            f.setSize(800, 300);
            f.setVisible(true);

            f.addKeyListener(new KeyAdapter() {

                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyChar() == 'o') {
                        manualOverride = !manualOverride;
                        System.out.println("manualOverride=" + manualOverride);
                    }
                    if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                        forceL = +1;
                        forceR = 0;
                    } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        forceR = +1;
                        forceL = 0;
                    } else if (e.getKeyChar() == ' ') {
                        forceL = forceR = 0;

                    }
                }
            });
        }

    }

    public static void main(String[] arg) {

        int N = 1;
        Player P = new Player(fps, n -> {
            for (int i = 0; i < N; i++) {
                Atomic baseName = $.the(PoleCart.class.getSimpleName());
                PoleCart g = new PoleCart(N > 1 ? p(baseName, $.the(i)) : baseName, true);
                n.add(g);

                n.runLater(() -> window(NARui.beliefCharts(
                        g.sensors.sensorComponents().collect(toList()), n), 900, 900
                ));

            }
        });
        //P.volMax = 16;
//        P.controlLerp();
//        P.answerQuestionsFromTaskLinks = true;

//        P.reflexBoost = true;
        //P.deltaIntro = true;
//        P.selfMetaReflex = P.subMetaReflex = true;


        P.start();
//        P.the(Game.class).forEach(g -> {
//            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
//                File f = new File("/tmp/" + g.id + ".nal");
//                try {
//                    PrintStream p = new PrintStream(new FileOutputStream(f));
//                    g.vocabulary.forEach(v ->
//                        p.println("//" + v)
//                    );
//                    long c = g.knowledge().filter(z -> !(z instanceof SerialTask)).sorted(
//                            Comparator.comparingDouble((NALTask z) -> z.BELIEF_OR_GOAL() ? z.conf() : 0)
//                                    .thenComparingInt(NALTask::volume)
//                                    .thenComparing(NALTask::term))
//                            .peek(p::println).count();
//                    System.err.println(f.toPath() + "\n\t" + g + " " + c + " tasks");
//                    p.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }));
//        });


        P.the(PoleCart.class).forEach(p -> {
            Iterable<? extends Termed> predicting = Iterables.concat(
                    p.angX.components(), p.angY.components(),
                    p.x.components()
                    //ang.sensors.stream().flatMap(x->Streams.stream(x.components())).collect(toList()),
//                    p.ang.components(),
            );
            if (inputVelocities) {
                predicting = Iterables.concat(predicting,
                        p.angVel.components(), p.dx.components()
                );
            }

            if (sensorPredict) {


                Iterable<? extends Termed> PP = predicting;
                p.nar.add(new TruthPredict(
                        () -> Stream.concat(Streams.stream(PP), p.actions.stream()
                                .flatMap(x1 -> Streams.stream(x1.components()))).iterator()//Iterables.concat(predicting, actions.actions),
                        ,
                        predicting,
                        8,
                        () -> p.dur() * 8,
                        8,

                        mlpPredictor()
                        //ntmPredictor()

              /*.adapt(
                            freq -> (freq - 0.5) * 2
                            , value -> (value / 2) + 0.5
                    )*/

//                            (i,o)-> {
//                                final LSTM x = new LSTM(i,o,8);
////                                x.alpha(5f);
//                                x.randomize(0.1f, ThreadLocalRandom.current());
//                                return x;
//                            }

                        , p,
                        true) {

                    @Override
                    protected void starting(NAR n) {
                        super.starting(n);
                        n.runLater(() -> {
                            Util.sleepMS(2000);
                            Predictor P = this.predictor.p;
                            if (P instanceof LSTM)
                                window(DurSurface.get(new LSTMView(((LSTM) P)), LSTMView::run, n), 400, 400);
                            else if (P instanceof MLP)
                                window(DurSurface.get(new MLPView((MLP) P), MLPView::run, n), 400, 400);

                        });
                    }
                });


            }
            if (actionPredict) {
                p.nar.add(new TruthPredict(
                        p.actions, //Iterables.concat(predicting, p.actions.actions),
                        p.actions,
                        8,
                        () -> 3 * p.dur(),
                        6,
                        (i, o) -> new LSTM(i, o, 2).alpha(0.04f),
                        //								new LivePredictor.MLPPredictor(0.1f).adapt(
                        //									freq -> (freq-0.5)*2,
                        //									value -> (value/2)+0.5
                        //								),
                        p,
                        false));
            }
        });

    }

    private static IntIntToObjectFunction<Predictor> ntmPredictor() {
        return (i, o) -> new LivePredictor.NTMPredictor(i, o, 2);
    }

    private static IntIntToObjectFunction<Predictor> mlpPredictor() {
        return (i, o) -> {
            MLP m = new MLP(i,
//                    new MLP.Dense((i + o), SigmoidActivation.the),
//                    new MLP.Dense(o, SigmoidActivation.the)

                    //new MLP.Layer( 1 * (i + o), TanhActivation.the),
                    //                            new MLP.Layer( Math.max(i/2,o*2), TanhActivation.the),
                    //new MLP.Layer( Math.max(i/2,o*2), TanhActivation.the),
                    //                            new MLP.Layer( (int)Util.mean(i, o), TanhActivation.the),

                    //new MLP.Dense((i + o), new SigLinearActivation()),
                    new MLP.Dense(o, new SigLinearActivation())

                    //new MLP.Layer( o, DoubleDiffableFunction.DirectUnitize)

                    //                            new MLP.Layer( 1 * (i + o), TanhActivation.the),
                    //                            new MLP.Layer( o, SigmoidActivation.the)
                    //new MLP.Layer( (i + o), SigmoidActivation.the)
                    //new MLP.Layer( (x + y), TanhActivation.the),
            ).optimizer(new SGDOptimizer(0.9f));
            m.clear(ThreadLocalRandom.current());
            return m;
        };
    }

    private static int px(Dimension d, double v) {
        return (int) Math.round((v + 2.5) / 5.0 * d.width);
    }

    private static int py(Dimension d, double v) {
        return (int) Math.round(d.height - (v + 0.5f) / 2.0 * d.height);
    }

    private static int pixDX(Dimension d, double v) {
        return (int) Math.round(v / 5.0 * d.width);
    }

    private static int pixDY(Dimension d, double v) {
        return (int) Math.round(-v / 2.0 * d.height);
    }


    public void force(float f) {
        forceL = f < 0 ? -f : 0;
        forceR = f > 0 ? +f : 0;
    }

    private void actionBipolar() {
        this.actionBipolar(id, f -> {
            force(f);
            return f;
        }).resolution(0.1f);
    }

    private void actionBipolarVelocity() {
        this.actionBipolar(id, new FloatToFloatFunction() {

            final MiniPID pid = new MiniPID(
                    1, 1, 1
            );

            double maxForce = 1;

            @Override
            public float valueOf(float v) {
                pid.outRange(-maxForce, +maxForce);
                double f = pid.out(posDot, v);
                force((float) f);
                return (float) posDot;
            }
        }).resolution(0.1f);
    }

    protected void update(double dt) {

        forceNext = (forceR - forceL) * forceAmp.floatValue();

        //TODO this is actually impulse * dt?
        double force = forceNext;

        double sinA = Math.sin(ang);
        double cosA = Math.cos(ang);
        double angleDotSq = Util.sqr(angDot);
        double common = (force + poleMassLength * angleDotSq * sinA
                - fricCart.doubleValue() * (posDot < 0 ? -1 : +1)) / totalMass;


        pos = fma(posDot, dt, pos);


        if (pos >= posMax || pos <= posMin) {
            pos = Util.clamp((float) pos, posMin, posMax);
            posDot = -1 /* restitution */ * posDot;
        }

        double angAcc = (gravity * sinA - cosA * common
                - fricPole.doubleValue() * this.angDot / poleMassLength) /
                (cartLength * (fourthirds - poleMass * Util.sqr(cosA) / totalMass));

        double posDotNext = common - poleMassLength * angAcc * cosA / totalMass;
        posDot = Util.clamp(posDot + posDotNext * dt, -posSpeedMax, +posSpeedMax);

        ;
        ang += this.angDot * dt;
        this.angDot = Util.clamp(this.angDot + angAcc * dt,
                -angSpeedMax, +angSpeedMax);


//		if (drawFinished.compareAndSet(true, false))
        //SwingUtilities.invokeLater(panel::repaint);

    }

    private float reward1() {
        return (float) Math.pow(1 - (Math.cos(ang) / 2 + 0.5), 2);
    }

    private float reward0() {
        float rewardLinear = (float) Math.max(0, 1 - Math.abs(normalizeAngle(ang, 0))/(Math.PI));

        //float rewardUniform = rewardLinear / 2 + 0.5f;

        //return rewardUniform;
        //return Util.sqr(rewardUniform);
        return (float) Math.pow(rewardLinear, rewardPower);
    }


    private float reward00() {
        float rewardLinear = (float) Math.cos(ang);

        float rewardUniform = rewardLinear / 2 + 0.5f;

        //return rewardUniform;
        //return Util.sqr(rewardUniform);
        return (float) Math.pow(rewardUniform, rewardPower);
    }

    public void initUnipolar() {
        AbstractAction L = action(
                $.inh(id, "L"), a -> {
                    if (!manualOverride)
                        forceL = a;///*a > 0.5f ?*/power((a - 0.5f) * 2);// : 0;
                    return a;
                });
        AbstractAction R = action(
                $.inh(id, "R"), a -> {
                    if (!manualOverride)
                        forceR = a;///*a > 0.5f ?*/ power((a - 0.5f) * 2);// : 0;
                    return a;
                });

//        L.goalDefault($.t(0, 0.00001f), nar);
//        R.goalDefault($.t(0, 0.00001f), nar);
    }

    public void reset() {
        pos = posDot = ang = angDot = 0;
    }


}