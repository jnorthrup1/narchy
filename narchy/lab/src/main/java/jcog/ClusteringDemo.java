package jcog;

import jcog.cluster.KMeansPlusPlus;
import jcog.exe.Loop;
import spacegraph.space2d.container.Splitting;
import spacegraph.space2d.widget.meter.BitmapMatrixView;
import spacegraph.space2d.widget.meter.ScatterPlot2D;
import spacegraph.video.Draw;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.stream.IntStream;

import static spacegraph.SpaceGraph.window;

public class ClusteringDemo {
    public static void main(String[] args) {

        int W = 16, H = 16;
        int C = 4;
        float maxDensity = 0.33f;
        int iterationsMax = C;

        ArrayDeque<int[]> points = new ArrayDeque();
        int maxPoints = (int)Math.ceil((W*H)*maxDensity);

//        Clustering n =
//                new NeuralGasNet(2, C).population(maxPoints);
        KMeansPlusPlus<int[]> n = new KMeansPlusPlus<>(C, 2) {

            @Override public double[] coord(int[] x, double[] X) {
                X[0] = x[0]; X[1] = x[1];
                return X;
            }
        };

        BitmapMatrixView b = new BitmapMatrixView(W, H, (x, y) -> {
            boolean point = contains(points, x, y);

            int centroid =
                //n.get(new double[]{x, y})._id;
                n.nearest(new int[] { x, y });

            float c = centroid / ((float) C);
            return Draw.colorHSB(c, point ? 0.5f : 1f, point ? 1 : 0.5f);
        });
        ScatterPlot2D<double[]> s = new ScatterPlot2D<>(new ScatterPlot2D.SimpleXYScatterPlotModel<>() {
            @Override
            public void coord(double[] c, float[] target) {
                target[0] = (float) c[0] / W;
                target[1] = (float) c[1] / H;
            }

            @Override
            public float width(double[] o, int n) {
                return 0.1f;
            }

            @Override
            public float height(double[] o, int n) {
                return 0.1f;
            }
        });
        Loop.of(() -> {

            boolean learn = true;
            if (b.touchState > 0) {
                int tx = b.touchPix.x, ty = b.touchPix.y;
                if (tx >= 0 && ty >= 0) {
                    if (!contains(points, tx, ty)) {
                        points.add(new int[]{tx, ty});
                        if (points.size() > maxPoints)
                            points.removeFirst();

                        //learn = true;
                    }
                }
            }

            if (learn) {
                if (points.size() >= C) {
                    n.clusterDirect(points, iterationsMax);
                    n.sortClusters();
                }

                b.updateIfShowing();
                s.set(IntStream.range(0, n.k).mapToObj(n::center));

            }
        }).fps(10f);

        window(new Splitting(b, 0.5f, s).horizontal(), 1200, 600);
    }


    private static boolean contains(ArrayDeque<int[]> points, int x, int y) {
        int[] xy = {x, y};
        for (int[] p : points) {
            if (Arrays.equals(p, xy))
                return true;
        }
        return false;
    }
}